Imports System.Data.SqlClient
Imports System.Web

Namespace DB
    Public Class SqlConnect

        Private _DB_Provider As String
        Private _DB_UserName As String
        Private _DB_Password As String
        Private _DB_DataSource As String
        Private _DB_Name As String
        Private _ConnectStr As String
        Private _UseStoredProc As Boolean = True
        Private _Conn As SqlConnection

        Public Sub New()
            ReadDALConfigurations()
        End Sub

        Protected Overrides Sub Finalize()
            MyBase.Finalize()
        End Sub

        Public Property Connection()
            Get
                Return _Conn
            End Get
            Set(ByVal value)
                _Conn = value
            End Set
        End Property
        Public Sub ReadDALConfigurations()
            Try


                _ConnectStr = "Data Source=" & _DB_DataSource & ";User ID=" & _DB_UserName & ";Password=" & _DB_Password
                If _DB_Name <> "" Then _ConnectStr += ";Initial Catalog=" & _DB_Name

                '_UseStoredProc = ((System.Configuration.ConfigurationSettings.AppSettings("UserStoredProc") & "").ToLower = "true")

            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End Sub


        Public Function getDataTable(ByVal SQL As String) As DataTable
            Dim DA As SqlDataAdapter = Nothing
            Dim DT As New DataTable
            Try
                DA = New SqlDataAdapter(SQL, _ConnectStr)
                DA.Fill(DT)
                DA.Dispose()
                DA = Nothing
                Return DT
            Catch ex As Exception
                If Not IsNothing(DA) Then
                    DA.Dispose()
                    DA = Nothing
                End If
                Throw (ex)
            End Try
            Return Nothing
        End Function

        Public Function getDataTable(ByVal SQL As String, ByVal sCon As SqlConnection) As DataTable
            Dim DA As SqlDataAdapter = Nothing
            Dim DT As New DataTable
            Try
                DA = New SqlDataAdapter(SQL, sCon)
                DA.Fill(DT)
                DA.Dispose()
                DA = Nothing
                Return DT
            Catch ex As Exception
                If Not IsNothing(DA) Then
                    DA.Dispose()
                    DA = Nothing
                End If
                Throw (ex)
            End Try
            Return Nothing
        End Function

        Public Function getDataTable(ByVal SQL As String, ByVal connstr As String) As DataTable
            Dim DT As New DataTable
            Try
                Using conn As New SqlConnection(connstr)

                    conn.Open()

                    Dim DA As SqlDataAdapter


                    DA = New SqlDataAdapter(SQL, conn)
                    DA.Fill(DT)
                    DA.Dispose()
                    DA = Nothing
                End Using
                Return DT
            Catch ex As Exception

                Throw (ex)
            End Try
            Return Nothing
        End Function

        Public Function getScalar(ByVal SQL As String, Connstr As String) As String
            Dim sResult As String = ""
            Try

                Using cn As New SqlConnection(Connstr)
                    Using cmd As New SqlCommand
                        cn.Open()
                        cmd.Connection = cn
                        cmd.CommandText = SQL
                        Dim obj As Object
                        obj = cmd.ExecuteScalar()
                        If Not IsDBNull(obj) Then
                            sResult = obj
                        End If
                    End Using
                End Using

            Catch ex As Exception
                sResult = ""
                ' MsgBox(ex.Message)
                ' Throw (ex)
            Finally

            End Try
            Return sResult
        End Function

        Public Function getScalar(ByVal SQL As String) As String
            Dim cn As SqlConnection
            cn = getConn()
            Dim sResult As String = ""
            Try
                Using cmd As New SqlCommand
                    cmd.Connection = cn
                    cmd.CommandText = SQL
                    sResult = cmd.ExecuteScalar()
                End Using
            Catch ex As Exception
                sResult = "Error"
                ' MsgBox(ex.Message)
                Throw (ex)
            Finally
                CloseConn(cn)
            End Try
            Return sResult
        End Function

        Public Function getDataView(ByVal SQL As String) As DataView
            Dim DA As SqlDataAdapter = Nothing
            Dim DT As New DataTable
            Try
                DA = New SqlDataAdapter(SQL, _ConnectStr)
                DA.Fill(DT)
                DA.Dispose()
                DA = Nothing
                Return DT.DefaultView
            Catch ex As Exception
                If Not IsNothing(DA) Then
                    DA.Dispose()
                    DA = Nothing
                End If
                Throw (ex)
            End Try
            Return Nothing
        End Function

        Public Function getDataView(ByVal cn As SqlConnection, ByVal SQL As String) As DataView
            Dim DA As SqlDataAdapter = Nothing
            Dim DT As New DataTable
            Try
                DA = New SqlDataAdapter(SQL, cn)
                DA.Fill(DT)
                DA.Dispose()
                DA = Nothing
                Return DT.DefaultView
            Catch ex As Exception
                If Not IsNothing(DA) Then
                    DA.Dispose()
                    DA = Nothing
                End If
                Throw (ex)
            End Try
            Return Nothing
        End Function


        Public Function getDataTable(ByVal cn As SqlConnection, ByVal SQL As String) As DataTable
            Dim DA As SqlDataAdapter = Nothing
            Dim DT As New DataTable
            Try
                DA = New SqlDataAdapter(SQL, cn)
                DA.Fill(DT)
                DA.Dispose()
                DA = Nothing
                Return DT
            Catch ex As Exception
                If Not IsNothing(DA) Then
                    DA.Dispose()
                    DA = Nothing
                End If
                Throw (ex)
            End Try
            Return Nothing
        End Function

        Public Function getServerDate() As Date
            Dim DA As SqlDataAdapter = Nothing
            Dim DT As New DataTable
            Try

                Using cmd As New SqlCommand
                    cmd.Connection = getConn()
                    cmd.CommandText = "Select getDate()"
                    Return cmd.ExecuteScalar()
                End Using


                'DA = New SqlDataAdapter("Select getDate()", _ConnectStr)
                'DA.Fill(DT)
                'DA.Dispose()
                'DA = Nothing
                'Return CDate(DT.Rows(0).Item(0))
            Catch ex As Exception
                If Not IsNothing(DA) Then
                    DA.Dispose()
                    DA = Nothing
                End If
                Throw (ex)
            End Try
            Return Nothing
        End Function

        Public Function GetExecute(ByVal strSql As String) As Integer
            Dim nRow As Integer
            Try
                Using cmd As New SqlCommand
                    cmd.Connection = getConn()
                    cmd.CommandText = strSql
                    nRow = cmd.ExecuteNonQuery()
                End Using
            Catch ex As Exception
                Return -1
                Throw (ex)

            End Try
            Return nRow
        End Function

        Public Function GetExecute(ByVal strSql As String, ByVal cnn As SqlConnection) As Integer
            Dim nRow As Integer
            Try
                Using cmd As New SqlCommand
                    cnn.Close()

                    If cnn.State = ConnectionState.Closed Then
                        cnn.Open()
                    End If
                    cmd.Connection = cnn
                    cmd.CommandText = strSql
                    nRow = cmd.ExecuteNonQuery()
                End Using
            Catch ex As Exception
                ' Return -1
                Throw (ex)

            End Try
            Return nRow
        End Function
        Public Function GetExecute(ByVal strSql As String, ByVal Connstr As String) As Integer
            Dim nRow As Integer
            Try
                Using conn As New SqlConnection(Connstr)


                    Using cmd As New SqlCommand
                        If conn.State = ConnectionState.Closed Then
                            conn.Open()
                        End If
                        cmd.Connection = conn
                        cmd.CommandText = strSql
                        nRow = cmd.ExecuteNonQuery()
                    End Using
                End Using
            Catch ex As Exception
                'Return -1
                Throw (ex)

            End Try
            Return nRow
        End Function

        Public Function getConn() As SqlConnection
            Dim Conn As New SqlConnection
            Conn.ConnectionString = _ConnectStr
            Try
                Conn.Open()
            Catch ex As Exception
                '  MsgBox("Cannot connect To DB Server: " & Project.DB_Provider)
                Return Nothing
            End Try
            Return Conn
        End Function

        Public Function CloseConn(ByRef conn As SqlConnection) As Boolean
            conn.Close()
        End Function

        Public Sub Update(dt As DataTable, ByVal SQL As String, ByVal connSTR As String)
            Using connection As New SqlConnection(connSTR)
                Dim adapter As New SqlDataAdapter()
                adapter.SelectCommand = New SqlCommand(SQL, connection)
                Dim builder As SqlCommandBuilder = New SqlCommandBuilder(adapter)
                builder.ConflictOption = ConflictOption.OverwriteChanges
                adapter.InsertCommand = builder.GetInsertCommand
                adapter.UpdateCommand = builder.GetUpdateCommand
                adapter.DeleteCommand = builder.GetDeleteCommand
                connection.Open()


                ' Code to modify data in DataTable here 

                adapter.Update(dt)

            End Using
        End Sub
#Region "Container Detail"


#End Region


    End Class
End Namespace









