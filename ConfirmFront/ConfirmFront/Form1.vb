﻿Imports System.Threading
Public Class Form1
    Dim Timer_LoadLog As Threading.Timer
    Dim CurID, CurLOT, CurSEQ, CurState, CurPlant, Oldid, OldLOT, OldSeq, OldPlant, OldState As String
    Dim Status_Color As Color
    Dim Status_setlbl As String
    Dim Status_time As Integer
    Dim BarcodeResult As String
    Public KANBANlen As Integer = 0


    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = False
        DGVLog.AutoGenerateColumns = False
        SetDoubleBuffered(DGVLog)
        If sqlitePath.Substring(0, 1) = "\" Then
            sqlitePath = "\\" & sqlitePath
        End If
        Connstr_lite = "Data Source=" & sqlitePath & ";Version=3;"

        loadserver()

        LoadProcess()
        LoadCurState()

        loadSetting()
        'Dim TDS1 As TimerCallback = AddressOf LoadLog
        'Timer_LoadLog = New Threading.Timer(TDS1, Nothing, 1000, 1000)
        txtBarcode.Focus()
        LoadLog()
    End Sub
    Sub SetDoubleBuffered(ByVal control As Control)
        ' set instance non-public property with name "DoubleBuffered" to true 
        GetType(Control).InvokeMember("DoubleBuffered", System.Reflection.BindingFlags.SetProperty Or System.Reflection.BindingFlags.Instance Or System.Reflection.BindingFlags.NonPublic, Nothing, control, New Object() {True})
    End Sub
    Sub loadSetting()
        Dim sql As String
        sql = "select * from setting"
        Dim dt As New DataTable
        dt = DALlite.getDataTable(sql, Connstr_lite)
        For Each dr As DataRow In dt.Rows
            If dr("name").ToString.ToUpper = "Export_LOT".ToUpper Then
                lblExport.Text = dr("Value").ToString
            End If
            If dr("name").ToString.ToUpper = "Export_act".ToUpper Then
                lblExport_act.Text = dr("Value").ToString
            End If
            If dr("name").ToString.ToUpper = "Export_qty".ToUpper Then
                lblExport_QTY.Text = dr("Value").ToString
            End If

            If dr("name").ToString.ToUpper = "PLCIP".ToUpper Then
                PLCIP = dr("Value").ToString
            End If


            If dr("name").ToString.ToUpper = "PLC_LOT".ToUpper Then
                PLC_LOT = dr("Value").ToString
            End If

            If dr("name").ToString.ToUpper = "PLC_Complete".ToUpper Then
                PLC_Complete = dr("Value").ToString
            End If

            If dr("name").ToString.ToUpper = "onlyplant".ToUpper Then
                lblOnlyPlant.Text = dr("Value").ToString
            End If

            If dr("name").ToString.ToUpper = "FAXIP".ToUpper Then
                FAXIP = dr("Value").ToString

            End If
            If dr("name").ToString.ToUpper = "KANBANlen".ToUpper Then
                If IsNumeric(dr("Value").ToString) Then
                    KANBANlen = dr("Value").ToString
                Else
                    KANBANlen = 0
                End If

            End If

            If dr("name").ToString.ToUpper = "ModelFrom".ToUpper Then
                ModelFrom = dr("Value").ToString

            End If

        Next
    End Sub
    Sub LoadProcess()
        Dim sql As String
        sql = "select * from ProcessConfig"
        Dim dt As New DataTable
        dt = DALlite.getDataTable(sql, Connstr_lite)
        For Each dr As DataRow In dt.Rows
            sLinename = dr("LineName").ToString()
            sPitch = dr("Pitch").ToString()
            sStation = dr("Station").ToString()
        Next
        lblProcess.Text = sLinename & ", P" & sPitch & ", " & "S" & sStation
        ProcessName = lblProcess.Text
    End Sub

    Sub UpdateCurState()
        Try


            Oldid = CurID
            OldLOT = CurLOT
            OldSeq = CurSEQ
            OldState = CurState
            OldPlant = CurPlant

            Dim sql As String
            sql = "update tb_CurState set CurID='" & CurID & "'"
            sql += " , CurLOT='" & CurLOT & "'"
            sql += ", CurSEQ='" & CurSEQ & "'"
            sql += ", CurState='" & CurState & "'"
            sql += ", CurPlant='" & CurPlant & "'"
            sql += ", Oldid='" & Oldid & "'"
            sql += ", OldLOT='" & OldLOT & "'"
            sql += ", OldSeq='" & OldSeq & "'"
            sql += ", OldPlant='" & OldPlant & "'"
            sql += ", OldState='" & OldState & "'"

            DALlite.GetExecute(sql, Connstr_lite)
        Catch ex As Exception
            Dim Err As String = "UpdateCurState:" & ex.Message
            ShowStatus(Err, 5, Color.Red)
            SaveMSG(Err, "Err")
        End Try
    End Sub
    Sub LoadCurState()
        Try
            Dim dt As New DataTable
            Dim sql As String
            sql = "select * from tb_curstate"
            dt = DALlite.getDataTable(sql, Connstr_lite)
            For Each dr As DataRow In dt.Rows
                CurLOT = dr("CurLOT").ToString
                CurSEQ = dr("CurSEQ").ToString
                CurState = dr("CurState").ToString
                CurPlant = dr("CurPlant").ToString
                CurID = dr("CurID").ToString
                Oldid = dr("Oldid").ToString
                OldLOT = dr("OldLOT").ToString
                OldSeq = dr("OldSeq").ToString
                OldPlant = dr("OldPlant").ToString
                OldState = dr("OldState").ToString


            Next
        Catch ex As Exception
            Dim Err As String = "LoadCurState:" & ex.Message
        ShowStatus(Err, 5, Color.Red)
        SaveMSG(Err, "Err")
        End Try
    End Sub
    Sub ShowStatus(str As String, time As Integer, Optional sColor As Color = Nothing)
        Status_setlbl = str
        Status_time = time

        Status_Color = sColor
    End Sub
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Try


            If Status_time > 0 Then
                lblstatus.Text = Status_setlbl
                Status_time -= 1
                If Status_Color <> Nothing Then
                    lblstatus.BackColor = Status_Color
                End If

            Else
                lblstatus.Text = ""
                lblstatus.BackColor = Color.Transparent
            End If

            If BarcodeConfirm = False Then
                If barcodeFlage Then
                    txtBarcode.BackColor = Color.Red
                Else
                    txtBarcode.BackColor = Color.Yellow
                End If
                barcodeFlage = Not barcodeFlage
            Else

            End If


            If lblPlant.Text = "E" And lblExport.Text <> "" Then
                If lblExport.Text <> lblLOtcode.Text Then
                    lblExport.BackColor = Color.Red
                Else
                    lblExport.BackColor = Color.LimeGreen
                End If

            Else
                lblExport.BackColor = Color.White
            End If
            LoadCurLOT()
            DisplayData()



            If CurState <> "3" Then
                txtBarcode.Enabled = True
            Else
                '  txtBarcode.Enabled = False

            End If
            If OldLOT & OldSeq <> CurLOT & CurSEQ Or CurState = "2" Or (BarcodeResult = "" And CurState <> "3") Then
                getBarcodeResult()
                lblRH.BackColor = Color.DimGray
                lblLH.BackColor = Color.DimGray

                UpdateState("1")
                CurState = "1"
                UpdateCurState()
                txtBarcode.Enabled = True
                If BarcodeResult <> "" Then
                    txtBarcode.Focus()
                End If

                If lblOnlyPlant.Text <> "" Then
                    Try

                        Dim pass As Boolean = False
                        For Each s As String In lblOnlyPlant.Text.Split(",")
                            If s.ToUpper = CurPlant.ToUpper Then
                                pass = True
                            End If
                        Next
                        If pass = False Then
                            UpdateState("3")
                            CurState = "3"
                            UpdateCurState()
                        End If
                    Catch ex As Exception
                        Dim Err As String = "Timer1OnlyPlant:" & ex.GetBaseException.ToString
                        ShowStatus(Err, 5, Color.Red)
                        SaveMSG(Err, "Err")
                    End Try
                End If
            End If
            If Not ActiveForm Is Nothing Then


                If Now.ToString("ss") Mod 5 = 0 And ActiveForm.Name = Me.Name Then
                    txtBarcode.Focus()
                End If
            End If
        Catch ex As Exception
            Dim Err As String = "Timer1:" & ex.GetBaseException.ToString
            ShowStatus(Err, 5, Color.Red)
            SaveMSG(Err, "Err")
        End Try
    End Sub
    Dim lastModelKanban As String
    Dim dtModelKanban As DataTable
    Function CheckListKanban(model As String, barcode As String)
        Try


            Dim result As Boolean = False
            If lastModelKanban <> model Then
                dtModelKanban = DAL.getDataTable("select kanban,'' as [check],side From tb_model_kanban where model like '" & model & "'", Connstr)
                lastModelKanban = model
            End If
            If dtModelKanban.Rows.Count > 0 Then
                result = True
            End If

            For Each dr As DataRow In dtModelKanban.Rows
                If dr("kanban").ToString = barcode Then
                    dr("Check") = 1

                    If dr("side").ToString.ToUpper = "LH" Then
                        lblLH_KANBAN.BackColor = Color.LimeGreen
                    ElseIf dr("side").ToString.ToUpper = "RH" Then
                        lblRH_KANBAN.BackColor = Color.LimeGreen
                    End If
                End If

                If dr("Check").ToString <> "1" Then
                    result = False
                End If
            Next

            Return result
        Catch ex As Exception
            Dim Err As String = "CheckListKanban:" & ex.GetBaseException.ToString
            ShowStatus(Err, 5, Color.Red)
            SaveMSG(Err, "Err")
            Return False
        End Try
    End Function
    Sub CheckKanban()
        Try
            Dim barcode As String
            txtlastBarcode.Text = txtBarcode.Text
            barcode = txtBarcode.Text

            Dim Model As String
            Model = DAL.getScalar("select model From tb_model_kanban where kanban like '" & txtBarcode.Text & "'", Connstr)

            txtBarcode.Text = ""
            If Model = "" Then Exit Sub
            If CheckListKanban(Model, barcode) Then
                SetExport(Model)
            End If


        Catch ex As Exception
            Dim Err As String = "CheckKanban:" & ex.GetBaseException.ToString
            ShowStatus(Err, 5, Color.Red)
            SaveMSG(Err, "Err")
        Finally
            txtBarcode.Focus()
        End Try

    End Sub
    Sub CheckBarcode()
        Try


            txtlastBarcode.Text = txtBarcode.Text

            If BarcodeResult = "NOEXPORT" Then
                BarcodeConfirm = False
                ShowStatus("NO EXPORT", 5)
            End If
            If BarcodeResult = "" Then
                BarcodeConfirm = False
                txtBarcode.Text = ""
                ShowStatus("Barcode Result Empty", 5)
                SaveMSG("Barcode Result Empty:" & CurSEQ & ":" & CurLOT, "ERR")
                Exit Sub
            End If
            If txtBarcode.Text.Length < 6 Then
                BarcodeConfirm = False
                txtBarcode.Text = ""
                Exit Sub
            End If
            If BarcodeResult = txtBarcode.Text.Substring(0, 5) Then
                BarcodeConfirm = True
                txtBarcode.BackColor = Color.White
                If txtBarcode.Text.Substring(5, 1) = 1 Then
                    lblRH.BackColor = Color.LimeGreen
                ElseIf txtBarcode.Text.Substring(5, 1) = 2 Then
                    lblLH.BackColor = Color.LimeGreen
                End If
                savecomplete("Barcode")
                Try
                    AB.IPAddress = PLCIP
                    AB.WriteData(PLC_Complete, "1")
                Catch ex As Exception
                    ShowStatus("PLCComplete:Err", 5)
                End Try
                If lblRH.BackColor = Color.LimeGreen And lblLH.BackColor = Color.LimeGreen Then
                    UpdateState("3")
                    CurState = "3"
                    UpdateCurState()
                    savecomplete("Complete")

                    If CurPlant = "E" Then
                        lblExport_act.Text += 1

                        If CInt(lblExport_act.Text) = CInt(lblExport_QTY.Text) Then
                            Try
                                AB.WriteData(PLC_LOT, "0")
                            Catch ex As Exception
                                ShowStatus("PLCComplete:Err_PLC_LOT", 5)
                            End Try
                            lblExport_act.Text = 0
                            lblLH_KANBAN.BackColor = Color.DimGray
                            lblRH_KANBAN.BackColor = Color.DimGray
                        End If
                        SaveSetting("export_act", lblExport_act.Text)
                    End If
                End If
                '  Application.DoEvents()
                LoadLog()

            Else
                BarcodeConfirm = False
            End If
            txtBarcode.Text = ""
        Catch ex As Exception
            Dim Err As String = "CheckBarcode:" & ex.Message
            ShowStatus(Err, 5, Color.Red)
            SaveMSG(Err, "Err")
        Finally
            txtBarcode.Focus()
        End Try
    End Sub
    Dim BarcodeConfirm As Boolean = True
    Dim barcodeFlage As Boolean



    Private Sub UpdateState(ByVal sState As String)
        Dim strSql_update As String
        Try
            strSql_update = "update tb_pitch set STATE='" & sState & "' where Linename like '" & sLinename & "' and Pitch =" & sPitch & " and [No] =" & sStation

            DAL.GetExecute(strSql_update, Connstr)

        Catch ex As Exception
            Dim Err As String = "UpdateState:" & ex.Message
            ShowStatus(Err, 5, Color.Red)
            SaveMSG(Err, "Err")
            ' MsgBox(ex.Message)
        End Try

    End Sub
    Sub getBarcodeResult()
        Try

            Dim sql As String
            Dim dt As DataTable
            Dim lot As String
            If CurPlant = "E" Then
                lot = lblExport.Text
                If lblExport.Text = "" Then

                    BarcodeResult = "NOEXPORT"
                    Exit Sub
                End If
            Else
                lot = CurLOT
            End If
            sql = "select * from tb_lotcode where lotcode like '" & lot & "'"

            dt = DAL.getDataTable(sql, Connstr)
            If dt.Rows.Count < 1 Then
                ShowStatus("NO LOT", 5)
                SaveMSG("NO LOT:" & lot, "ERR")
                BarcodeResult = ""
                Exit Sub
            End If

            BarcodeResult = CurSEQ
            For Each dr As DataRow In dt.Rows
                BarcodeResult += CInt(dr("PLC_COMPARE").ToString()).ToString("00")
            Next
        Catch ex As Exception
            Dim Err As String = "getBarcodeResult:" & ex.Message
            ShowStatus(Err, 5, Color.Red)
            SaveMSG(Err, "Err")
        End Try
        '  BarcodeResult
    End Sub


    Private Sub btnProcessConfig_Click(sender As Object, e As EventArgs) Handles btnProcessConfig.Click
        Dim frm As New ProcessConfig
        frm.Show()
    End Sub

    Private Sub btnsetExportAct_Click(sender As Object, e As EventArgs) Handles btnsetExportAct.Click
        Try
            Dim str As String = InputBox("Input Actual QTY")
            If IsNumeric(str) = False Then
                Exit Sub
            End If
            lblExport_act.Text = str
            SaveSetting("export_act", str)

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            txtBarcode.Focus()
        End Try
    End Sub

    Private Sub btnSetExportQTY_Click(sender As Object, e As EventArgs) Handles btnSetExportQTY.Click
        Try

            Dim str As String = InputBox("Input  QTY")
            If IsNumeric(str) = False Then

                Exit Sub
            End If
            lblExport_QTY.Text = str
            SaveSetting("export_qty", str)



        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            txtBarcode.Focus()
        End Try
    End Sub

    Private Sub btnSetExport_Click(sender As Object, e As EventArgs) Handles btnSetExport.Click

        Try
            Dim str As String = InputBox("Input LOT")
            If str = "" Then
                Exit Sub
            End If
            Dim plc As String = ModelgetPLC(str)
            If plc = "" Then
                MsgBox("No Export in PLC")
                Exit Sub
            End If

            lblExport.Text = str
            SaveSetting("export_lot", str)
            getBarcodeResult()


            Try
                AB.IPAddress = PLCIP
                AB.WriteData(PLC_LOT, plc)


            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            txtBarcode.Focus()
        End Try
    End Sub

    Private Sub btnserverConfig_Click(sender As Object, e As EventArgs) Handles btnserverConfig.Click
        Dim frm As New ServerConfig
        frm.Show()
    End Sub



    Dim ProcessDisable As Boolean
    Private Sub LoadCurLOT()
        Dim sql As String
        Try
            sql = "SELECT [Plant],[Linename],[STATE],[Pitch],[Disable],[IP],[No],[COM],[ERR],[Run],[LOT],[SEQ],[JOBID],lotPlant,[pitchName],lotonly,isolate,Disable"
            sql += " FROM [TB_Pitch]"
            sql += " where Linename like '" & sLinename & "' and Pitch =" & sPitch & " and [No]=" & sStation & " " 'and Disable=0 " 'and (STATE=0 or STATE=1 or STATE=2 or STATE=3)"

            Dim dt As DataTable
            ' Dim dv As DataView


            dt = DAL.getDataTable(sql, Connstr)
            'dv = dt.DefaultView
            If dt Is Nothing Then
                Exit Sub
            End If
            If dt.Rows.Count > 0 Then
                If dt.Rows(0).Item("Disable").ToString() = "1" Then
                    Me.BeginInvoke(New MethodInvoker(Sub()
                                                         Me.Text = "Process Disable "
                                                         ' Me.BackColor = Color.Gray
                                                     End Sub))


                    '  showErrTime = 10
                    ProcessDisable = True
                    Exit Sub
                Else
                    Me.BeginInvoke(New MethodInvoker(Sub()


                                                         Me.Text = "Confirm Process"
                                                         '   Me.BackColor = Color.LightGray
                                                     End Sub))
                End If
                ProcessDisable = False
                CurState = dt.Rows(0).Item("STATE").ToString()
                '  If CurState = "3" Then

                '   lblStationName.Text = dt.Rows(0).Item("pitchName").ToString()
                CurSEQ = dt.Rows(0).Item("SEQ").ToString()
                CurLOT = dt.Rows(0).Item("LOT").ToString()
                CurPlant = dt.Rows(0).Item("lotPlant").ToString()
                'If dt.Rows(0).Item("Plant").ToString().ToUpper <> "COMMON" Then
                '    CurPlant = dt.Rows(0).Item("Plant").ToString()
                'End If


                CurID = dt.Rows(0).Item("JOBID").ToString()
                'End If

                'If dt.Rows(0).Item("IP").ToString = "" Then

                '    UpdateIP(Winsock1.LocalIP.ToString, sLinename)
                '    sql = "update tb_pitch set ip ='" & Winsock1.LocalIP.ToString & "' where "
                'End If


                'Try


                '    Project.Lotonly = dt.Rows(0).Item("LOTONLY").ToString()
                '    setLotonlyButton(Project.Lotonly)
                'Catch ex As Exception

                'End Try
                'If lblError.Text = "NO LOT CODE" Then

                '    lblError.Text = ""
                '    showErrTime = 0
                'End If
            Else

                'lblStatus.Text = "NO Pitch Data "
                'lblStatus.BackColor = Color.Red
                ShowStatus("NO Pitch Data ", 10, Color.Red)
                '  showErrTime = 10

            End If

        Catch ex As Exception


            Dim Err As String = "LoadCurLOT:" & ex.Message
            ShowStatus(Err, 5, Color.Red)
            SaveMSG(Err, "Err")
            Exit Sub
        End Try


    End Sub

    Sub DisplayData()


        Try
            lblPlant.Text = CurPlant
            lblSEQ.Text = CurSEQ
            lblLOtcode.Text = CurLOT
            If CurState = 1 Then
                lblJobState.Text = "WORKING"
                lblJobState.BackColor = Color.Yellow
            ElseIf CurState = 2 Then
                lblJobState.Text = "RESTART"
                lblJobState.BackColor = Color.Red
            ElseIf CurState = 3 Then
                lblJobState.Text = "COMPLETE"
                lblJobState.BackColor = Color.LimeGreen
            ElseIf CurState = 0 Then
                lblJobState.Text = "IDLE"
                lblJobState.BackColor = Color.White
            Else
                lblJobState.Text = ""
                lblJobState.BackColor = Color.White
            End If

        Catch ex As Exception

        End Try


    End Sub
    Sub savecomplete(ACT As String)
        Try
            Dim strSQL As String
            strSQL = "insert into TB_LOG" &
                        "([Process]" &
                        ",[Source]" &
                      ",[LOG_TIME]" &
                      ",[LOT]" &
                      ",[SEQ]" &
                       ",[Jobseq]" &
                        ",[Description]" &
                          ",[model]" &
                             ",[LOTPLANT]" &
                             ",[judgement]" &
                              ",[barcode]" &
                        ",[JOBNO])" &
                      " Values(" &
                      "'" & lblProcess.Text & "'," &
                      "'" & ACT & "'," &
                      "getdate()," &
                      "'" & lblLOtcode.Text & "'," &
                      "'" & lblSEQ.Text & "'," &
                      "'0'," &
                       "'" & ACT & "'," &
                           "''," &
                             "'" & lblPlant.Text & "'," &
                             "'" & ACT & "'," &
                              "'" & txtBarcode.Text & "'," &
                        "'')"
            DAL.GetExecute(strSQL, Connstr)

        Catch ex As Exception
            'MsgBox(ex.Message)
        End Try
    End Sub


    Private Sub txtBarcode_KeyDown(sender As Object, e As KeyEventArgs) Handles txtBarcode.KeyDown

        If e.KeyCode = Keys.Enter Then

            If txtBarcode.Text.Length = KANBANlen And txtBarcode.Text <> "" And ModelFrom = "KANBAN" Then
                CheckKanban()
            Else
                If CurState = "3" Then
                    ShowStatus("JOB IS Complete", 5, Color.Red)
                    txtBarcode.Text = ""
                    Exit Sub
                End If
                CheckBarcode()
            End If

        End If
    End Sub

    Private Sub btnModel_Click(sender As Object, e As EventArgs) Handles btnPLCSetting.Click
        Dim frm As New ModelFrm
        frm.frm = Me
        frm.Show()
    End Sub

    Dim dtlog As New DataTable
    Dim reloadDGV As New ReloadDGV

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim str As String = InputBox("Input Only Source",, " ")
        If str = "" Then Exit Sub
        lblOnlyPlant.Text = str.Trim
        SaveSetting("onlyplant", lblOnlyPlant.Text)
    End Sub

    Sub LoadLog()

        '  Timer_LoadLog.Change(Timeout.Infinite, Timeout.Infinite)
        Try
            Dim sql As String
            Dim dt As DataTable
            sql = "select top 100 log_id,format([log_time],'dd/MM/yyyy HH:mm:ss') as log_time,Judgement as act,lot,seq,barcode from tb_log where not isnull(barcode,'') like '' and process like '" & lblProcess.Text & "' order by log_id desc "
            dt = DAL.getDataTable(sql, Connstr)

            'If DGVLog.DataSource Is Nothing Then
            '    dtlog = dt

            '    DGVLog.BeginInvoke(New MethodInvoker(Sub()
            '                                             DGVLog.DataSource = dtlog
            '                                         End Sub))
            'Else
            '    reloadDGV.reloadDGVPlantnoSet(dtlog, dt, "log_id")
            'End If

            ' DGVLog.BeginInvoke(New MethodInvoker(Sub()
            DGVLog.DataSource = dt
            '                                     End Sub))

            Dim bSeq As String = ""
            Dim Bcolor As Color = Color.DodgerBlue
            For Each dr As DataGridViewRow In DGVLog.Rows
                Try
                    If dr.Cells("SEQ").Value Is Nothing Then
                        Continue For
                    End If
                    If bSeq <> dr.Cells("SEQ").Value.ToString Then
                        bSeq = dr.Cells("SEQ").Value.ToString
                        If Bcolor = Color.DodgerBlue Then
                            Bcolor = Color.White
                        Else
                            Bcolor = Color.DodgerBlue
                        End If

                    End If
                    dr.DefaultCellStyle.BackColor = Bcolor

                Catch ex As Exception

                End Try
            Next

            '    DGVLog.BeginInvoke(New MethodInvoker(Sub()
            '                                             DGVLog.Update()
            '                                         End Sub))
        Catch ex As Exception
            Dim Err As String = "LoadLog:" & ex.Message
            ShowStatus(Err, 5, Color.Red)
            ' SaveMSG(Err, "Err")
        Finally
            '     Timer_LoadLog.Change(5000, 5000)
        End Try
    End Sub


#Region "PLC"
    Dim AB As New EthernetIPforSLCMicroComm

    Private Sub btnSetBufferMin_Click(sender As Object, e As EventArgs) Handles btnSetBufferMin.Click
        Try

            Dim str As String = InputBox("Input  Buffer Minimum")
            If IsNumeric(str) = False Then

                Exit Sub
            End If
            lblBufferMin.Text = str
            Dim sql As String
            sql = "update tb_pis set buffer_min=" & str
            DAL.GetExecute(sql, Connstr)


        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            txtBarcode.Focus()
        End Try
    End Sub


    Public PLCIP As String
    Public PLC_LOT, PLC_Complete As String

    Function PLCgetModel(model As Int16)
        Dim sql As String
        sql = "select model from tb_model where plc='" & model & "'"
        Return DAL.getScalar(sql, Connstr)
    End Function
    Function ModelgetPLC(model As String)
        Dim sql As String
        sql = "select plc from tb_model where model='" & model & "'"
        Return DAL.getScalar(sql, Connstr)
    End Function
    Public ModelFrom As String
    Private Sub TimerPLC_Tick(sender As Object, e As EventArgs) Handles TimerPLC.Tick
        lblPLCaddress.Text = "PLC:" & PLCIP

        If PLCIP = "" Or ModelFrom <> "PLC" Then
            PNPLCStatus.BackColor = Color.DimGray
            Exit Sub
        End If
        Try
            If CurPlant <> "E" Then
                Exit Sub
            End If
            AB.IPAddress = PLCIP
            Dim modelstr As String
            Dim model As Int16
            model = AB.ReadAny(PLC_LOT, 1)(0)
            modelstr = PLCgetModel(model)

            SetExport(modelstr)


            PNPLCStatus.BackColor = Color.LimeGreen
        Catch ex As Exception
            PNPLCStatus.BackColor = Color.Red
        End Try
    End Sub
    Sub SetExport(modelstr As String)
        If lblExport.Text <> modelstr Then

            lblExport.Text = modelstr
            If modelstr = "" Then
                BarcodeResult = "NOEXPORT"
            Else
                getBarcodeResult()
            End If

            SaveSetting("export_lot", modelstr)

        End If

    End Sub
    Private Sub btnSetPLC_Click(sender As Object, e As EventArgs)
        Dim str As String = InputBox("Input PLC IP Address")

        PLCIP = str
        SaveSetting("PLCIP", PLCIP)
    End Sub





    Private Sub Form1_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        txtBarcode.Focus()
    End Sub

    Private Sub txtlastBarcode_TextChanged(sender As Object, e As EventArgs) Handles txtlastBarcode.TextChanged
        If txtlastBarcode.Text.Length > 10 Then
            Me.txtlastBarcode.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!)
            txtlastBarcode.SelectionStart = txtlastBarcode.TextLength - 1
        Else
            Me.txtlastBarcode.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!)
        End If
    End Sub

    Private Sub DGVLog_Paint(sender As Object, e As PaintEventArgs) Handles DGVLog.Paint
        Try

        Catch ex As Exception
            SaveMSG("DGVLOG_Paint:" & ex.Message, "ERR")
        End Try
    End Sub
#End Region
#Region "Alarm"
    Sub CheckBufferAlarm()
        Try

            Dim sql As String
            Dim dt As DataTable
            sql = "select buffer,buffer_min from tb_pis"
            dt = DAL.getDataTable(sql, Connstr)

            For Each dr As DataRow In dt.Rows
                lblBuffer.Text = dr("buffer").ToString
                lblBufferMin.Text = dr("buffer_min").ToString
                If CInt(dr("buffer")) <= CInt(dr("buffer_min")) Then
                    showAlarm = True
                Else
                    showAlarm = False
                End If
            Next
            lblBuffer.BackColor = Color.White
            lblBufferMin.BackColor = Color.White
        Catch ex As Exception
            lblBuffer.BackColor = Color.Red
            lblBufferMin.BackColor = Color.Red
            SaveMSG("CheckBufferAlarm:" & ex.Message, "ERR")
        End Try
    End Sub
    Dim showAlarm, AlarmFlage As Boolean
    Dim ShowAlarmCount As Integer

    Dim ShowalarmFax As Boolean
    Public FAXIP As String
    Dim PinglossCount As Integer = 0
    Private Sub TimerAlarm_Tick(sender As Object, e As EventArgs) Handles TimerAlarm.Tick
        If Now.Second Mod 5 = 0 Then 'check ทุก 5วินาที
            Dim pingFax As Boolean
            If FAXIP <> "" Then
                pingFax = IPReady(FAXIP)
                If pingFax = False Then
                    PinglossCount += 1
                    If PinglossCount >= 2 Then
                        ShowalarmFax = True
                    Else
                        ShowalarmFax = False
                    End If
                Else
                    PinglossCount = 0
                    ShowalarmFax = False
                End If
            Else
                ShowalarmFax = False
            End If

            CheckBufferAlarm()
        End If

        If showAlarm Or ShowalarmFax Then
            If ShowAlarmCount = 0 Then 'ถ้าครั้งแรก Savelog 
                If showAlarm Then
                    SaveMSG("Buffer:" & lblBuffer.Text & " ,min:" & lblBufferMin.Text, "ERR")
                End If
            End If
            If ShowAlarmCount = 15 Then 'ถ้าผ่านไป15วิ หรือ3ครั้ง Savelog 
                If ShowalarmFax Then
                    SaveMSG("Fax " & FAXIP & " Cannot Connect", "ERR")
                End If
            End If

            ShowAlarmCount += 1
            If ShowAlarmCount >= 30 Then
                ShowAlarmCount = 0
            End If

            'ที่จอจะแสดงทันที
            If ShowalarmFax Then
                lblAlarm.Text = "Fax " & FAXIP & " Cannot Connect"
            Else
                    lblAlarm.Text = "PIS Buffer Low"
                End If
                If AlarmFlage Then
                    Me.BackColor = Color.Red
                    lblBuffer.BackColor = Color.Yellow
                    lblAlarm.ForeColor = Color.Yellow

                Else
                    Me.BackColor = Color.White
                    lblBuffer.BackColor = Color.Red

                    lblAlarm.ForeColor = Color.Red
                End If
                AlarmFlage = Not AlarmFlage
                lblAlarm.Visible = True
            Else
                ShowAlarmCount = 0
            lblAlarm.Visible = False
            Me.BackColor = Color.LightGray
            lblBuffer.BackColor = Color.White


            lblModelForm.Text = "Model From : " & ModelFrom
        End If
    End Sub

    Private Sub txtBarcode_GotFocus(sender As Object, e As EventArgs) Handles txtBarcode.GotFocus
        If BarcodeConfirm = True Then
            txtBarcode.BackColor = Color.LightGreen
        End If

    End Sub

    Private Sub txtBarcode_LostFocus(sender As Object, e As EventArgs) Handles txtBarcode.LostFocus
        If BarcodeConfirm = True Then
            txtBarcode.BackColor = Color.DimGray
        End If

    End Sub
#End Region
End Class
