﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ModelFrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DGVModel = New System.Windows.Forms.DataGridView()
        Me.btnPLCsave = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnplccancel = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtipaddress = New System.Windows.Forms.TextBox()
        Me.txtplcLOT = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtPLCComplete = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtFaxIP = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.DGVKANBAN = New System.Windows.Forms.DataGridView()
        Me.txtKANBANlen = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.CBmodelFrom = New System.Windows.Forms.ComboBox()
        CType(Me.DGVModel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DGVKANBAN, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DGVModel
        '
        Me.DGVModel.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.SteelBlue
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGVModel.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DGVModel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVModel.EnableHeadersVisualStyles = False
        Me.DGVModel.Location = New System.Drawing.Point(11, 154)
        Me.DGVModel.Name = "DGVModel"
        Me.DGVModel.Size = New System.Drawing.Size(242, 136)
        Me.DGVModel.TabIndex = 1
        '
        'btnPLCsave
        '
        Me.btnPLCsave.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnPLCsave.FlatAppearance.BorderSize = 0
        Me.btnPLCsave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPLCsave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnPLCsave.ForeColor = System.Drawing.Color.White
        Me.btnPLCsave.Location = New System.Drawing.Point(-2, 8)
        Me.btnPLCsave.Name = "btnPLCsave"
        Me.btnPLCsave.Size = New System.Drawing.Size(127, 31)
        Me.btnPLCsave.TabIndex = 2
        Me.btnPLCsave.Text = "SAVE"
        Me.btnPLCsave.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.SlateGray
        Me.Panel1.Controls.Add(Me.btnPLCsave)
        Me.Panel1.Controls.Add(Me.btnplccancel)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel1.Location = New System.Drawing.Point(420, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(119, 565)
        Me.Panel1.TabIndex = 3
        '
        'btnplccancel
        '
        Me.btnplccancel.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnplccancel.FlatAppearance.BorderSize = 0
        Me.btnplccancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnplccancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnplccancel.ForeColor = System.Drawing.Color.White
        Me.btnplccancel.Location = New System.Drawing.Point(-3, 45)
        Me.btnplccancel.Name = "btnplccancel"
        Me.btnplccancel.Size = New System.Drawing.Size(130, 31)
        Me.btnplccancel.TabIndex = 3
        Me.btnplccancel.Text = "CANCEL"
        Me.btnplccancel.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.SkyBlue
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(11, 125)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(134, 20)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Model Config"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(21, 36)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(64, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "IP Address :"
        '
        'txtipaddress
        '
        Me.txtipaddress.Location = New System.Drawing.Point(125, 36)
        Me.txtipaddress.Name = "txtipaddress"
        Me.txtipaddress.Size = New System.Drawing.Size(100, 20)
        Me.txtipaddress.TabIndex = 5
        '
        'txtplcLOT
        '
        Me.txtplcLOT.Location = New System.Drawing.Point(125, 62)
        Me.txtplcLOT.Name = "txtplcLOT"
        Me.txtplcLOT.Size = New System.Drawing.Size(100, 20)
        Me.txtplcLOT.TabIndex = 7
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(21, 65)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(75, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "LOT Address :"
        '
        'txtPLCComplete
        '
        Me.txtPLCComplete.Location = New System.Drawing.Point(125, 88)
        Me.txtPLCComplete.Name = "txtPLCComplete"
        Me.txtPLCComplete.Size = New System.Drawing.Size(100, 20)
        Me.txtPLCComplete.TabIndex = 9
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(21, 91)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(98, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Complete Address :"
        '
        'txtFaxIP
        '
        Me.txtFaxIP.Location = New System.Drawing.Point(114, 299)
        Me.txtFaxIP.Name = "txtFaxIP"
        Me.txtFaxIP.Size = New System.Drawing.Size(100, 20)
        Me.txtFaxIP.TabIndex = 11
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(21, 302)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(87, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "FAX IP Address :"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.DGVKANBAN)
        Me.GroupBox1.Controls.Add(Me.txtKANBANlen)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Location = New System.Drawing.Point(15, 335)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(398, 230)
        Me.GroupBox1.TabIndex = 12
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "KANBAN"
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.Color.SkyBlue
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(6, 45)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(134, 26)
        Me.Label8.TabIndex = 15
        Me.Label8.Text = "KANBAN Model"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'DGVKANBAN
        '
        Me.DGVKANBAN.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.SteelBlue
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGVKANBAN.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.DGVKANBAN.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVKANBAN.EnableHeadersVisualStyles = False
        Me.DGVKANBAN.Location = New System.Drawing.Point(6, 74)
        Me.DGVKANBAN.Name = "DGVKANBAN"
        Me.DGVKANBAN.Size = New System.Drawing.Size(386, 135)
        Me.DGVKANBAN.TabIndex = 14
        '
        'txtKANBANlen
        '
        Me.txtKANBANlen.Location = New System.Drawing.Point(102, 22)
        Me.txtKANBANlen.Name = "txtKANBANlen"
        Me.txtKANBANlen.Size = New System.Drawing.Size(50, 20)
        Me.txtKANBANlen.TabIndex = 13
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 25)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(90, 13)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "KANBAN Lenght:"
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.SkyBlue
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(11, 125)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(134, 20)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Model Config"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label9.Location = New System.Drawing.Point(21, 9)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(80, 13)
        Me.Label9.TabIndex = 13
        Me.Label9.Text = "Model From :"
        '
        'CBmodelFrom
        '
        Me.CBmodelFrom.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.CBmodelFrom.FormattingEnabled = True
        Me.CBmodelFrom.Items.AddRange(New Object() {"PLC", "KANBAN"})
        Me.CBmodelFrom.Location = New System.Drawing.Point(107, 6)
        Me.CBmodelFrom.Name = "CBmodelFrom"
        Me.CBmodelFrom.Size = New System.Drawing.Size(107, 21)
        Me.CBmodelFrom.TabIndex = 14
        '
        'ModelFrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(539, 565)
        Me.Controls.Add(Me.CBmodelFrom)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.txtFaxIP)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtPLCComplete)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtplcLOT)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtipaddress)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.DGVModel)
        Me.Name = "ModelFrm"
        Me.Text = "Setting"
        CType(Me.DGVModel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DGVKANBAN, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DGVModel As DataGridView
    Friend WithEvents btnPLCsave As Button
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label1 As Label
    Friend WithEvents btnplccancel As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents txtipaddress As TextBox
    Friend WithEvents txtplcLOT As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txtPLCComplete As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txtFaxIP As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label8 As Label
    Friend WithEvents DGVKANBAN As DataGridView
    Friend WithEvents txtKANBANlen As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents CBmodelFrom As ComboBox
End Class
