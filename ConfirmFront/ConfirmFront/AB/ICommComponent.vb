﻿Public Interface ICommComponent
    Delegate Sub ReturnValues(ByVal Values As String())
    Function Subscribe(ByVal PLCAddress As String, ByVal updateRate As Integer, ByVal callBack As ReturnValues) As Integer
    Function UnSubscribe(ByVal ID As Integer) As Integer
    Function ReadAny(ByVal startAddress As String) As String
    Function ReadAny(ByVal startAddress As String, ByVal numberOfElements As Integer) As String()
    Function WriteData(ByVal startAddress As String, ByVal dataToWrite As String) As String
    Property DisableSubscriptions() As Boolean
End Interface
