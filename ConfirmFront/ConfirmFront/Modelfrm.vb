﻿Imports System.ComponentModel

Public Class ModelFrm

    Dim selectModelKANBAN As String = "select * from tb_model_KANBAN order by KANBAN"

    Dim selectModel As String = "select * from tb_model order by plc"
    Dim Keys As String = "id"
    Dim dtPLC, dtmap, dtKANBAN As DataTable

    Dim sql As String
    Dim dal As New DB.SqlConnect
    Private Sub PLCfrm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        loadModel()
        loadSetting()
        loadKANBAN()
    End Sub

    Sub loadSetting()
        Dim sql As String
        sql = "select * from setting"
        Dim dt As New DataTable
        dt = DALlite.getDataTable(sql, Connstr_lite)
        For Each dr As DataRow In dt.Rows


            If dr("name").ToString.ToUpper = "PLCIP".ToUpper Then
                txtipaddress.Text = dr("Value").ToString
            End If


            If dr("name").ToString.ToUpper = "PLC_LOT".ToUpper Then
                txtplcLOT.Text = dr("Value").ToString
            End If

            If dr("name").ToString.ToUpper = "PLC_Complete".ToUpper Then
                txtPLCComplete.Text = dr("Value").ToString
            End If

            If dr("name").ToString.ToUpper = "FAXIP".ToUpper Then
                txtFaxIP.Text = dr("Value").ToString
            End If
            If dr("name").ToString.ToUpper = "KANBANlen".ToUpper Then
                txtKANBANlen.Text = dr("Value").ToString
            End If
            If dr("name").ToString.ToUpper = "ModelFrom".ToUpper Then
                CBmodelFrom.Text = dr("Value").ToString
            End If
        Next
    End Sub
    Sub loadModel()
        dtPLC = dal.getDataTable(selectModel, Connstr)
        DGVModel.DataSource = dtPLC
        DGVModel.Columns(Keys).ReadOnly = True
        DGVModel.Columns(Keys).DefaultCellStyle.BackColor = Color.LightGray

    End Sub
    Sub loadKANBAN()
        dtKANBAN = dal.getDataTable(selectModelKANBAN, Connstr)
        DGVKANBAN.DataSource = dtKANBAN
        '  DGVKANBAN.Columns("KANBAN").ReadOnly = True
        ' DGVKANBAN.Columns("KANBAN").DefaultCellStyle.BackColor = Color.LightGray

    End Sub
    Public frm As Form1
    Public Reload As Boolean
    Private Sub btnPLCsave_Click(sender As Object, e As EventArgs) Handles btnPLCsave.Click
        Try


            frm.PLCIP = txtipaddress.Text
            frm.PLC_LOT = txtplcLOT.Text
            frm.PLC_Complete = txtPLCComplete.Text
            frm.KANBANlen = txtKANBANlen.Text
            frm.FAXIP = txtFaxIP.Text
            frm.ModelFrom = CBmodelFrom.Text

            SaveSetting("ModelFrom", CBmodelFrom.Text)
            SaveSetting("KANBANlen", txtKANBANlen.Text)
            SaveSetting("FAXIP", txtFaxIP.Text)
            SaveSetting("PLC_LOT", txtplcLOT.Text)
            SaveSetting("PLC_Complete", txtPLCComplete.Text)
            SaveSetting("PLCIP", txtipaddress.Text)
            dal.Update(dtPLC, selectModel, Connstr)
            dal.Update(dtKANBAN, selectModelKANBAN, Connstr)
            Reload = True
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub





    Private Sub btnplccancel_Click(sender As Object, e As EventArgs) Handles btnplccancel.Click
        loadModel()
        loadSetting()
        loadKANBAN()
    End Sub


    Private Sub PLCfrm_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        If Reload Then
            DialogResult = DialogResult.OK
        Else
            DialogResult = DialogResult.Cancel
        End If

    End Sub
End Class