﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.lblJobState = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.lblPlant = New System.Windows.Forms.Label()
        Me.lblProcess = New System.Windows.Forms.Label()
        Me.lblSEQ = New System.Windows.Forms.Label()
        Me.lblLOtcode = New System.Windows.Forms.Label()
        Me.lblLH = New System.Windows.Forms.Label()
        Me.lblRH = New System.Windows.Forms.Label()
        Me.txtBarcode = New System.Windows.Forms.TextBox()
        Me.txtlastBarcode = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.DGVLog = New System.Windows.Forms.DataGridView()
        Me.Logtime = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Barcode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LOTCODE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SEQ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ACT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lblExport_act = New System.Windows.Forms.Label()
        Me.lblExport_QTY = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblExport = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btnserverConfig = New System.Windows.Forms.Button()
        Me.btnProcessConfig = New System.Windows.Forms.Button()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.lblstatus = New System.Windows.Forms.Label()
        Me.btnSetExport = New System.Windows.Forms.Button()
        Me.btnsetExportAct = New System.Windows.Forms.Button()
        Me.btnSetExportQTY = New System.Windows.Forms.Button()
        Me.TimerPLC = New System.Windows.Forms.Timer(Me.components)
        Me.lblPLCaddress = New System.Windows.Forms.Label()
        Me.PNPLCStatus = New System.Windows.Forms.Panel()
        Me.btnPLCSetting = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblOnlyPlant = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.lblBuffer = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.lblBufferMin = New System.Windows.Forms.Label()
        Me.btnSetBufferMin = New System.Windows.Forms.Button()
        Me.TimerAlarm = New System.Windows.Forms.Timer(Me.components)
        Me.lblAlarm = New System.Windows.Forms.Label()
        Me.lblModelForm = New System.Windows.Forms.Label()
        Me.lblLH_KANBAN = New System.Windows.Forms.Label()
        Me.lblRH_KANBAN = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.DGVLog, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Azure
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.lblJobState)
        Me.Panel1.Controls.Add(Me.Label14)
        Me.Panel1.Controls.Add(Me.Label13)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.lblPlant)
        Me.Panel1.Controls.Add(Me.lblProcess)
        Me.Panel1.Controls.Add(Me.lblSEQ)
        Me.Panel1.Controls.Add(Me.lblLOtcode)
        Me.Panel1.Location = New System.Drawing.Point(1, 1)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(534, 82)
        Me.Panel1.TabIndex = 47
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label8.Location = New System.Drawing.Point(311, 52)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(43, 13)
        Me.Label8.TabIndex = 52
        Me.Label8.Text = "Status :"
        '
        'lblJobState
        '
        Me.lblJobState.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.lblJobState.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblJobState.Location = New System.Drawing.Point(360, 45)
        Me.lblJobState.Name = "lblJobState"
        Me.lblJobState.Size = New System.Drawing.Size(162, 23)
        Me.lblJobState.TabIndex = 51
        Me.lblJobState.Text = "WORKING"
        Me.lblJobState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label14.Location = New System.Drawing.Point(363, 4)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(45, 13)
        Me.Label14.TabIndex = 50
        Me.Label14.Text = "Process"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label13.Location = New System.Drawing.Point(224, 4)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(58, 13)
        Me.Label13.TabIndex = 49
        Me.Label13.Text = "LOTCODE"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label12.Location = New System.Drawing.Point(118, 4)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(29, 13)
        Me.Label12.TabIndex = 48
        Me.Label12.Text = "SEQ"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label11.Location = New System.Drawing.Point(29, 4)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(41, 13)
        Me.Label11.TabIndex = 47
        Me.Label11.Text = "Source"
        '
        'lblPlant
        '
        Me.lblPlant.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.lblPlant.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblPlant.Location = New System.Drawing.Point(17, 20)
        Me.lblPlant.Name = "lblPlant"
        Me.lblPlant.Size = New System.Drawing.Size(64, 48)
        Me.lblPlant.TabIndex = 42
        Me.lblPlant.Text = "E"
        Me.lblPlant.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblProcess
        '
        Me.lblProcess.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.lblProcess.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblProcess.Location = New System.Drawing.Point(311, 20)
        Me.lblProcess.Name = "lblProcess"
        Me.lblProcess.Size = New System.Drawing.Size(212, 23)
        Me.lblProcess.TabIndex = 45
        Me.lblProcess.Text = "Fr ,P2 ,S1"
        Me.lblProcess.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblSEQ
        '
        Me.lblSEQ.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.lblSEQ.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblSEQ.Location = New System.Drawing.Point(87, 20)
        Me.lblSEQ.Name = "lblSEQ"
        Me.lblSEQ.Size = New System.Drawing.Size(91, 48)
        Me.lblSEQ.TabIndex = 43
        Me.lblSEQ.Text = "000"
        Me.lblSEQ.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblLOtcode
        '
        Me.lblLOtcode.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.lblLOtcode.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblLOtcode.Location = New System.Drawing.Point(184, 20)
        Me.lblLOtcode.Name = "lblLOtcode"
        Me.lblLOtcode.Size = New System.Drawing.Size(121, 48)
        Me.lblLOtcode.TabIndex = 44
        Me.lblLOtcode.Text = "RQVB"
        Me.lblLOtcode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblLH
        '
        Me.lblLH.BackColor = System.Drawing.Color.Gray
        Me.lblLH.Font = New System.Drawing.Font("Microsoft Sans Serif", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblLH.Location = New System.Drawing.Point(45, 122)
        Me.lblLH.Name = "lblLH"
        Me.lblLH.Size = New System.Drawing.Size(103, 62)
        Me.lblLH.TabIndex = 48
        Me.lblLH.Text = "LH"
        Me.lblLH.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblRH
        '
        Me.lblRH.BackColor = System.Drawing.Color.Gray
        Me.lblRH.Font = New System.Drawing.Font("Microsoft Sans Serif", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblRH.Location = New System.Drawing.Point(216, 122)
        Me.lblRH.Name = "lblRH"
        Me.lblRH.Size = New System.Drawing.Size(103, 62)
        Me.lblRH.TabIndex = 49
        Me.lblRH.Text = "RH"
        Me.lblRH.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtBarcode
        '
        Me.txtBarcode.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtBarcode.Location = New System.Drawing.Point(12, 487)
        Me.txtBarcode.Name = "txtBarcode"
        Me.txtBarcode.Size = New System.Drawing.Size(181, 49)
        Me.txtBarcode.TabIndex = 50
        '
        'txtlastBarcode
        '
        Me.txtlastBarcode.Enabled = False
        Me.txtlastBarcode.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!)
        Me.txtlastBarcode.Location = New System.Drawing.Point(199, 487)
        Me.txtlastBarcode.Name = "txtlastBarcode"
        Me.txtlastBarcode.Size = New System.Drawing.Size(181, 49)
        Me.txtlastBarcode.TabIndex = 51
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(21, 471)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(47, 13)
        Me.Label2.TabIndex = 52
        Me.Label2.Text = "Barcode"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(202, 471)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(70, 13)
        Me.Label3.TabIndex = 53
        Me.Label3.Text = "Last Barcode"
        '
        'DGVLog
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGVLog.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DGVLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVLog.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Logtime, Me.Barcode, Me.LOTCODE, Me.SEQ, Me.ACT})
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DGVLog.DefaultCellStyle = DataGridViewCellStyle3
        Me.DGVLog.Location = New System.Drawing.Point(386, 89)
        Me.DGVLog.Name = "DGVLog"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGVLog.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.DGVLog.RowHeadersVisible = False
        Me.DGVLog.Size = New System.Drawing.Size(395, 471)
        Me.DGVLog.TabIndex = 54
        '
        'Logtime
        '
        Me.Logtime.DataPropertyName = "Log_time"
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle2.Format = "s"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.Logtime.DefaultCellStyle = DataGridViewCellStyle2
        Me.Logtime.HeaderText = "DateTime"
        Me.Logtime.Name = "Logtime"
        Me.Logtime.Width = 120
        '
        'Barcode
        '
        Me.Barcode.DataPropertyName = "Barcode"
        Me.Barcode.HeaderText = "Barcode"
        Me.Barcode.Name = "Barcode"
        Me.Barcode.Width = 80
        '
        'LOTCODE
        '
        Me.LOTCODE.DataPropertyName = "LOT"
        Me.LOTCODE.HeaderText = "LOT"
        Me.LOTCODE.Name = "LOTCODE"
        Me.LOTCODE.Width = 60
        '
        'SEQ
        '
        Me.SEQ.DataPropertyName = "SEQ"
        Me.SEQ.HeaderText = "SEQ"
        Me.SEQ.Name = "SEQ"
        Me.SEQ.Width = 50
        '
        'ACT
        '
        Me.ACT.DataPropertyName = "ACT"
        Me.ACT.HeaderText = "ACT"
        Me.ACT.Name = "ACT"
        Me.ACT.Width = 80
        '
        'lblExport_act
        '
        Me.lblExport_act.BackColor = System.Drawing.Color.White
        Me.lblExport_act.Font = New System.Drawing.Font("Microsoft Sans Serif", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblExport_act.Location = New System.Drawing.Point(25, 306)
        Me.lblExport_act.Name = "lblExport_act"
        Me.lblExport_act.Size = New System.Drawing.Size(103, 62)
        Me.lblExport_act.TabIndex = 55
        Me.lblExport_act.Text = "0"
        Me.lblExport_act.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblExport_QTY
        '
        Me.lblExport_QTY.BackColor = System.Drawing.Color.White
        Me.lblExport_QTY.Font = New System.Drawing.Font("Microsoft Sans Serif", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblExport_QTY.Location = New System.Drawing.Point(218, 306)
        Me.lblExport_QTY.Name = "lblExport_QTY"
        Me.lblExport_QTY.Size = New System.Drawing.Size(103, 62)
        Me.lblExport_QTY.TabIndex = 56
        Me.lblExport_QTY.Text = "10"
        Me.lblExport_QTY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label4.Location = New System.Drawing.Point(158, 306)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(38, 55)
        Me.Label4.TabIndex = 57
        Me.Label4.Text = "/"
        '
        'lblExport
        '
        Me.lblExport.BackColor = System.Drawing.Color.White
        Me.lblExport.Font = New System.Drawing.Font("Microsoft Sans Serif", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblExport.Location = New System.Drawing.Point(112, 237)
        Me.lblExport.Name = "lblExport"
        Me.lblExport.Size = New System.Drawing.Size(156, 62)
        Me.lblExport.TabIndex = 58
        Me.lblExport.Text = "E002"
        Me.lblExport.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label6.Location = New System.Drawing.Point(15, 250)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(91, 42)
        Me.Label6.TabIndex = 59
        Me.Label6.Text = "LOT"
        '
        'btnserverConfig
        '
        Me.btnserverConfig.BackColor = System.Drawing.Color.DimGray
        Me.btnserverConfig.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnserverConfig.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnserverConfig.ForeColor = System.Drawing.Color.White
        Me.btnserverConfig.Location = New System.Drawing.Point(622, 31)
        Me.btnserverConfig.Name = "btnserverConfig"
        Me.btnserverConfig.Size = New System.Drawing.Size(75, 41)
        Me.btnserverConfig.TabIndex = 184
        Me.btnserverConfig.Text = "Server Config"
        Me.btnserverConfig.UseVisualStyleBackColor = False
        '
        'btnProcessConfig
        '
        Me.btnProcessConfig.BackColor = System.Drawing.Color.DimGray
        Me.btnProcessConfig.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnProcessConfig.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnProcessConfig.ForeColor = System.Drawing.Color.White
        Me.btnProcessConfig.Location = New System.Drawing.Point(541, 31)
        Me.btnProcessConfig.Name = "btnProcessConfig"
        Me.btnProcessConfig.Size = New System.Drawing.Size(75, 41)
        Me.btnProcessConfig.TabIndex = 183
        Me.btnProcessConfig.Text = "Process Config"
        Me.btnProcessConfig.UseVisualStyleBackColor = False
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 1000
        '
        'lblstatus
        '
        Me.lblstatus.AutoSize = True
        Me.lblstatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblstatus.Location = New System.Drawing.Point(9, 539)
        Me.lblstatus.Name = "lblstatus"
        Me.lblstatus.Size = New System.Drawing.Size(48, 16)
        Me.lblstatus.TabIndex = 185
        Me.lblstatus.Text = "Status:"
        '
        'btnSetExport
        '
        Me.btnSetExport.BackColor = System.Drawing.Color.DimGray
        Me.btnSetExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSetExport.ForeColor = System.Drawing.Color.White
        Me.btnSetExport.Location = New System.Drawing.Point(274, 276)
        Me.btnSetExport.Name = "btnSetExport"
        Me.btnSetExport.Size = New System.Drawing.Size(46, 23)
        Me.btnSetExport.TabIndex = 186
        Me.btnSetExport.Text = "SET"
        Me.btnSetExport.UseVisualStyleBackColor = False
        '
        'btnsetExportAct
        '
        Me.btnsetExportAct.BackColor = System.Drawing.Color.DimGray
        Me.btnsetExportAct.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnsetExportAct.ForeColor = System.Drawing.Color.White
        Me.btnsetExportAct.Location = New System.Drawing.Point(57, 371)
        Me.btnsetExportAct.Name = "btnsetExportAct"
        Me.btnsetExportAct.Size = New System.Drawing.Size(46, 23)
        Me.btnsetExportAct.TabIndex = 187
        Me.btnsetExportAct.Text = "SET"
        Me.btnsetExportAct.UseVisualStyleBackColor = False
        '
        'btnSetExportQTY
        '
        Me.btnSetExportQTY.BackColor = System.Drawing.Color.DimGray
        Me.btnSetExportQTY.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSetExportQTY.ForeColor = System.Drawing.Color.White
        Me.btnSetExportQTY.Location = New System.Drawing.Point(246, 371)
        Me.btnSetExportQTY.Name = "btnSetExportQTY"
        Me.btnSetExportQTY.Size = New System.Drawing.Size(46, 23)
        Me.btnSetExportQTY.TabIndex = 188
        Me.btnSetExportQTY.Text = "SET"
        Me.btnSetExportQTY.UseVisualStyleBackColor = False
        '
        'TimerPLC
        '
        Me.TimerPLC.Enabled = True
        Me.TimerPLC.Interval = 1000
        '
        'lblPLCaddress
        '
        Me.lblPLCaddress.AutoSize = True
        Me.lblPLCaddress.Location = New System.Drawing.Point(42, 89)
        Me.lblPLCaddress.Name = "lblPLCaddress"
        Me.lblPLCaddress.Size = New System.Drawing.Size(30, 13)
        Me.lblPLCaddress.TabIndex = 189
        Me.lblPLCaddress.Text = "PLC:"
        '
        'PNPLCStatus
        '
        Me.PNPLCStatus.BackColor = System.Drawing.Color.DimGray
        Me.PNPLCStatus.Location = New System.Drawing.Point(24, 90)
        Me.PNPLCStatus.Name = "PNPLCStatus"
        Me.PNPLCStatus.Size = New System.Drawing.Size(12, 12)
        Me.PNPLCStatus.TabIndex = 190
        '
        'btnPLCSetting
        '
        Me.btnPLCSetting.BackColor = System.Drawing.Color.DimGray
        Me.btnPLCSetting.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPLCSetting.ForeColor = System.Drawing.Color.White
        Me.btnPLCSetting.Location = New System.Drawing.Point(703, 31)
        Me.btnPLCSetting.Name = "btnPLCSetting"
        Me.btnPLCSetting.Size = New System.Drawing.Size(65, 41)
        Me.btnPLCSetting.TabIndex = 192
        Me.btnPLCSetting.Text = "Setting"
        Me.btnPLCSetting.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(225, 90)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(47, 13)
        Me.Label1.TabIndex = 193
        Me.Label1.Text = "Source :"
        '
        'lblOnlyPlant
        '
        Me.lblOnlyPlant.AutoSize = True
        Me.lblOnlyPlant.Location = New System.Drawing.Point(278, 90)
        Me.lblOnlyPlant.Name = "lblOnlyPlant"
        Me.lblOnlyPlant.Size = New System.Drawing.Size(23, 13)
        Me.lblOnlyPlant.TabIndex = 194
        Me.lblOnlyPlant.Text = "E,1"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DimGray
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(316, 85)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(46, 23)
        Me.Button1.TabIndex = 195
        Me.Button1.Text = "SET"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'lblBuffer
        '
        Me.lblBuffer.BackColor = System.Drawing.Color.White
        Me.lblBuffer.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblBuffer.Location = New System.Drawing.Point(101, 426)
        Me.lblBuffer.Name = "lblBuffer"
        Me.lblBuffer.Size = New System.Drawing.Size(51, 41)
        Me.lblBuffer.TabIndex = 196
        Me.lblBuffer.Text = "0"
        Me.lblBuffer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label9.Location = New System.Drawing.Point(12, 441)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(83, 20)
        Me.Label9.TabIndex = 197
        Me.Label9.Text = "PIS Buffer"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label10.Location = New System.Drawing.Point(158, 441)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(97, 20)
        Me.Label10.TabIndex = 198
        Me.Label10.Text = "Alarm Below"
        '
        'lblBufferMin
        '
        Me.lblBufferMin.BackColor = System.Drawing.Color.White
        Me.lblBufferMin.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblBufferMin.Location = New System.Drawing.Point(261, 426)
        Me.lblBufferMin.Name = "lblBufferMin"
        Me.lblBufferMin.Size = New System.Drawing.Size(51, 41)
        Me.lblBufferMin.TabIndex = 199
        Me.lblBufferMin.Text = "0"
        Me.lblBufferMin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnSetBufferMin
        '
        Me.btnSetBufferMin.BackColor = System.Drawing.Color.DimGray
        Me.btnSetBufferMin.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSetBufferMin.ForeColor = System.Drawing.Color.White
        Me.btnSetBufferMin.Location = New System.Drawing.Point(318, 441)
        Me.btnSetBufferMin.Name = "btnSetBufferMin"
        Me.btnSetBufferMin.Size = New System.Drawing.Size(46, 23)
        Me.btnSetBufferMin.TabIndex = 200
        Me.btnSetBufferMin.Text = "SET"
        Me.btnSetBufferMin.UseVisualStyleBackColor = False
        '
        'TimerAlarm
        '
        Me.TimerAlarm.Enabled = True
        Me.TimerAlarm.Interval = 1000
        '
        'lblAlarm
        '
        Me.lblAlarm.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblAlarm.Location = New System.Drawing.Point(1, 406)
        Me.lblAlarm.Name = "lblAlarm"
        Me.lblAlarm.Size = New System.Drawing.Size(379, 20)
        Me.lblAlarm.TabIndex = 201
        Me.lblAlarm.Text = "PIS Buffer Low"
        Me.lblAlarm.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAlarm.Visible = False
        '
        'lblModelForm
        '
        Me.lblModelForm.AutoSize = True
        Me.lblModelForm.Location = New System.Drawing.Point(21, 105)
        Me.lblModelForm.Name = "lblModelForm"
        Me.lblModelForm.Size = New System.Drawing.Size(68, 13)
        Me.lblModelForm.TabIndex = 202
        Me.lblModelForm.Text = "Model From :"
        '
        'lblLH_KANBAN
        '
        Me.lblLH_KANBAN.BackColor = System.Drawing.Color.Gray
        Me.lblLH_KANBAN.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblLH_KANBAN.Location = New System.Drawing.Point(45, 186)
        Me.lblLH_KANBAN.Name = "lblLH_KANBAN"
        Me.lblLH_KANBAN.Size = New System.Drawing.Size(103, 29)
        Me.lblLH_KANBAN.TabIndex = 203
        Me.lblLH_KANBAN.Text = "KANBAN"
        Me.lblLH_KANBAN.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblRH_KANBAN
        '
        Me.lblRH_KANBAN.BackColor = System.Drawing.Color.Gray
        Me.lblRH_KANBAN.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblRH_KANBAN.Location = New System.Drawing.Point(216, 186)
        Me.lblRH_KANBAN.Name = "lblRH_KANBAN"
        Me.lblRH_KANBAN.Size = New System.Drawing.Size(103, 29)
        Me.lblRH_KANBAN.TabIndex = 204
        Me.lblRH_KANBAN.Text = "KANBAN"
        Me.lblRH_KANBAN.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(784, 561)
        Me.Controls.Add(Me.lblRH_KANBAN)
        Me.Controls.Add(Me.lblLH_KANBAN)
        Me.Controls.Add(Me.lblModelForm)
        Me.Controls.Add(Me.lblAlarm)
        Me.Controls.Add(Me.btnSetBufferMin)
        Me.Controls.Add(Me.lblBufferMin)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.lblBuffer)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.lblOnlyPlant)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnPLCSetting)
        Me.Controls.Add(Me.PNPLCStatus)
        Me.Controls.Add(Me.lblPLCaddress)
        Me.Controls.Add(Me.btnSetExportQTY)
        Me.Controls.Add(Me.btnsetExportAct)
        Me.Controls.Add(Me.btnSetExport)
        Me.Controls.Add(Me.lblstatus)
        Me.Controls.Add(Me.btnserverConfig)
        Me.Controls.Add(Me.btnProcessConfig)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.lblExport)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lblExport_QTY)
        Me.Controls.Add(Me.lblExport_act)
        Me.Controls.Add(Me.DGVLog)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtlastBarcode)
        Me.Controls.Add(Me.txtBarcode)
        Me.Controls.Add(Me.lblRH)
        Me.Controls.Add(Me.lblLH)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "Form1"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DGVLog, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label8 As Label
    Friend WithEvents lblJobState As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents lblPlant As Label
    Friend WithEvents lblProcess As Label
    Friend WithEvents lblSEQ As Label
    Friend WithEvents lblLOtcode As Label
    Friend WithEvents lblLH As Label
    Friend WithEvents lblRH As Label
    Friend WithEvents txtBarcode As TextBox
    Friend WithEvents txtlastBarcode As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents DGVLog As DataGridView
    Friend WithEvents lblExport_act As Label
    Friend WithEvents lblExport_QTY As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents lblExport As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents btnserverConfig As Button
    Friend WithEvents btnProcessConfig As Button
    Friend WithEvents Timer1 As Timer
    Friend WithEvents lblstatus As Label
    Friend WithEvents Logtime As DataGridViewTextBoxColumn
    Friend WithEvents Barcode As DataGridViewTextBoxColumn
    Friend WithEvents LOTCODE As DataGridViewTextBoxColumn
    Friend WithEvents SEQ As DataGridViewTextBoxColumn
    Friend WithEvents ACT As DataGridViewTextBoxColumn
    Friend WithEvents btnSetExport As Button
    Friend WithEvents btnsetExportAct As Button
    Friend WithEvents btnSetExportQTY As Button
    Friend WithEvents TimerPLC As Timer
    Friend WithEvents lblPLCaddress As Label
    Friend WithEvents PNPLCStatus As Panel
    Friend WithEvents btnPLCSetting As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents lblOnlyPlant As Label
    Friend WithEvents Button1 As Button
    Friend WithEvents lblBuffer As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents lblBufferMin As Label
    Friend WithEvents btnSetBufferMin As Button
    Friend WithEvents TimerAlarm As Timer
    Friend WithEvents lblAlarm As Label
    Friend WithEvents lblModelForm As Label
    Friend WithEvents lblLH_KANBAN As Label
    Friend WithEvents lblRH_KANBAN As Label
End Class
