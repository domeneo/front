﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MixMonitorFrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MixMonitorFrm))
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DGV_P1 = New System.Windows.Forms.DataGridView()
        Me.P1_LOT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.P1_SEQ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.family_code = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.P1_Time = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DGV_E = New System.Windows.Forms.DataGridView()
        Me.ExportSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.E_LOT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.E_SEQ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Export_seq = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.E_insert = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.E_DELETE = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.E_id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EX_BCDATA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DGV_Common = New System.Windows.Forms.DataGridView()
        Me.Source = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cmmon_family_code = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewButtonColumn1 = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.DataGridViewButtonColumn2 = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.btnRatiosetE = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtRatioE = New System.Windows.Forms.TextBox()
        Me.txtExportLOT = New System.Windows.Forms.TextBox()
        Me.btnExportInsert = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtexportQTY = New System.Windows.Forms.TextBox()
        Me.txtRatioP1 = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnRatiosetP1 = New System.Windows.Forms.Button()
        Me.lblRecord_E = New System.Windows.Forms.Label()
        Me.lblRecord_P1 = New System.Windows.Forms.Label()
        Me.lblRecord_Common = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.PB_common = New System.Windows.Forms.PictureBox()
        Me.PB_P1 = New System.Windows.Forms.PictureBox()
        Me.PB_E = New System.Windows.Forms.PictureBox()
        Me.PBmask_Common = New System.Windows.Forms.PictureBox()
        Me.PBmask_P1 = New System.Windows.Forms.PictureBox()
        Me.PBmask_E = New System.Windows.Forms.PictureBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.toolbar = New System.Windows.Forms.StatusStrip()
        Me.lblStatus = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ServerPN = New System.Windows.Forms.Panel()
        Me.Label75 = New System.Windows.Forms.Label()
        Me.ServerPingPN = New System.Windows.Forms.Panel()
        Me.lblServer1 = New System.Windows.Forms.Label()
        Me.ServerPingPN2 = New System.Windows.Forms.Panel()
        Me.lblServer2 = New System.Windows.Forms.Label()
        Me.Label74 = New System.Windows.Forms.Label()
        Me.ServerLinkPN = New System.Windows.Forms.Panel()
        Me.ServerLinkPN2 = New System.Windows.Forms.Panel()
        Me.TimerServerLink = New System.Windows.Forms.Timer(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.lblLogistic = New System.Windows.Forms.Label()
        Me.btnSetLogistic = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.BtnStartMoveQ = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.DGV_E_SHOW = New System.Windows.Forms.DataGridView()
        Me.BCDATA_SHOW = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.export_SEQ_show = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LOTCODE_SHOW = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QTY_SHOW = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MIX_SHOW = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtExport_DATA = New System.Windows.Forms.TextBox()
        Me.btnEdit = New System.Windows.Forms.Button()
        Me.btnExFinish = New System.Windows.Forms.Button()
        Me.btnEx_del = New System.Windows.Forms.Button()
        Me.TimerDelete = New System.Windows.Forms.Timer(Me.components)
        Me.DGVOT = New System.Windows.Forms.DataGridView()
        Me.SelectOT = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.C_Stopmix = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OTid = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnOTsetting = New System.Windows.Forms.Button()
        Me.TimerStopMix = New System.Windows.Forms.Timer(Me.components)
        CType(Me.DGV_P1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGV_E, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGV_Common, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PB_common, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PB_P1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PB_E, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PBmask_Common, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PBmask_P1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PBmask_E, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.toolbar.SuspendLayout()
        Me.ServerPN.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.DGV_E_SHOW, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGVOT, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DGV_P1
        '
        Me.DGV_P1.AllowUserToAddRows = False
        Me.DGV_P1.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.DGV_P1.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.DimGray
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGV_P1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.DGV_P1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_P1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.P1_LOT, Me.P1_SEQ, Me.family_code, Me.P1_Time})
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DGV_P1.DefaultCellStyle = DataGridViewCellStyle3
        Me.DGV_P1.EnableHeadersVisualStyles = False
        Me.DGV_P1.Location = New System.Drawing.Point(24, 394)
        Me.DGV_P1.Name = "DGV_P1"
        Me.DGV_P1.ReadOnly = True
        Me.DGV_P1.RowHeadersVisible = False
        Me.DGV_P1.Size = New System.Drawing.Size(316, 286)
        Me.DGV_P1.TabIndex = 0
        '
        'P1_LOT
        '
        Me.P1_LOT.DataPropertyName = "LOTCODE"
        Me.P1_LOT.HeaderText = "LOT"
        Me.P1_LOT.Name = "P1_LOT"
        Me.P1_LOT.ReadOnly = True
        Me.P1_LOT.Width = 50
        '
        'P1_SEQ
        '
        Me.P1_SEQ.DataPropertyName = "Sequence"
        Me.P1_SEQ.HeaderText = "SEQ"
        Me.P1_SEQ.Name = "P1_SEQ"
        Me.P1_SEQ.ReadOnly = True
        Me.P1_SEQ.Width = 50
        '
        'family_code
        '
        Me.family_code.DataPropertyName = "family_code"
        Me.family_code.HeaderText = "family"
        Me.family_code.Name = "family_code"
        Me.family_code.ReadOnly = True
        Me.family_code.Width = 60
        '
        'P1_Time
        '
        Me.P1_Time.DataPropertyName = "Eventdate"
        Me.P1_Time.HeaderText = "Time"
        Me.P1_Time.Name = "P1_Time"
        Me.P1_Time.ReadOnly = True
        Me.P1_Time.Width = 150
        '
        'DGV_E
        '
        Me.DGV_E.AllowUserToAddRows = False
        Me.DGV_E.AllowUserToDeleteRows = False
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.DGV_E.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle4
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.DimGray
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGV_E.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.DGV_E.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_E.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ExportSelect, Me.E_LOT, Me.E_SEQ, Me.DataGridViewTextBoxColumn3, Me.Export_seq, Me.E_insert, Me.E_DELETE, Me.E_id, Me.EX_BCDATA})
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.Fuchsia
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DGV_E.DefaultCellStyle = DataGridViewCellStyle6
        Me.DGV_E.EnableHeadersVisualStyles = False
        Me.DGV_E.Location = New System.Drawing.Point(584, 394)
        Me.DGV_E.Name = "DGV_E"
        Me.DGV_E.RowHeadersVisible = False
        Me.DGV_E.Size = New System.Drawing.Size(424, 242)
        Me.DGV_E.TabIndex = 1
        '
        'ExportSelect
        '
        Me.ExportSelect.HeaderText = ""
        Me.ExportSelect.Name = "ExportSelect"
        Me.ExportSelect.Visible = False
        Me.ExportSelect.Width = 20
        '
        'E_LOT
        '
        Me.E_LOT.DataPropertyName = "LOTCODE"
        Me.E_LOT.HeaderText = "LOT"
        Me.E_LOT.Name = "E_LOT"
        Me.E_LOT.Width = 50
        '
        'E_SEQ
        '
        Me.E_SEQ.DataPropertyName = "sequence"
        Me.E_SEQ.HeaderText = "SEQ"
        Me.E_SEQ.Name = "E_SEQ"
        Me.E_SEQ.Width = 50
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Eventdate"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Time"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Width = 150
        '
        'Export_seq
        '
        Me.Export_seq.DataPropertyName = "Export_seq"
        Me.Export_seq.HeaderText = "E_SEQ"
        Me.Export_seq.Name = "Export_seq"
        Me.Export_seq.Width = 70
        '
        'E_insert
        '
        Me.E_insert.DataPropertyName = "insert"
        Me.E_insert.HeaderText = "Insert"
        Me.E_insert.Name = "E_insert"
        Me.E_insert.Width = 50
        '
        'E_DELETE
        '
        Me.E_DELETE.DataPropertyName = "Del"
        Me.E_DELETE.HeaderText = "DEL"
        Me.E_DELETE.Name = "E_DELETE"
        Me.E_DELETE.Width = 50
        '
        'E_id
        '
        Me.E_id.DataPropertyName = "id"
        Me.E_id.HeaderText = "id"
        Me.E_id.Name = "E_id"
        Me.E_id.Visible = False
        '
        'EX_BCDATA
        '
        Me.EX_BCDATA.DataPropertyName = "BCDATA"
        Me.EX_BCDATA.HeaderText = "BCDATA"
        Me.EX_BCDATA.Name = "EX_BCDATA"
        '
        'DGV_Common
        '
        Me.DGV_Common.AllowUserToAddRows = False
        Me.DGV_Common.AllowUserToDeleteRows = False
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.WhiteSmoke
        Me.DGV_Common.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle7
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.DimGray
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGV_Common.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.DGV_Common.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_Common.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Source, Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn4, Me.Cmmon_family_code, Me.DataGridViewTextBoxColumn5, Me.Status, Me.DataGridViewButtonColumn1, Me.DataGridViewButtonColumn2})
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DGV_Common.DefaultCellStyle = DataGridViewCellStyle9
        Me.DGV_Common.EnableHeadersVisualStyles = False
        Me.DGV_Common.Location = New System.Drawing.Point(191, 41)
        Me.DGV_Common.Name = "DGV_Common"
        Me.DGV_Common.ReadOnly = True
        Me.DGV_Common.RowHeadersVisible = False
        Me.DGV_Common.Size = New System.Drawing.Size(513, 251)
        Me.DGV_Common.TabIndex = 2
        '
        'Source
        '
        Me.Source.DataPropertyName = "plant"
        Me.Source.HeaderText = "Source"
        Me.Source.Name = "Source"
        Me.Source.ReadOnly = True
        Me.Source.Width = 60
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "LOTCODE"
        Me.DataGridViewTextBoxColumn1.HeaderText = "LOT"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 50
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "sequence"
        Me.DataGridViewTextBoxColumn4.HeaderText = "SEQ"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 50
        '
        'Cmmon_family_code
        '
        Me.Cmmon_family_code.DataPropertyName = "family_code"
        Me.Cmmon_family_code.HeaderText = "Family"
        Me.Cmmon_family_code.Name = "Cmmon_family_code"
        Me.Cmmon_family_code.ReadOnly = True
        Me.Cmmon_family_code.Width = 80
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Eventdate"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Time"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 140
        '
        'Status
        '
        Me.Status.DataPropertyName = "Status"
        Me.Status.HeaderText = "Status"
        Me.Status.Name = "Status"
        Me.Status.ReadOnly = True
        '
        'DataGridViewButtonColumn1
        '
        Me.DataGridViewButtonColumn1.DataPropertyName = "insert"
        Me.DataGridViewButtonColumn1.HeaderText = "Insert"
        Me.DataGridViewButtonColumn1.Name = "DataGridViewButtonColumn1"
        Me.DataGridViewButtonColumn1.ReadOnly = True
        Me.DataGridViewButtonColumn1.Visible = False
        Me.DataGridViewButtonColumn1.Width = 50
        '
        'DataGridViewButtonColumn2
        '
        Me.DataGridViewButtonColumn2.DataPropertyName = "Del"
        Me.DataGridViewButtonColumn2.HeaderText = "DEL"
        Me.DataGridViewButtonColumn2.Name = "DataGridViewButtonColumn2"
        Me.DataGridViewButtonColumn2.ReadOnly = True
        Me.DataGridViewButtonColumn2.Visible = False
        Me.DataGridViewButtonColumn2.Width = 50
        '
        'btnRatiosetE
        '
        Me.btnRatiosetE.BackColor = System.Drawing.Color.Gray
        Me.btnRatiosetE.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRatiosetE.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnRatiosetE.ForeColor = System.Drawing.Color.White
        Me.btnRatiosetE.Location = New System.Drawing.Point(534, 450)
        Me.btnRatiosetE.Name = "btnRatiosetE"
        Me.btnRatiosetE.Size = New System.Drawing.Size(44, 25)
        Me.btnRatiosetE.TabIndex = 3
        Me.btnRatiosetE.Text = "Set"
        Me.btnRatiosetE.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.Location = New System.Drawing.Point(535, 394)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(43, 16)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Ratio:"
        '
        'txtRatioE
        '
        Me.txtRatioE.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtRatioE.Location = New System.Drawing.Point(534, 413)
        Me.txtRatioE.Name = "txtRatioE"
        Me.txtRatioE.ReadOnly = True
        Me.txtRatioE.Size = New System.Drawing.Size(44, 31)
        Me.txtRatioE.TabIndex = 5
        Me.txtRatioE.Text = "5"
        Me.txtRatioE.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtExportLOT
        '
        Me.txtExportLOT.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtExportLOT.Location = New System.Drawing.Point(752, 642)
        Me.txtExportLOT.Name = "txtExportLOT"
        Me.txtExportLOT.Size = New System.Drawing.Size(73, 31)
        Me.txtExportLOT.TabIndex = 6
        Me.txtExportLOT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnExportInsert
        '
        Me.btnExportInsert.BackColor = System.Drawing.Color.Gray
        Me.btnExportInsert.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExportInsert.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnExportInsert.ForeColor = System.Drawing.Color.White
        Me.btnExportInsert.Location = New System.Drawing.Point(940, 642)
        Me.btnExportInsert.Name = "btnExportInsert"
        Me.btnExportInsert.Size = New System.Drawing.Size(68, 30)
        Me.btnExportInsert.TabIndex = 7
        Me.btnExportInsert.Text = "Insert"
        Me.btnExportInsert.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.Location = New System.Drawing.Point(703, 651)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(37, 16)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "LOT:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label3.Location = New System.Drawing.Point(831, 651)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(39, 16)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "QTY:"
        '
        'txtexportQTY
        '
        Me.txtexportQTY.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtexportQTY.Location = New System.Drawing.Point(876, 642)
        Me.txtexportQTY.Name = "txtexportQTY"
        Me.txtexportQTY.Size = New System.Drawing.Size(58, 31)
        Me.txtexportQTY.TabIndex = 9
        Me.txtexportQTY.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtRatioP1
        '
        Me.txtRatioP1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtRatioP1.Location = New System.Drawing.Point(346, 413)
        Me.txtRatioP1.Name = "txtRatioP1"
        Me.txtRatioP1.ReadOnly = True
        Me.txtRatioP1.Size = New System.Drawing.Size(44, 31)
        Me.txtRatioP1.TabIndex = 13
        Me.txtRatioP1.Text = "5"
        Me.txtRatioP1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label4.Location = New System.Drawing.Point(347, 394)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(43, 16)
        Me.Label4.TabIndex = 12
        Me.Label4.Text = "Ratio:"
        '
        'btnRatiosetP1
        '
        Me.btnRatiosetP1.BackColor = System.Drawing.Color.Gray
        Me.btnRatiosetP1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRatiosetP1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnRatiosetP1.ForeColor = System.Drawing.Color.White
        Me.btnRatiosetP1.Location = New System.Drawing.Point(346, 450)
        Me.btnRatiosetP1.Name = "btnRatiosetP1"
        Me.btnRatiosetP1.Size = New System.Drawing.Size(44, 25)
        Me.btnRatiosetP1.TabIndex = 11
        Me.btnRatiosetP1.Text = "Set"
        Me.btnRatiosetP1.UseVisualStyleBackColor = False
        '
        'lblRecord_E
        '
        Me.lblRecord_E.AutoSize = True
        Me.lblRecord_E.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblRecord_E.Location = New System.Drawing.Point(855, 375)
        Me.lblRecord_E.Name = "lblRecord_E"
        Me.lblRecord_E.Size = New System.Drawing.Size(56, 16)
        Me.lblRecord_E.TabIndex = 14
        Me.lblRecord_E.Text = "Record:"
        '
        'lblRecord_P1
        '
        Me.lblRecord_P1.AutoSize = True
        Me.lblRecord_P1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblRecord_P1.Location = New System.Drawing.Point(208, 375)
        Me.lblRecord_P1.Name = "lblRecord_P1"
        Me.lblRecord_P1.Size = New System.Drawing.Size(56, 16)
        Me.lblRecord_P1.TabIndex = 15
        Me.lblRecord_P1.Text = "Record:"
        '
        'lblRecord_Common
        '
        Me.lblRecord_Common.AutoSize = True
        Me.lblRecord_Common.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblRecord_Common.Location = New System.Drawing.Point(262, 19)
        Me.lblRecord_Common.Name = "lblRecord_Common"
        Me.lblRecord_Common.Size = New System.Drawing.Size(56, 16)
        Me.lblRecord_Common.TabIndex = 16
        Me.lblRecord_Common.Text = "Record:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(19, 361)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(86, 30)
        Me.Label5.TabIndex = 17
        Me.Label5.Text = "Maru A"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(635, 361)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(79, 30)
        Me.Label6.TabIndex = 18
        Me.Label6.Text = "Export"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(205, 8)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(51, 30)
        Me.Label7.TabIndex = 19
        Me.Label7.Text = "Mix"
        '
        'PB_common
        '
        Me.PB_common.BackColor = System.Drawing.Color.Transparent
        Me.PB_common.Image = Global.MixMonitor.My.Resources.Resources.arrow_96_128__2_
        Me.PB_common.Location = New System.Drawing.Point(421, 296)
        Me.PB_common.Name = "PB_common"
        Me.PB_common.Size = New System.Drawing.Size(44, 47)
        Me.PB_common.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PB_common.TabIndex = 22
        Me.PB_common.TabStop = False
        '
        'PB_P1
        '
        Me.PB_P1.BackColor = System.Drawing.Color.Transparent
        Me.PB_P1.Image = Global.MixMonitor.My.Resources.Resources.arrow_96_64
        Me.PB_P1.Location = New System.Drawing.Point(346, 341)
        Me.PB_P1.Name = "PB_P1"
        Me.PB_P1.Size = New System.Drawing.Size(44, 47)
        Me.PB_P1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PB_P1.TabIndex = 21
        Me.PB_P1.TabStop = False
        '
        'PB_E
        '
        Me.PB_E.BackColor = System.Drawing.Color.Transparent
        Me.PB_E.Image = Global.MixMonitor.My.Resources.Resources.arrow_96_128__1_
        Me.PB_E.Location = New System.Drawing.Point(584, 341)
        Me.PB_E.Name = "PB_E"
        Me.PB_E.Size = New System.Drawing.Size(44, 47)
        Me.PB_E.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PB_E.TabIndex = 20
        Me.PB_E.TabStop = False
        '
        'PBmask_Common
        '
        Me.PBmask_Common.BackColor = System.Drawing.Color.Transparent
        Me.PBmask_Common.Image = CType(resources.GetObject("PBmask_Common.Image"), System.Drawing.Image)
        Me.PBmask_Common.Location = New System.Drawing.Point(471, 296)
        Me.PBmask_Common.Name = "PBmask_Common"
        Me.PBmask_Common.Size = New System.Drawing.Size(44, 47)
        Me.PBmask_Common.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PBmask_Common.TabIndex = 23
        Me.PBmask_Common.TabStop = False
        '
        'PBmask_P1
        '
        Me.PBmask_P1.BackColor = System.Drawing.Color.Transparent
        Me.PBmask_P1.Image = CType(resources.GetObject("PBmask_P1.Image"), System.Drawing.Image)
        Me.PBmask_P1.Location = New System.Drawing.Point(357, 341)
        Me.PBmask_P1.Name = "PBmask_P1"
        Me.PBmask_P1.Size = New System.Drawing.Size(44, 47)
        Me.PBmask_P1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PBmask_P1.TabIndex = 24
        Me.PBmask_P1.TabStop = False
        '
        'PBmask_E
        '
        Me.PBmask_E.BackColor = System.Drawing.Color.Transparent
        Me.PBmask_E.Image = CType(resources.GetObject("PBmask_E.Image"), System.Drawing.Image)
        Me.PBmask_E.Location = New System.Drawing.Point(534, 341)
        Me.PBmask_E.Name = "PBmask_E"
        Me.PBmask_E.Size = New System.Drawing.Size(44, 47)
        Me.PBmask_E.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PBmask_E.TabIndex = 25
        Me.PBmask_E.TabStop = False
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 800
        '
        'toolbar
        '
        Me.toolbar.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblStatus})
        Me.toolbar.Location = New System.Drawing.Point(0, 707)
        Me.toolbar.Name = "toolbar"
        Me.toolbar.Size = New System.Drawing.Size(1028, 22)
        Me.toolbar.TabIndex = 26
        Me.toolbar.Text = "StatusStrip1"
        '
        'lblStatus
        '
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(42, 17)
        Me.lblStatus.Text = "Status:"
        '
        'ServerPN
        '
        Me.ServerPN.BackColor = System.Drawing.Color.White
        Me.ServerPN.Controls.Add(Me.Label75)
        Me.ServerPN.Controls.Add(Me.ServerPingPN)
        Me.ServerPN.Controls.Add(Me.lblServer1)
        Me.ServerPN.Controls.Add(Me.ServerPingPN2)
        Me.ServerPN.Controls.Add(Me.lblServer2)
        Me.ServerPN.Controls.Add(Me.Label74)
        Me.ServerPN.Controls.Add(Me.ServerLinkPN)
        Me.ServerPN.Controls.Add(Me.ServerLinkPN2)
        Me.ServerPN.Location = New System.Drawing.Point(3, 7)
        Me.ServerPN.Name = "ServerPN"
        Me.ServerPN.Size = New System.Drawing.Size(130, 88)
        Me.ServerPN.TabIndex = 225
        '
        'Label75
        '
        Me.Label75.AutoSize = True
        Me.Label75.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label75.ForeColor = System.Drawing.Color.DodgerBlue
        Me.Label75.Location = New System.Drawing.Point(9, 6)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(51, 13)
        Me.Label75.TabIndex = 205
        Me.Label75.Text = "SERVER"
        '
        'ServerPingPN
        '
        Me.ServerPingPN.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.ServerPingPN.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.ServerPingPN.Location = New System.Drawing.Point(74, 4)
        Me.ServerPingPN.Name = "ServerPingPN"
        Me.ServerPingPN.Size = New System.Drawing.Size(20, 18)
        Me.ServerPingPN.TabIndex = 202
        '
        'lblServer1
        '
        Me.lblServer1.AutoSize = True
        Me.lblServer1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblServer1.ForeColor = System.Drawing.Color.DodgerBlue
        Me.lblServer1.Location = New System.Drawing.Point(9, 25)
        Me.lblServer1.Name = "lblServer1"
        Me.lblServer1.Size = New System.Drawing.Size(68, 13)
        Me.lblServer1.TabIndex = 204
        Me.lblServer1.Text = "STMTGW01"
        '
        'ServerPingPN2
        '
        Me.ServerPingPN2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.ServerPingPN2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.ServerPingPN2.Location = New System.Drawing.Point(72, 53)
        Me.ServerPingPN2.Name = "ServerPingPN2"
        Me.ServerPingPN2.Size = New System.Drawing.Size(20, 18)
        Me.ServerPingPN2.TabIndex = 206
        '
        'lblServer2
        '
        Me.lblServer2.AutoSize = True
        Me.lblServer2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblServer2.ForeColor = System.Drawing.Color.DodgerBlue
        Me.lblServer2.Location = New System.Drawing.Point(9, 72)
        Me.lblServer2.Name = "lblServer2"
        Me.lblServer2.Size = New System.Drawing.Size(68, 13)
        Me.lblServer2.TabIndex = 207
        Me.lblServer2.Text = "STMTGW01"
        '
        'Label74
        '
        Me.Label74.AutoSize = True
        Me.Label74.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label74.ForeColor = System.Drawing.Color.DodgerBlue
        Me.Label74.Location = New System.Drawing.Point(9, 53)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(50, 13)
        Me.Label74.TabIndex = 208
        Me.Label74.Text = "BACKUP"
        '
        'ServerLinkPN
        '
        Me.ServerLinkPN.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.ServerLinkPN.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.ServerLinkPN.Location = New System.Drawing.Point(95, 4)
        Me.ServerLinkPN.Name = "ServerLinkPN"
        Me.ServerLinkPN.Size = New System.Drawing.Size(20, 18)
        Me.ServerLinkPN.TabIndex = 203
        '
        'ServerLinkPN2
        '
        Me.ServerLinkPN2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.ServerLinkPN2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.ServerLinkPN2.Location = New System.Drawing.Point(93, 53)
        Me.ServerLinkPN2.Name = "ServerLinkPN2"
        Me.ServerLinkPN2.Size = New System.Drawing.Size(20, 18)
        Me.ServerLinkPN2.TabIndex = 209
        '
        'TimerServerLink
        '
        Me.TimerServerLink.Enabled = True
        Me.TimerServerLink.Interval = 1000
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.LightSkyBlue
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.ServerPN)
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(146, 112)
        Me.Panel1.TabIndex = 226
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(435, 8)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(162, 25)
        Me.Label8.TabIndex = 227
        Me.Label8.Text = "Logistic Process :"
        '
        'lblLogistic
        '
        Me.lblLogistic.AutoSize = True
        Me.lblLogistic.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLogistic.Location = New System.Drawing.Point(603, 8)
        Me.lblLogistic.Name = "lblLogistic"
        Me.lblLogistic.Size = New System.Drawing.Size(53, 25)
        Me.lblLogistic.TabIndex = 228
        Me.lblLogistic.Text = "FR_0"
        '
        'btnSetLogistic
        '
        Me.btnSetLogistic.BackColor = System.Drawing.Color.Gray
        Me.btnSetLogistic.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSetLogistic.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnSetLogistic.ForeColor = System.Drawing.Color.White
        Me.btnSetLogistic.Location = New System.Drawing.Point(662, 6)
        Me.btnSetLogistic.Name = "btnSetLogistic"
        Me.btnSetLogistic.Size = New System.Drawing.Size(44, 25)
        Me.btnSetLogistic.TabIndex = 229
        Me.btnSetLogistic.Text = "Set"
        Me.btnSetLogistic.UseVisualStyleBackColor = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label9.Location = New System.Drawing.Point(784, 332)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(41, 16)
        Me.Label9.TabIndex = 256
        Me.Label9.Text = "MIX :"
        '
        'BtnStartMoveQ
        '
        Me.BtnStartMoveQ.BackColor = System.Drawing.Color.Red
        Me.BtnStartMoveQ.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnStartMoveQ.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnStartMoveQ.Location = New System.Drawing.Point(828, 318)
        Me.BtnStartMoveQ.Margin = New System.Windows.Forms.Padding(2)
        Me.BtnStartMoveQ.Name = "BtnStartMoveQ"
        Me.BtnStartMoveQ.Size = New System.Drawing.Size(116, 41)
        Me.BtnStartMoveQ.TabIndex = 255
        Me.BtnStartMoveQ.Text = "START"
        Me.BtnStartMoveQ.UseVisualStyleBackColor = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(712, 4)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(60, 21)
        Me.Label10.TabIndex = 257
        Me.Label10.Text = "Export"
        '
        'Timer2
        '
        Me.Timer2.Interval = 5000
        '
        'DGV_E_SHOW
        '
        Me.DGV_E_SHOW.AllowUserToAddRows = False
        Me.DGV_E_SHOW.AllowUserToDeleteRows = False
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.WhiteSmoke
        Me.DGV_E_SHOW.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle10
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.DimGray
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGV_E_SHOW.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle11
        Me.DGV_E_SHOW.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_E_SHOW.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.BCDATA_SHOW, Me.export_SEQ_show, Me.LOTCODE_SHOW, Me.QTY_SHOW, Me.MIX_SHOW})
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.Color.Fuchsia
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DGV_E_SHOW.DefaultCellStyle = DataGridViewCellStyle12
        Me.DGV_E_SHOW.EnableHeadersVisualStyles = False
        Me.DGV_E_SHOW.Location = New System.Drawing.Point(710, 61)
        Me.DGV_E_SHOW.Name = "DGV_E_SHOW"
        Me.DGV_E_SHOW.ReadOnly = True
        Me.DGV_E_SHOW.RowHeadersVisible = False
        Me.DGV_E_SHOW.Size = New System.Drawing.Size(318, 231)
        Me.DGV_E_SHOW.TabIndex = 258
        '
        'BCDATA_SHOW
        '
        Me.BCDATA_SHOW.DataPropertyName = "BCDATA"
        Me.BCDATA_SHOW.HeaderText = "BCDATA"
        Me.BCDATA_SHOW.Name = "BCDATA_SHOW"
        Me.BCDATA_SHOW.ReadOnly = True
        '
        'export_SEQ_show
        '
        Me.export_SEQ_show.DataPropertyName = "export_SEQ"
        Me.export_SEQ_show.HeaderText = "Export SEQ"
        Me.export_SEQ_show.Name = "export_SEQ_show"
        Me.export_SEQ_show.ReadOnly = True
        Me.export_SEQ_show.Width = 60
        '
        'LOTCODE_SHOW
        '
        Me.LOTCODE_SHOW.DataPropertyName = "LOTCODE"
        Me.LOTCODE_SHOW.HeaderText = "LOT CODE"
        Me.LOTCODE_SHOW.Name = "LOTCODE_SHOW"
        Me.LOTCODE_SHOW.ReadOnly = True
        Me.LOTCODE_SHOW.Width = 60
        '
        'QTY_SHOW
        '
        Me.QTY_SHOW.DataPropertyName = "QTY"
        Me.QTY_SHOW.HeaderText = "QTY"
        Me.QTY_SHOW.Name = "QTY_SHOW"
        Me.QTY_SHOW.ReadOnly = True
        Me.QTY_SHOW.Width = 40
        '
        'MIX_SHOW
        '
        Me.MIX_SHOW.DataPropertyName = "MIX"
        Me.MIX_SHOW.HeaderText = "MIX"
        Me.MIX_SHOW.Name = "MIX_SHOW"
        Me.MIX_SHOW.ReadOnly = True
        Me.MIX_SHOW.Width = 40
        '
        'txtExport_DATA
        '
        Me.txtExport_DATA.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtExport_DATA.Location = New System.Drawing.Point(712, 28)
        Me.txtExport_DATA.Multiline = True
        Me.txtExport_DATA.Name = "txtExport_DATA"
        Me.txtExport_DATA.Size = New System.Drawing.Size(295, 27)
        Me.txtExport_DATA.TabIndex = 259
        '
        'btnEdit
        '
        Me.btnEdit.BackColor = System.Drawing.Color.Gray
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.White
        Me.btnEdit.Location = New System.Drawing.Point(584, 642)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(68, 44)
        Me.btnEdit.TabIndex = 260
        Me.btnEdit.Text = "Delete Mode"
        Me.btnEdit.UseVisualStyleBackColor = False
        '
        'btnExFinish
        '
        Me.btnExFinish.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnExFinish.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExFinish.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnExFinish.ForeColor = System.Drawing.Color.White
        Me.btnExFinish.Location = New System.Drawing.Point(634, 642)
        Me.btnExFinish.Name = "btnExFinish"
        Me.btnExFinish.Size = New System.Drawing.Size(68, 30)
        Me.btnExFinish.TabIndex = 261
        Me.btnExFinish.Text = "Finish"
        Me.btnExFinish.UseVisualStyleBackColor = False
        Me.btnExFinish.Visible = False
        '
        'btnEx_del
        '
        Me.btnEx_del.BackColor = System.Drawing.Color.Red
        Me.btnEx_del.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEx_del.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnEx_del.ForeColor = System.Drawing.Color.White
        Me.btnEx_del.Location = New System.Drawing.Point(560, 642)
        Me.btnEx_del.Name = "btnEx_del"
        Me.btnEx_del.Size = New System.Drawing.Size(68, 30)
        Me.btnEx_del.TabIndex = 262
        Me.btnEx_del.Text = "Delete"
        Me.btnEx_del.UseVisualStyleBackColor = False
        Me.btnEx_del.Visible = False
        '
        'TimerDelete
        '
        Me.TimerDelete.Interval = 60000
        '
        'DGVOT
        '
        Me.DGVOT.AllowUserToAddRows = False
        Me.DGVOT.AllowUserToDeleteRows = False
        DataGridViewCellStyle13.BackColor = System.Drawing.Color.WhiteSmoke
        Me.DGVOT.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle13
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle14.BackColor = System.Drawing.Color.DimGray
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGVOT.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle14
        Me.DGVOT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVOT.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.SelectOT, Me.DataGridViewTextBoxColumn2, Me.C_Stopmix, Me.OTid})
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle15.ForeColor = System.Drawing.Color.Crimson
        DataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DGVOT.DefaultCellStyle = DataGridViewCellStyle15
        Me.DGVOT.EnableHeadersVisualStyles = False
        Me.DGVOT.Location = New System.Drawing.Point(346, 481)
        Me.DGVOT.Name = "DGVOT"
        Me.DGVOT.RowHeadersVisible = False
        Me.DGVOT.Size = New System.Drawing.Size(232, 150)
        Me.DGVOT.TabIndex = 263
        '
        'SelectOT
        '
        Me.SelectOT.DataPropertyName = "SelectOT"
        Me.SelectOT.HeaderText = ""
        Me.SelectOT.Name = "SelectOT"
        Me.SelectOT.Width = 30
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "OTName"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Name"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Width = 80
        '
        'C_Stopmix
        '
        Me.C_Stopmix.DataPropertyName = "StopMixTime"
        Me.C_Stopmix.HeaderText = "Stopmix"
        Me.C_Stopmix.Name = "C_Stopmix"
        '
        'OTid
        '
        Me.OTid.DataPropertyName = "OTid"
        Me.OTid.HeaderText = "OTid"
        Me.OTid.Name = "OTid"
        Me.OTid.Visible = False
        '
        'btnOTsetting
        '
        Me.btnOTsetting.BackColor = System.Drawing.Color.Gray
        Me.btnOTsetting.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOTsetting.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnOTsetting.ForeColor = System.Drawing.Color.White
        Me.btnOTsetting.Location = New System.Drawing.Point(346, 631)
        Me.btnOTsetting.Name = "btnOTsetting"
        Me.btnOTsetting.Size = New System.Drawing.Size(68, 30)
        Me.btnOTsetting.TabIndex = 264
        Me.btnOTsetting.Text = "Setting"
        Me.btnOTsetting.UseVisualStyleBackColor = False
        '
        'TimerStopMix
        '
        Me.TimerStopMix.Enabled = True
        Me.TimerStopMix.Interval = 10000
        '
        'MixMonitorFrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1028, 729)
        Me.Controls.Add(Me.btnOTsetting)
        Me.Controls.Add(Me.DGVOT)
        Me.Controls.Add(Me.btnExFinish)
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.txtExport_DATA)
        Me.Controls.Add(Me.DGV_E_SHOW)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.BtnStartMoveQ)
        Me.Controls.Add(Me.btnSetLogistic)
        Me.Controls.Add(Me.lblLogistic)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.toolbar)
        Me.Controls.Add(Me.PB_E)
        Me.Controls.Add(Me.PBmask_E)
        Me.Controls.Add(Me.PB_common)
        Me.Controls.Add(Me.PB_P1)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lblRecord_Common)
        Me.Controls.Add(Me.lblRecord_P1)
        Me.Controls.Add(Me.lblRecord_E)
        Me.Controls.Add(Me.txtRatioP1)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.btnRatiosetP1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtexportQTY)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnExportInsert)
        Me.Controls.Add(Me.txtExportLOT)
        Me.Controls.Add(Me.txtRatioE)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnRatiosetE)
        Me.Controls.Add(Me.DGV_Common)
        Me.Controls.Add(Me.DGV_E)
        Me.Controls.Add(Me.DGV_P1)
        Me.Controls.Add(Me.PBmask_Common)
        Me.Controls.Add(Me.PBmask_P1)
        Me.Controls.Add(Me.btnEx_del)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "MixMonitorFrm"
        Me.Text = "Sequence Monitor"
        CType(Me.DGV_P1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGV_E, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGV_Common, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PB_common, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PB_P1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PB_E, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PBmask_Common, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PBmask_P1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PBmask_E, System.ComponentModel.ISupportInitialize).EndInit()
        Me.toolbar.ResumeLayout(False)
        Me.toolbar.PerformLayout()
        Me.ServerPN.ResumeLayout(False)
        Me.ServerPN.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        CType(Me.DGV_E_SHOW, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGVOT, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DGV_P1 As System.Windows.Forms.DataGridView
    Friend WithEvents DGV_E As System.Windows.Forms.DataGridView
    Friend WithEvents DGV_Common As System.Windows.Forms.DataGridView
    Friend WithEvents btnRatiosetE As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtRatioE As System.Windows.Forms.TextBox
    Friend WithEvents txtExportLOT As System.Windows.Forms.TextBox
    Friend WithEvents btnExportInsert As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtexportQTY As System.Windows.Forms.TextBox
    Friend WithEvents txtRatioP1 As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnRatiosetP1 As System.Windows.Forms.Button
    Friend WithEvents lblRecord_E As System.Windows.Forms.Label
    Friend WithEvents lblRecord_P1 As System.Windows.Forms.Label
    Friend WithEvents lblRecord_Common As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents PB_E As System.Windows.Forms.PictureBox
    Friend WithEvents PB_P1 As System.Windows.Forms.PictureBox
    Friend WithEvents PB_common As System.Windows.Forms.PictureBox
    Friend WithEvents PBmask_Common As System.Windows.Forms.PictureBox
    Friend WithEvents PBmask_P1 As System.Windows.Forms.PictureBox
    Friend WithEvents PBmask_E As System.Windows.Forms.PictureBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents toolbar As System.Windows.Forms.StatusStrip
    Friend WithEvents lblStatus As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ServerPN As Panel
    Friend WithEvents Label75 As Label
    Friend WithEvents ServerPingPN As Panel
    Public WithEvents lblServer1 As Label
    Friend WithEvents ServerPingPN2 As Panel
    Public WithEvents lblServer2 As Label
    Friend WithEvents Label74 As Label
    Friend WithEvents ServerLinkPN As Panel
    Friend WithEvents ServerLinkPN2 As Panel
    Friend WithEvents TimerServerLink As Timer
    Friend WithEvents P1_LOT As DataGridViewTextBoxColumn
    Friend WithEvents P1_SEQ As DataGridViewTextBoxColumn
    Friend WithEvents family_code As DataGridViewTextBoxColumn
    Friend WithEvents P1_Time As DataGridViewTextBoxColumn
    Friend WithEvents Source As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As DataGridViewTextBoxColumn
    Friend WithEvents Cmmon_family_code As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As DataGridViewTextBoxColumn
    Friend WithEvents Status As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewButtonColumn1 As DataGridViewButtonColumn
    Friend WithEvents DataGridViewButtonColumn2 As DataGridViewButtonColumn
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label8 As Label
    Friend WithEvents lblLogistic As Label
    Friend WithEvents btnSetLogistic As Button
    Friend WithEvents Label9 As Label
    Friend WithEvents BtnStartMoveQ As Button
    Friend WithEvents Label10 As Label
    Friend WithEvents txtExportQTY_show As TextBox
    Friend WithEvents Timer2 As Timer
    Friend WithEvents DGV_E_SHOW As DataGridView
    Friend WithEvents txtExport_DATA As TextBox
    Friend WithEvents BCDATA_SHOW As DataGridViewTextBoxColumn
    Friend WithEvents export_SEQ_show As DataGridViewTextBoxColumn
    Friend WithEvents LOTCODE_SHOW As DataGridViewTextBoxColumn
    Friend WithEvents QTY_SHOW As DataGridViewTextBoxColumn
    Friend WithEvents MIX_SHOW As DataGridViewTextBoxColumn
    Friend WithEvents btnEdit As Button
    Friend WithEvents ExportSelect As DataGridViewCheckBoxColumn
    Friend WithEvents E_LOT As DataGridViewTextBoxColumn
    Friend WithEvents E_SEQ As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents Export_seq As DataGridViewTextBoxColumn
    Friend WithEvents E_insert As DataGridViewButtonColumn
    Friend WithEvents E_DELETE As DataGridViewButtonColumn
    Friend WithEvents E_id As DataGridViewTextBoxColumn
    Friend WithEvents EX_BCDATA As DataGridViewTextBoxColumn
    Friend WithEvents btnExFinish As Button
    Friend WithEvents btnEx_del As Button
    Friend WithEvents TimerDelete As Timer
    Friend WithEvents DGVOT As DataGridView
    Friend WithEvents btnOTsetting As Button
    Friend WithEvents SelectOT As DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents C_Stopmix As DataGridViewTextBoxColumn
    Friend WithEvents OTid As DataGridViewTextBoxColumn
    Friend WithEvents TimerStopMix As Timer
End Class
