﻿Imports System.Threading
Imports System.IO
Public Class MixMonitorFrm


    Dim DAL As New DB.SqlConnect
    Dim reloadDGV As New ReloadDGV

    Dim timer_getdata As Threading.Timer
    Dim timer_getExport As Threading.Timer
    Dim Pitch As String

    Dim DTQ_P1, DTQ_E, DTQ_Common As New DataTable


    Dim StopLoadExport As Boolean = False
    Dim MoveQ As Boolean

    Private Sub MixMonitorFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        loadSERVERSQL()

        DisplayArrow()

        DGV_Common.AutoGenerateColumns = False
        System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = False
        SetDoubleBuffered(DGV_Common)

        DGV_E.AutoGenerateColumns = False

        SetDoubleBuffered(DGV_E)

        DGV_P1.AutoGenerateColumns = False

        SetDoubleBuffered(DGV_P1)


        DGV_E_SHOW.AutoGenerateColumns = False
        SetDoubleBuffered(DGV_E_SHOW)

        DGVOT.AutoGenerateColumns = False
        loadShift()
        getShift()


        Pitch = initialData("PROCESS").ToString
        lblLogistic.Text = initialData("LOGISTIC").ToString
        Dim TDS5 As TimerCallback = AddressOf runPData
        timer_getdata = New Threading.Timer(TDS5, Nothing, 1000, 1000)


        Dim TDS_Export As TimerCallback = AddressOf getExportQTY
        timer_getExport = New Threading.Timer(TDS_Export, Nothing, 1000, 1000)


        timer_serverstate = New Threading.Timer(TDS_Serverlink, Nothing, 0, 3)
    End Sub

    Sub SetDoubleBuffered(ByVal control As Control)
        ' set instance non-public property with name "DoubleBuffered" to true 
        GetType(Control).InvokeMember("DoubleBuffered", System.Reflection.BindingFlags.SetProperty Or System.Reflection.BindingFlags.Instance Or System.Reflection.BindingFlags.NonPublic, Nothing, control, New Object() {True})
    End Sub

#Region "Arrow"


    Sub DisplayArrow()
        PB_common.Image.RotateFlip(RotateFlipType.Rotate90FlipX)
        PB_P1.Image.RotateFlip(RotateFlipType.Rotate180FlipY)
        PBmask_Common.Image.RotateFlip(RotateFlipType.Rotate90FlipX)
        PBmask_P1.Image.RotateFlip(RotateFlipType.Rotate180FlipY)

        PB_P1.Top = DGV_P1.Top - 40 - (PB_common.Width \ 2)
        PB_E.Top = DGV_P1.Top - 40 - (PB_common.Width \ 2)
        PB_common.Left = (DGV_Common.Left + (DGV_Common.Width \ 2)) - (PB_common.Width \ 2)

        PBmask_Common.Location = PB_common.Location
        PBmask_E.Location = PB_E.Location
        PBmask_P1.Location = PB_P1.Location
    End Sub

    Private Sub MixMonitorFrm_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Me.Paint
        Dim pen1 As New Pen(Color.DimGray, 3)



        e.Graphics.DrawLine(pen1, DGV_P1.Left + (DGV_P1.Width \ 2), DGV_P1.Top - 40, DGV_E.Left + (DGV_E.Width \ 2), DGV_P1.Top - 40)
        e.Graphics.DrawLine(pen1, DGV_P1.Left + (DGV_P1.Width \ 2), DGV_P1.Top - 40, DGV_P1.Left + (DGV_P1.Width \ 2), DGV_P1.Top)
        e.Graphics.DrawLine(pen1, DGV_E.Left + (DGV_E.Width \ 2), DGV_E.Top, DGV_E.Left + (DGV_E.Width \ 2), DGV_P1.Top - 40)
        e.Graphics.DrawLine(pen1, DGV_Common.Left + (DGV_Common.Width \ 2), DGV_Common.Top + DGV_Common.Height, DGV_Common.Left + (DGV_Common.Width \ 2), DGV_P1.Top - 40)
    End Sub
#End Region

#Region "Timer"


    Dim Status_Color As Color
    Dim Status_setlbl As String
    Dim Status_time As Integer
    Sub ShowStatus(ByVal str As String, ByVal time As Integer, Optional ByVal sColor As Color = Nothing)
        Status_setlbl = str
        Status_time = time

        Status_Color = sColor
    End Sub
    Private Sub Timer2_Tick(sender As Object, e As EventArgs) Handles Timer2.Tick
        '  getExportQTY()
    End Sub






    'Dim TDS1 As TimerCallback = AddressOf GetMSG
    'Dim timer_getMSG As New Threading.Timer(TDS1, Nothing, 0, 1000)

    Dim Dayfrom, DayTo, NightFrom, Nightto As String


    Sub getExportQTY()

        Try
            timer_getExport.Change(Timeout.Infinite, Timeout.Infinite)

            Dim str As String

            Dim whereTime As String
            If Now.ToString("HH:mm") >= NightFrom Then
                whereTime = " EventDate between '" & Now.ToString("yyyy-MM-dd ") & NightFrom & ":00' and '" & Now.AddDays(1).ToString("yyyy-MM-dd ") & Nightto & ":00'"
                Str = "Shift Night " & Now.ToString("dd/MM/yyyy")

            ElseIf Now.ToString("HH:mm") <= Nightto Then

                whereTime = " EventDate between '" & Now.AddDays(-1).ToString("yyyy-MM-dd ") & NightFrom & ":00' and '" & Now.ToString("yyyy-MM-dd ") & Nightto & ":00'"
                Str = "Shift Night " & Now.AddDays(-1).ToString("dd/MM/yyyy")
            Else
                whereTime = " EventDate between '" & Now.ToString("yyyy-MM-dd ") & Dayfrom & ":00' and '" & Now.ToString("yyyy-MM-dd ") & DayTo & ":00'"
                Str = "Shift Day " & Now.ToString("dd/MM/yyyy")
            End If

            Dim sql As String


            sql = "select  BCdata,export_SEQ ,LOTCODE ,count(id) as QTY,sum(commonplant) as mix "
            '  sql = "     ROW_NUMBER() OVER (ORDER BY  min(id)) AS RowNumber  "

            sql +=" from tb_bc_data_ex "
            sql += " where " & whereTime
            sql += " Group by  BCdata,export_SEQ,LOTCODE"
            sql += " order by min(id) "


            Dim E_PLAN, E_MIX As Integer
            Dim dt As New DataTable
            dt = DAL.getDataTable(sql, Connstr)



            E_PLAN = 0
            E_MIX = 0
            For Each dr As DataRow In dt.Rows
                If IsNumeric(dr("QTY").ToString) Then
                    E_PLAN += dr("QTY")
                End If
                If IsNumeric(dr("MIX").ToString) Then
                    E_MIX += dr("MIX")
                End If
            Next


            Me.BeginInvoke(New MethodInvoker(Sub()
                                                 DGV_E_SHOW.DataSource = dt
                                                 ' DGV_E_SHOW.AutoResizeColumns()
                                                 txtExport_DATA.Text = str & " QTY:" & E_PLAN & " MIX:" & E_MIX
                                             End Sub))


        Catch ex As Exception
            ShowStatus("getExportdata:" & ex.Message, 5, Color.Red)
            SaveMSG("getExportQTY:" & ex.GetBaseException.ToString, "ERR")
        Finally
            timer_getExport.Change(10000, 10000)
        End Try

    End Sub
    Dim Flage As Boolean
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Flage = Not Flage
        PB_common.Visible = Flage
        PB_P1.Visible = Flage
        PB_E.Visible = Flage

        If Status_time > 0 Then
            lblStatus.Text = Status_setlbl
            Status_time -= 1
            If Status_Color <> Nothing Then
                toolbar.BackColor = Status_Color
            End If

        Else
            lblStatus.Text = ""
            lblStatus.BackColor = Color.Transparent
        End If

        lblRecord_Common.Text = "Record:" & DTQ_Common.Rows.Count
        lblRecord_E.Text = "Record:" & DTQ_E.Rows.Count
        lblRecord_P1.Text = "Record:" & DTQ_P1.Rows.Count
        getratio()
    End Sub
#End Region
#Region "Ratio"

    Sub getratio()
        Try


            Dim sql As String
            sql = "select id,ratio from tb_plantUC"
            Dim dt As New DataTable
            dt = DAL.getDataTable(sql, Connstr)
            For Each dr As DataRow In dt.Rows
                If dr("id").ToString = "1" Then
                    txtRatioP1.Text = dr("ratio").ToString
                End If
                If dr("id").ToString = "E" Then
                    txtRatioE.Text = dr("ratio").ToString
                End If
            Next

            sql = "select id,val,plant from tb_setting "
            sql += " where (id='RATIO' or id='Capacity' or id='Takttime' or id='decimal' or id='RatioFromTaktTime' or id='MoveQ')"



            dt = DAL.getDataTable(sql, Connstr)
            For Each dr As DataRow In dt.Rows


                If String.Compare(dr("ID").ToString, "MoveQ", True) = 0 Then
                    If String.Compare(dr("val").ToString, "True", True) = 0 Then
                        MoveQ = True

                        BtnStartMoveQ.BackColor = Color.LimeGreen

                    Else
                        BtnStartMoveQ.BackColor = Color.Red
                        MoveQ = False
                    End If

                End If
            Next
        Catch ex As Exception
            ShowStatus("getratio:" & ex.Message, 10, Color.Red)
            SaveMSG("getratio:" & ex.Message, "ERR")
        End Try
    End Sub
    Sub setRatio(ratio As Integer, id As String)

        Dim sql As String
        sql = "update tb_plantUC set ratio =" & ratio & " where id ='" & id & "'"
        DAL.GetExecute(sql, Connstr)

        Try


            sql = "insert into tb_msg([M_Time],[M_Msg],[M_ACT],[M_STATION])"
            sql += " values(getdate(),'set Ratio " & id & " to " & ratio & "','Ratio','Line')"
            DAL.GetExecute(sql, Connstr)
        Catch ex As Exception

        End Try
    End Sub
    Private Sub btnRatiosetE_Click(sender As Object, e As EventArgs) Handles btnRatiosetE.Click
        Try

            Dim str As String
            str = InputBox("Set Ratio Export:")
            If IsNumeric(str) Then
                setRatio(str, "E")
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub btnRatiosetP1_Click(sender As Object, e As EventArgs) Handles btnRatiosetP1.Click
        Try


            Dim str As String
            str = InputBox("Set Ratio MaruA:")
            If IsNumeric(str) Then
                setRatio(str, "1")
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

#End Region
#Region "Loaddata"


    Sub runPData()
        timer_getdata.Change(Timeout.Infinite, Timeout.Infinite)
        Try


            Me.BeginInvoke(New MethodInvoker(Sub()
                                                 getData()
                                             End Sub))
        Catch ex As Exception
            ShowStatus("runPData:" & ex.Message, 10, Color.Red)
            SaveMSG("runPData:" & ex.Message, "ERR")
        Finally
            timer_getdata.Change(3000, 3000)

        End Try

    End Sub



    Sub getData()
        Try


            Dim sql As String
            Dim SetDT As New DataTable
            sql = "SELECT  t1.id,[Plant],t1.[LotCode],[Sequence],convert(varchar,[EVENTDATE],120) as EVENTDATE,iif(isnull(" & Pitch & ",0)=1,'Processing',iif(isnull(" & lblLogistic.Text & ",0)=3,'Logistic','Wait')) as status,family_code"

            sql += ",'Insert' as [Insert],'Del' as [Del] FROM [dbo].[TB_BC_DATA] as t1 left join [TB_Lotcode] on t1.lotcode=TB_Lotcode.lotcode where isnull(" & Pitch & ",0)<3 order by id "

            ' Dim dt As New DataTable
            SetDT = DAL.getDataTable(sql, Connstr)


            If DTQ_Common.Columns.Count = 0 Then
                DTQ_Common = SetDT
                DGV_Common.DataSource = DTQ_Common
            Else
                reloadDGV.reloadDGVPlantnoSet(DTQ_Common, SetDT, "id")
                If reloadDGV.lblStatus <> "" Then
                    Dim msg As String = "LoadDGVCommon:" & reloadDGV.lblStatus
                    ShowStatus(msg, 5)
                    SaveMSG(msg, "ERR")
                End If
            End If

            For Each dr As DataGridViewRow In DGV_Common.Rows
                ' If IsNumeric(dr.Cells("id").Value) = False Then Continue For

                If dr.Cells("status").Value.ToString = "Logistic" Then
                    dr.DefaultCellStyle.BackColor = Color.LightGray

                Else
                    dr.DefaultCellStyle.BackColor = Color.White
                End If

            Next
            Dim plant As String = "P1"
            sql = "select convert(varchar,[EVENTDATE],120) as EVENTDATE,t1.LOTCODE,Sequence,bcdata,family_code"
            sql += " ,t1.id,alcid,'Insert' as [Insert],'Del' as [Del] from tb_bc_data_" & plant & " as t1 left join [TB_Lotcode] on t1.lotcode=TB_Lotcode.lotcode where isnull(commonPlant,0)=0 order by id"
            SetDT = DAL.getDataTable(sql, Connstr)

            If DTQ_P1.Columns.Count = 0 Then
                DTQ_P1 = SetDT
                DGV_P1.DataSource = DTQ_P1
            Else
                reloadDGV.reloadDGVPlantnoSet(DTQ_P1, SetDT, "id")
                If reloadDGV.lblStatus <> "" Then
                    Dim msg As String = "LoadDGVP1:" & reloadDGV.lblStatus
                    ShowStatus(msg, 5)
                    SaveMSG(msg, "ERR")
                End If
            End If


            If StopLoadExport = False Then


                plant = "ex"
                sql = "select  convert(varchar,[EVENTDATE],120) as EVENTDATE,LOTCODE,Sequence,bcdata,export_seq"
                sql += " ,id,alcid,'Insert' as [Insert],'Del' as [Del] from tb_bc_data_" & plant & " where isnull(commonPlant,0)=0 order by id"
                SetDT = DAL.getDataTable(sql, Connstr)

                If DTQ_E.Columns.Count = 0 Then
                    DTQ_E = SetDT
                    DGV_E.DataSource = DTQ_E
                Else
                    reloadDGV.reloadDGVPlantnoSet(DTQ_E, SetDT, "id")
                    If reloadDGV.lblStatus <> "" Then
                        Dim msg As String = "LoadDGVEX:" & reloadDGV.lblStatus
                        ShowStatus(msg, 5)
                        SaveMSG(msg, "ERR")
                    End If
                End If

            End If


        Catch ex As Exception
            ShowStatus("getData:" & ex.Message, 10, Color.Red)
            SaveMSG("getData:" & ex.GetBaseException.ToString, "ERR")
        Finally
            '   timer_server.Change(3000, 3000)
        End Try

    End Sub
#End Region
    Private Sub btnExportInsert_Click(sender As Object, e As EventArgs) Handles btnExportInsert.Click
        Try


            If IsNumeric(txtexportQTY.Text) = False Then
                MsgBox("กรุณาใส่จำนวนเป็นตัวเลข")
                Exit Sub
            End If
            If txtExportLOT.Text.Trim = "" Then
                MsgBox("กรุณาใส่ LOTCODE")
                Exit Sub
            End If
            Dim table As String = "TB_BC_DATA_EX"
            Dim maxseq As String


            maxseq = DAL.getScalar("select SEQUENCE from  " & table & " where id = (select max(id) from " & table & ")", Connstr)
            If IsNumeric(maxseq) = False Then maxseq = "0"
            maxseq = CInt(CInt(maxseq) + 1).ToString("000")
            If CInt(maxseq) >= 1000 Then maxseq = "001"


            Dim QTY As Integer = txtexportQTY.Text
            Dim sql As String

            For i = 1 To QTY


                sql = "insert into " & table & "(id,EventDate,bcdata,Lotcode,Sequence,[source])"
                sql += " values((select isnull(max(id),-1)+1 from " & table & "),getdate(),'" & maxseq & "_" & txtExportLOT.Text & "','" & txtExportLOT.Text & "','" & maxseq & "'"
                sql += ",'E')"

                DAL.GetExecute(sql, Connstr)

                maxseq = CInt(CInt(maxseq) + 1).ToString("000")
                If CInt(maxseq) >= 1000 Then maxseq = "001"
            Next
            txtExportLOT.Text = ""
            txtexportQTY.Text = ""

            SaveMSG("Insert LOT:" & txtExportLOT.Text & ",QTY:" & txtexportQTY.Text, "ERR")
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
#Region "serverLink"


    Dim ServerPing, ServerPing2, ServerLink, ServerLink2 As Boolean
    Dim timer_serverstate As Threading.Timer

    Private Sub btnSetLogistic_Click(sender As Object, e As EventArgs) Handles btnSetLogistic.Click
        Dim str As String = InputBox("Input Logistic Process")
        If str = "" Then Exit Sub
        initialData.Item("LOGISTIC") = str
        lblLogistic.Text = str
        saveInitialData()
    End Sub

    Private Sub BtnStartMoveQ_Click(sender As Object, e As EventArgs) Handles BtnStartMoveQ.Click
        Dim sql As String
        sql = "select id,val,plant from tb_setting "
        sql += " where (id='RATIO' or id='Capacity' or id='Takttime' or id='decimal' or id='RatioFromTaktTime' or id='MoveQ')"


        Dim dt As DataTable
        dt = DAL.getDataTable(sql, Connstr)
        For Each dr As DataRow In dt.Rows


            If String.Compare(dr("ID").ToString, "MoveQ", True) = 0 Then
                If String.Compare(dr("val").ToString, "True", True) = 0 Then
                    MoveQ = False

                    BtnStartMoveQ.BackColor = Color.Red

                Else
                    BtnStartMoveQ.BackColor = Color.LimeGreen
                    MoveQ = True
                End If

            End If
        Next
        SaveSetting("MoveQ", MoveQ)
    End Sub
    Sub SaveSetting(Keys As String, Val As String, Optional plant As String = "all")
        Try


            Dim sqlstr As String
            sqlstr = "update tb_setting set val='" & Val & "' where id='" & Keys & "'"
            If plant <> "" Then sqlstr += " and plant ='" & plant & "'"

            If DAL.GetExecute(sqlstr, Connstr) = 0 Then
                sqlstr = "insert into tb_setting(id,val,plant)"
                sqlstr += " values('" & Keys & "','" & Val & "','" & plant & "')"
                DAL.GetExecute(sqlstr, Connstr)
            End If
        Catch ex As Exception
            ShowStatus("SaveSetting:" & ex.Message, 5)
            SaveMSG("SaveSetting:" & ex.Message, "ERR")
        End Try
    End Sub
    Dim TDS_Serverlink As TimerCallback = AddressOf checkstateServer

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        StopLoadExport = True
        DGV_E.Columns("ExportSelect").Visible = True
        btnExFinish.Visible = True
        btnEx_del.Visible = True
        btnEdit.Visible = False

        TimerDelete.Enabled = True
    End Sub



    Private Sub btnExCanCel_Click(sender As Object, e As EventArgs) Handles btnExFinish.Click
        StopLoadExport = False
        DGV_E.Columns("ExportSelect").Visible = False
        For Each dr As DataGridViewRow In DGV_E.Rows
            dr.Cells("ExportSelect").Value = False
        Next
        btnExFinish.Visible = False
        btnEx_del.Visible = False
        btnEdit.Visible = True

        TimerDelete.Enabled = False
    End Sub

    Private Sub btnEx_del_Click(sender As Object, e As EventArgs) Handles btnEx_del.Click
        Dim c As Int16
        Dim sId, sSeq, BCDATA, slot As String
        For Each dr As DataGridViewRow In DGV_E.Rows
            If dr.Cells("ExportSelect").Value Then
                '  sId = ",'" & sId & "'"
                c += 1
            End If
        Next

        If c <= 0 Then Exit Sub

        '  sId = sId.Substring(1)

        If MsgBox("ต้องการลบจำนวน " & c & "รายการ ใช่หรือไม่", vbYesNo) = MsgBoxResult.No Then
            Exit Sub
        End If

        Dim sql As String
        For i = DGV_E.Rows.Count - 1 To 0 Step -1

            ' Next
            'For Each dr As DataGridViewRow In DGV_E.Rows

            Dim dr As DataGridViewRow = DGV_E.Rows(i)
            If dr.Cells("ExportSelect").Value Then
                slot = dr.Cells("E_LOT").Value.ToString
                sSeq = dr.Cells("E_SEQ").Value.ToString
                sId = dr.Cells("E_ID").Value.ToString
                BCDATA = dr.Cells("EX_BCDATA").Value.ToString


                sql = "delete from tb_bc_data_ex where id=" & sId & ";"
                sql += " update tb_bc_data_ex set id=id-1 where id>" & sId
                'strSQL += "update tb_bc_data_Export set commonplant =0 where id=" & sID

                DAL.GetExecute(sql, Connstr)

                ' ReloadPlantQNow()
                '  GetQueue()
                SaveMSG("Delete LOT:" & slot & ",SEQ:" & sSeq, "USER")

                sql = " update tb_lot_ex set E_QTY=E_QTY-1 where E_id='" & BCDATA & "'"
                DAL.GetExecute(sql, Connstr)
                sql = " delete from tb_lot_ex where E_QTY<=0 and E_id='" & BCDATA & "'"
                DAL.GetExecute(sql, Connstr)


                dr.Cells("ExportSelect").Value = False
            End If
        Next

        sql = "select  convert(varchar,[EVENTDATE],120) as EVENTDATE,LOTCODE,Sequence,bcdata,export_seq"
        sql += " ,id,alcid,'Insert' as [Insert],'Del' as [Del] from tb_bc_data_ex where isnull(commonPlant,0)=0 order by id"
        DTQ_E = DAL.getDataTable(sql, Connstr)
        DGV_E.DataSource = DTQ_E

        TimerDelete.Enabled = False
    End Sub

    Private Sub TimerDelete_Tick(sender As Object, e As EventArgs) Handles TimerDelete.Tick
        btnExFinish.PerformClick()

    End Sub

    Private Sub TimerServerLink_Tick(sender As Object, e As EventArgs) Handles TimerServerLink.Tick


        lblServer1.Text = Server.server
        lblServer2.Text = Server.server2

        If ServerPing Then

            ServerPingPN.BackColor = Color.Lime
        Else
            ServerPingPN.BackColor = Color.Red
        End If

        If Server.server2 <> "" Then
            If ServerPing2 Then
                ServerPingPN2.BackColor = Color.Lime
            Else
                ServerPingPN2.BackColor = Color.Red
            End If
        Else
            ServerPingPN2.BackColor = Color.Gray

        End If



        If Server.server2 <> "" Then
            If ServerLink Then
                ServerLinkPN.BackColor = Color.Lime
            Else
                ServerLinkPN.BackColor = Color.Gray
            End If

            If ServerLink2 Then
                ServerLinkPN2.BackColor = Color.Lime
            Else
                ServerLinkPN2.BackColor = Color.Gray
            End If
        Else
            ServerLinkPN.BackColor = Color.Lime
            ServerLinkPN2.BackColor = Color.Gray
        End If

        'If StartALC = False Then
        '    ServerLinkPN.BackColor = Color.Gray
        '    ServerLinkPN2.BackColor = Color.Gray

        '    Exit Sub
        'End If

    End Sub
    Sub checkstateServer()
        timer_serverstate.Change(Timeout.Infinite, Timeout.Infinite)
        Try
            ServerPing = IPReady(Server.server)


            If Server.server2 <> "" Then
                ServerPing2 = IPReady(Server.server2)
            End If

            If ServerPing Then


                If DAL.getScalar(" Select  mirroring_role From sys.database_mirroring Where DB_NAME(database_id) = 'PARTSHOP_INTERLOCK'", "Data Source=" & Server.server & ";User ID=sa;Password=" & Server.password & ";Initial Catalog=master") = "1" Then

                    ServerLink = True
                Else
                    ServerLink = False
                End If
            End If

            If ServerPing2 Then


                If DAL.getScalar(" Select  mirroring_role From sys.database_mirroring Where DB_NAME(database_id) = 'PARTSHOP_INTERLOCK'", "Data Source=" & Server.server2 & ";User ID=sa;Password=" & Server.password & ";Initial Catalog=master") = "1" Then

                    ServerLink2 = True
                Else
                    ServerLink2 = False
                End If
            End If
        Catch ex As Exception
        Finally
            timer_serverstate.Change(1000, 1000)
        End Try

    End Sub




#End Region
    Private Sub DGV_Common_DataBindingComplete(sender As Object, e As DataGridViewBindingCompleteEventArgs) Handles DGV_Common.DataBindingComplete
        For Each dr As DataGridViewRow In DGV_Common.Rows
            If dr.Cells("Source").Value.ToString = "" Then Continue For

            If dr.Cells("Source").Value.ToString = "E" Then
                dr.DefaultCellStyle.ForeColor = Color.Fuchsia
                '   dr.Cells("state").Value = "Logistic"
            Else
                dr.DefaultCellStyle.ForeColor = Color.Black
            End If

        Next
    End Sub

    Private Sub DGV_E_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGV_E.CellClick
        Dim dgv As DataGridView = sender
        Try
            If e.RowIndex < 0 OrElse e.ColumnIndex < 0 Then
                Exit Sub
            End If
            Dim strSQL As String
            Dim sRow, sLot, sSeq, sPitch, BCDATA As String
            Dim Rowselect As Boolean
            Dim sID As String
            sPitch = sender.name.ToString.Substring(sender.name.ToString.Length - 1)
            With dgv
                sRow = .CurrentRow.Index.ToString

                '   Rowselect = .Rows(sRow).Cells("EXPORTSELECT").Value
                sLot = .Rows(sRow).Cells("E_LOT").Value.ToString
                '  sLot = .Rows(sRow).Cells("E_LOT").Value.ToString
                sSeq = .Rows(sRow).Cells("E_SEQ").Value.ToString
                sID = .Rows(sRow).Cells("E_ID").Value.ToString
                BCDATA = .Rows(sRow).Cells("EX_BCDATA").Value.ToString
                '  sPlant = .Rows(sRow).Cells("cPlant").Value.ToString
                If .Columns(e.ColumnIndex).Name.ToUpper = "E_DELETE".ToUpper Then
                    Dim sResult = MsgBox("Do you want to delete Lot: " & sLot & " ,Seq: " & sSeq & " ?", MsgBoxStyle.OkCancel)
                    If sResult = DialogResult.OK Then


                        'If sPlant = "E" Then
                        '    sPlant = "Export"
                        'ElseIf sPlant = "1" Then
                        '    sPlant = "P2"
                        'ElseIf sPlant = "2" Then
                        '    sPlant = "P2"
                        'End If

                        'Dim id_original As String
                        'id_original = DAL.getScalar("select top 1 id_original from tb_bc_data  where id =" & sID & "", Project.Connstr)



                        strSQL = "delete from tb_bc_data_ex where id=" & sID & ";"
                        strSQL += " update tb_bc_data_ex set id=id-1 where id>" & sID
                        'strSQL += "update tb_bc_data_Export set commonplant =0 where id=" & sID

                        DAL.GetExecute(strSQL, Connstr)

                        ' ReloadPlantQNow()
                        '  GetQueue()
                        SaveMSG("Delete LOT:" & sLot & ",SEQ:" & sSeq, "USER")

                        strSQL = " update tb_lot_ex set E_QTY=E_QTY-1 where E_id='" & BCDATA & "'"
                        DAL.GetExecute(strSQL, Connstr)
                        strSQL = " delete from tb_lot_ex where E_QTY<=0 and E_id='" & BCDATA & "'"
                        DAL.GetExecute(strSQL, Connstr)
                    End If

                ElseIf .Columns(e.ColumnIndex).Name.ToUpper = "E_INSERT" Then
                    Dim LOT As String = InputBox("Insert LOTCODE")
                    If LOT = "" Then
                        Exit Sub
                    End If

                    strSQL = " update tb_bc_data_ex set id=id+1,sequence=format(convert(int,[sequence])+1,'000') where id>=" & sID & ";"
                    strSQL += " update tb_bc_data_ex set sequence=format(convert(int,[sequence])-999,'000') where sequence>999 and id>=" & sID & ";"
                    strSQL += "insert into tb_bc_data_ex(id,EventDate,bcdata,Lotcode,Sequence,[source])"
                    strSQL += "values(" & sID & ",getdate(),'','" & LOT & "','" & sSeq & "','E')"

                    'strSQL += "update tb_bc_data_Export set commonplant =0 where id=" & sID
                    DAL.GetExecute(strSQL, Connstr)
                    SaveMSG("Insert LOT:" & LOT & ",SEQ:" & sSeq, "USER")

                ElseIf .Columns(e.ColumnIndex).Name.ToUpper = "EXPORTSELECT" Then

                Else
                    ' txtadjCommon.Text = dgv.Rows(dgv.CurrentRow.Index.ToString).Cells("IDcommon").Value.ToString
                End If
            End With
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub MixMonitorFrm_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        timer_getdata.Dispose()
        timer_serverstate.Dispose()
    End Sub

#Region "StopMixTime"


    Dim dtStopmix As New DataTable
    Dim Shift, befShift As String
    Sub getShift()

        Try


            '  Dim whereTime As String
            If Now.ToString("HH:mm") >= NightFrom Then
                '  whereTime = " EventDate between '" & Now.ToString("yyyy-MM-dd ") & NightFrom & ":00' and '" & Now.AddDays(1).ToString("yyyy-MM-dd ") & Nightto & ":00'"
                Shift = "Night"

            ElseIf Now.ToString("HH:mm") <= Nightto Then

                '   whereTime = " EventDate between '" & Now.AddDays(-1).ToString("yyyy-MM-dd ") & NightFrom & ":00' and '" & Now.ToString("yyyy-MM-dd ") & Nightto & ":00'"
                Shift = "Night"
            Else
                '  whereTime = " EventDate between '" & Now.ToString("yyyy-MM-dd ") & Dayfrom & ":00' and '" & Now.ToString("yyyy-MM-dd ") & DayTo & ":00'"
                Shift = "Day"
            End If
            If befShift <> Shift Then
                LoadOT()
            End If
            befShift = Shift
        Catch ex As Exception
            ShowStatus("getShift:" & ex.Message, 5, Color.Red)
        End Try
    End Sub



    Sub loadShift()
        Try


            Dim sql As String
            sql = "select * from tb_shift"
            Dim dt As DataTable = DAL.getDataTable(sql, Connstr)
            For Each dr As DataRow In dt.Rows
                Dayfrom = dr("Day_from").ToString()
                DayTo = dr("Day_to").ToString()
                NightFrom = dr("Night_from").ToString()
                Nightto = dr("Night_to").ToString()
            Next

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Sub LoadOT()
        Try


            Dim sql As String
            sql = "select * from TB_STOPMIX where Shift like '" & Shift & "' order by seq"
            dtStopmix = DAL.getDataTable(sql, Connstr)

            DGVOT.DataSource = dtStopmix

            StopMixTime = ""
            For Each dr As DataRow In dtStopmix.Rows
                If dr("selectOT").ToString = "True" Then
                    StopMixTime = dr("Stopmixtime").ToString
                End If
            Next
        Catch ex As Exception
            MsgBox("LoadOT:" & ex.Message)
        End Try
    End Sub
    Private Sub DGVOT_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGVOT.CellContentClick

    End Sub
    Private Sub DGVOT_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles DGVOT.CellEndEdit

    End Sub
    Private Sub DGVOT_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGVOT.CellClick
        If DGVOT.Columns(e.ColumnIndex).Name.ToUpper = "SELECTOT" Then

            Dim sql As String
            Dim selot As String = DGVOT.Rows(e.RowIndex).Cells("SELECTOT").Value.ToString

            sql = "update TB_STOPMIX set selectot=0 where shift like '" & Shift & "'"
            DAL.GetExecute(sql, Connstr)

            StopMixTime = ""

            If selot <> "True" Then

                StopMixTime = DGVOT.Rows(e.RowIndex).Cells("C_Stopmix").Value

                For Each dr As DataGridViewRow In DGVOT.Rows
                    dr.Cells("SELECTOT").Value = False
                Next
                DGVOT.Rows(e.RowIndex).Cells("SELECTOT").Value = True

                sql = "update TB_STOPMIX set selectot=1 where OTid like '" & DGVOT.Rows(e.RowIndex).Cells("OTid").Value & "'"
                DAL.GetExecute(sql, Connstr)



            End If

        End If
    End Sub
    Private Sub DGVOT_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles DGVOT.CellValueChanged

    End Sub
    Private Sub btnOTsetting_Click(sender As Object, e As EventArgs) Handles btnOTsetting.Click
        Dim frm As New OTfrm
        If frm.ShowDialog() = DialogResult.OK Then
            loadShift()
            getShift()
            LoadOT()
        End If

    End Sub
    Dim StopMixTime As String
    Sub CheckStopMix()
        For Each s As String In StopMixTime.Split(",")

            If Now.ToString("HH:mm:ss") >= s & ":00" And Now.ToString("HH:mm:ss") <= s & ":20" Then
                If MoveQ Then
                    MoveQ = False
                    SaveSetting("MoveQ", MoveQ)

                    SaveMSG("AutoStopMix", "Act")
                End If

            End If
        Next
    End Sub
    Private Sub TimerStopMix_Tick(sender As Object, e As EventArgs) Handles TimerStopMix.Tick
        CheckStopMix()
        getShift()
        ' lblStopmix.Text = StopMixTime
    End Sub





#End Region
End Class