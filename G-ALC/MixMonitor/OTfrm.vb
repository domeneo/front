﻿Imports System.ComponentModel

Public Class OTfrm
    Dim Select_preset As String = "select * from tb_stopmix order by seq"
    Dim select_shift As String = "select * from tb_shift "
    Dim dal As New DB.SqlConnect
    Dim sql As String
    Dim dtpreset, dtShift As DataTable
    ' Dim Connstr As String = "Data Source=localhost;User ID=sa;Password=p@ssw0rd;Initial Catalog=PARTSHOP_INTERLOCK_FR"

    Private Sub OTfrm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dtpreset = dal.getDataTable(Select_preset, Connstr)

        DGV_Preset.DataSource = dtpreset


        dtShift = dal.getDataTable(select_shift, Connstr)

        DGV_shift.DataSource = dtShift
        DGV_shift.AutoResizeColumns()
        DGV_Preset.AutoResizeColumns()
        DGV_Preset.Columns(0).Visible = False
        DGV_Preset.Columns("StartMixTime").Visible = False

    End Sub
    Dim Edit As Boolean = False
    Private Sub btnExportInsert_Click(sender As Object, e As EventArgs) Handles btnExportInsert.Click
        dal.Update(dtpreset, Select_preset, Connstr)
        dal.Update(dtShift, select_shift, Connstr)
        Edit = True
    End Sub

    Private Sub OTfrm_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        If Edit Then
            Me.DialogResult = DialogResult.OK
        Else
            Me.DialogResult = DialogResult.No
        End If


    End Sub
End Class