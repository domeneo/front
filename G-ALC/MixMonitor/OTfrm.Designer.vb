﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class OTfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DGV_Preset = New System.Windows.Forms.DataGridView()
        Me.btnExportInsert = New System.Windows.Forms.Button()
        Me.DGV_shift = New System.Windows.Forms.DataGridView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Dayfrom = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Dayto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NightFrom = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NightTo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.DGV_Preset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGV_shift, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DGV_Preset
        '
        Me.DGV_Preset.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_Preset.Location = New System.Drawing.Point(12, 29)
        Me.DGV_Preset.Name = "DGV_Preset"
        Me.DGV_Preset.Size = New System.Drawing.Size(456, 150)
        Me.DGV_Preset.TabIndex = 0
        '
        'btnExportInsert
        '
        Me.btnExportInsert.BackColor = System.Drawing.Color.Gray
        Me.btnExportInsert.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExportInsert.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnExportInsert.ForeColor = System.Drawing.Color.White
        Me.btnExportInsert.Location = New System.Drawing.Point(474, 29)
        Me.btnExportInsert.Name = "btnExportInsert"
        Me.btnExportInsert.Size = New System.Drawing.Size(68, 30)
        Me.btnExportInsert.TabIndex = 8
        Me.btnExportInsert.Text = "Save"
        Me.btnExportInsert.UseVisualStyleBackColor = False
        '
        'DGV_shift
        '
        Me.DGV_shift.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_shift.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Dayfrom, Me.Dayto, Me.NightFrom, Me.NightTo, Me.id})
        Me.DGV_shift.Location = New System.Drawing.Point(12, 207)
        Me.DGV_shift.Name = "DGV_shift"
        Me.DGV_shift.Size = New System.Drawing.Size(456, 89)
        Me.DGV_shift.TabIndex = 9
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 20)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "OT"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 184)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(90, 20)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "Shift Time"
        '
        'Dayfrom
        '
        Me.Dayfrom.DataPropertyName = "Day_from"
        Me.Dayfrom.HeaderText = "Day From"
        Me.Dayfrom.Name = "Dayfrom"
        '
        'Dayto
        '
        Me.Dayto.DataPropertyName = "Day_to"
        Me.Dayto.HeaderText = "Day To"
        Me.Dayto.Name = "Dayto"
        '
        'NightFrom
        '
        Me.NightFrom.DataPropertyName = "Night_From"
        Me.NightFrom.HeaderText = "Night From"
        Me.NightFrom.Name = "NightFrom"
        '
        'NightTo
        '
        Me.NightTo.DataPropertyName = "Night_To"
        Me.NightTo.HeaderText = "Night To"
        Me.NightTo.Name = "NightTo"
        '
        'id
        '
        Me.id.DataPropertyName = "id"
        Me.id.HeaderText = "id"
        Me.id.Name = "id"
        Me.id.Visible = False
        '
        'OTfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(724, 338)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.DGV_shift)
        Me.Controls.Add(Me.btnExportInsert)
        Me.Controls.Add(Me.DGV_Preset)
        Me.Name = "OTfrm"
        Me.Text = "Setting"
        CType(Me.DGV_Preset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGV_shift, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents DGV_Preset As DataGridView
    Friend WithEvents btnExportInsert As Button
    Friend WithEvents DGV_shift As DataGridView
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Dayfrom As DataGridViewTextBoxColumn
    Friend WithEvents Dayto As DataGridViewTextBoxColumn
    Friend WithEvents NightFrom As DataGridViewTextBoxColumn
    Friend WithEvents NightTo As DataGridViewTextBoxColumn
    Friend WithEvents id As DataGridViewTextBoxColumn
End Class
