﻿Imports MySql.Data.MySqlClient

'
' このクラスを使用する際には、必ずDBをオープンしてから使用してください。
'
' pSQLOpen[DBオープン関数]
'

Public Class clsSQL

    'Public Enum eCols
    '    Col01 = 1
    '    Col02 = 2
    '    Col03 = 3
    '    Col04 = 4
    '    Col05 = 5
    '    Col06 = 6
    '    Col07 = 7
    '    Col08 = 8
    '    Col09 = 9
    '    Col10 = 10
    '    ColOther = 11
    'End Enum

    Public pCon As MySqlConnection      ' MySQLコネクション変数

    Public pCmdDummy1 As MySqlCommand   ' MySQLコマンド変数(DBﾀﾞﾐｰ読込で使用)
    Public pCmdDummy2 As MySqlCommand   ' MySQLコマンド変数(DBﾀﾞﾐｰ読込で使用)
    Public pCmdDisp As MySqlCommand     ' MySQLコマンド変数(ﾃﾞｰﾀ表示画面で使用)
    Public pCmdDispAlcLoop As MySqlCommand     ' MySQLコマンド変数(AlcLoop内表示用で使用)
    Public pCmdALC As MySqlCommand      ' MySQLコマンド変数(ALC読込側で使用)
    Public pCmd As MySqlCommand         ' MySQLコマンド変数(PLC書込側で使用)

    Public pDrd As MySqlDataReader          ' MySQLデータリーダー変数
    Public pDrd2 As MySqlDataReader         ' MySQLデータリーダー変数
    Public pDrdAlcLoop As MySqlDataReader         ' MySQLデータリーダー変数　ALCLoop内表示用で使用

    Public pstrVehicle() As String

    Public blnDbStatus As Boolean

    ' ※pDrdを使用するときは、必ず使い終わったらCloseを行うこと
    Public strMySQLConnectString As String
    Public Sub pSQLOpen(ByVal strSrvName As String, ByVal strDbName As String, _
                        ByVal strUser As String, ByVal strPass As String)



        Try
            ' MYSQL接続処理
            strMySQLConnectString = "server=" & strSrvName & ";" & _
                                    "database=" & strDbName & ";" & _
                                    "user=" & strUser & ";" & _
                                    "password=" & strPass

            ' MySQLサーバ接続処理
            pCon = New MySqlConnection(strMySQLConnectString)
            pCon.Open()

            blnDbStatus = True
        Catch ex As Exception
            blnDbStatus = False
        End Try

    End Sub

    Public Sub pSQLClose()

        pCon.Close()

    End Sub

    Public Sub sDummySQL1()

        Dim intCount As Integer

        Try
            intCount = Val(pCmdDummy1.ExecuteScalar)
        Catch ex As Exception
        End Try

    End Sub

    Public Sub sDummySQL2()

        Dim intCount As Integer

        Try
            intCount = Val(pCmdDummy2.ExecuteScalar)
        Catch ex As Exception
        End Try

    End Sub

    Public Function fGetVehicleData() As Integer

        Dim strSQL As String
        'Dim intCount As Integer
        'Dim intReadCount As Integer

        Try

            If Not pDrd Is Nothing Then
                If Not pDrd.IsClosed Then
                    pDrd.Close()
                End If
            End If

            strSQL = "SELECT * FROM tb_vehicle ORDER BY id"
            pCmd = New MySqlCommand(strSQL, pCon)
            pDrd = pCmd.ExecuteReader

            ReDim pstVehicleData(0)

            pintVehicleCount = 0
            While pDrd.Read()
                ReDim Preserve pstVehicleData(pintVehicleCount)
                pstVehicleData(pintVehicleCount).id = pDrd(eVehicle.id)
                pstVehicleData(pintVehicleCount).strAssyNo = _
                                            pDrd(eVehicle.AssyNo).ToString
                pstVehicleData(pintVehicleCount).strVehicleName = _
                                            pDrd(eVehicle.VehicleName).ToString
                pintVehicleCount = pintVehicleCount + 1
            End While
            'If intReadCount < 95 Then
            '    For intCount = intReadCount To 95 Step 1
            '        ReDim Preserve pstVehicleData(intCount)
            '    Next intCount
            'End If
            fGetVehicleData = mdlErrorCode.pCSuccess
            If Not pDrd Is Nothing Then
                If Not pDrd.IsClosed Then
                    pDrd.Close()
                End If
            End If
        Catch ex As Exception
            mdlCommon.pblnEXFlg = True
            mdlCommon.psysException = ex
            mdlCommon.pstrErrorMsg = "clsSQL.fGetVehicleData"
            fGetVehicleData = mdlErrorCode.pCErrDataRead
            If Not pDrd Is Nothing Then
                If Not pDrd.IsClosed Then
                    pDrd.Close()
                End If
            End If
        End Try

    End Function

    Public Function fGetAlcData() As Integer

        Dim strSQL As String

        '送信済みﾃﾞｰﾀ格納ｴﾘｱを初期化
        pstAlcData2PLC.id = 0
        pstAlcData2PLC.strBCNo = ""
        pstAlcData2PLC.strAssyNo = ""
        pstAlcData2PLC.strVehicleName = ""
        pstAlcData2PLC.intStatus = 0
        pstAlcData2PLC.intDelFlg = 0
        pstAlcData2PLC.strRecvDate = ""
        pstAlcData2PLC.strLOTcode = ""
        pstAlcData2PLC.strFamilycode = ""
        pblnAlcDataFlg = False
        Try

            If Not pDrd Is Nothing Then
                If Not pDrd.IsClosed Then
                    pDrd.Close()
                End If
            End If

            'tb_alcより受信ﾃﾞｰﾀの取得
            strSQL = "SELECT * FROM tb_alc ORDER BY id ASC"
            'pCmd = New MySqlCommand(strSQL, pCon)
            'pDrd = pCmd.ExecuteReader

            Using conn As New MySqlConnection(strMySQLConnectString)
                Using pCmd As New MySqlCommand(strSQL, conn)
                    If conn.State = ConnectionState.Closed Then
                        conn.Open()
                    End If
                    pDrd = pCmd.ExecuteReader


                    If pDrd.Read() Then
                        pstAlcData2PLC.id = pDrd(eAlc.id)
                        pstAlcData2PLC.strBCNo = pDrd(eAlc.strBCNo).ToString
                        pstAlcData2PLC.strAssyNo = pDrd(eAlc.strAssyNo).ToString
                        pstAlcData2PLC.strVehicleName =
                                            pDrd(eAlc.strVehicleName).ToString
                        pstAlcData2PLC.strLOTcode = pDrd(eAlc.strLOTcode).ToString          'ﾛｯﾄｺｰﾄﾞ追加（2012/5）
                        pstAlcData2PLC.strFamilycode = pDrd(eAlc.strFamilycode).ToString    'ﾌｧﾐﾘｰｺｰﾄﾞ追加（2012/5）
                        pstAlcData2PLC.intStatus = pDrd(eAlc.intStatus)
                        pstAlcData2PLC.intDelFlg = pDrd(eAlc.intDelFlg)
                        pstAlcData2PLC.strRecvDate = pDrd(eAlc.strRecvDate).ToString
                        pblnAlcDataFlg = True
                    End If
                End Using
            End Using
            fGetAlcData = mdlErrorCode.pCSuccess
            If Not pDrd Is Nothing Then
                If Not pDrd.IsClosed Then
                    pDrd.Close()
                End If
            End If
        Catch ex As Exception
            mdlCommon.pblnEXFlg = True
            mdlCommon.psysException = ex
            mdlCommon.pstrErrorMsg = "clsSQL.fGetAlcData"
            fGetAlcData = mdlErrorCode.pCErrDataRead
            If Not pDrd Is Nothing Then
                If Not pDrd.IsClosed Then
                    pDrd.Close()
                End If
            End If
        End Try

    End Function

    Public Function fGetAlcDataDisp() As Integer

        Dim strSQL As String
        Dim intCount As Integer

        '受信ﾃﾞｰﾀ格納ｴﾘｱを初期化
        ReDim pstAlcData(0)
        Try

            If Not pDrd2 Is Nothing Then
                If Not pDrd2.IsClosed Then
                    pDrd2.Close()
                End If
            End If

            'tb_alcより受信ﾃﾞｰﾀの取得
            'strSQL = "SELECT * FROM 800l.tb_alc ORDER BY id ASC"
            strSQL = "SELECT * FROM " & pstIni.strDbName & ".tb_alc ORDER BY id ASC"     '860L対応(2011/9/23）

            pCmdDisp = New MySqlCommand(strSQL, pCon)
            pDrd2 = pCmdDisp.ExecuteReader
            intCount = 0
            While pDrd2.Read()
                ReDim Preserve pstAlcData(intCount)
                pstAlcData(intCount).id = pDrd2(eAlc.id)
                pstAlcData(intCount).strBCNo = pDrd2(eAlc.strBCNo).ToString
                pstAlcData(intCount).strAssyNo = pDrd2(eAlc.strAssyNo).ToString
                pstAlcData(intCount).strVehicleName = _
                                            pDrd2(eAlc.strVehicleName).ToString
                pstAlcData(intCount).strLOTcode = pDrd2(eAlc.strLOTcode).ToString          'ﾛｯﾄｺｰﾄﾞ追加（2012/5）
                pstAlcData(intCount).strFamilycode = pDrd2(eAlc.strFamilycode).ToString    'ﾌｧﾐﾘｰｺｰﾄﾞ追加（2012/5）
                pstAlcData(intCount).intStatus = pDrd2(eAlc.intStatus)
                pstAlcData(intCount).intDelFlg = pDrd2(eAlc.intDelFlg)
                pstAlcData(intCount).strRecvDate = _
                                            pDrd2(eAlc.strRecvDate).ToString
                intCount = intCount + 1
            End While
            pintAlcDataCount = intCount
            fGetAlcDataDisp = mdlErrorCode.pCSuccess
            If Not pDrd2 Is Nothing Then
                If Not pDrd2.IsClosed Then
                    pDrd2.Close()
                End If
            End If
        Catch ex As Exception
            mdlCommon.pblnEXFlg = True
            mdlCommon.psysException = ex
            mdlCommon.pstrErrorMsg = "clsSQL.fGetAlcDataDisp"
            fGetAlcDataDisp = mdlErrorCode.pCErrDataRead
            If Not pDrd2 Is Nothing Then
                If Not pDrd2.IsClosed Then
                    pDrd2.Close()
                End If
            End If
        End Try

    End Function
    ''' <summary>
    ''' ALCループ内で参照するデータ表示
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function fGetAlcLoopDataDisp() As Integer

        Dim strSQL As String
        Dim intCount As Integer

        '受信ﾃﾞｰﾀ格納ｴﾘｱを初期化
        ReDim pstAlcLoopData(0)
        Try

            If Not pDrdAlcLoop Is Nothing Then
                If Not pDrdAlcLoop.IsClosed Then
                    pDrdAlcLoop.Close()
                End If
            End If

            'tb_alcより受信ﾃﾞｰﾀの取得
            'strSQL = "SELECT * FROM 800l.tb_alc ORDER BY id ASC"
            strSQL = "SELECT * FROM " & pstIni.strDbName & ".tb_alc ORDER BY id ASC"     '860L対応(2011/9/23）

            pCmdDispAlcLoop = New MySqlCommand(strSQL, pCon)
            pDrdAlcLoop = pCmdDispAlcLoop.ExecuteReader
            intCount = 0
            While pDrdAlcLoop.Read()
                ReDim Preserve pstAlcLoopData(intCount)
                pstAlcLoopData(intCount).id = pDrdAlcLoop(eAlc.id)
                pstAlcLoopData(intCount).strBCNo = pDrdAlcLoop(eAlc.strBCNo).ToString
                pstAlcLoopData(intCount).strAssyNo = pDrdAlcLoop(eAlc.strAssyNo).ToString
                pstAlcLoopData(intCount).strLOTcode = pDrdAlcLoop(eAlc.strLOTcode).ToString             'ﾛｯﾄｺｰﾄﾞ（2012/6/7）
                pstAlcLoopData(intCount).strFamilycode = pDrdAlcLoop(eAlc.strFamilycode).ToString       'ﾌｧﾐﾘｰｺｰﾄﾞ（2012/6/7）
                pstAlcLoopData(intCount).strVehicleName = _
                                            pDrdAlcLoop(eAlc.strVehicleName).ToString
                pstAlcLoopData(intCount).intStatus = pDrdAlcLoop(eAlc.intStatus)
                pstAlcLoopData(intCount).intDelFlg = pDrdAlcLoop(eAlc.intDelFlg)
                pstAlcLoopData(intCount).strRecvDate = _
                                            pDrdAlcLoop(eAlc.strRecvDate).ToString
                intCount = intCount + 1
            End While
            pintAlcLoopDataCount = intCount
            fGetAlcLoopDataDisp = mdlErrorCode.pCSuccess
            If Not pDrdAlcLoop Is Nothing Then
                If Not pDrdAlcLoop.IsClosed Then
                    pDrdAlcLoop.Close()
                End If
            End If
        Catch ex As Exception
            'mdlCommon.pblnEXFlg = True
            'mdlCommon.psysException = ex
            'mdlCommon.pstrErrorMsg = "clsSQL.fGetAlcDataDisp"
            'fGetAlcLoopDataDisp = mdlErrorCode.pCErrDataRead
            pInsErrMsg.psubErrMsgShow(ex, "clsSQL.fGetAlcDataDisp")
            If Not pDrdAlcLoop Is Nothing Then
                If Not pDrdAlcLoop.IsClosed Then
                    pDrdAlcLoop.Close()
                End If
            End If
        End Try

    End Function

    Public Function fDeleteAlcData(ByVal strid As String) As Integer

        Dim strSQL As String

        Try
            strSQL = "DELETE FROM tb_alc"
            If strid <> "ALL" Then
                strSQL = strSQL & " WHERE id = " & strid
            End If
            pCmdDisp = New MySqlCommand(strSQL, pCon)
            pCmdDisp.ExecuteNonQuery()
            fDeleteAlcData = pCSuccess
        Catch ex As Exception
            mdlCommon.pblnEXFlg = True
            mdlCommon.psysException = ex
            mdlCommon.pstrErrorMsg = "clsSQL.fDeleteAlcData"
            'pInsErrMsg.psubErrMsgShow(ex, "clsSQL.fDeleteAlcData")
            fDeleteAlcData = pCErrOther
        End Try

    End Function

    Public Function fSetAlcDataALC(ByVal strSQL As String) As Integer




        Try
            Using conn As New MySqlConnection(strMySQLConnectString)


                Using cmd As New MySqlCommand
                    If conn.State = ConnectionState.Closed Then
                        conn.Open()
                    End If
                    cmd.Connection = conn
                    cmd.CommandText = strSQL
                    cmd.ExecuteNonQuery()
                End Using
            End Using
        Catch ex As Exception
            mdlCommon.pblnEXFlg = True
            mdlCommon.psysException = ex
            mdlCommon.pstrErrorMsg = "clsSQL.fSetAlcData"
            'pInsErrMsg.psubErrMsgShow(ex, "clsSQL.fSetAlcData")
            fSetAlcDataALC = pCErrOther

        End Try


        ''受信ﾃﾞｰﾀをﾃﾞｰﾀﾍﾞｰｽに保存
        'Try
        '    pCon.Close()
        '    pCon.Open()
        'Catch ex As Exception

        'End Try

        'pCmdALC = New MySqlCommand(strSQL, pCon)
        'Try
        '    pCmdALC.ExecuteNonQuery()
        '    fSetAlcDataALC = pCSuccess
        'Catch ex As Exception
        '    mdlCommon.pblnEXFlg = True
        '    mdlCommon.psysException = ex
        '    mdlCommon.pstrErrorMsg = "clsSQL.fSetAlcData"
        '    'pInsErrMsg.psubErrMsgShow(ex, "clsSQL.fSetAlcData")
        '    fSetAlcDataALC = pCErrOther
        'End Try

    End Function

    Public Function fSetAlcDataPLC(ByVal strSQL As String) As Integer

        '受信ﾃﾞｰﾀをﾃﾞｰﾀﾍﾞｰｽに保存
        pCmd = New MySqlCommand(strSQL, pCon)
        Try
            pCmd.ExecuteNonQuery()
            fSetAlcDataPLC = pCSuccess
        Catch ex As Exception
            mdlCommon.pblnEXFlg = True
            mdlCommon.psysException = ex
            mdlCommon.pstrErrorMsg = "clsSQL.fSetAlcData"
            'pInsErrMsg.psubErrMsgShow(ex, "clsSQL.fSetAlcData")
            fSetAlcDataPLC = pCErrOther
        End Try

    End Function

    Public Function fSetSendData(ByVal strSQL As String) As Integer

        '送信済みﾃﾞｰﾀをﾃﾞｰﾀﾍﾞｰｽに保存
        pCmd = New MySqlCommand(strSQL, pCon)
        Try
            pCmd.ExecuteNonQuery()
            fSetSendData = pCSuccess
        Catch ex As Exception
            mdlCommon.pblnEXFlg = True
            mdlCommon.psysException = ex
            mdlCommon.pstrErrorMsg = "clsSQL.fSetSendData"
            'pInsErrMsg.psubErrMsgShow(ex, "clsSQL.fSetSendData")
            fSetSendData = pCErrOther
        End Try

    End Function

    Public Function fSetVehicleTable(ByVal strAssyNo As String(), _
                                     ByVal strVehicle As String()) As Integer

        Dim strSQL As String
        Dim intCount As Integer

        Try
            strSQL = "DELETE FROM tb_vehicle"
            pCmd = New MySqlCommand(strSQL, pCon)
            pCmd.ExecuteNonQuery()

            For intCount = 0 To strVehicle.Length - 1
                strSQL = "INSERT INTO tb_vehicle (id, strAssyNo, "
                strSQL = strSQL + "strVehicleName) VALUES ("
                strSQL = strSQL + (intCount + 1).ToString + ",'"
                If CInt(strAssyNo(intCount)) < 9 Then
                    strSQL = strSQL + "0" + CInt(strAssyNo(intCount)).ToString
                Else
                    strSQL = strSQL + CInt(strAssyNo(intCount)).ToString
                End If
                strSQL = strSQL + "','" + strVehicle(intCount) + "')"
                pCmd = New MySqlCommand(strSQL, pCon)
                pCmd.ExecuteNonQuery()
            Next intCount
            fSetVehicleTable = pCSuccess

        Catch ex As Exception
            mdlCommon.pblnEXFlg = True
            mdlCommon.psysException = ex
            mdlCommon.pstrErrorMsg = "clsSQL.fSetVehicleTable"
            'pInsErrMsg.psubErrMsgShow(ex, "clsSQL.fSetVehicleTable")
            fSetVehicleTable = pCErrOther
        End Try

    End Function

End Class
