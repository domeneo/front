﻿Imports System.IO
Module Module1
    Public iniFile As String = Application.StartupPath & "\server.ini"
    Public Connstr As String
    Public initialData As Hashtable = New Hashtable
    Public Server As New ServerStruct
    Dim DAL As New DB.SqlConnect
    Structure ServerStruct
        Dim server, password, server2, dbname As String

    End Structure
    Public Sub loadSERVERSQL()
        getInitialData()





        Server.server = initialData("SERVER").ToString
        Server.server2 = initialData("SERVER2").ToString
        Server.password = initialData("PASSWORD").ToString
        Server.dbname = initialData("DBNAME").ToString

        Connstr = "Server=" & Server.server & "; Failover Partner=" & Server.server & ";User ID=sa;Password=" & Server.password
        Connstr += ";Initial Catalog=" & Server.dbname
    End Sub

#Region "Init"


    Public Sub getInitialData()

        Dim initialString As String
        Dim key As String
        Dim value As String

        If File.Exists(iniFile) Then

            Dim SocketStream As New FileStream(iniFile, FileMode.Open)
            Dim SocketStreamRead As New StreamReader(SocketStream)
            Try
                While SocketStreamRead.Peek() > 0
                    initialString = SocketStreamRead.ReadLine()
                    If initialString.IndexOf("=") > 0 Then
                        key = initialString.Split("=")(0).Trim.ToUpper
                        value = initialString.Split("=")(1).Trim
                        initialData.Add(key, value)
                    End If
                End While

            Catch
                MsgBox(Err.Description)
            Finally
                SocketStreamRead.Close()
                SocketStream.Close()
            End Try
        Else
            MsgBox("Error:= Initial File Does Not Exist " + iniFile, MsgBoxStyle.Critical, "SocketPara Error")
            '  Me.Close()
        End If
    End Sub

    Public Sub saveInitialData()
        Dim UpdateStream As New FileStream(iniFile, FileMode.Open, FileAccess.Write)
        Dim UpdateStreamWrite As New StreamWriter(UpdateStream)
        Try

            UpdateStreamWrite.WriteLine("SERVER= " & initialData.Item("SERVER"))
            UpdateStreamWrite.WriteLine("SERVER2= " & initialData.Item("SERVER2"))
            UpdateStreamWrite.WriteLine("PASSWORD= " & initialData.Item("PASSWORD"))
            UpdateStreamWrite.WriteLine("DBNAME= " & initialData.Item("DBNAME"))
            UpdateStreamWrite.WriteLine("PROCESS= " & initialData.Item("PROCESS"))
            UpdateStreamWrite.WriteLine("LOGISTIC= " & initialData.Item("LOGISTIC"))


        Catch

        Finally
            UpdateStreamWrite.Close()
            UpdateStream.Close()

        End Try
    End Sub
#End Region
    Public Sub SaveMSG(MSG As String, ACT As String)
        Try

            MSG = MSG.Replace("'", "''")
            Dim sqlstr As String
            sqlstr = "insert into tb_msg(m_time,m_msg,m_act,m_station,m_user)"
            sqlstr += " values(getdate(),'" & MSG & "','" & ACT & "','MixMonitor','FR')"
            DAL.GetExecute(sqlstr, Connstr)
        Catch ex As Exception

        End Try
    End Sub
End Module
