﻿'---------1---------2---------3---------4---------5---------6---------7---------8
'クラス名 | clsPc10G          |PC10G用アクセスグラスモジュール
'---------+-------------------+--------------------------------------------------
'公開関数群
'------------------------------------+------------+--------------+--------------+
'               関数名               |   公開日   |    作成者    |    変更日    |
'------------------------------------+------------+--------------+--------------+
' fReadWordAdr                       | 2009/04/07 | Shinya.T     | 2009/12/23   |
'------------------------------------+------------+--------------+--------------+
' fWriteWordAdr                      | 2009/04/07 | Shinya.T     | 2009/12/23   |
'------------------------------------+------------+--------------+--------------+
' fReadBitAdr                        | 2009/12/23 | Shinya.T     |              |
'------------------------------------+------------+--------------+--------------+
' fWriteBitAdr                       | 2009/12/23 | Shinya.T     |              |
'------------------------------------+------------+--------------+--------------+
' fErrMsgShow                        | 2009/12/23 | Shinya.T     |              |
'------------------------------------+------------+--------------+--------------+
' fErrWrite                          | 2009/12/23 | Shinya.T     |              |
'------------------------------------+------------+--------------+--------------+
'変更履歴
'------------------------------------+------------+--------------+--------------+
' 2009/04/07 ワード操作関数作成
' 2009/12/23 ビット操作関数作成
' 2009/12/23 エラーウィンドウをクラス内に作成
' 2009/12/23 全関数の整理
'---------1---------2---------3---------4---------5---------6---------7---------8
'
Imports System.Net.Sockets
Imports System.Text

Public Class clsPc10G
    Inherits System.Windows.Forms.Form

    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents btnClose As System.Windows.Forms.Button

    Private components As System.ComponentModel.IContainer

    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblMessage = New System.Windows.Forms.Label
        Me.btnClose = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'lblMessage
        '
        Me.lblMessage.Location = New System.Drawing.Point(12, 10)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(499, 73)
        Me.lblMessage.TabIndex = 0
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(436, 94)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 25)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'clsPc10G
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(523, 125)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.lblMessage)
        Me.Name = "clsPc10G"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Error"
        Me.ResumeLayout(False)

    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, _
                ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Const cstrFileHead = "Error"
    Const cdteFormat = "yyyy_MM"
    Const cstrKakutyousi = ".txt"

    Const cstrFileMidasi = "Date,Function Name,Error Content,Data"
    'Const cstrFileMidasi = "日付,関数名,エラー内容,Data"

    Public pCSuccess As Integer = 0
    Public pCErrOther As Integer = -1
    Public pCErrDisConnect As Integer = -2
    Public pCErrAddressOver As Integer = -3
    Public pCErrDataRead As Integer = -4
    Public pCErrDataWrite As Integer = -5
    Public pCErrAnser As Integer = -6

    Public pCBitOn As Integer = 1
    Public pCBitOff As Integer = 0

    Public Enum eChkStr
        Control = 0
        Sign = 1
        Numeric = 2
        AlphabetB = 3
        AlphabetS = 4
        Kana = 5
    End Enum

    Public Structure ST_REG
        Public intPGNo As Integer       'COM=0,P1=1,P2=2,P3=3
        Public intRegType As Integer    'P=1,K=2,V=3,TC=4,L=5,XY=6,M=7,S=8,N=9,
        '                               'R=10,D=11,JL=12,JS=13,EP=14,EK=15,
        '                               'EV=16,ETC=17,EL=18,EXY=19,EM=20,ES=21,
        '                               'EN=22,H=23,GXY=24,GM=25,U=26,EB=27,
        '                               'FR=28
        Public strReg As String         'ｱﾄﾞﾚｽの指定("0-0":ﾋﾞｯﾄ,"0":ﾜｰﾄﾞ)
        '                               '※L100の場合"10"(L10Wなので)
    End Structure


    Public Structure _stPlcDateTime
        Public iYear As Integer
        Public iMonth As Integer
        Public iDay As Integer
        Public iHour As Integer
        Public iMinute As Integer
        Public iSecond As Integer
        Public iDayOfWeek As Integer

        Public Sub New(ByVal i As Integer)
            iYear = i
            iMonth = i
            iDay = i
            iHour = i
            iMinute = i
            iSecond = i
            iDayOfWeek = i
        End Sub
    End Structure

    Public socketTcpCL As TcpClient
    Public bytSocketFormatRead As Byte()
    Public bytSocketFormatWrite As Byte()

    Public Enum eTcpCmds
        PC10_BYTE_READ_S = &HC2     ' PC10データバイト読出し
        PC10_BYTE_WRITE_S = &HC3    ' PC10データバイト書込み
        PC10_BYTE_READ_M = &HC4     ' PC10多点読出し
        PC10_BYTE_WRITE_M = &HC5    ' PC10多点書込み
        PC10_REGIST_FR = &HCA       ' PC10FRレジスタ登録
        PC10_CLOCK = &H32
        PC10_CLOCK_WRITE = &H71
        PC10_CLOCK_READ = &H70
    End Enum

    Public Enum eRegTypes
        P = 1
        K = 2
        V = 3
        TC = 4
        L = 5
        XY = 6
        M = 7
        S = 8
        N = 9
        R = 10
        D = 11
        JL = 12
        JS = 13
        EP = 14
        EK = 15
        EV = 16
        ETC = 17
        EL = 18
        EXY = 19
        EM = 20
        ES = 21
        EN = 22
        H = 23
        GXY = 24
        GM = 25
        U = 26
        EB = 27
        FR = 28
    End Enum

    Public Enum ePgNos
        COM = 0
        P1 = 1
        P2 = 2
        P3 = 3
    End Enum

    Private Sub CmnLng2IntInt(ByVal lngData As Integer, _
                              ByRef intLower As Short, ByRef intUpper As Short)

        Dim lngTempLower As Short
        Dim lngTempUpper As Short

        Try
            ' Lowerデータ算出
            lngTempLower = lngData Mod 256
            ' Upperデータ算出
            lngTempUpper = (lngData - lngTempLower) / 256

            intLower = lngTempLower
            intUpper = lngTempUpper
        Catch ex As Exception
        End Try
    End Sub

    Private Sub CmnIntInt2Lng(ByRef lngData As Integer, _
                              ByVal intLower As Short, ByVal intUpper As Short)
        Try
            lngData = intUpper * 256 + intLower
        Catch ex As Exception

        End Try
    End Sub

    Private Function CmnCheckChar(ByVal iAsc As Integer) As Integer
        CmnCheckChar = eChkStr.Control

        Select Case iAsc
            Case &H20 To &H2F, &H3A To &H40, _
                 &H5B To &H60, &H7B To &H7E ' 記号
                CmnCheckChar = eChkStr.Sign
            Case &H30 To &H39               ' 数字
                CmnCheckChar = eChkStr.Numeric
            Case &H41 To &H5A               ' 大文字
                CmnCheckChar = eChkStr.AlphabetB
            Case &H61 To &H7A               ' 小文字
                CmnCheckChar = eChkStr.AlphabetS
            Case &HA1 To &HDF               ' 半角カナ
                CmnCheckChar = eChkStr.Kana
            Case Else                       ' 制御記号
                CmnCheckChar = eChkStr.Control
        End Select

    End Function


    ''' <summary>
    ''' エラー表示処理 Exceptionクラスに対応
    ''' </summary>
    ''' <param name="Msgex">Exceptionクラス変数</param>
    ''' <param name="strFunctionName">ファンクション名称</param>
    ''' <remarks></remarks>
    Public Sub fErrMsgShow(ByVal Msgex As Exception, _
                           ByVal strFunctionName As String)
        Dim strErr As String
        Dim strErrCrlfNon As String
        Dim strErrFileName As String

        Try
            'エラー内容を表示
            strErr = Now & "," & strFunctionName & "," & _
                     Msgex.GetBaseException.ToString
            Me.Show()
            Me.lblMessage.Text = strErr

            'ファイル名作成
            strErrFileName = Application.StartupPath & "\" & cstrFileHead & _
                             Date.Today.ToString(cdteFormat) & cstrKakutyousi

            'ファイルがなかった場合は見出しを書き込む
            If System.IO.File.Exists(strErrFileName) = False Then
                My.Computer.FileSystem.WriteAllText(strErrFileName, _
                            cstrFileMidasi & ControlChars.CrLf, True)
            End If

            strErrCrlfNon = strErr.Replace(ControlChars.CrLf, "")
            ''エラー内容をファイルに書き込む
            My.Computer.FileSystem.WriteAllText(strErrFileName, _
                        strErrCrlfNon & ControlChars.CrLf, True)

        Catch ex As Exception

            strErr = Now & " : " & ex.GetBaseException.ToString
            Me.lblMessage.Text = strErr
        End Try
    End Sub

    ''' <summary>
    ''' エラーメッセージをファイルに保存（NoWindow版）
    ''' </summary>
    ''' <param name="Msgex">Exceptionクラス変数</param>
    ''' <param name="strFunctionName">ファンクション名称</param>
    ''' <remarks></remarks>
    Public Sub fErrWrite(ByVal Msgex As Exception, _
                         ByVal strFunctionName As String)

        Dim strErr As String
        Dim strErrCrlfNon As String
        Dim strErrFileName As String

        Try
            'エラー内容を表示
            strErr = Now & "," & strFunctionName & "," & _
                     Msgex.GetBaseException.ToString

            'ファイル名作成
            strErrFileName = Application.StartupPath & "\" & cstrFileHead & _
                             Date.Today.ToString(cdteFormat) & cstrKakutyousi

            'ファイルがなかった場合は見出しを書き込む
            If System.IO.File.Exists(strErrFileName) = False Then
                My.Computer.FileSystem.WriteAllText(strErrFileName, _
                            cstrFileMidasi & ControlChars.CrLf, True)
            End If

            strErrCrlfNon = strErr.Replace(ControlChars.CrLf, "")
            'エラー内容をファイルに書き込む
            My.Computer.FileSystem.WriteAllText(strErrFileName, strErrCrlfNon _
                                                & ControlChars.CrLf, True)

        Catch ex As Exception
        End Try
    End Sub


    ''' <summary>
    ''' ワードレジスタ算出関数
    ''' </summary>
    ''' <param name="stReg">レジスタ構造体（ST_REG）</param>
    ''' <param name="intExNo">EXNo</param>
    ''' <param name="lngAdr">算出バイトレジスタ</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function fGetWordAddressByte(ByVal stReg As ST_REG, _
                     ByRef intExNo As Integer, ByRef lngAdr As Long) As Boolean
        Dim lngReg As Long
        Dim lngBit As Long
        Dim lngBaseAdr As Long
        Dim iBitWordFlg As Integer

        fGetWordAddressByte = False

        Select Case stReg.intRegType
            Case eRegTypes.P To eRegTypes.M, _
                 eRegTypes.EP To eRegTypes.EM, _
                 eRegTypes.GXY To eRegTypes.GM
                ' ビットレジスタ
                iBitWordFlg = 0
            Case eRegTypes.S To eRegTypes.JL, _
                 eRegTypes.JS, _
                 eRegTypes.ES To eRegTypes.H, _
                 eRegTypes.U To eRegTypes.FR
                iBitWordFlg = 1
        End Select

        If iBitWordFlg = 0 Then
            lngReg = CLng("&H" + stReg.strReg.Substring(0, stReg.strReg.Length _
                                                        - 1))   ' ワードアドレス
            lngBit = CLng("&H" + stReg.strReg.Substring(stReg.strReg.Length _
                                                        - 1))   ' ビットアドレス
        Else
            If stReg.strReg.IndexOf("-") = -1 Then
                lngReg = CLng("&H" + stReg.strReg.Trim) ' ワードアドレス
                lngBit = 0                              ' ビットアドレス
            Else
                ' ワードレジスタ
                Dim aryBuffer As String() = stReg.strReg.Split("-")
                lngReg = CLng("&H" + aryBuffer(0).ToString)
                lngBit = CLng("&H" + aryBuffer(1).ToString)
            End If
        End If

        Select Case stReg.intPGNo
            Case ePgNos.COM
                intExNo = &H0
            Case ePgNos.P1
                intExNo = &HD
            Case ePgNos.P2
                intExNo = &HE
            Case ePgNos.P3
                intExNo = &HF
        End Select

        Select Case stReg.intRegType
            Case eRegTypes.P
                lngAdr = &H0

                If lngReg >= &H100 And lngReg <= &H17F Then
                    ' 拡張
                    lngBaseAdr = &HC000
                    fGetWordAddressByte = True
                End If
            Case eRegTypes.K
                If lngReg >= &H0 And lngReg <= &H2F Then
                    lngBaseAdr = &H40
                    fGetWordAddressByte = True
                End If
            Case eRegTypes.V
                If lngReg >= &H0 And lngReg <= &HF Then
                    lngBaseAdr = &HA0
                    fGetWordAddressByte = True
                End If
                If lngReg >= &H100 And lngReg <= &H17F Then
                    ' 拡張
                    lngBaseAdr = &HC100
                    fGetWordAddressByte = True
                End If
            Case eRegTypes.TC
                If lngReg >= &H0 And lngReg <= &H1F Then
                    lngBaseAdr = &HC0
                    fGetWordAddressByte = True
                End If
                If lngReg >= &H100 And lngReg <= &H17F Then
                    ' 拡張
                    lngBaseAdr = &HC200
                    fGetWordAddressByte = True
                End If
            Case eRegTypes.L
                If lngReg >= &H0 And lngReg <= &H7F Then
                    lngBaseAdr = &H100
                    fGetWordAddressByte = True
                End If
                If lngReg >= &H100 And lngReg <= &H2FF Then
                    ' 拡張
                    lngBaseAdr = &HC400
                    fGetWordAddressByte = True
                End If
            Case eRegTypes.XY
                If lngReg >= &H0 And lngReg <= &H7F Then
                    lngBaseAdr = &H200
                    fGetWordAddressByte = True
                End If
            Case eRegTypes.M
                If lngReg >= &H0 And lngReg <= &H7F Then
                    lngBaseAdr = &H300
                    fGetWordAddressByte = True
                End If
                If lngReg >= &H100 And lngReg <= &H17F Then
                    ' 拡張
                    lngBaseAdr = &HC300
                    fGetWordAddressByte = True
                End If
            Case eRegTypes.S
                If lngReg >= &H0 And lngReg <= &H3FF Then
                    lngBaseAdr = &H400
                    fGetWordAddressByte = True
                End If
                If lngReg >= &H1000 And lngReg <= &H13FF Then
                    ' 拡張
                    lngBaseAdr = &HC800
                    fGetWordAddressByte = True
                End If
            Case eRegTypes.N
                If lngReg >= &H0 And lngReg <= &H1FF Then
                    lngBaseAdr = &HC00
                    fGetWordAddressByte = True
                End If
                If lngReg >= &H1000 And lngReg <= &H17FF Then
                    ' 拡張
                    lngBaseAdr = &HD000
                    fGetWordAddressByte = True
                End If
            Case eRegTypes.R
                If lngReg >= &H0 And lngReg <= &H7FF Then
                    lngBaseAdr = &H1000
                    fGetWordAddressByte = True
                End If
            Case eRegTypes.D
                If lngReg >= &H0 And lngReg <= &H2FFF Then
                    lngBaseAdr = &H2000
                    fGetWordAddressByte = True
                End If
            Case eRegTypes.JL
                If lngReg >= &H0 And lngReg <= &H1FFF Then
                    lngBaseAdr = &H8000
                    fGetWordAddressByte = True
                End If
            Case eRegTypes.JS
                If lngReg >= &H0 And lngReg <= &HFFF Then
                    lngBaseAdr = &HE000
                    fGetWordAddressByte = True
                End If
            Case eRegTypes.EP
                intExNo = &H1
            Case eRegTypes.EK
                intExNo = &H1
                If lngReg >= &H0 And lngReg <= &HFF Then
                    lngBaseAdr = &H200
                    fGetWordAddressByte = True
                End If
            Case eRegTypes.EV
                intExNo = &H1
                If lngReg >= &H0 And lngReg <= &HFF Then
                    lngBaseAdr = &H400
                    fGetWordAddressByte = True
                End If
            Case eRegTypes.ETC
                intExNo = &H1
                If lngReg >= &H0 And lngReg <= &H7F Then
                    lngBaseAdr = &H600
                    fGetWordAddressByte = True
                End If
            Case eRegTypes.EL
                intExNo = &H1
                If lngReg >= &H0 And lngReg <= &H1FF Then
                    lngBaseAdr = &H700
                    fGetWordAddressByte = True
                End If
            Case eRegTypes.EXY
                intExNo = &H1
                If lngReg >= &H0 And lngReg <= &H7F Then
                    lngBaseAdr = &HB00
                    fGetWordAddressByte = True
                End If
            Case eRegTypes.EM
                intExNo = &H1
                If lngReg >= &H0 And lngReg <= &H1FF Then
                    lngBaseAdr = &HC00
                    fGetWordAddressByte = True
                End If
            Case eRegTypes.ES
                intExNo = &H1
                If lngReg >= &H0 And lngReg <= &H7FF Then
                    lngBaseAdr = &H1000
                    fGetWordAddressByte = True
                End If
            Case eRegTypes.EN
                intExNo = &H1
                If lngReg >= &H0 And lngReg <= &H7FF Then
                    lngBaseAdr = &H2000
                    fGetWordAddressByte = True
                End If
            Case eRegTypes.H
                intExNo = &H1
                If lngReg >= &H0 And lngReg <= &H7FF Then
                    lngBaseAdr = &H3000
                    fGetWordAddressByte = True
                End If
            Case eRegTypes.GXY
                intExNo = &H2
                If lngReg >= &H0 And lngReg <= &HFFF Then
                    lngBaseAdr = &HC000
                    fGetWordAddressByte = True
                End If
            Case eRegTypes.GM
                intExNo = &H2
                If lngReg >= &H0 And lngReg <= &HFFF Then
                    lngBaseAdr = &HE000
                    fGetWordAddressByte = True
                End If
            Case eRegTypes.U
                ' EXNo算出
                intExNo = &H3 + CInt(lngReg \ &H8000)

                If intExNo >= &H3 And intExNo <= &H6 Then
                    lngBaseAdr = (lngReg Mod &H8000) * 2
                    lngReg = &H0

                    fGetWordAddressByte = True
                End If
            Case eRegTypes.EB
                ' EXNo算出
                intExNo = &H10 + CInt(lngReg \ &H8000)

                If intExNo >= &H10 And intExNo <= &H17 Then
                    lngBaseAdr = (lngReg Mod &H8000) * 2
                    lngReg = &H0

                    fGetWordAddressByte = True
                End If
            Case eRegTypes.FR
                ' EXNo算出
                intExNo = &H40 + CInt(lngReg \ &H8000)

                If intExNo >= &H40 And intExNo <= &H7F Then
                    lngBaseAdr = (lngReg Mod &H8000) * 2
                    lngReg = &H0

                    fGetWordAddressByte = True
                End If
        End Select

        lngAdr = lngBaseAdr + lngReg * 2
    End Function

    ''' <summary>
    ''' ビットレジスタ算出関数
    ''' </summary>
    ''' <param name="stReg">レジスタ構造体（ST_REG）</param>
    ''' <param name="intExNo">EXNo</param>
    ''' <param name="lngAdr">算出ビットレジスタ</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function fGetWordAddressBit(ByVal stReg As ST_REG, _
                     ByRef intExNo As Integer, ByRef lngAdr As Long) As Boolean
        Dim lngReg As Long
        Dim lngBit As Long
        Dim lngBaseAdr As Long
        Dim iBitWordFlg As Integer

        fGetWordAddressBit = False

        Select Case stReg.intRegType
            Case eRegTypes.P To eRegTypes.M, _
                 eRegTypes.EP To eRegTypes.EM, _
                 eRegTypes.GXY To eRegTypes.GM
                ' ビットレジスタ
                iBitWordFlg = 0
            Case eRegTypes.S To eRegTypes.JL, _
                 eRegTypes.JS, _
                 eRegTypes.ES To eRegTypes.H, _
                 eRegTypes.U To eRegTypes.FR
                iBitWordFlg = 1
        End Select

        If iBitWordFlg = 0 Then
            ' ビットレジスタのビット処理
            lngReg = CLng("&H" + stReg.strReg.Substring(0, stReg.strReg.Length _
                                                        - 1))   ' ワードアドレス
            lngBit = CLng("&H" + stReg.strReg.Substring(stReg.strReg.Length _
                                                        - 1))   ' ビットアドレス
        Else
            ' ワードレジスタのビット処理
            If stReg.strReg.IndexOf("-") = -1 Then
                ' ビット指定がない場合は、0ビット目固定
                lngReg = CLng("&H" + stReg.strReg.Trim) ' ワードアドレス
                lngBit = 0                              ' ビットアドレス
            Else
                ' ビット指定がある場合
                Dim aryBuffer As String() = stReg.strReg.Split("-")
                lngReg = CLng("&H" + aryBuffer(0).ToString)
                lngBit = CLng("&H" + aryBuffer(1).ToString)
            End If
        End If

        Select Case stReg.intPGNo
            Case ePgNos.COM
                intExNo = &H0
            Case ePgNos.P1
                intExNo = &HD
            Case ePgNos.P2
                intExNo = &HE
            Case ePgNos.P3
                intExNo = &HF
        End Select

        Select Case stReg.intRegType
            Case eRegTypes.P
                lngAdr = &H0

                If lngReg >= &H100 And lngReg <= &H17F Then
                    ' 拡張
                    lngBaseAdr = &HC000
                    fGetWordAddressBit = True
                End If
            Case eRegTypes.K
                If lngReg >= &H0 And lngReg <= &H2F Then
                    lngBaseAdr = &H40
                    fGetWordAddressBit = True
                End If
            Case eRegTypes.V
                If lngReg >= &H0 And lngReg <= &HF Then
                    lngBaseAdr = &HA0
                    fGetWordAddressBit = True
                End If
                If lngReg >= &H100 And lngReg <= &H17F Then
                    ' 拡張
                    lngBaseAdr = &HC100
                    fGetWordAddressBit = True
                End If
            Case eRegTypes.TC
                If lngReg >= &H0 And lngReg <= &H1F Then
                    lngBaseAdr = &HC0
                    fGetWordAddressBit = True
                End If
                If lngReg >= &H100 And lngReg <= &H17F Then
                    ' 拡張
                    lngBaseAdr = &HC200
                    fGetWordAddressBit = True
                End If
            Case eRegTypes.L
                If lngReg >= &H0 And lngReg <= &H7F Then
                    lngBaseAdr = &H100
                    fGetWordAddressBit = True
                End If
                If lngReg >= &H100 And lngReg <= &H2FF Then
                    ' 拡張
                    lngBaseAdr = &HC400
                    fGetWordAddressBit = True
                End If
            Case eRegTypes.XY
                If lngReg >= &H0 And lngReg <= &H7F Then
                    lngBaseAdr = &H200
                    fGetWordAddressBit = True
                End If
            Case eRegTypes.M
                If lngReg >= &H0 And lngReg <= &H7F Then
                    lngBaseAdr = &H300
                    fGetWordAddressBit = True
                End If
                If lngReg >= &H100 And lngReg <= &H17F Then
                    ' 拡張
                    lngBaseAdr = &HC300
                    fGetWordAddressBit = True
                End If
            Case eRegTypes.S
                If lngReg >= &H0 And lngReg <= &H3FF Then
                    lngBaseAdr = &H400
                    fGetWordAddressBit = True
                End If
                If lngReg >= &H1000 And lngReg <= &H13FF Then
                    ' 拡張
                    lngBaseAdr = &HC800
                    fGetWordAddressBit = True
                End If
            Case eRegTypes.N
                If lngReg >= &H0 And lngReg <= &H1FF Then
                    lngBaseAdr = &HC00
                    fGetWordAddressBit = True
                End If
                If lngReg >= &H1000 And lngReg <= &H17FF Then
                    ' 拡張
                    lngBaseAdr = &HD000
                    fGetWordAddressBit = True
                End If
            Case eRegTypes.R
                If lngReg >= &H0 And lngReg <= &H7FF Then
                    lngBaseAdr = &H1000
                    fGetWordAddressBit = True
                End If
            Case eRegTypes.D
                If lngReg >= &H0 And lngReg <= &H2FFF Then
                    lngBaseAdr = &H2000
                    fGetWordAddressBit = True
                End If
            Case eRegTypes.JL
                If lngReg >= &H0 And lngReg <= &H1FFF Then
                    lngBaseAdr = &H8000
                    fGetWordAddressBit = True
                End If
            Case eRegTypes.JS
                If lngReg >= &H0 And lngReg <= &HFFF Then
                    lngBaseAdr = &HE000
                    fGetWordAddressBit = True
                End If
            Case eRegTypes.EP
                intExNo = &H1
            Case eRegTypes.EK
                intExNo = &H1
                If lngReg >= &H0 And lngReg <= &HFF Then
                    lngBaseAdr = &H200
                    fGetWordAddressBit = True
                End If
            Case eRegTypes.EV
                intExNo = &H1
                If lngReg >= &H0 And lngReg <= &HFF Then
                    lngBaseAdr = &H400
                    fGetWordAddressBit = True
                End If
            Case eRegTypes.ETC
                intExNo = &H1
                If lngReg >= &H0 And lngReg <= &H7F Then
                    lngBaseAdr = &H600
                    fGetWordAddressBit = True
                End If
            Case eRegTypes.EL
                intExNo = &H1
                If lngReg >= &H0 And lngReg <= &H1FF Then
                    lngBaseAdr = &H700
                    fGetWordAddressBit = True
                End If
            Case eRegTypes.EXY
                intExNo = &H1
                If lngReg >= &H0 And lngReg <= &H7F Then
                    lngBaseAdr = &HB00
                    fGetWordAddressBit = True
                End If
            Case eRegTypes.EM
                intExNo = &H1
                If lngReg >= &H0 And lngReg <= &H1FF Then
                    lngBaseAdr = &HC00
                    fGetWordAddressBit = True
                End If
            Case eRegTypes.ES
                intExNo = &H1
                If lngReg >= &H0 And lngReg <= &H7FF Then
                    lngBaseAdr = &H1000
                    fGetWordAddressBit = True
                End If
            Case eRegTypes.EN
                intExNo = &H1
                If lngReg >= &H0 And lngReg <= &H7FF Then
                    lngBaseAdr = &H2000
                    fGetWordAddressBit = True
                End If
            Case eRegTypes.H
                intExNo = &H1
                If lngReg >= &H0 And lngReg <= &H7FF Then
                    lngBaseAdr = &H3000
                    fGetWordAddressBit = True
                End If
            Case eRegTypes.GXY
                intExNo = &H2
                If lngReg >= &H0 And lngReg <= &HFFF Then
                    lngBaseAdr = &HC000
                    fGetWordAddressBit = True
                End If
            Case eRegTypes.GM
                intExNo = &H2
                If lngReg >= &H0 And lngReg <= &HFFF Then
                    lngBaseAdr = &HE000
                    fGetWordAddressBit = True
                End If
            Case eRegTypes.U
                ' EXNo算出
                intExNo = &H3 + CInt(lngReg \ &H8000)

                If intExNo >= &H3 And intExNo <= &H6 Then
                    lngBaseAdr = (lngReg Mod &H8000) * 2
                    lngReg = &H0

                    fGetWordAddressBit = True
                End If
            Case eRegTypes.EB
                ' EXNo算出
                intExNo = &H10 + CInt(lngReg \ &H8000)

                If intExNo >= &H10 And intExNo <= &H17 Then
                    lngBaseAdr = (lngReg Mod &H8000) * 2
                    lngReg = &H0

                    fGetWordAddressBit = True
                End If
            Case eRegTypes.FR
                ' EXNo算出
                intExNo = &H40 + CInt(lngReg \ &H8000)

                If intExNo >= &H40 And intExNo <= &H7F Then
                    lngBaseAdr = (lngReg Mod &H8000) * 2
                    lngReg = &H0

                    fGetWordAddressBit = True
                End If
        End Select

        ' 先頭間接アドレス（Byte）＊8＋レジスタワード部＊16＋ビット部
        lngAdr = lngBaseAdr * 8 + lngReg * 16 + lngBit

        ' EXNoを3ビット左シフトEXNo left shifted 3 bits
        intExNo = intExNo << 19
    End Function


    ''' <summary>
    ''' ソケットオープン関数
    ''' </summary>
    ''' <param name="strIP">相手先IPアドレス</param>
    ''' <param name="intPort">相手先ポート番号</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function fConnectON(ByVal strIP As String, _
                               ByVal intPort As Integer) As Boolean
        socketTcpCL = New System.Net.Sockets.TcpClient

        Try
            socketTcpCL.SendTimeout = 3000
            socketTcpCL.ReceiveTimeout = 3000

            socketTcpCL.Connect(strIP, intPort)
            fConnectON = True
        Catch ex As Exception
            'mdlCommon.pblnEXFlg = True
            'mdlCommon.psysException = ex
            'mdlCommon.pstrErrorMsg = "clsPC10G.fConnectON"
            pInsErrMsg.psubErrMsgShow(ex, "clsPC10G.fConnectON")
            'fErrMsgShow(ex, "clsPC10G.fConnectON")

            '            socketTcpCL.Close()
            fConnectON = False
        End Try

    End Function

    ''' <summary>
    ''' ソケットクローズ関数
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function fConnectOFF() As Boolean
        Try
            If socketTcpCL IsNot Nothing Then
                socketTcpCL.Close()
            End If

            fConnectOFF = True
        Catch ex As Exception

            socketTcpCL.Close()
            fConnectOFF = False
        End Try
    End Function

    ''' <summary>
    ''' ワードレジスタ読込み関数
    ''' </summary>
    ''' <param name="stReg">レジスタ構造体</param>
    ''' <param name="intWordNum">読込みワード数</param>
    ''' <param name="bytRecvBuffer">読込みデータ（バイト配列）</param>
    ''' <returns>
    ''' 　エラーコード
    ''' </returns>
    ''' <remarks></remarks>
    Public Function fReadWordAdr(ByVal stReg As ST_REG, _
                    ByVal intWordNum As Integer, _
                    ByRef bytRecvBuffer As Byte()) As Integer
        Dim ns As System.Net.Sockets.NetworkStream
        Dim bRET As Boolean
        Dim intEXNo As Integer
        Dim lngAdr As Long
        Dim intLowReg As Short
        Dim intUprReg As Short
        Dim intLowByte As Short
        Dim intUprByte As Short

        fReadWordAdr = pCErrOther

        Try
            bRET = fGetWordAddressByte(stReg, intEXNo, lngAdr)
            If bRET = True Then
                ns = socketTcpCL.GetStream

                CmnLng2IntInt(CInt(lngAdr), intLowReg, intUprReg)
                ' 取得ワード数から取得バイト数に変換
                CmnLng2IntInt(intWordNum * 2, intLowByte, intUprByte)

                ReDim bytSocketFormatRead(10)
                ' "00"
                bytSocketFormatRead(0) = &H0
                ' "00"
                bytSocketFormatRead(1) = &H0
                ' 転送数L（コマンド～取得バイト数）
                bytSocketFormatRead(2) = &H7
                ' 転送数H（コマンド～取得バイト数）
                bytSocketFormatRead(3) = &H0
                ' コマンド
                bytSocketFormatRead(4) = eTcpCmds.PC10_BYTE_READ_S
                ' 先頭バイトアドレスL
                bytSocketFormatRead(5) = CByte(intLowReg)
                ' 先頭バイトアドレスH
                bytSocketFormatRead(6) = CByte(intUprReg)
                ' EX番号
                bytSocketFormatRead(7) = CByte(intEXNo)
                ' "00"
                bytSocketFormatRead(8) = &H0
                ' 取得バイト数L
                bytSocketFormatRead(9) = CByte(intLowByte)
                ' 取得バイト数H
                bytSocketFormatRead(10) = CByte(intUprByte)

                If ns.CanWrite Then
                    ns.Write(bytSocketFormatRead, 0, bytSocketFormatRead.Length)
                Else
                    fReadWordAdr = pCErrDataWrite
                    fConnectOFF()
                    Exit Function
                End If

                ' 送信から受信の間に間隔を設ける
                System.Threading.Thread.Sleep(100)

                If ns.CanRead Then
                    ReDim bytRecvBuffer(socketTcpCL.ReceiveBufferSize)
                    ns.Read(bytRecvBuffer, 0, socketTcpCL.ReceiveBufferSize)
                Else
                    fReadWordAdr = pCErrDataRead
                    fConnectOFF()
                    Exit Function
                End If

                fReadWordAdr = bytRecvBuffer(1)
            Else
                fReadWordAdr = pCErrAddressOver

                fConnectOFF()
            End If

        Catch ex As Exception
            mdlCommon.pblnEXFlg = True
            mdlCommon.psysException = ex
            mdlCommon.pstrErrorMsg = "clsPC10G.fReadWordAdr"
            'Me.fErrMsgShow(ex, "clsPC10G.fReadWordAdr")
            fReadWordAdr = pCErrOther
            fConnectOFF()
        End Try
    End Function

    ''' <summary>
    ''' ワードレジスタ書込み関数
    ''' </summary>
    ''' <param name="stReg">レジスタ構造体（ST_REG）</param>
    ''' <param name="intSendBuffer">書込みデータ配列（１配列ワード）</param> Write data array (1 array word
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function fWriteWordAdr(ByVal stReg As ST_REG, _
                                  ByVal intSendBuffer As Integer()) As Integer
        Dim ns As System.Net.Sockets.NetworkStream
        Dim bRET As Boolean
        Dim intEXNo As Integer
        Dim intByteNum As Integer
        Dim intSendSize As Integer
        Dim lngAdr As Long
        Dim intLowReg As Short
        Dim intUprReg As Short
        Dim intLowByte As Short
        Dim intUprByte As Short
        Dim intLowDat As Short
        Dim intUprDat As Short
        Dim bytRecvBuffer As Byte()

        fWriteWordAdr = pCSuccess
        Try

            bRET = fGetWordAddressByte(stReg, intEXNo, lngAdr)
            If bRET = True Then
                ns = socketTcpCL.GetStream
                intByteNum = intSendBuffer.Length * 2

                ReDim bytSocketFormatWrite(intByteNum + 8)
                intSendSize = intByteNum + 5

                CmnLng2IntInt(CInt(lngAdr), intLowReg, intUprReg)
                CmnLng2IntInt(intSendSize, intLowByte, intUprByte)

                ' "00"
                bytSocketFormatWrite(0) = &H0
                ' "00"
                bytSocketFormatWrite(1) = &H0
                ' 転送数L（コマンド～データn）
                bytSocketFormatWrite(2) = CByte(intLowByte)
                ' 転送数H（コマンド～データn）
                bytSocketFormatWrite(3) = CByte(intUprByte)
                ' コマンド
                bytSocketFormatWrite(4) = eTcpCmds.PC10_BYTE_WRITE_S
                ' 先頭バイトアドレスL
                bytSocketFormatWrite(5) = CByte(intLowReg)
                ' 先頭バイトアドレスH
                bytSocketFormatWrite(6) = CByte(intUprReg)
                ' EX番号
                bytSocketFormatWrite(7) = CByte(intEXNo)
                ' "00"
                bytSocketFormatWrite(8) = &H0

                For i = 0 To intSendBuffer.Length - 1               '配列要素数
                    CmnLng2IntInt(intSendBuffer(i), intLowDat, intUprDat)
                    ' 取得バイト数L
                    bytSocketFormatWrite(9 + 2 * i) = CByte(intLowDat)
                    ' 取得バイト数H
                    bytSocketFormatWrite(10 + 2 * i) = CByte(intUprDat)
                Next i

                ' 送信から受信の間に間隔を設ける
                System.Threading.Thread.Sleep(100)

                If ns.CanWrite Then
                    ns.Write(bytSocketFormatWrite, 0, _
                             bytSocketFormatWrite.Length)
                    If ns.CanRead Then
                        ReDim bytRecvBuffer(socketTcpCL.ReceiveBufferSize)

                        ns.Read(bytRecvBuffer, 0, socketTcpCL.ReceiveBufferSize)

                        fWriteWordAdr = bytRecvBuffer(1)
                    Else
                        fWriteWordAdr = pCErrDisConnect

                        fConnectOFF()
                        Exit Function
                    End If
                Else
                    fWriteWordAdr = pCErrDataWrite

                    fConnectOFF()
                    Exit Function
                End If
            Else
                fWriteWordAdr = pCErrAddressOver
                fConnectOFF()
            End If

        Catch ex As Exception
            mdlCommon.pblnEXFlg = True
            mdlCommon.psysException = ex
            mdlCommon.pstrErrorMsg = "clsPC10G.fWriteWordAdr"
            'Me.fErrMsgShow(ex, "clsPC10G.fWriteWordAdr")
            fWriteWordAdr = pCErrOther
            fConnectOFF()
        End Try

    End Function

    ''' <summary>
    ''' ビットレジスタ読込み関数
    ''' </summary>
    ''' <param name="stReg">レジスタ構造体（ST_REG）</param>
    ''' <returns>
    ''' 　ビット情報（0:OFF　1:ON）
    ''' </returns>
    ''' <remarks></remarks>
    Public Function fReadBitAdr(ByVal stReg As ST_REG) As Integer
        Dim ns As System.Net.Sockets.NetworkStream
        Dim bRET As Boolean
        Dim intEXNo As Integer
        Dim lngAdr As Long
        Dim lngBitAdr As Long
        Dim bytRecvBuffer As Byte() = {0}
        fReadBitAdr = -1

        Try

            bRET = fGetWordAddressBit(stReg, intEXNo, lngAdr)
            If bRET = True Then
                ns = socketTcpCL.GetStream

                ' ビットアドレス計算
                lngBitAdr = intEXNo + lngAdr

                ReDim bytSocketFormatRead(12)
                ' "00"
                bytSocketFormatRead(0) = &H0
                ' "00"
                bytSocketFormatRead(1) = &H0
                ' 転送数L（コマンド～取得バイト数）
                bytSocketFormatRead(2) = &H9
                ' 転送数H（コマンド～取得バイト数）
                bytSocketFormatRead(3) = &H0
                ' コマンド（多点読出し）
                bytSocketFormatRead(4) = eTcpCmds.PC10_BYTE_READ_M
                ' ビット点数
                bytSocketFormatRead(5) = 1
                ' バイト点数
                bytSocketFormatRead(6) = 0
                ' ワード点数
                bytSocketFormatRead(7) = 0
                ' ロング点数
                bytSocketFormatRead(8) = 0
                ' 下位ワードLOW
                bytSocketFormatRead(9) = lngBitAdr And &HFF
                ' 下位ワードHIGH
                bytSocketFormatRead(10) = (lngBitAdr And &HFF00) / &H100
                ' 上位ワードLOW
                bytSocketFormatRead(11) = (lngBitAdr And &HFF0000) / &H10000
                ' 上位ワードHIGH
                bytSocketFormatRead(12) = (lngBitAdr And &HFF000000) / &H1000000

                If ns.CanWrite Then
                    ns.Write(bytSocketFormatRead, 0, bytSocketFormatRead.Length)
                Else
                    fConnectOFF()
                    Exit Function
                End If

                ' 送信から受信の間に間隔を設ける
                System.Threading.Thread.Sleep(100)

                If ns.CanRead Then
                    ReDim bytRecvBuffer(socketTcpCL.ReceiveBufferSize)
                    ns.Read(bytRecvBuffer, 0, socketTcpCL.ReceiveBufferSize)
                Else
                    fConnectOFF()
                    Exit Function
                End If

                fReadBitAdr = True
            Else
                fConnectOFF()
            End If
            fReadBitAdr = bytRecvBuffer(9)

        Catch ex As Exception
            mdlCommon.pblnEXFlg = True
            mdlCommon.psysException = ex
            mdlCommon.pstrErrorMsg = "clsPC10G.fReadBitAdr"
            'Me.fErrMsgShow(ex, "clsPC10G.fReadBitAdr")
            fConnectOFF()
        End Try
    End Function

    ''' <summary>
    ''' ビットレジスタ書込み関数
    ''' </summary>
    ''' <param name="stReg">レジスタ構造体（ST_REG）</param>
    ''' <param name="bytBit">書込みビット情報（0or1）</param>
    ''' <returns>書込みリターンコード</returns>
    ''' <remarks></remarks>
    Public Function fWriteBitAdr(ByVal stReg As ST_REG, _
                                 ByVal bytBit As Byte) As Integer
        Dim ns As System.Net.Sockets.NetworkStream
        Dim bRET As Boolean
        Dim intEXNo As Integer
        Dim lngAdr As Long
        Dim lngBitAdr As Long
        Dim bytRecvBuffer As Byte() = {0}

        fWriteBitAdr = -1

        Try

            bRET = fGetWordAddressBit(stReg, intEXNo, lngAdr)
            If bRET = True Then
                ns = socketTcpCL.GetStream

                ' ビットアドレス計算
                lngBitAdr = intEXNo + lngAdr

                ReDim bytSocketFormatWrite(13)
                ' "00"
                bytSocketFormatWrite(0) = &H0
                ' "00"
                bytSocketFormatWrite(1) = &H0
                ' 転送数L（コマンド～取得バイト数）
                bytSocketFormatWrite(2) = &HA
                ' 転送数H（コマンド～取得バイト数）
                bytSocketFormatWrite(3) = &H0
                ' コマンド（多点書出し）
                bytSocketFormatWrite(4) = eTcpCmds.PC10_BYTE_WRITE_M
                ' ビット点数
                bytSocketFormatWrite(5) = 1
                ' バイト点数
                bytSocketFormatWrite(6) = 0
                ' ワード点数
                bytSocketFormatWrite(7) = 0
                ' ロング点数
                bytSocketFormatWrite(8) = 0
                ' 下位ワードLOW
                bytSocketFormatWrite(9) = lngBitAdr And &HFF
                ' 下位ワードHIGH
                bytSocketFormatWrite(10) = (lngBitAdr And &HFF00) / &H100
                ' 上位ワードLOW
                bytSocketFormatWrite(11) = (lngBitAdr And &HFF0000) / &H10000
                ' 上位ワードHIGH
                bytSocketFormatWrite(12) = (lngBitAdr And _
                                            &HFF000000) / &H1000000
                bytSocketFormatWrite(13) = bytBit

                If ns.CanWrite Then
                    ns.Write(bytSocketFormatWrite, 0, _
                             bytSocketFormatWrite.Length)
                Else
                    fConnectOFF()
                    Exit Function
                End If

                ' 送信から受信の間に間隔を設ける
                System.Threading.Thread.Sleep(100)

                If ns.CanRead Then
                    ReDim bytRecvBuffer(socketTcpCL.ReceiveBufferSize)
                    ns.Read(bytRecvBuffer, 0, socketTcpCL.ReceiveBufferSize)
                Else
                    fConnectOFF()
                    Exit Function
                End If

                fWriteBitAdr = True
            Else
                fConnectOFF()
            End If
            fWriteBitAdr = bytRecvBuffer(1)

        Catch ex As Exception
            mdlCommon.pblnEXFlg = True
            mdlCommon.psysException = ex
            mdlCommon.pstrErrorMsg = "clsPC10G.fWriteBitAdr"
            'Me.fErrMsgShow(ex, "clsPC10G.fWriteBitAdr")
            fConnectOFF()
        End Try
    End Function

    ''' <summary>
    ''' 車種情報読込み関数1
    ''' </summary>
    ''' <param name="strIP">IPｱﾄﾞﾚｽ（String）</param>
    ''' <param name="iPort">ﾎﾟｰﾄ番号（Integer）</param>
    ''' <param name="stReg">ﾚｼﾞｽﾀ構造体（ST_REG）</param>
    ''' <param name="iWord">?????（Integer）</param>
    ''' <param name="iLen">?????（Integer）</param>
    ''' <returns>読込み車種名</returns>
    ''' <remarks></remarks>
    Public Function fGetVehicleInfo(ByVal strIP As String, _
                    ByVal iPort As Integer, ByVal stREG As ST_REG, _
                    ByVal iWord As Integer, ByVal iLen As Integer) As String()

        Dim bRET As Boolean
        Dim bytBuffer(0 To 2048) As Byte
        Dim bytVehicleBuf(0 To 2048) As Byte

        Dim iRET As Integer
        Dim i As Integer
        Dim j As Integer
        Dim strBuffer As String = ""
        Dim strVehicle(0) As String
        Dim iVehicleNum As Integer
        Dim iRecvNum As Integer
        Dim iLoopNum As Integer
        Dim iHex As Integer

        fGetVehicleInfo = Nothing

        Try
            If iWord * 2 > &H3F0 Then
                iLoopNum = iWord \ 256
                iRecvNum = iWord \ iLoopNum
            Else
                iLoopNum = 1
                iRecvNum = iWord
            End If

            iVehicleNum = (iWord * 2) \ iLen
            ReDim Preserve strVehicle(0 To iVehicleNum)

            bRET = fConnectON(strIP, iPort)
            If bRET = True Then
                For j = 1 To iLoopNum
                    ReDim bytBuffer(0 To 5 + iRecvNum * 2)
                    iHex = "&H" + stREG.strReg
                    iHex = iHex + iRecvNum * (j - 1)

                    stREG.strReg = Hex(iHex)
                    iRET = fReadWordAdr(stREG, iRecvNum, bytBuffer)
                    If iRET = pCSuccess Then
                        For i = 5 To (iRecvNum * 2) - 1 + 5
                            If CmnCheckChar(CInt(bytBuffer(i))) = _
                               eChkStr.Control Or bytBuffer(i) = 0 Or _
                               (CmnCheckChar(CInt(bytBuffer(i)))) = vbNull Then
                                strBuffer = strBuffer + " "
                            Else
                                strBuffer = strBuffer + Chr(bytBuffer(i))
                            End If
                        Next i
                    End If
                Next j

                For i = 0 To iVehicleNum    '車種数分ﾙｰﾌﾟする
                    'ﾊﾞｯﾌｧを4文字刻みで車種名配列に格納 จัดเก็บบัฟเฟอร์ไว้ในอาร์เรย์ชื่อชื่อยานพาหนะโดยเพิ่มทีละ 4 ตัว
                    strVehicle(i) = Mid(strBuffer, (i * iLen) + 1, 4).Trim
                Next i
                fGetVehicleInfo = strVehicle
            End If

            fConnectOFF()

        Catch ex As Exception
            mdlCommon.pblnEXFlg = True
            mdlCommon.psysException = ex
            mdlCommon.pstrErrorMsg = "clsPC10G.fGetVehicleInfo"
            'pInsErrMsg.psubErrMsgShow(ex, "clsPC10G.fGetVehicleInfo")
            fConnectOFF()
        End Try
    End Function

    ''' <summary>
    ''' 車種情報読込み関数2
    ''' </summary>
    ''' <param name="stReg">ﾚｼﾞｽﾀ構造体（ST_REG）</param>
    ''' <param name="iWord">車種数（Integer）</param>
    ''' <param name="iLen">車種名文字数（Integer）</param>
    ''' <returns>読込み車種名</returns>
    ''' <remarks></remarks>
    Public Function fGetVehicleInfo2(ByVal stREG As ST_REG, _
                    ByVal iWord As Integer, ByVal iLen As Integer) As String()
        Dim bytBuffer(0 To 2048) As Byte
        Dim bytVehicleBuf(0 To 2048) As Byte

        Dim iRET As Integer
        Dim i As Integer
        Dim j As Integer
        Dim strBuffer As String = ""
        Dim strVehicle(0) As String
        Dim iVehicleNum As Integer
        Dim iRecvNum As Integer
        Dim iLoopNum As Integer
        Dim iHex As Integer

        fGetVehicleInfo2 = Nothing

        Try
            If iWord * 2 > &H3F0 Then
                iLoopNum = iWord \ 256
                iRecvNum = iWord \ iLoopNum
            Else
                iLoopNum = 1
                iRecvNum = iWord
            End If

            iVehicleNum = (iWord * 2) \ iLen            '車種の登録件数 จำนวนยานพาหนะที่จดทะเบียน
            ReDim Preserve strVehicle(0 To iVehicleNum) '車種名を格納する配列

            For j = 1 To iLoopNum                       '256ﾊﾞｲﾄずつ取り込む回数 จำนวนครั้งที่จะจับภาพ 256 ไบต์ต่อครั้ง
                ReDim bytBuffer(0 To 5 + iRecvNum * 2)  '車種数×2＋6ﾊﾞｲﾄ←? 
                '前回(初期)のｱﾄﾞﾚｽ開始位置の算出 การคำนวณตำแหน่งเริ่มต้นของที่อยู่ของก่อนหน้า (เริ่มต้น)
                iHex = "&H" + stREG.strReg
                '今回のｱﾄﾞﾚｽ開始位置の算出การคำนวณตำแหน่งเริ่มต้นของที่อยู่ปัจจุบัน
                iHex = iHex + iRecvNum * (j - 1)
                '今回のｱﾄﾞﾚｽ開始位置の設定 ตั้งค่าตำแหน่งเริ่มต้นของที่อยู่ในขณะนี้
                stREG.strReg = Hex(iHex)
                '車種名ﾃﾞｰﾀﾃﾞｰﾀ取得 การได้มาซึ่งข้อมูลข้อมูลชื่อโมเดล Shashu-mei dētadēta shutoku
                iRET = fReadWordAdr(stREG, iRecvNum, bytBuffer)
                If iRET = pCSuccess Then
                    For i = 5 To (iRecvNum * 2) - 1 + 5 '256ﾊﾞｲﾄ分のﾙｰﾌﾟ
                        If CmnCheckChar(CInt(bytBuffer(i))) = _
                           eChkStr.Control Then  '制御記号が入っていた場合
                            strBuffer = strBuffer + " " 'ｽﾍﾟｰｽを入れる
                        Else
                            'ﾊﾞｯﾌｧの値を格納
                            strBuffer = strBuffer + Chr(bytBuffer(i))
                        End If
                    Next i
                End If
            Next j

            For i = 0 To iVehicleNum    '車種数分ﾙｰﾌﾟする
                'ﾊﾞｯﾌｧを4文字刻みで車種名配列に格納
                strVehicle(i) = Mid(strBuffer, (i * iLen) + 1, 4).Trim
            Next i
            fGetVehicleInfo2 = strVehicle


        Catch ex As Exception
            mdlCommon.pblnEXFlg = True
            mdlCommon.psysException = ex
            mdlCommon.pstrErrorMsg = "clsPC10G.fGetVehicleInfo2"
            'pInsErrMsg.psubErrMsgShow(ex, "clsPC10G.fGetVehicleInfo2")
            fConnectOFF()
        End Try
    End Function

    ' PLC内蔵時計を書き換える
    ''' <summary>
    ''' パソコン内蔵時刻でPLC内蔵時計を書き換える
    ''' </summary>
    ''' <returns>
    ''' 成功　　　　　　：pCSuccess
    ''' コマンド転送異常：pCErrDataWrite
    ''' 電文異常　　　　：pCErrAnser
    ''' 応答電文受信異常：pCEeeDataRead
    ''' </returns>
    ''' <remarks></remarks>
    ''' 
    Public Function fSetPlcClock() As Integer
        Dim ns As System.Net.Sockets.NetworkStream
        Dim dtDateNow As DateTime = DateTime.Now
        Dim bytReciveBuffer As Byte()

        ReDim bytSocketFormatRead(0 To 13)
        ' "00"
        bytSocketFormatRead(0) = &H0
        ' "00"
        bytSocketFormatRead(1) = &H0
        ' 転送数L（コマンド～取得バイト数）
        bytSocketFormatRead(2) = &HA
        ' 転送数H（コマンド～取得バイト数）
        bytSocketFormatRead(3) = &H0
        ' コマンド
        bytSocketFormatRead(4) = eTcpCmds.PC10_CLOCK
        ' サブコマンド１
        bytSocketFormatRead(5) = eTcpCmds.PC10_CLOCK_WRITE
        ' サブコマンド２
        bytSocketFormatRead(6) = &H0
        ' 秒(BCD)
        bytSocketFormatRead(7) = mdlCommon.CmnInt2BCD(dtDateNow.Second)
        ' 分(BCD)
        bytSocketFormatRead(8) = CmnInt2BCD(dtDateNow.Minute)
        ' 時(24H)(BCD)
        bytSocketFormatRead(9) = CmnInt2BCD(dtDateNow.Hour)
        ' 日(BCD)
        bytSocketFormatRead(10) = CmnInt2BCD(dtDateNow.Day)
        ' 月(BCD)
        bytSocketFormatRead(11) = CmnInt2BCD(dtDateNow.Month)
        ' 年(下2桁)(BCD)
        bytSocketFormatRead(12) = CmnInt2BCD(dtDateNow.Year - 2000)
        ' 曜日(BCD)
        bytSocketFormatRead(13) = CmnInt2BCD(dtDateNow.DayOfWeek)

        ns = socketTcpCL.GetStream

        If ns.CanWrite Then
            ns.Write(bytSocketFormatRead, 0, bytSocketFormatRead.Length)
        Else
            fSetPlcClock = pCErrDataWrite
            fConnectOFF()
            Exit Function
        End If

        If ns.CanRead Then
            ReDim bytReciveBuffer(socketTcpCL.ReceiveBufferSize)
            ns.Read(bytReciveBuffer, 0, socketTcpCL.ReceiveBufferSize)
            If bytReciveBuffer(1) <> 0 Then
                fSetPlcClock = pCErrAnser
                fConnectOFF()
                Exit Function
            End If
        Else
            fSetPlcClock = pCErrDataRead
            fConnectOFF()
            Exit Function
        End If

    End Function

    ' PLC内蔵時計を読み出す
    ''' <summary>
    ''' 現在のPLC内蔵時刻を取得する
    ''' </summary>
    ''' <param name="stPlcDateTime">PLC時刻構造体(_stPlcDateTime)</param>
    ''' <returns>
    ''' 成功　　　　　　：pCSuccess
    ''' コマンド転送異常：pCErrDataWrite
    ''' 電文異常　　　　：pCErrAnser
    ''' 応答電文受信異常：pCErrDataRead
    ''' </returns>
    ''' <remarks></remarks>
    Public Function fGetPlcClock(ByRef stPlcDateTime As _stPlcDateTime) _
                                                                    As Integer
        Dim ns As System.Net.Sockets.NetworkStream
        Dim dtDateNow As DateTime = DateTime.Now
        Dim bytReciveBuffer As Byte()

        fGetPlcClock = pCSuccess

        ReDim bytSocketFormatRead(0 To 6)
        stPlcDateTime = New _stPlcDateTime(0)
        ' "00"
        bytSocketFormatRead(0) = &H0
        ' "00"
        bytSocketFormatRead(1) = &H0
        ' 転送数L（コマンド～取得バイト数）
        bytSocketFormatRead(2) = &H3
        ' 転送数H（コマンド～取得バイト数）
        bytSocketFormatRead(3) = &H0
        ' コマンド
        bytSocketFormatRead(4) = eTcpCmds.PC10_CLOCK
        ' サブコマンド１
        bytSocketFormatRead(5) = eTcpCmds.PC10_CLOCK_READ
        ' サブコマンド２
        bytSocketFormatRead(6) = &H0

        ns = socketTcpCL.GetStream

        If ns.CanWrite Then
            ns.Write(bytSocketFormatRead, 0, bytSocketFormatRead.Length)
        Else
            fGetPlcClock = pCErrDataWrite
            fConnectOFF()
            Exit Function
        End If

        If ns.CanRead Then
            ReDim bytReciveBuffer(socketTcpCL.ReceiveBufferSize)
            ns.Read(bytReciveBuffer, 0, socketTcpCL.ReceiveBufferSize)
            If bytReciveBuffer(1) <> 0 Then
                fGetPlcClock = pCErrAnser
                fConnectOFF()
                Exit Function
            Else
                stPlcDateTime.iYear = CmnBCD2Int(bytReciveBuffer(12))
                stPlcDateTime.iMonth = CmnBCD2Int(bytReciveBuffer(11))
                stPlcDateTime.iDay = CmnBCD2Int(bytReciveBuffer(10))
                stPlcDateTime.iHour = CmnBCD2Int(bytReciveBuffer(9))
                stPlcDateTime.iMinute = CmnBCD2Int(bytReciveBuffer(8))
                stPlcDateTime.iSecond = CmnBCD2Int(bytReciveBuffer(7))
                stPlcDateTime.iDayOfWeek = CmnBCD2Int(bytReciveBuffer(13))
            End If
        Else
            fGetPlcClock = pCErrDataRead
            fConnectOFF()
            Exit Function
        End If
    End Function

    Public Function fncPGNoGet(ByVal strPGNoString As String) As Integer

        Select Case strPGNoString
            Case "COM" : fncPGNoGet = 0
            Case "P1" : fncPGNoGet = 1
            Case "P2" : fncPGNoGet = 2
            Case "P3" : fncPGNoGet = 3
            Case Else : fncPGNoGet = -1
        End Select

    End Function

    Public Function fncRegTypeGet(ByVal strTypeString As String) As Integer

        Select Case strTypeString
            Case "P" : fncRegTypeGet = 1
            Case "K" : fncRegTypeGet = 2
            Case "V" : fncRegTypeGet = 3
            Case "TC" : fncRegTypeGet = 4
            Case "L" : fncRegTypeGet = 5
            Case "XY" : fncRegTypeGet = 6
            Case "M" : fncRegTypeGet = 7
            Case "S" : fncRegTypeGet = 8
            Case "N" : fncRegTypeGet = 9
            Case "R" : fncRegTypeGet = 10
            Case "D" : fncRegTypeGet = 11
            Case "JL" : fncRegTypeGet = 12
            Case "JS" : fncRegTypeGet = 13
            Case "EP" : fncRegTypeGet = 14
            Case "EK" : fncRegTypeGet = 15
            Case "EV" : fncRegTypeGet = 16
            Case "ETC" : fncRegTypeGet = 17
            Case "EL" : fncRegTypeGet = 18
            Case "EXY" : fncRegTypeGet = 19
            Case "EM" : fncRegTypeGet = 20
            Case "ES" : fncRegTypeGet = 21
            Case "EN" : fncRegTypeGet = 22
            Case "H" : fncRegTypeGet = 23
            Case "GXY" : fncRegTypeGet = 24
            Case "GM" : fncRegTypeGet = 25
            Case "U" : fncRegTypeGet = 26
            Case "EB" : fncRegTypeGet = 27
            Case "FR" : fncRegTypeGet = 28
            Case Else : fncRegTypeGet = 0
        End Select

    End Function

    Private Sub lblMessage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblMessage.Click

    End Sub

    Private Sub clsPc10G_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class

