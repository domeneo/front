﻿Imports System
Imports System.Text
Imports System.Runtime.InteropServices

Public Class clsApi

    Const SETON As Integer = 1
    Const SETOFF As Integer = 0
    Const SPI_SETLOWPOWERTIMEOUT As Integer = 81
    Const SPI_SETPOWEROFFTIMEOUT As Integer = 82
    Const SPI_SETLOWPOWERACTIVE As Integer = 85
    Const SPI_SETPOWEROFFACTIVE As Integer = 86
    Const SPI_GETPOWEROFFACTIVE As Integer = 84

    Const HWND_BROADCAST As Long = &HFFFF
    Const WM_SYSCOMMAND As Long = &H112
    Const SC_MONITORPOWER As Long = &HF170
    Const SC_POWEROFF As Long = 2
    Const SC_POWERON As Long = -1


    Public Declare Auto Function GetPrivateProfileString Lib "Kernel32" _
        Alias "GetPrivateProfileString" ( _
        <MarshalAs(UnmanagedType.LPTStr)> ByVal lpApplicationName As String, _
        <MarshalAs(UnmanagedType.LPTStr)> ByVal lpKeyName As String, _
        <MarshalAs(UnmanagedType.LPTStr)> ByVal lpDefault As String, _
        <MarshalAs(UnmanagedType.LPTStr)> ByVal lpReturnedString As StringBuilder, _
        ByVal nSize As UInt16, _
        <MarshalAs(UnmanagedType.LPTStr)> ByVal lpFileName As String) As UInt32

    Public Declare Auto Function GetPrivateProfileInt Lib "Kernel32" _
        Alias "GetPrivateProfileInt" ( _
        <MarshalAs(UnmanagedType.LPTStr)> ByVal lpApplicationName As String, _
        <MarshalAs(UnmanagedType.LPTStr)> ByVal lpKeyName As String, _
        ByVal nDefault As UInt16, _
        <MarshalAs(UnmanagedType.LPTStr)> ByVal lpFileName As String) As UInt32

    Public Declare Auto Function WritePrivateProfileString Lib "Kernel32.dll" _
            Alias "WritePrivateProfileString" ( _
            <MarshalAs(UnmanagedType.LPTStr)> ByVal lpAppName As String, _
            <MarshalAs(UnmanagedType.LPTStr)> ByVal lpKeyName As String, _
            <MarshalAs(UnmanagedType.LPTStr)> ByVal lpString As String, _
            <MarshalAs(UnmanagedType.LPTStr)> ByVal lpFileName As String) As UInt32

    <DllImport("user32.dll")> _
    Shared Function SendMessage( _
        ByVal hWnd As Integer, _
        ByVal MSG As Integer, _
        ByVal wParam As Integer, _
        ByVal lParam As Integer) As Integer
    End Function


    <DllImport("user32.dll")> _
    Shared Function EnumDisplayMonitors( _
        ByVal hdc As IntPtr, _
        ByVal lprcClip As IntPtr, _
        ByVal lpfnEnum As MonitorEnumProc, _
        ByRef dwData As IntPtr) _
        As Boolean
    End Function

    '  EnumDisplayMonitorsからのコールバック
    Delegate Function MonitorEnumProc( _
         ByVal hMonitor As IntPtr, _
         ByVal hdcMonitor As IntPtr, _
         ByVal lprcMonitor As IntPtr, _
         ByRef dwData As IntPtr) _
         As Boolean

    <DllImport("user32.dll")> _
    Shared Function GetMonitorInfo( _
        ByVal hMonitor As Integer, _
        ByRef lpmi As MONITORINFO) As Integer
    End Function

    <DllImport("kernel32.dll")> _
    Shared Sub ZeroMemory( _
        ByVal Destination As Long, _
        ByVal Length As Integer)
    End Sub


    <DllImport("kernel32.dll")> _
    Shared Function GetDevicePowerState( _
        ByVal hDevice As Integer, _
        ByRef pfOn As Integer) As Integer
    End Function

    <DllImport("user32.dll")> _
    Shared Function SystemParametersInfo( _
        ByVal uAction As Integer, _
        ByVal uParam As Integer, _
        ByVal pvParam As Integer, _
        ByVal fuWinIni As Integer) As Integer
    End Function

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Auto)> _
    Structure MONITORINFO
        Public cbSize As Integer
        Public rcMonitor As RECT
        Public rcWork As RECT
        Public dwFlags As Integer
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=32)> _
        Public szDevice As String
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Auto)> _
    Structure RECT
        Dim Left As Integer            ' 矩形の左上隅のX座標
        Dim Top As Integer             ' 同、Y座標
        Dim Right As Integer           ' 矩形の右下隅のX座標
        Dim Bottom As Integer          ' 同、Y座標
    End Structure

    Public Function GetMonitor(ByVal hMonitor As IntPtr, _
                                ByVal hdcMonitor As IntPtr, _
                                ByVal lprcMonitor As IntPtr, _
                                ByRef dwData As IntPtr) As Boolean


        Dim info As New MONITORINFO

        info.cbSize = Marshal.SizeOf(info)

        Dim ret As Integer = GetMonitorInfo(hMonitor, info)

        ' プライマリモニタの場合
        If info.dwFlags = 0 Then
            Select Case info.dwFlags
                Case 0
                    ' プライマリモニタの電源状態を取得する
                    Dim monitorPowerStatus As Integer
                    ret = GetDevicePowerState(hMonitor, monitorPowerStatus)

                    ' 電源がONの場合
                    If ret = 0 Then
                        ' 電源を切る
                        Debug.Print(info.rcMonitor.ToString)
                        Dim iRet As IntPtr = SendMessage(hMonitor, WM_SYSCOMMAND, SC_MONITORPOWER, SC_POWEROFF)
                    Else
                        Dim iRet As IntPtr = SendMessage(hMonitor, WM_SYSCOMMAND, SC_MONITORPOWER, SC_POWERON)
                    End If
                Case 1
                    ' セカンダリモニタの電源状態を取得する
                    Dim monitorPowerStatus As Integer
                    ret = GetDevicePowerState(hMonitor, monitorPowerStatus)

                    ' 電源がONの場合
                    If ret = 0 Then
                        ' 電源を切る
                        Debug.Print(info.rcMonitor.ToString)
                        Dim iRet As IntPtr = SendMessage(hMonitor, WM_SYSCOMMAND, SC_MONITORPOWER, SC_POWEROFF)
                    Else
                        Dim iRet As IntPtr = SendMessage(hMonitor, WM_SYSCOMMAND, SC_MONITORPOWER, SC_POWERON)
                    End If
            End Select

        End If

        Return True

    End Function


End Class
