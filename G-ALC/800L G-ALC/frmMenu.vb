﻿Imports System
Imports MySql.Data.MySqlClient
Imports System.Threading
Imports System.Text.Encoding
Imports System.ComponentModel

Public Class frmMenu

    Delegate Sub SetTextCallback(ByVal strText As String)

    Private pintRecvCount As Integer = 0
    Private pstrVehicleName As String
    Private pbytRecvBuffer(255) As Byte
    Private pbytSendBuffer(25) As Byte
    Private pblnDisconnectFlg As Boolean
    Private blnSetFocusFlg As Boolean
    Private strControlName As String
    Private blnNotNumericFlg As Boolean
    Private pintNextNo As Integer
    Private Listner As System.Net.Sockets.TcpListener
    Private pflgSequenceErr As Boolean

    Private Sub frmMenu_Activated(ByVal sender As Object,
                ByVal e As System.EventArgs) Handles Me.Activated

        If blnSetFocusFlg Then
            Select Case strControlName
                Case "txtBcNextRecv"
                    txtBcNextRecv.Focus()
            End Select
            blnSetFocusFlg = False
        End If

    End Sub

    Private Sub frmMenu_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles Me.DragDrop

    End Sub

    Private Sub frmMenu_Load(ByVal sender As Object,
                ByVal e As System.EventArgs) Handles Me.Load


        Control.CheckForIllegalCrossThreadCalls = False

        loadSERVERSQL()


        Try
            SaveMSG("Open GALC", "ERR")
        Catch ex As Exception

        End Try

        Dim TDS2 As TimerCallback = AddressOf CheckSTRUCK
        timer_checkstruck = New Threading.Timer(TDS2, Nothing, 1000, 1000)


        btnGalcDisconnect.Enabled = False
        btnPlcDisconnect.Enabled = False
        btnGalcConnect.Enabled = True
        btnPlcConnect.Enabled = True
        mdlCommon.pblnEXFlg = False
        mdlCommon.pstrErrorMsg = ""
        pblnDisconnectFlg = False

        For i As Integer = 1 To 65536

        Next
        pWatetimer = 0      '2011/10/14 Kasuga

        '環境設定ﾌｧｲﾙ読み込み
        Call clsPublicIni.psubIniLoad()
        If mdlCommon.pstrErrorMsg <> "" Then
            Call psubBgwErrorMsg()
            Exit Sub
        End If
        '環境設定ﾌｧｲﾙから読み込んだﾚｼﾞｽﾀ情報の種別とｱﾄﾞﾚｽへの分割
        Call subIniDataChange()

        txtLastSEQ.Text = pstAlcRecv.strLastSEQ

        'ﾃﾞｰﾀﾍﾞｰｽ接続処理
        pClsSql.pSQLOpen(pstIni.strDbIp, pstIni.strDbName, pstIni.strDbUser,
                         pstIni.strDbPass)
        If pClsSql.blnDbStatus = False Then
            'mdlCommon.pstrErrorMsg = "DBが正しくオープン出来ませんでした。" + _
            'vbCrLf + "DBの環境設定が正しいか確認してください。"
            mdlCommon.pstrErrorMsg = "DB was not able to be opened correctly." _
                                   + vbCrLf + "Please confirm whether " _
                                   + "the environmental setting of DB " _
                                   + "correct. "
            pInsErrMsg.psubErrMsgShow(mdlCommon.pstrErrorMsg)
            Exit Sub
        End If

        lblBcRecv.Text = pstIni.strBcNo             '受信済BC連番をｾｯﾄ
        txtBcNextRecv.Text = pstIni.strBcNo + 1     '受信済BC連番+1をｾｯﾄ

        If pClsSql.fGetVehicleData() = pCSuccess Then '車種ﾃﾞｰﾀ取り込み(DB→PC)
            If pintVehicleCount > 0 Then
                '車種名登録が1件でもあれば､PLCからの車種名取込は行わない
                Exit Sub
            End If
        End If

        '生産指示接続処理
        If pClsPC10G.fConnectON(pstIni.strPlcIp, pstIni.strPlcPort) Then
            If mdlCommon.pstrErrorMsg <> "" Then
                Call psubBgwErrorMsg()
                GoTo End_Label
            End If

            '車種ﾃﾞｰﾀ取込み処理(PLC→PC)
            Call psubGetVehicle()
            If mdlCommon.pstrErrorMsg <> "" Then
                Call psubBgwErrorMsg()
                GoTo End_Label
            End If

        End If
        lblBcRecv.Text = pstIni.strBcNo             '受信済BC連番をｾｯﾄ
        txtBcNextRecv.Text = pstIni.strBcNo + 1     '受信済BC連番+1をｾｯﾄ

        If pClsSql.fGetVehicleData() = pCSuccess Then '車種ﾃﾞｰﾀ取り込み(DB→PC)
            If pintVehicleCount > 0 Then
                '車種名登録が1件でもあれば､PLCからの車種名取込は行わない
                Exit Sub
            End If
        End If



End_Label:

        '生産指示切断処理
        pClsPC10G.fConnectOFF()

    End Sub

    Private Sub btnGalcConnect_Click(ByVal sender As Object,
                ByVal e As System.EventArgs) Handles btnGalcConnect_kako.Click

        If btnGalcConnect_kako.Enabled = False Then
            Exit Sub
        End If
        pintNextNo = txtBcNextRecv.Text
        MsgBox("The connection begins.")
        If Not bgwGalcConnect.IsBusy Then
            txtBcNextRecv.Enabled = False
            btnGalcConnect_kako.Enabled = False
            btnGalcDisconnect_kako.Enabled = True
            Me.Refresh()

            lblGalcStatus.BackColor = Color.Yellow
            Call psubSetlblGalcStatus("Waiting")
            'Call psubSetlblGalcStatus("Espera")    'ポルトガル対応

            'Galc使用ﾎﾟｰﾄでﾘｽﾅｰを起動
            Listner = New System.Net.Sockets.TcpListener(System.Net.IPAddress.Any,
                                                         pstIni.strAlcPort)
            Listner.Start()

            bgwGalcConnect.RunWorkerAsync() 'bgwGalcConnectを非同期起動
        End If
        pblnDisconnectFlg = False

    End Sub

    Private Sub btnGalcDisconnect_Click(ByVal sender As Object,
                ByVal e As System.EventArgs) Handles btnGalcDisconnect_kako.Click

        'If MsgBox("Do you disconnect the connection?", MsgBoxStyle.OkCancel) = _
        '                                               MsgBoxResult.Cancel Then
        If MsgBox("Você terminar a comunicação?", MsgBoxStyle.OkCancel) =
                                                       MsgBoxResult.Cancel Then
            Exit Sub
        End If

        pblnDisconnectFlg = True
        bgwGalcConnect.CancelAsync()    'bgwGalcConnectを停止
        Do
            Application.DoEvents()
            If Not bgwGalcConnect.IsBusy Then
                Exit Do
            End If
        Loop
        Listner.Stop()
        btnGalcDisconnect_kako.Enabled = False
        txtBcNextRecv.Enabled = True
        btnGalcConnect_kako.Enabled = True

    End Sub

    'PLC接続ﾎﾞﾀﾝｸﾘｯｸ
    Private Sub btnPlcConnect_Click(ByVal sender As Object,
                ByVal e As System.EventArgs) Handles btnPlcConnect.Click, btnPlcConnect.Click

        If btnPlcConnect.Enabled = False Then
            Exit Sub
        End If
        '   MsgBox("The connection begins.")
        'MsgBox("Iniciar a comunicação")     'ﾎﾟﾙﾄｶﾞﾙ対応

        If Not bgwPlcConnect.IsBusy Then
            btnPlcConnect.Enabled = False
            btnPlcDisconnect.Enabled = True
            Me.Refresh()
            bgwPlcConnect.RunWorkerAsync()  'bgwPlcConnectを非同期起動
        End If

    End Sub

    Private Sub btnPlcDisconnect_Click(ByVal sender As Object,
                ByVal e As System.EventArgs) Handles btnPlcDisconnect.Click

        'If MsgBox("Do you disconnect the connection?", MsgBoxStyle.OkCancel) = _
        '                                               MsgBoxResult.Cancel Then
        '    'If MsgBox("Você terminar a comunicação?", MsgBoxStyle.OkCancel) = _
        '    '                                               MsgBoxResult.Cancel Then         'ﾎﾟﾙﾄｶﾞﾙ対応
        '    Exit Sub
        'End If

        bgwPlcConnect.CancelAsync()     'bgwPlcDisconnectを停止
        Do
            Application.DoEvents()
            If Not bgwPlcConnect.IsBusy Then
                Exit Do
            End If
        Loop
        btnPlcDisconnect.Enabled = False
        btnPlcConnect.Enabled = True
        reconPLC = -1
    End Sub

    Private Sub txtBcNextRecv_GotFocus(ByVal sender As Object,
                ByVal e As System.EventArgs) Handles txtBcNextRecv.GotFocus

        If txtBcNextRecv.Enabled = True Then
            '入力されている文字列を全選択
            txtBcNextRecv.SelectionStart = 0
            txtBcNextRecv.SelectionLength = txtBcNextRecv.TextLength
        End If

    End Sub

    Private Sub txtBcNextRecv_KeyDown(ByVal sender As Object,
                ByVal e As System.Windows.Forms.KeyEventArgs) _
                Handles txtBcNextRecv.KeyDown

        blnNotNumericFlg = False

        If e.KeyCode = Keys.Enter Then
            Exit Sub
        End If

        ' Determine whether the keystroke is a number from the top of the keyboard.
        If e.KeyCode < Keys.D0 OrElse e.KeyCode > Keys.D9 Then
            ' Determine whether the keystroke is a number from the keypad.
            If e.KeyCode < Keys.NumPad0 OrElse e.KeyCode > Keys.NumPad9 Then
                ' Determine whether the keystroke is a backspace.
                If e.KeyCode <> Keys.Back Then
                    ' A non-numerical keystroke was pressed. 
                    ' Set the flag to true and evaluate in KeyPress event.
                    blnNotNumericFlg = True
                    pintNextNo = CInt(txtBcNextRecv.Text)
                End If
            End If
        End If

    End Sub

    Private Sub txtBcNextRecv_KeyPress(ByVal sender As Object,
                ByVal e As System.Windows.Forms.KeyPressEventArgs) _
                Handles txtBcNextRecv.KeyPress

        If txtBcNextRecv.Enabled = True Then
            '文字列が数値ﾁｪｯｸ
            If blnNotNumericFlg = True Then
                mdlCommon.pstrErrorMsg = "Things except the numerical value " _
                                       + "were input. " + vbCrLf + "Please " _
                                       + "input the numerical value. "
                pInsErrMsg.psubErrMsgShow(mdlCommon.pstrErrorMsg)

                strControlName = txtBcNextRecv.Name
                blnSetFocusFlg = True
                Exit Sub
            End If
        End If

    End Sub

    Private Sub txtBcNextRecv_Leave(ByVal sender As Object,
                ByVal e As System.EventArgs) Handles txtBcNextRecv.Leave

        If txtBcNextRecv.Enabled = True Then
            '文字列が数値ﾁｪｯｸ
            If Not IsNumeric(txtBcNextRecv.Text) Then
                mdlCommon.pstrErrorMsg = "Things except the numerical value " _
                                       + "were input. " + vbCrLf + "Please " _
                                       + "input the numerical value. "
                pInsErrMsg.psubErrMsgShow(mdlCommon.pstrErrorMsg)
                strControlName = txtBcNextRecv.Name
                blnSetFocusFlg = True
                Exit Sub
            End If
            '文字列が範囲ﾁｪｯｸ
            If Val(txtBcNextRecv.Text) < 0 Or Val(txtBcNextRecv.Text) > 999 Then
                mdlCommon.pstrErrorMsg = "Numerical values other than " _
                                       + "999 from 0 were input. " + vbCrLf _
                                       + "Please input the numerical value " _
                                       + "from 0 to 999. "
                pInsErrMsg.psubErrMsgShow(mdlCommon.pstrErrorMsg)
                strControlName = txtBcNextRecv.Name
                blnSetFocusFlg = True
                Exit Sub
            End If
        End If

    End Sub

    Private Sub btnRecvDataDisp_Click(ByVal sender As Object,
                ByVal e As System.EventArgs) Handles btnRecvDataDisp.Click

        '受信ﾃﾞｰﾀ表示画面を表示
        Call pfrmDataDisp.Show()

    End Sub

    Private Sub btnClose_Click(ByVal sender As Object,
                ByVal e As System.EventArgs) Handles btnClose.Click

        If MsgBox("Do you quit the application?", MsgBoxStyle.OkCancel) =
                                                       MsgBoxResult.Cancel Then
            'If MsgBox("Deseja encerrar Sistema?", MsgBoxStyle.OkCancel) = _
            '                                   MsgBoxResult.Cancel Then             'ﾎﾟﾙﾄｶﾞﾙ対応
            Exit Sub
        End If

        '        pblnDisconnectFlg = True
        mdlCommon.pblnAlcLoop = False
        '        mdlCommon.pblnPlcLoop = False
        'bgwGalcConnect.CancelAsync()    'bgwGalcConnectを停止
        bgwPlcConnect.CancelAsync()     'bgwPlcDisconnectを停止
        Do
            Application.DoEvents()
            If Not bgwGalcConnect.IsBusy And Not bgwPlcConnect.IsBusy Then
                Exit Do
            End If
        Loop

        'ﾃﾞｰﾀﾍﾞｰｽ切断処理(ｻﾌﾞﾙｰﾁﾝCall)
        pClsSql.pSQLClose()
        pClsPC10G.Close()
        pInsfrmMsg.Close()
        pfrmDataDisp.Close()
        Me.Close()

    End Sub
    Dim GalcStatus As String
    '現在この関数は使われていません。
    Private Sub bgwGalcConnect_DoWork(ByVal sender As System.Object,
                ByVal e As System.ComponentModel.DoWorkEventArgs) _
                Handles bgwGalcConnect.DoWork

        'Dim enc As System.Text.Encoding = System.Text.Encoding.Default
        Dim Tcp As System.Net.Sockets.TcpClient
        Dim ns As System.Net.Sockets.NetworkStream
        Dim intCheckResult As Integer   'ALCﾃﾞｰﾀ受信結果を格納
        Dim intCount As Integer         '受信ﾃﾞｰﾀ結合用ｶｳﾝﾀ
        Dim strBuffer As String         '受信ﾃﾞｰﾀ結合文字列
        Dim intDummyCount As Integer    'DBﾀﾞﾐｰﾘｰﾄﾞの回数を減らす為のｶｳﾝﾀ

        Dim bRET As Boolean = False

        pflgSequenceErr = False

        Do
            If Listner.Pending() = True Then Exit Do
            If bgwGalcConnect.CancellationPending = True Then
                'G-ALC通信状態ﾗﾍﾞﾙの背景を赤にする
                lblGalcStatus.BackColor = Color.Red
                'Call psubSetlblGalcStatus("受信切断")
                Call psubSetlblGalcStatus("Disconnect")
                Application.DoEvents()
                e.Cancel = True
                Exit Sub
            End If
            Thread.Sleep(100)
        Loop
        '接続要求がきたら接続
        Tcp = Listner.AcceptTcpClient()
        'G-ALCﾃﾞｰﾀ受信
        ns = Tcp.GetStream()

        Try
            'G-ALC通信状態ﾗﾍﾞﾙの背景を緑にする
            lblGalcStatus.BackColor = Color.LightGreen
            'G-ALC通信状態ﾗﾍﾞﾙの表記を｢受信中｣にする
            'lblGalcStatus.Text = "受信中"  
            'Call psubSetlblGalcStatus("受信中")
            Call psubSetlblGalcStatus("Connect")

            intDummyCount = 0
            'ﾀﾞﾐｰ用SQL
            pClsSql.pCmdDummy1 = New MySqlCommand("DESCRIBE tb_vehicle",
                                                  pClsSql.pCon)
            Do                                      'ALC受信処理
                '処理中にｷｬﾝｾﾙされていないかを定期的にﾁｪｯｸする
                If bgwGalcConnect.CancellationPending = True Then
                    e.Cancel = True
                    Exit Do
                End If
                Application.DoEvents()
                If ns.DataAvailable = True Then
                    pintRecvSize = ns.Read(pbytRecvBuffer, 0,
                                           pbytRecvBuffer.Length)

                    '受信ﾃﾞｰﾀのﾛｷﾞﾝｸﾞ
                    strBuffer = ""
                    For intCount = 0 To pintRecvSize - 1 Step 1
                        strBuffer = strBuffer & Chr(pbytRecvBuffer(intCount))
                    Next intCount
                    Call CmnAlcRecvDataLog(strBuffer)

                    '受信ﾃﾞｰﾀﾁｪｯｸ
                    intCheckResult = pfncAlcConnect(pbytRecvBuffer)

                    '受信応答電文送信
                    ns.Write(pbytSendBuffer, 0, pbytSendBuffer.Length)

                    '受信ﾃﾞｰﾀﾁｪｯｸ
                    If intCheckResult <> pCSuccess Then
                        e.Cancel = True
                        Exit Do
                    End If

                End If
                If intDummyCount > 30000 Then
                    'MySQLが8h放置で勝手にDisconnectの防止用ﾀﾞﾐｰSQL
                    Call pClsSql.sDummySQL1()
                    Application.DoEvents()
                    intDummyCount = 0
                Else
                    intDummyCount = intDummyCount + 1
                End If
                Thread.Sleep(100)
            Loop


            If Not pClsSql.pCmdDummy1 Is Nothing Then
                'MySQLを起こしておく為のSQLｺﾏﾝﾄﾞをDispose
                pClsSql.pCmdDummy1.Dispose()
            End If

            If pflgSequenceErr = False Then
                'G-ALC通信状態ﾗﾍﾞﾙの背景を赤にする
                lblGalcStatus.BackColor = Color.Red
                'G-ALC通信状態ﾗﾍﾞﾙの表記を｢受信切断｣にする
                'lblGalcStatus.Text = "受信切断"     
                'Call psubSetlblGalcStatus("受信切断")
                Call psubSetlblGalcStatus("Disconnect")
                Application.DoEvents()
            Else
                lblGalcStatus.BackColor = Color.Yellow
                Call psubSetlblGalcStatus("Waiting")
                'Call psubSetlblGalcStatus("Espera")    'ポルトガル対応
                Application.DoEvents()
                Exit Sub
            End If

        Catch ex As Exception
            mdlCommon.pblnEXFlg = True
            mdlCommon.psysException = ex
            mdlCommon.pstrErrorMsg = "frmMenu.bgwGalcConnect_DoWork"
        End Try

        '切断処理
        ns.Close()
        Tcp.Close()

    End Sub

    'ﾀｲﾏｰ起動でPLCから受信する
    Private Sub bgwPlcConnect_DoWork(ByVal sender As System.Object,
                ByVal e As System.ComponentModel.DoWorkEventArgs) _
                Handles bgwPlcConnect.DoWork

        Dim intDummyCount As Integer    'DBﾀﾞﾐｰﾘｰﾄﾞの回数を減らす為のｶｳﾝﾀ Counter for reducing the number of dummy reads

        '通信開始 Communication start
        If Not pClsPC10G.fConnectON(pstIni.strPlcIp, pstIni.strPlcPort) Then
            GoTo Disconnect_Label   '切断処理
        End If

        Try
            lblPlcStatus.BackColor = Color.LightGreen
            'lblPlcStatus.Text = "送信中"
            'Call psubSetlblPlcStatus("送信中")
            Call psubSetlblPlcStatus("Connect")
            'Call psubSetlblPlcStatus("Conexão")     'ポルトガル対応    
            pClsPC10G.fSetPlcClock()                '時計を変更(PC→PLC)

            intDummyCount = 0
            pClsSql.pCmdDummy2 = New MySqlCommand("DESCRIBE tb_vehicle",
                                                  pClsSql.pCon)   'ﾀﾞﾐｰ用SQL Dummy SQL
            Do 'ﾃﾞｰﾀ読み込み อ่านข้อมูล

                '処理中にｷｬﾝｾﾙされていないかを定期的にﾁｪｯｸする ตรวจสอบว่ามีการยกเลิกหรือไม่
                If bgwPlcConnect.CancellationPending = True Then
                    e.Cancel = True
                    GoTo Disconnect_Label   '切断処理
                End If

                '<PLC送受信処理> reception processing
                Call psubPlcSendRecv()

                If intDummyCount > 30000 Then
                    'MySQLが8h放置で勝手にDisconnectの防止用ﾀﾞﾐｰSQL Dummy SQL for preventing Disconnect without permission by MySQL 8 hours
                    Call pClsSql.sDummySQL2()
                    Application.DoEvents()
                    intDummyCount = 0
                Else
                    intDummyCount = intDummyCount + 1
                End If

                'エラーが起きたらループから脱出 Escape from the loop if an error occurs
                If mdlCommon.pblnEXFlg Then
                    Exit Do
                End If

                Thread.Sleep(100)
            Loop
            If Not pClsSql.pCmdDummy2 Is Nothing Then
                'MySQLを起こしておく為のSQLｺﾏﾝﾄﾞをDispose
                pClsSql.pCmdDummy2.Dispose()
            End If
        Catch ex As Exception
            mdlCommon.pblnEXFlg = True
            mdlCommon.psysException = ex
            mdlCommon.pstrErrorMsg = "frmMenu.bgwPlcConnect_DoWork"

        End Try

Disconnect_Label:
        lblPlcStatus.BackColor = Color.Red
        'lblPlcStatus.Text = "送信停止"
        'Call psubSetlblPlcStatus("送信停止")
        Call psubSetlblPlcStatus("Disconnect")
        '切断処理
        pClsPC10G.fConnectOFF()
        Application.DoEvents()
        reconPLC = 10
    End Sub
    Dim reconPLC As Integer = -1
    Sub ReconnectPLC()
        If reconPLC >= 0 Then
            If reconPLC = 0 Then
                btnPlcConnect.PerformClick()
            End If
            btnPlcConnect.Text = "Connect in " & reconPLC
            reconPLC -= 1

        Else
            btnPlcConnect.Text = "Connect"
            reconPLC -= 1
        End If


    End Sub

    '受信ﾃﾞｰﾀﾁｪｯｸしてDBに格納


    Private Function pfncAlcConnect(ByRef bytBuffer() As Byte) As Integer

        Dim intCount As Integer
        Dim strSQL As String
        Dim strVehicleName As String

        pfncAlcConnect = pCErrOther
        pflgSequenceErr = False

        Try
            'S = 特殊コメント D = ダイレクトコメント, L = ログ, C = 予約コメント, U = 端末アップ
            'H = 保留コメント は何もしない
            If Chr(bytBuffer(22)) = "S" Or
                Chr(bytBuffer(22)) = "D" Or
                Chr(bytBuffer(22)) = "L" Or
                Chr(bytBuffer(22)) = "C" Or
                Chr(bytBuffer(22)) = "U" Or
                Chr(bytBuffer(22)) = "H" Then
                pfncAlcConnect = pCSuccess
                Call psubAlcSend("00")
                Exit Function
            End If



            '受信ﾚﾝｸﾞｽが違う場合
            'If pintRecvSize <> 42 Then
            If pintRecvSize <> 50 Then      'ﾛｯﾄｺｰﾄﾞ,ﾌｧﾐﾘｰｺｰﾄﾞ追加（2012/5）
                'ﾚﾝｸﾞｽｴﾗｰ
                Call psubAlcSend("76")
                '                mdlCommon.pstrErrorMsg = "frmMenu.pfncAlcConnect - LengthError"
                pInsErrMsg.psubErrMsgShow("frmMenu.pfncAlcConnect - LengthError Length = " & pintRecvSize.ToString)
                Exit Function
            End If

            pstAlcRecv.strBCNo = Trim(Chr(bytBuffer(37)) & Chr(bytBuffer(38)) & Chr(bytBuffer(39)))
            pstAlcRecv.strAssyNo = Trim(Chr(bytBuffer(40)) & Chr(bytBuffer(41)))
            pstAlcRecv.strLOTCode = Trim(Chr(bytBuffer(42)) & Chr(bytBuffer(43)) & Chr(bytBuffer(44)) & Chr(bytBuffer(45)))     'ﾛｯﾄｺｰﾄﾞ追加（2012/5）
            pstAlcRecv.strFamilyCode = Trim(Chr(bytBuffer(46)) & Chr(bytBuffer(47)) & Chr(bytBuffer(48)) & Chr(bytBuffer(49)))  'ﾌｧﾐﾘｰｺｰﾄﾞ追加（2012/5）

            'Check Duplicate Data
            If pstAlcRecv.strLOTCode & pstAlcRecv.strBCNo <> pstAlcRecv.strLastSEQ Then
                pstAlcRecv.strLastSEQ = pstAlcRecv.strLOTCode & pstAlcRecv.strBCNo
                txtLastSEQ.BeginInvoke(New MethodInvoker(Sub()
                                                             txtLastSEQ.Text = pstAlcRecv.strLastSEQ
                                                             txtLastSEQ.BackColor = Color.LimeGreen
                                                         End Sub))
            Else
                Call psubAlcSend("00")
                pfncAlcConnect = pCSuccess
                txtLastSEQ.BeginInvoke(New MethodInvoker(Sub()

                                                             txtLastSEQ.BackColor = Color.Red
                                                         End Sub))
                Exit Function
            End If
            '///////////////////////////

            If Chr(bytBuffer(22)).ToString <> "0" And
               Chr(bytBuffer(22)).ToString <> "1" Then '処理区分1が0,1以外の場合
                'ｺﾒﾝﾄﾃﾞｰﾀ破棄
                pfncAlcConnect = pCSuccess
                Call psubAlcSend("00")
                Exit Function
            ElseIf Chr(bytBuffer(22)).ToString = "0" Then   '処理区分1が0の場合
                '空ハンガーはDBに上げない
                If Chr(bytBuffer(23)).ToString = "E" Then
                    Call psubAlcSend("00")
                    pfncAlcConnect = pCSuccess
                    Exit Function
                End If
                'Check Matching SEQ
                'BC連番の連番飛びﾁｪｯｸがNG
                'If CInt(pstAlcRecv.strBCNo) <> 0 Then   '初回起動のシーケンスNo.「0000」はチェックしない。
                '    If CInt(pstAlcRecv.strBCNo) <> pintNextNo Then
                '        'BC連番ｴﾗｰ
                '        pflgSequenceErr = True
                '        Call psubAlcSend("90")
                '        '                mdlCommon.pstrErrorMsg = "frmMenu.pfncAlcConnect - BCNo " + _
                '        '                                        "SequenceError"
                '        pInsErrMsg.psubErrMsgShow("frmMenu.pfncAlcConnect - BCNo " & pintNextNo.ToString & _
                '        '                         "SequenceError")
                '        Exit Function
                '    End If
                'End If
                '処理区分2が0以外の場合
                If Chr(bytBuffer(23)).ToString <> "0" Then
                    bytBuffer(23) = Asc("0")
                End If

            ElseIf Chr(bytBuffer(22)).ToString = "1" Then '処理区分1が1の場合
                '空ハンガーはDBに上げない
                If Chr(bytBuffer(23)).ToString = "E" Then
                    Call psubAlcSend("00")
                    pfncAlcConnect = pCSuccess
                    Exit Function
                End If
                '試し打ちはDBに上げない
                If Chr(bytBuffer(23)).ToString = "2" Then
                    Call psubAlcSend("00")
                    pfncAlcConnect = pCSuccess
                    Exit Function
                End If

            End If
        Catch ex As Exception
            pInsErrMsg.psubErrMsgShow(ex, "frmMenu.pfncAlcConnect check")

        End Try


        Try
            pstrVehicleName = ""
            '車種番号をｷｰとして車種名をDBから取得
            For intCount = 0 To pstVehicleData.Count - 1 Step 1
                If Trim(pstVehicleData(intCount).strAssyNo) =
                   Trim(Chr(bytBuffer(40)).ToString &
                        Chr(bytBuffer(41)).ToString) Then
                    pstrVehicleName = Trim(pstVehicleData(intCount).
                                           strVehicleName.ToString)
                    Exit For
                Else
                    Application.DoEvents()
                End If
            Next intCount

            '登録していない車種はDBに登録しません。
            If pstrVehicleName <> "" Then
                'BC連番/車種番号/車種名/受信日時をDBに登録
                pstAlcRecvData2PLC = New ST_ALC
                pstAlcRecvData2PLC.strBCNo = pstAlcRecv.strBCNo.ToString
                pstAlcRecvData2PLC.strAssyNo = pstAlcRecv.strAssyNo.ToString
                strVehicleName = pstrVehicleName
                pstAlcRecvData2PLC.intStatus = 0
                pstAlcRecvData2PLC.intDelFlg = 0
                pstAlcRecvData2PLC.strRecvDate = CmnGetNow(Now)
                'strSQL = "INSERT INTO tb_alc (strBCNo,strAssyNo,strVehicleName," + _
                '         "intStatus,intDelFlg,strRecvDate) VALUES('" _
                '        & pstAlcRecv.strBCNo.ToString & "','" _
                '        & pstAlcRecv.strAssyNo.ToString & "','" _
                '        & strVehicleName & "','0','0','" _
                '        & CmnGetNow(Now).ToString & "')"           'ﾛｯﾄｺｰﾄﾞ,ﾌｧﾐﾘｰｺｰﾄﾞ追加（2012/5）
                strSQL = "INSERT INTO tb_alc (strBCNo,strAssyNo,strVehicleName,strLOTcode,strFamilycode," +
                         "intStatus,intDelFlg,strRecvDate) VALUES('" _
                        & pstAlcRecv.strBCNo.ToString & "','" _
                        & pstAlcRecv.strAssyNo.ToString & "','" _
                        & strVehicleName & "','" & pstAlcRecv.strLOTCode & "','" & pstAlcRecv.strFamilyCode & "','0','0','" _
                        & CmnGetNow(Now).ToString & "')"

                If pClsSql.fSetAlcDataALC(strSQL) = pCErrOther Then
                    pfncAlcConnect = pCErrOther
                    Exit Function
                End If
            End If

            If Chr(bytBuffer(22)).ToString = "0" Then '処理区分1(通常BC)が0の場合
                '環境設定ﾌｧｲﾙ更新(受信済BC連番)
                '            pstAlcRecv.strBCNo = txtBcNextRecv.Text.ToString
                Call clsPublicIni.psubIniSave()
                '            Call psubSetlblBcRecv(pintNextNo)
                lblBcRecv.Text = pstAlcRecv.strBCNo 'pintNextNo.ToString
                pintNextNo = Val(pstAlcRecv.strBCNo) + 1 'pintNextNo + 1
                If pintNextNo = 1000 Then
                    '               Call psubSettxtBcNextRecv("0")
                    txtBcNextRecv.Text = "0"
                    pintNextNo = 0

                Else
                    '                Call psubSettxtBcNextRecv(CStr(pintNextNo))
                    txtBcNextRecv.Text = pintNextNo.ToString
                End If
            ElseIf Chr(bytBuffer(22)).ToString = "1" Then '処理区分1(再作成BC)が0の場合

            End If

        Catch ex As Exception
            '            mdlCommon.pblnEXFlg = True
            '            mdlCommon.psysException = ex
            '            mdlCommon.pstrErrorMsg = "frmMenu.pfncAlcConnect"
            pInsErrMsg.psubErrMsgShow(ex, "frmMenu.pfncAlcConnect Data Set")
        End Try

        '正常終了
        Call psubAlcSend("00")

        pfncAlcConnect = pCSuccess

    End Function

    Private Sub psubGetVehicle()

        Dim stREG As clsPc10G.ST_REG
        Dim bytVehicleBuffer(389) As Byte
        Dim strAssyNo() As String
        Dim strVehicleName() As String
        Dim intCount As Integer
        Dim intPG As Integer
        Dim intReg As Integer
        Dim intRecCount As Integer
        Dim strBuffer As String

        '車種ﾃﾞｰﾀを全件取込む
        intPG = pClsPC10G.fncPGNoGet(pstIni.strVehiName_PG)
        If intPG < 0 Then
            mdlCommon.pblnEXFlg = False
            mdlCommon.pstrErrorMsg = "frmMenu.psubGetVehicle - ENV.ini " +
                                     "[REG]VEHI_NAME_PG ERROR"
            Exit Sub
        End If
        intReg = pClsPC10G.fncRegTypeGet(pstIni.strVehiName_TY)
        If intReg < 1 Then
            mdlCommon.pblnEXFlg = False
            mdlCommon.pstrErrorMsg = "frmMenu.psubGetVehicle - ENV.ini " +
                                     "[REG]VEHI_NAME_AD ERROR"
            Exit Sub
        End If
        stREG.intPGNo = intPG
        stREG.intRegType = intReg
        stREG.strReg = pstIni.strVehiName_AD
        Try
            Call pClsPC10G.fReadWordAdr(stREG, 389, bytVehicleBuffer)
            For intCount = 5 To 388
                If Chr(bytVehicleBuffer(intCount)) = ControlChars.NullChar Then
                    '00(Null)を32(ｽﾍﾟｰｽ)に置き換える
                    bytVehicleBuffer(intCount) = 32
                End If
            Next
            intRecCount = 0
            ReDim strAssyNo(0)
            ReDim strVehicleName(0)
            For intCount = 0 To 95 Step 1       '2012/6/7 3桁から4桁に変更（kasuga）
                strBuffer = Chr(bytVehicleBuffer(intCount * 4 + 5)) _
                          & Chr(bytVehicleBuffer(intCount * 4 + 6)) _
                          & Chr(bytVehicleBuffer(intCount * 4 + 7)) _
                          & Chr(bytVehicleBuffer(intCount * 4 + 8))
                '↑bytVehicleBufferの受信ｺﾏﾝﾄﾞ(5byte)は無視する
                If Trim(strBuffer) <> "" Then
                    ReDim Preserve strAssyNo(intRecCount)
                    ReDim Preserve strVehicleName(intRecCount)
                    intRecCount = intRecCount + 1
                    strAssyNo(intRecCount - 1) = Format(intCount + 1, "00")
                    strVehicleName(intRecCount - 1) = strBuffer

                End If
            Next intCount
            Call pClsSql.fSetVehicleTable(strAssyNo, strVehicleName)
        Catch ex As Exception
            mdlCommon.pblnEXFlg = True
            mdlCommon.psysException = ex
            mdlCommon.pstrErrorMsg = "frmMenu.psubGetVehicle"
            'pInsErrMsg.psubErrMsgShow(ex, "frmMenu.psubGetVehicle")
        End Try

    End Sub

    Private Sub psubPlcSendRecv()

        Dim stREG As clsPc10G.ST_REG
        Dim intALCTransSignal As Integer
        Dim intVehicleDimand As Integer
        Dim intVehicleComplete As Integer
        Dim intDemandSignal As Integer
        Dim intTransCompSignal As Integer
        Dim intBCNo(1) As Integer
        Dim intAssyNo(1) As Integer
        Dim intLOTcode(3) As Integer        'ﾛｯﾄｺｰﾄﾞ追加（2012/5）
        Dim intFamilycode(3) As Integer     'ﾌｧﾐﾘｰｺｰﾄﾞ追加（2012/5）
        Dim intSendBuffer As Integer()
        Dim strSQL As String
        Dim bytRecvBuffer() As Byte
        Dim intPG As Integer
        Dim intReg As Integer
        Dim strProcName As String

        Try
            strProcName = "frmMenu.psubPlcSendRecv - "
            'ALC転送信号取得
            intPG = pClsPC10G.fncPGNoGet(pstIni.strAlcConnD_PG)
            If intPG < 0 Then
                mdlCommon.pblnEXFlg = False
                mdlCommon.pstrErrorMsg = strProcName + "ENV.ini " +
                                         "[REG]ALC_CON_D_PG ERROR"
                Exit Sub
            End If
            intReg = pClsPC10G.fncRegTypeGet(pstIni.strAlcConnD_TY)
            If intReg < 1 Then
                mdlCommon.pblnEXFlg = False
                mdlCommon.pstrErrorMsg = strProcName + "ENV.ini " +
                                         "[REG]ALC_CON_D_AD ERROR"
                Exit Sub
            End If
            stREG.intPGNo = intPG
            stREG.intRegType = intReg
            stREG.strReg = pstIni.strAlcConnD_AD
            Try
                intALCTransSignal = pClsPC10G.fReadBitAdr(stREG)
            Catch ex As Exception
                mdlCommon.pblnEXFlg = True
                mdlCommon.psysException = ex
                mdlCommon.pstrErrorMsg = strProcName + "Get ALCTransSignal"
                Exit Sub
            End Try
            If intALCTransSignal = 0 Then 'L700
                Exit Sub
            End If

            '車種取込要求信号取得
            intPG = pClsPC10G.fncPGNoGet(pstIni.strVehicleD_PG)
            If intPG < 0 Then
                mdlCommon.pblnEXFlg = False
                mdlCommon.pstrErrorMsg = strProcName + "ENV.ini " +
                                         "[REG]VEHICLE_D_PG ERROR"
                Exit Sub
            End If
            intReg = pClsPC10G.fncRegTypeGet(pstIni.strVehicleD_TY)
            If intReg < 1 Then
                mdlCommon.pblnEXFlg = False
                mdlCommon.pstrErrorMsg = strProcName + "ENV.ini " +
                                         "[REG]VEHICLE_D_AD ERROR"
                Exit Sub
            End If
            stREG.intPGNo = intPG
            stREG.intRegType = intReg
            stREG.strReg = pstIni.strVehicleD_AD
            Try
                intVehicleDimand = pClsPC10G.fReadBitAdr(stREG)     'P2 - L702
            Catch ex As Exception
                mdlCommon.pblnEXFlg = True
                mdlCommon.psysException = ex
                mdlCommon.pstrErrorMsg = strProcName + "Get VehicleDimand"
                Exit Sub
            End Try

            '車種取込完了信号取得
            intPG = pClsPC10G.fncPGNoGet(pstIni.strVehicleC_PG)
            If intPG < 0 Then
                mdlCommon.pblnEXFlg = False
                mdlCommon.pstrErrorMsg = strProcName + "ENV.ini " +
                                         "[REG]VEHICLE_C_PG ERROR"
                Exit Sub
            End If
            intReg = pClsPC10G.fncRegTypeGet(pstIni.strVehicleC_TY)
            If intReg < 1 Then
                mdlCommon.pblnEXFlg = False
                mdlCommon.pstrErrorMsg = strProcName + "ENV.ini " +
                                         "[REG]VEHICLE_C_AD ERROR"
                Exit Sub
            End If
            stREG.intPGNo = intPG
            stREG.intRegType = intReg
            stREG.strReg = pstIni.strVehicleC_AD
            Try
                intVehicleComplete = pClsPC10G.fReadBitAdr(stREG) 'P2 - L712
            Catch ex As Exception
                mdlCommon.pblnEXFlg = True
                mdlCommon.psysException = ex
                mdlCommon.pstrErrorMsg = strProcName + "Get VehicleComplete"
                Exit Sub
            End Try

            '車種変更信号が、要求=0,完了=1の場合
            If intVehicleDimand = 0 And intVehicleComplete = 1 Then
                '車種取込完了信号OFFを送信
                intPG = pClsPC10G.fncPGNoGet(pstIni.strVehicleC_PG)
                If intPG < 0 Then
                    mdlCommon.pblnEXFlg = False
                    mdlCommon.pstrErrorMsg = strProcName + "ENV.ini " +
                                             "[REG]VEHICLE_C_PG ERROR"
                    Exit Sub
                End If
                intReg = pClsPC10G.fncRegTypeGet(pstIni.strVehicleC_TY)
                If intReg < 1 Then
                    mdlCommon.pblnEXFlg = False
                    mdlCommon.pstrErrorMsg = strProcName + "ENV.ini " +
                                             "[REG]VEHICLE_C_AD ERROR"
                    Exit Sub
                End If
                stREG.intPGNo = intPG
                stREG.intRegType = intReg
                stREG.strReg = pstIni.strVehicleC_AD
                Try
                    Call pClsPC10G.fWriteBitAdr(stREG, 0) 'P2 - L712
                Catch ex As Exception
                    mdlCommon.pblnEXFlg = True
                    mdlCommon.psysException = ex
                    mdlCommon.pstrErrorMsg = strProcName + "Set 'OFF' " +
                                             "VehicleComplete"
                    Exit Sub
                End Try

                '(車種変更信号が、要求=1,完了=0の場合)…Else
            ElseIf intVehicleDimand = 1 And intVehicleComplete = 0 Then 'P2 - L702=1  and P2 - L712=0
                Call psubGetVehicle()
                '車種取込完了信号ONを送信
                intPG = pClsPC10G.fncPGNoGet(pstIni.strVehicleC_PG)
                If intPG < 0 Then
                    mdlCommon.pblnEXFlg = False
                    mdlCommon.pstrErrorMsg = strProcName + "ENV.ini " +
                                             "[REG]VEHICLE_C_PG ERROR"
                    Exit Sub
                End If
                intReg = pClsPC10G.fncRegTypeGet(pstIni.strVehicleC_TY)
                If intReg < 1 Then
                    mdlCommon.pblnEXFlg = False
                    mdlCommon.pstrErrorMsg = strProcName + "ENV.ini " +
                                             "[REG]VEHICLE_C_AD ERROR"
                    Exit Sub
                End If
                stREG.intPGNo = intPG
                stREG.intRegType = intReg
                stREG.strReg = pstIni.strVehicleC_AD
                Try
                    Call pClsPC10G.fWriteBitAdr(stREG, 1) 'P2-L712
                Catch ex As Exception
                    mdlCommon.pblnEXFlg = True
                    mdlCommon.psysException = ex
                    mdlCommon.pstrErrorMsg = strProcName +
                                             "Set 'ON' VehicleComplete"
                    Exit Sub
                End Try
            End If




            'ALCﾃﾞｰﾀ要求信号取得
            intPG = pClsPC10G.fncPGNoGet(pstIni.strAlcTrnsD_PG)
            If intPG < 0 Then
                mdlCommon.pblnEXFlg = False
                mdlCommon.pstrErrorMsg = strProcName + "[REG]ALC_TRN_D_PG ERROR"
                Exit Sub
            End If
            intReg = pClsPC10G.fncRegTypeGet(pstIni.strAlcTrnsD_TY)
            If intReg < 1 Then
                mdlCommon.pblnEXFlg = False
                mdlCommon.pstrErrorMsg = strProcName + "[REG]ALC_TRN_D_AD ERROR"
                Exit Sub
            End If
            stREG.intPGNo = intPG
            stREG.intRegType = intReg
            stREG.strReg = pstIni.strAlcTrnsD_AD
            Try
                intDemandSignal = pClsPC10G.fReadBitAdr(stREG) 'P2-L701
            Catch ex As Exception
                mdlCommon.pblnEXFlg = True
                mdlCommon.psysException = ex
                mdlCommon.pstrErrorMsg = strProcName + "Get ALCDemandSignal"
                Exit Sub
            End Try



            'ALCﾃﾞｰﾀ送信完了信号取得 Acquisition of ALC data transmission completion signal
            intPG = pClsPC10G.fncPGNoGet(pstIni.strAlcTrnsC_PG)
            If intPG < 0 Then
                mdlCommon.pblnEXFlg = False
                mdlCommon.pstrErrorMsg = strProcName + "ENV.ini " +
                                         "[REG]ALC_TRN_C_PG ERROR"
                Exit Sub
            End If
            intReg = pClsPC10G.fncRegTypeGet(pstIni.strAlcTrnsC_TY)
            If intReg < 1 Then
                mdlCommon.pblnEXFlg = False
                mdlCommon.pstrErrorMsg = strProcName + "ENV.ini " +
                                         "[REG]ALC_TRN_C_AD ERROR"
                Exit Sub
            End If
            stREG.intPGNo = intPG
            stREG.intRegType = intReg
            stREG.strReg = pstIni.strAlcTrnsC_AD
            Try
                intTransCompSignal = pClsPC10G.fReadBitAdr(stREG) 'P2-L711
            Catch ex As Exception
                mdlCommon.pblnEXFlg = True
                mdlCommon.psysException = ex
                mdlCommon.pstrErrorMsg = strProcName + "Get TransCompSignal"
                Exit Sub
            End Try



            '要求信号=0,送信完了信号=1
            If intDemandSignal = 0 And intTransCompSignal = 1 Then
                '　送信完了信号をOFFする
                intPG = pClsPC10G.fncPGNoGet(pstIni.strAlcTrnsC_PG)
                If intPG < 0 Then
                    mdlCommon.pblnEXFlg = False
                    mdlCommon.pstrErrorMsg = strProcName + "ENV.ini " +
                                             "[REG]ALC_TRN_C_PG ERROR"
                    Exit Sub
                End If
                intReg = pClsPC10G.fncRegTypeGet(pstIni.strAlcTrnsC_TY)
                If intReg < 1 Then
                    mdlCommon.pblnEXFlg = False
                    mdlCommon.pstrErrorMsg = strProcName + "ENV.ini " +
                                             "[REG]ALC_TRN_C_AD ERROR"
                    Exit Sub
                End If
                stREG.intPGNo = intPG
                stREG.intRegType = intReg
                stREG.strReg = pstIni.strAlcTrnsC_AD
                Try
                    Call pClsPC10G.fWriteBitAdr(stREG, 0) 'P2-L711
                Catch ex As Exception
                    mdlCommon.pblnEXFlg = True
                    mdlCommon.psysException = ex
                    mdlCommon.pstrErrorMsg = strProcName + "Set 'OFF' " +
                                             "TransCompSignal"
                    Exit Sub
                End Try

                '要求信号=1,送信完了信号=0 Request signal = 1, ส่งสัญญาณเสร็จ = 0
            ElseIf intDemandSignal = 1 And intTransCompSignal = 0 Then ' ส่งคันใหม่เข้า 'P2-L701=1 and P2-L711=0
                'DBからﾃﾞｰﾀを1件取得
                If pClsSql.fGetAlcData() = mdlErrorCode.pCErrDataRead Then
                    Exit Sub
                End If
                If Not pblnAlcDataFlg Then
                    Exit Sub
                End If

                Do
                    Application.DoEvents()

                    'PLCの指定ﾚｼﾞｽﾀにﾃﾞｰﾀ書き込み(BC連番)
                    intPG = pClsPC10G.fncPGNoGet(pstIni.strBCNo_PG)
                    If intPG < 0 Then
                        mdlCommon.pblnEXFlg = False
                        mdlCommon.pstrErrorMsg = strProcName + "ENV.ini " +
                                                 "[REG]BCNO_PG ERROR"
                        Exit Sub
                    End If
                    intReg = pClsPC10G.fncRegTypeGet(pstIni.strBCNo_TY)
                    If intReg < 1 Then
                        mdlCommon.pblnEXFlg = False
                        mdlCommon.pstrErrorMsg = strProcName + "ENV.ini " +
                                                 "[REG]BCNO_AD ERROR"
                        Exit Sub
                    End If
                    stREG.intPGNo = intPG
                    stREG.intRegType = intReg
                    stREG.strReg = pstIni.strBCNo_AD
                    Try
                        intBCNo(0) = CInt(pstAlcData2PLC.strBCNo.ToString)
                        ReDim intSendBuffer(0)
                        intSendBuffer(0) = intBCNo(0)
                        Call pClsPC10G.fWriteWordAdr(stREG, intSendBuffer)
                    Catch ex As Exception
                        mdlCommon.pblnEXFlg = True
                        mdlCommon.psysException = ex
                        mdlCommon.pstrErrorMsg = strProcName + "Set BCNo"
                        Exit Sub
                    End Try

                    'PLCの指定ﾚｼﾞｽﾀにﾃﾞｰﾀ書き込み(車種番号) Write data (vehicle type number) to specified register of PLC
                    intPG = pClsPC10G.fncPGNoGet(pstIni.strAssyNo_PG)
                    If intPG < 0 Then
                        mdlCommon.pblnEXFlg = False
                        mdlCommon.pstrErrorMsg = strProcName + "ENV.ini " +
                                                 "[REG]ASSYNO_PG ERROR"
                        Exit Sub
                    End If
                    intReg = pClsPC10G.fncRegTypeGet(pstIni.strAssyNo_TY)
                    If intReg < 1 Then
                        mdlCommon.pblnEXFlg = False
                        mdlCommon.pstrErrorMsg = strProcName + "ENV.ini " +
                                                 "[REG]ASSYNO_AD ERROR"
                        Exit Sub
                    End If
                    stREG.intPGNo = intPG
                    stREG.intRegType = intReg
                    stREG.strReg = pstIni.strAssyNo_AD
                    Try
                        intAssyNo(0) = pstAlcData2PLC.strAssyNo.ToString
                        ReDim intSendBuffer(0)
                        intSendBuffer(0) = "&H" & intAssyNo(0)
                        Call pClsPC10G.fWriteWordAdr(stREG, intSendBuffer)
                    Catch ex As Exception
                        mdlCommon.pblnEXFlg = True
                        mdlCommon.psysException = ex
                        mdlCommon.pstrErrorMsg = strProcName + "Set AssyNo"
                        Exit Sub
                    End Try


                    'PLCの指定ﾚｼﾞｽﾀにﾃﾞｰﾀ書き込み(ﾛｯﾄｺｰﾄﾞ) Write data (lot code) to specified register of PLC
                    intPG = pClsPC10G.fncPGNoGet(pstIni.strLOTcode_PG)
                    If intPG < 0 Then
                        mdlCommon.pblnEXFlg = False
                        mdlCommon.pstrErrorMsg = strProcName + "ENV.ini " +
                                                 "[REG]LOTCODE_PG ERROR"
                        Exit Sub
                    End If
                    intReg = pClsPC10G.fncRegTypeGet(pstIni.strLOTcode_TY)
                    If intReg < 1 Then
                        mdlCommon.pblnEXFlg = False
                        mdlCommon.pstrErrorMsg = strProcName + "ENV.ini " +
                                                 "[REG]LOTCODE_AD ERROR"
                        Exit Sub
                    End If
                    stREG.intPGNo = intPG
                    stREG.intRegType = intReg
                    stREG.strReg = pstIni.strLOTcode_AD
                    Try
                        intLOTcode(0) = Asc(pstAlcData2PLC.strLOTcode.Substring(0, 1))
                        intLOTcode(1) = Asc(pstAlcData2PLC.strLOTcode.Substring(1, 1))
                        intLOTcode(2) = Asc(pstAlcData2PLC.strLOTcode.Substring(2, 1))
                        intLOTcode(3) = Asc(pstAlcData2PLC.strLOTcode.Substring(3, 1))
                        ReDim intSendBuffer(1)
                        intSendBuffer(0) = intLOTcode(0) + intLOTcode(1) * 256
                        intSendBuffer(1) = intLOTcode(2) + intLOTcode(3) * 256
                        Call pClsPC10G.fWriteWordAdr(stREG, intSendBuffer)
                    Catch ex As Exception
                        mdlCommon.pblnEXFlg = True
                        mdlCommon.psysException = ex
                        mdlCommon.pstrErrorMsg = strProcName + "Set LOTcode"
                        Exit Sub
                    End Try

                    'PLCの指定ﾚｼﾞｽﾀにﾃﾞｰﾀ書き込み(ﾌｧﾐﾘｰｺｰﾄﾞ)
                    intPG = pClsPC10G.fncPGNoGet(pstIni.strFamilycode_PG)
                    If intPG < 0 Then
                        mdlCommon.pblnEXFlg = False
                        mdlCommon.pstrErrorMsg = strProcName + "ENV.ini " +
                                                 "[REG]FAMILYCODE_PG ERROR"
                        Exit Sub
                    End If
                    intReg = pClsPC10G.fncRegTypeGet(pstIni.strFamilycode_TY)
                    If intReg < 1 Then
                        mdlCommon.pblnEXFlg = False
                        mdlCommon.pstrErrorMsg = strProcName + "ENV.ini " +
                                                 "[REG]FAMILYCODE_AD ERROR"
                        Exit Sub
                    End If
                    stREG.intPGNo = intPG
                    stREG.intRegType = intReg
                    stREG.strReg = pstIni.strFamilycode_AD
                    Try
                        intFamilycode(0) = Asc(pstAlcData2PLC.strFamilycode.Substring(0, 1))
                        intFamilycode(1) = Asc(pstAlcData2PLC.strFamilycode.Substring(1, 1))
                        intFamilycode(2) = Asc(pstAlcData2PLC.strFamilycode.Substring(2, 1))
                        intFamilycode(3) = Asc(pstAlcData2PLC.strFamilycode.Substring(3, 1))
                        ReDim intSendBuffer(1)
                        intSendBuffer(0) = intFamilycode(0) + intFamilycode(1) * 256
                        intSendBuffer(1) = intFamilycode(2) + intFamilycode(3) * 256
                        Call pClsPC10G.fWriteWordAdr(stREG, intSendBuffer)
                    Catch ex As Exception
                        mdlCommon.pblnEXFlg = True
                        mdlCommon.psysException = ex
                        mdlCommon.pstrErrorMsg = strProcName + "Set FAMILYcode"
                        Exit Sub
                    End Try


                    'PLCの指定ﾚｼﾞｽﾀよりﾃﾞｰﾀ読み込み(BC連番) Read data from specified register of PLC (BC serial number)
                    intPG = pClsPC10G.fncPGNoGet(pstIni.strBCNo_PG)
                    If intPG < 0 Then
                        mdlCommon.pblnEXFlg = False
                        mdlCommon.pstrErrorMsg = strProcName + "ENV.ini " +
                                                 "[REG]BCNO_PG ERROR"
                        Exit Sub
                    End If
                    intReg = pClsPC10G.fncRegTypeGet(pstIni.strBCNo_TY)
                    If intReg < 1 Then
                        mdlCommon.pblnEXFlg = False
                        mdlCommon.pstrErrorMsg = strProcName + "ENV.ini " +
                                                 "[REG]BCNO_AD ERROR"
                        Exit Sub
                    End If
                    stREG.intPGNo = intPG
                    stREG.intRegType = intReg
                    stREG.strReg = pstIni.strBCNo_AD
                    Try
                        ReDim bytRecvBuffer(0)
                        Call pClsPC10G.fReadWordAdr(stREG, 1, bytRecvBuffer)
                        intBCNo(1) = bytRecvBuffer(6) * 256 + bytRecvBuffer(5)
                    Catch ex As Exception
                        mdlCommon.pblnEXFlg = True
                        mdlCommon.psysException = ex
                        mdlCommon.pstrErrorMsg = strProcName + "Get BCNo"
                        Exit Sub
                    End Try

                    'PLCの指定ﾚｼﾞｽﾀよりﾃﾞｰﾀ読み込み(車種番号) Read data from specified register of PLC (vehicle type number)
                    intPG = pClsPC10G.fncPGNoGet(pstIni.strAssyNo_PG)
                    If intPG < 0 Then
                        mdlCommon.pblnEXFlg = False
                        mdlCommon.pstrErrorMsg = strProcName + "ENV.ini " +
                                                 "[REG]ASSYNO_PG ERROR"
                        Exit Sub
                    End If
                    intReg = pClsPC10G.fncRegTypeGet(pstIni.strAssyNo_TY)
                    If intReg < 1 Then
                        mdlCommon.pblnEXFlg = False
                        mdlCommon.pstrErrorMsg = strProcName + "ENV.ini " +
                                                 "[REG]ASSYNO_AD ERROR"
                        Exit Sub
                    End If
                    stREG.intPGNo = intPG
                    stREG.intRegType = intReg
                    stREG.strReg = pstIni.strAssyNo_AD
                    Try
                        ReDim bytRecvBuffer(0)
                        Call pClsPC10G.fReadWordAdr(stREG, 1, bytRecvBuffer)
                        intAssyNo(1) = Hex(bytRecvBuffer(6) * 256 + bytRecvBuffer(5))
                    Catch ex As Exception
                        mdlCommon.pblnEXFlg = True
                        mdlCommon.psysException = ex
                        mdlCommon.pstrErrorMsg = strProcName + "GetAssyNo"
                        Exit Sub
                    End Try

                    If intBCNo(0) = intBCNo(1) And intAssyNo(0) = intAssyNo(1) Then
                        Exit Do
                    End If

                Loop

                '　送信完了信号をONする Turn on the transmission completion signal
                intPG = pClsPC10G.fncPGNoGet(pstIni.strAlcTrnsC_PG)
                If intPG < 0 Then
                    mdlCommon.pblnEXFlg = False
                    mdlCommon.pstrErrorMsg = strProcName + "ENV.ini " +
                                             "[REG]ALC_TRN_C_PG ERROR"
                    Exit Sub
                End If
                intReg = pClsPC10G.fncRegTypeGet(pstIni.strAlcTrnsC_TY)
                If intReg < 1 Then
                    mdlCommon.pblnEXFlg = False
                    mdlCommon.pstrErrorMsg = strProcName + "ENV.ini " +
                                             "[REG]ALC_TRN_C_AD ERROR"
                    Exit Sub
                End If
                stREG.intPGNo = intPG
                stREG.intRegType = intReg
                stREG.strReg = pstIni.strAlcTrnsC_AD
                Try
                    Call pClsPC10G.fWriteBitAdr(stREG, 1)
                Catch ex As Exception
                    pInsErrMsg.psubErrMsgShow(ex, strProcName +
                                              "Set TransCompSignal")
                End Try

                '送信したﾃﾞｰﾀを送信済みﾃｰﾌﾞﾙに追記 Add the posted data to the transmitted table
                'strSQL = "INSERT INTO tb_senddata (strAssyNo,strVehicleName," + _
                '         "intDelFlg,strSendDate) VALUES('" _
                '        & pstAlcData2PLC.strAssyNo.ToString & "','" _
                '        & pstAlcData2PLC.strVehicleName & "','0','" _
                '        & CmnGetNow(Now).ToString & "')"
                'If pClsSql.fSetSendData(strSQL) = pCErrOther Then
                '    Exit Sub
                'End If
                '送信したﾃﾞｰﾀを送信済みテキストに追記 Add the sent data to sent text
                mdlCommon.CmnPlcSendDataLog(pstAlcData2PLC.strAssyNo.ToString & "," & pstAlcData2PLC.strVehicleName & "," &
                      pstAlcData2PLC.strBCNo.ToString & "," & pstAlcData2PLC.strLOTcode & "," & pstAlcData2PLC.strFamilycode)

                'ALC受信ﾃｰﾌﾞﾙでPLCへ送信したﾃﾞｰﾀﾚｺｰﾄﾞを削除する
                strSQL = "DELETE FROM tb_alc WHERE id = " &
                         pstAlcData2PLC.id.ToString
                If pClsSql.fSetAlcDataPLC(strSQL) = pCErrOther Then
                    Exit Sub
                End If

            End If

        Catch ex As Exception
            'pInsErrMsg.psubErrMsgShow(ex, "frmMenu.psubPlcSendRecv")
            mdlCommon.pblnEXFlg = True
            mdlCommon.psysException = ex
            mdlCommon.pstrErrorMsg = "frmMenu.psubPlcSendRecv"
        End Try


    End Sub

    'ALC受信ﾃﾞｰﾀ(通知電文)からALC返信ﾃﾞｰﾀ(応答電文)を作成
    Private Sub psubAlcSend(ByVal strResult As String)

        Dim intCount As Integer

        For intCount = 0 To 5 Step 1
            pbytSendBuffer(intCount) = pbytRecvBuffer(intCount + 6)
            pbytSendBuffer(intCount + 6) = pbytRecvBuffer(intCount)
            pbytSendBuffer(intCount + 16) = Asc("0")
        Next intCount
        For intCount = 12 To 15 Step 1
            pbytSendBuffer(intCount) = pbytRecvBuffer(intCount)
        Next intCount
        pbytSendBuffer(22) = pbytRecvBuffer(22)
        pbytSendBuffer(23) = pbytRecvBuffer(23)
        pbytSendBuffer(24) = Asc(strResult.Substring(0, 1))
        pbytSendBuffer(25) = Asc(strResult.Substring(1, 1))

    End Sub

    'ALCからの受信状況ﾗﾍﾞﾙの変更
    Private Sub psubSetlblGalcStatus(ByVal strText As String)

        If lblGalcStatus.InvokeRequired Then
            Dim d As New SetTextCallback(AddressOf psubSetlblGalcStatus)
            Me.Invoke(d, New Object() {strText})
        Else
            lblGalcStatus.Text = strText
            Me.Refresh()
        End If
        GalcStatus = strText
    End Sub

    'PLCへの送信状況ﾗﾍﾞﾙの変更
    Private Sub psubSetlblPlcStatus(ByVal strText As String)

        If lblPlcStatus.InvokeRequired Then
            Dim d As New SetTextCallback(AddressOf psubSetlblPlcStatus)
            Me.Invoke(d, New Object() {strText})
        Else
            lblPlcStatus.Text = strText
            Me.Refresh()
        End If

    End Sub

    'lblBcRecvﾗﾍﾞﾙの変更
    Private Sub psubSetlblBcRecv(ByVal strText As String)

        If lblBcRecv.InvokeRequired Then
            Dim d As New SetTextCallback(AddressOf psubSetlblBcRecv)
            Me.Invoke(d, New Object() {strText})
        Else
            lblBcRecv.Text = strText
            Me.Refresh()
        End If

    End Sub

    'txtBcNextRecvﾗﾍﾞﾙの変更
    Private Sub psubSettxtBcNextRecv(ByVal strText As String)

        If txtBcNextRecv.InvokeRequired Then
            Dim d As New SetTextCallback(AddressOf psubSettxtBcNextRecv)
            Me.Invoke(d, New Object() {strText})
        Else
            txtBcNextRecv.Text = strText
            Me.Refresh()
        End If

    End Sub

    Private Sub bgwGalcConnect_RunWorkerCompleted(ByVal sender As Object,
                ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) _
                Handles bgwGalcConnect.RunWorkerCompleted

        If pflgSequenceErr = False Then

            btnGalcConnect_kako.Enabled = True
            btnGalcDisconnect_kako.Enabled = False
            If btnGalcConnect_kako.Enabled = True Then
                txtBcNextRecv.Enabled = True
            Else
                txtBcNextRecv.Enabled = False
            End If

        End If

        Call psubBgwErrorMsg()

        If pblnDisconnectFlg = False Then

            bgwGalcConnect.RunWorkerAsync()

        End If

    End Sub

    Private Sub bgwPlcConnect_RunWorkerCompleted(ByVal sender As Object,
                ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) _
                Handles bgwPlcConnect.RunWorkerCompleted

        btnPlcConnect.Enabled = True
        ' btnPlcDisconnect.Enabled = False
        Call psubBgwErrorMsg()

    End Sub

    Private Sub psubBgwErrorMsg()

        'SystemExceptionの有無で引数を変更
        If mdlCommon.pblnEXFlg Then
            pInsErrMsg.psubErrMsgShow(mdlCommon.psysException,
                                      mdlCommon.pstrErrorMsg)
            mdlCommon.pblnEXFlg = False
        Else
            If mdlCommon.pstrErrorMsg <> "" Then
                pInsErrMsg.psubErrMsgShow(mdlCommon.pstrErrorMsg)
            End If
        End If
        mdlCommon.pstrErrorMsg = ""

    End Sub

    'Iniﾌｧｲﾙより読み込んだﾚｼﾞｽﾀ情報を変換
    Private Sub subIniDataChange()

        Call subIniDataSprit(pstIni.strAlcConnD_TY, pstIni.strAlcConnD_AD)
        Call subIniDataSprit(pstIni.strAlcTrnsD_TY, pstIni.strAlcTrnsD_AD)
        Call subIniDataSprit(pstIni.strVehicleD_TY, pstIni.strVehicleD_AD)
        Call subIniDataSprit(pstIni.strVehiName_TY, pstIni.strVehiName_AD)
        Call subIniDataSprit(pstIni.strAlcTrnsC_TY, pstIni.strAlcTrnsC_AD)
        Call subIniDataSprit(pstIni.strVehicleC_TY, pstIni.strVehicleC_AD)
        Call subIniDataSprit(pstIni.strBCNo_TY, pstIni.strBCNo_AD)
        Call subIniDataSprit(pstIni.strAssyNo_TY, pstIni.strAssyNo_AD)
        Call subIniDataSprit(pstIni.strLOTcode_TY, pstIni.strLOTcode_AD)        'ﾛｯﾄｺｰﾄﾞ追加（2012/5）
        Call subIniDataSprit(pstIni.strFamilycode_TY, pstIni.strFamilycode_AD)  'ﾌｧﾐﾘｰｺｰﾄﾞ追加（2012/5）

    End Sub

    'ﾚｼﾞｽﾀ情報を種類とｱﾄﾞﾚｽに分割する
    Private Sub subIniDataSprit(ByRef strType As String, ByRef strAddr As String)

        strType = ""
        'strAddrの1文字目から数値になる直前までを抽出
        Do Until IsNumeric(Mid(strAddr, 1, 1))
            If True Then
                strType = strType & Mid(strAddr, 1, 1)
                strAddr = Mid(strAddr, 2, Len(strAddr) - 1)
            End If
        Loop

    End Sub

    Public Shared Sub subAlcSleep()
        'Thread.Sleep(100)
    End Sub
    Dim intCheckErrTimer As Integer

    Sub GALCConnect()
        Dim Tcp As System.Net.Sockets.TcpClient
        Dim ns As System.Net.Sockets.NetworkStream
        Dim strBuffer As String         '受信ﾃﾞｰﾀ結合文字列
        Dim intCheckResult As Integer   'ALCﾃﾞｰﾀ受信結果を格納
        Dim SocketAsyncEvent As New System.Net.Sockets.SocketAsyncEventArgs
        Dim bytSyncData(0) As Byte
        Dim intCheckDataSendTimer As Integer
        Dim intCheckDBTimer As Integer  'DBﾀﾞﾐｰﾘｰﾄﾞの回数を減らす為のクロック
        Dim strPortConnectState As String   'ポートの状態
        'Dim strHostIpAdr As String
        '        Dim threadAlcSleep As New Thread(AddressOf subAlcSleep)
        'Dim timeoutLoop As New TimeSpan(0, 0, 0, 0)

        Try
            'pintNextNo = txtBcNextRecv.Text
            'MsgBox("The connection begins.")

            '現在の連番を取得
            pintNextNo = txtBcNextRecv.Text
            'MsgBox("The connection begins.")
            Me.BeginInvoke(New MethodInvoker(Sub()
                                                 tmrAutoConnect.Enabled = False
                                             End Sub))


            txtBcNextRecv.Enabled = False
            btnGalcConnect.Enabled = False
            btnGalcDisconnect.Enabled = True

            Me.Refresh()

            '自分のIPを取得
            'strHostIpAdr = pfunLocalIpAdr()

            lblGalcStatus.BackColor = Color.Yellow
            '           Call psubSetlblGalcStatus("Waiting")
            lblGalcStatus.Text = "Waiting"
            GalcStatus = "Waiting"
            'lblGalcStatus.Text = "Espera"       'ﾎﾟﾙﾄｶﾞﾙ対応

            '           threadAlcSleep.Start()
            'Galc使用ﾎﾟｰﾄでﾘｽﾅｰを起動
            Listner = New System.Net.Sockets.TcpListener(System.Net.IPAddress.Any,
                                                        pstIni.strAlcPort)
            'Listner.Server.Connect(System.Net.IPAddress.Any, pstIni.strAlcPort)
            'Listner.Server.Blocking = False
            Listner.Start()
            '            strPortConnectState = pinsTcpState.pfunPortState(strHostIpAdr, pstIni.strAlcPort)

            '        pblnDisconnectFlg = False
            mdlCommon.pblnAlcLoop = True

            '-------------------------------------------------------------------------------
            ' 上位からの接続を待つ
            '-------------------------------------------------------------------------------
            Do
                If Listner.Pending() = True Then        '上位が接続するとここに来て抜ける
                    Exit Do
                End If
                '  Application.DoEvents()
                If mdlCommon.pblnAlcLoop = False Then
                    'G-ALC通信状態ﾗﾍﾞﾙの背景を赤にする
                    lblGalcStatus.BackColor = Color.Red
                    '                    Call psubSetlblGalcStatus("Disconnect")
                    lblGalcStatus.Text = "Disconnect"
                    GalcStatus = "Disconnect"
                    'lblGalcStatus.Text = "Conexão"    'ﾎﾟﾙﾄｶﾞﾙ対応

                    txtBcNextRecv.Enabled = True
                    btnGalcConnect.Enabled = True
                    btnGalcDisconnect.Enabled = False
                    Listner.Stop()
                    Exit Sub
                End If
                'System.Threading.Thread.Sleep(100)
                'System.Threading.Thread.CurrentThread.Interrupt()
                'System.Threading.Thread.CurrentThread.Join(100)
                'threadAlcSleep.Join(100)
                'threadAlcSleep.Start()
            Loop

            '-------------------------------------------------------------------------------
            ' 接続中...
            '-------------------------------------------------------------------------------
            'G-ALC通信状態ﾗﾍﾞﾙの背景を緑にする
            lblGalcStatus.BackColor = Color.LightGreen
            'G-ALC通信状態ﾗﾍﾞﾙの表記を｢受信中｣にする
            lblGalcStatus.Text = "Connect"
            GalcStatus = "Connect"
            'lblGalcStatus.Text = "Conexão"      'ﾎﾟﾙﾄｶﾞﾙ対応
            '接続要求がきたら接続
            Tcp = Listner.AcceptTcpClient()

            'pWatetimer = 5
            'Do While pWatetimer <> 0
            '    Application.DoEvents()
            '    If mdlCommon.pblnAlcLoop = False Then
            '        Exit Do
            '    End If
            'Loop

            If Tcp.Connected = True Then
            Else
                MessageBox.Show("NO CONNECT")
            End If
            'G-ALCﾃﾞｰﾀ受信
            ns = Tcp.GetStream()

            Tcp.ReceiveTimeout = 1000
            Tcp.SendTimeout = 1000

            ns.WriteTimeout = 1000
            ns.ReadTimeout = 1000
            'コネクト状態

            Dim iTcpStatus As Integer
            iTcpStatus = clsTCPStatus.GetPortStatus(pstIni.strAlcIP, pstIni.strAlcPort)

            ' ''-----------------------------------------------------
            ' ''TIME_WAITの場合はメッセージを表示してESTABになったら自動接続(2011/10/25 Kasuga)
            ''If iTcpStatus = ClassTcpStatus.TCP_STATUS.MIB_TCP_STATE_TIME_WAIT Then
            ''    lblGalcStatus.BackColor = Color.Red
            ''    txtBcNextRecv.Enabled = True
            ''    btnGalcConnect.Enabled = True
            ''    btnGalcDisconnect.Enabled = False
            ''    pInsfrmMsg.Show()
            ''    'pInsfrmMsg.ShowDialog()
            ''    'pInsfrmMsg.lblMsg.Text = "通信がクローズ処理中です。120秒間お待ちください。クローズ処理後に自動接続します。"
            ''    pInsfrmMsg.lblMsg.Text = "Estreita comunicação está em andamento. Por favor, espere 120 segundos." & vbCrLf & "Conectar-se automaticamente após o encerramento."

            ''    Dim start As DateTime = Now
            ''    Do
            ''        Application.DoEvents()
            ''        'ESTABになったら自動接続
            ''        iTcpStatus = clsTCPStatus.GetPortStatus(pstIni.strAlcIP, pstIni.strAlcPort)
            ''        If iTcpStatus = ClassTcpStatus.TCP_STATUS.MIB_TCP_STATE_ESTAB Then
            ''            pInsfrmMsg.Close()
            ''            lblGalcStatus.BackColor = Color.LightGreen
            ''            txtBcNextRecv.Enabled = False
            ''            btnGalcConnect.Enabled = False
            ''            btnGalcDisconnect.Enabled = True
            ''            tmrAutoConnect.Enabled = True
            ''            Exit Do
            ''        End If
            ''        System.Threading.Thread.Sleep(1000)
            ''        Dim span As TimeSpan = Now - start

            ''        '120秒以上経過したらエラーとする
            ''        If span.TotalSeconds > 150 Then
            ''            pInsfrmMsg.Close()
            ''            'MessageBox.Show("クローズできません。プログラムを再起動してください。", "メッセージ", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            ''            MessageBox.Show("Ser fechado. Por favor, inicie o programa novamente.", "mensagem", MessageBoxButtons.OK, MessageBoxIcon.Warning)

            ''            lblGalcStatus.BackColor = Color.Red
            ''            txtBcNextRecv.Enabled = True
            ''            btnGalcConnect.Enabled = True
            ''            btnGalcDisconnect.Enabled = False
            ''            Exit Sub
            ''        End If
            ''    Loop
            ''End If
            ' ''-----------------------------------------------------


            'strPortConnectState = pinsTcpState.pfunPortState(strHostIpAdr, pstIni.strAlcPort)
            'strPortConnectState = pinsTcpState.pfunPortState("192.168.0.102", pstIni.strAlcPort)
            'Do
            '    Application.DoEvents()
            '    If strPortConnectState = "ESTAB" Then
            '        Exit Do
            '    Else
            '        strPortConnectState = pinsTcpState.pfunPortState(strHostIpAdr, pstIni.strAlcPort)
            '    End If
            'Loop

            SocketAsyncEvent.SetBuffer(bytSyncData, 0, 0)
            Tcp.Client.SendBufferSize = 0
            Tcp.Client.SendAsync(SocketAsyncEvent)
            '送信チェック時間取得
            intCheckDataSendTimer = My.Computer.Clock.TickCount
            intCheckDBTimer = My.Computer.Clock.TickCount
            intCheckErrTimer = My.Computer.Clock.TickCount
            Do
                Struck = "-1"
                'ALC受信処理
                '   Application.DoEvents()
                'threadAlcSleep.Start()
                System.Threading.Thread.Sleep(100)
                'System.Threading.Thread.CurrentThread.Interrupt()
                'System.Threading.Thread.CurrentThread.Join(100)
                'ボタンを押して切断した場合は自動接続はしない
                If mdlCommon.pblnAlcLoop = False Then
                    Exit Do
                End If
                'System.Threading.Thread.Sleep(0)
                '処理中にｷｬﾝｾﾙされていないかを定期的にﾁｪｯｸする
                'If bgwGalcConnect.CancellationPending = True Then
                '    e.Cancel = True
                '    Exit Do
                'End If
                '送信チェック時間判定

                Struck = "0"
                If (My.Computer.Clock.TickCount - intCheckDataSendTimer) > 3000 Then
                    '接続状態かどうか調べる（0バイトを送信する）

                    intCheckDataSendTimer = My.Computer.Clock.TickCount
                    intCheckErrTimer = My.Computer.Clock.TickCount


                    Struck = "1"
                    SocketAsyncEvent.SetBuffer(0, 0)
                    Listner.Server.SendAsync(SocketAsyncEvent)
                    Tcp.Client.SendAsync(SocketAsyncEvent)
                    Tcp.Client.Send(bytSyncData, 0, Net.Sockets.SocketFlags.None)
                    Struck = "2"
                    ns.Write(bytSyncData, 0, 1) 'From  ns.Write(bytSyncData, 0, 0) not write data
                    'コネクト状態を取得 （DLL関係がエラーを出している場合は取得しない）
                    'If strPortConnectState <> "Error" Then
                    '    strPortConnectState = pinsTcpState.pfunPortState(strHostIpAdr, pstIni.strAlcPort)
                    'End If
                    If iTcpStatus <> ClassTcpStatus.TCP_STATUS.MIB_TCP_STATE_ERROR Then
                        iTcpStatus = clsTCPStatus.GetPortStatus(pstIni.strAlcIP, pstIni.strAlcPort)
                    End If

                    ' 使用ポートの接続状態を表示
                    ToolStripStatusLabel1.Text = "Connection Status:" + iTcpStatus.ToString

                ElseIf (My.Computer.Clock.TickCount - intCheckDataSendTimer) < 0 Then
                    'クロックのリセットが行われた時の処理
                    intCheckDataSendTimer = My.Computer.Clock.TickCount
                End If



                '切断状態なら
                If Tcp.Client.Connected = False Or IsConnected(Tcp) = False Then
                    '自動接続


                    pInsErrMsg.psubErrMsg("Remote Disconnect !! Tcp.Client.Connected = [Connection Status]:" & Tcp.Client.Connected)
                    'pInsErrMsg.psubErrMsgShow("Desligue remoto !! Tcp.Client.Connected = [Connection Status]:" & Tcp.Client.Connected)      'ﾎﾟﾙﾄｶﾞﾙ対応
                    Me.BeginInvoke(New MethodInvoker(Sub()
                                                         tmrAutoConnect.Enabled = True
                                                     End Sub))
                    Exit Do
                End If
                'DLL操作がエラーを出している時は判定しません 要調査（接続してもLISTEN状態）
                'If strPortConnectState <> "Error" Then

                Struck = "3"
                If iTcpStatus <> ClassTcpStatus.TCP_STATUS.MIB_TCP_STATE_ERROR Then
                    'If strPortConnectState <> "ESTAB" Then
                    If iTcpStatus <> ClassTcpStatus.TCP_STATUS.MIB_TCP_STATE_ESTAB Then

                        'TIME_WAITの場合はメッセージを表示(2011/10/19 Kasuga)
                        If iTcpStatus = ClassTcpStatus.TCP_STATUS.MIB_TCP_STATE_TIME_WAIT Then
                            'MessageBox.Show("通信がクローズ処理中です。30秒間お待ちください...", "メッセージ", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            MessageBox.Show("Connection is Closing. Please wait 120Second....   ", "Message", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            'MessageBox.Show("Estreita comunicação está em andamento. Por favor, aguarde 120 segundos．．．", "mensagem", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            Me.BeginInvoke(New MethodInvoker(Sub()
                                                                 tmrAutoConnect.Enabled = True
                                                             End Sub))
                            Exit Do
                        End If

                        '自動接続
                        pInsErrMsg.psubErrMsgShow("Remote Disconnect !! [Connection Status]:" & strPortConnectState)    '要確認！！
                        'pInsErrMsg.psubErrMsgShow("Desligue remoto !! [Connection Status]:" & pinsTcpState.GetState(iTcpStatus))      'ﾎﾟﾙﾄｶﾞﾙ対応
                        Me.BeginInvoke(New MethodInvoker(Sub()
                                                             tmrAutoConnect.Enabled = True
                                                         End Sub))

                        Exit Do
                    End If
                End If

                'エラー処理
                If pfunConnectionError(SocketAsyncEvent) = True Then
                    Exit Do
                End If

                '電文が来たら
                Struck = "4"
                If ns.DataAvailable = True Then
                    Struck = "4-1"
                    '  System.Threading.Thread.Sleep(310000)

                    pintRecvSize = ns.Read(pbytRecvBuffer, 0,
                                           pbytRecvBuffer.Length)

                    Struck = "5"
                    '受信ﾃﾞｰﾀのﾛｷﾞﾝｸﾞ
                    strBuffer = ""
                    For intCount = 0 To pintRecvSize - 1 Step 1
                        strBuffer = strBuffer & Chr(pbytRecvBuffer(intCount))
                    Next intCount
                    Call CmnAlcRecvDataLog(strBuffer)
                    Struck = "6"
                    '受信ﾃﾞｰﾀﾁｪｯｸしてDBに格納
                    intCheckResult = pfncAlcConnect(pbytRecvBuffer)
                    Struck = "7"
                    '受信応答電文送信
                    ns.Write(pbytSendBuffer, 0, pbytSendBuffer.Length)
                    Struck = "8"
                    pfrmDataDisp.psubAlcdispRefresh()
                    '受信ﾃﾞｰﾀﾁｪｯｸ
                    '切断はしないように変更しました。
                    'If intCheckResult <> pCSuccess Then
                    '    e.Cancel = True
                    '    Exit Do
                    'End If
                End If


                If (My.Computer.Clock.TickCount - intCheckDBTimer) > 3600000 Then
                    Struck = "9"
                    'MySQLが8h放置で勝手にDisconnectの防止用ﾀﾞﾐｰSQL
                    Call pClsSql.sDummySQL1()
                    '  Application.DoEvents()
                    intCheckDBTimer = My.Computer.Clock.TickCount
                ElseIf (My.Computer.Clock.TickCount - intCheckDBTimer) < 0 Then
                    Struck = "9-1"
                    intCheckDBTimer = My.Computer.Clock.TickCount
                End If
                '                Application.DoEvents()

            Loop
            '            Listner.Server.Shutdown(Net.Sockets.SocketShutdown.Both)
            Tcp.Client.Shutdown(Net.Sockets.SocketShutdown.Both)
            Listner.Stop()
            '            Listner.Server.Disconnect(True)
            Tcp.Client.Shutdown(Net.Sockets.SocketShutdown.Both)
            Tcp.Client.Disconnect(False)
            Tcp.Close()
            ns.Close()
            ns.Dispose()

            lblGalcStatus.BackColor = Color.Red
            'Call psubSetlblGalcStatus("受信切断")
            '                    Call psubSetlblGalcStatus("Disconnect")
            lblGalcStatus.Text = "Disconnect"
            GalcStatus = "Disconnect"
            'lblGalcStatus.Text = "Corte"    'ﾎﾟﾙﾄｶﾞﾙ対応

            txtBcNextRecv.Enabled = True
            btnGalcConnect.Enabled = True
            btnGalcDisconnect.Enabled = False


            If Not pClsSql.pCmdDummy1 Is Nothing Then
                'MySQLを起こしておく為のSQLｺﾏﾝﾄﾞをDispose
                pClsSql.pCmdDummy1.Dispose()
            End If
            Me.BeginInvoke(New MethodInvoker(Sub()
                                                 tmrAutoConnect.Enabled = True
                                             End Sub))

        Catch ex As Exception
            pInsErrMsg.psubErrMsgShow(ex, "frmMenu.btnGalcConnect_Click_1")
            ' Listner.Stop()


            Listner.Stop()
            '            Listner.Server.Disconnect(True)

            Try ' 30/10/2018 fix not auto connect
                Tcp.Client.Shutdown(Net.Sockets.SocketShutdown.Both)
                Tcp.Client.Disconnect(False)
                Tcp.Close()
                ns.Close()
                ns.Dispose()
            Catch ex1 As Exception

            End Try



            'Tcp.Close()
            'ns.Close()
            'ns.Dispose()
            lblGalcStatus.Text = "Disconnect"
            GalcStatus = "Disconnect"
            'lblGalcStatus.Text = "Corte"    'ﾎﾟﾙﾄｶﾞﾙ対応

            lblGalcStatus.BackColor = Color.Red
            txtBcNextRecv.Enabled = True
            btnGalcConnect.Enabled = True
            btnGalcDisconnect.Enabled = False
            If Not pClsSql.pCmdDummy1 Is Nothing Then
                'MySQLを起こしておく為のSQLｺﾏﾝﾄﾞをDispose
                pClsSql.pCmdDummy1.Dispose()
            End If

            '自動接続
            Me.BeginInvoke(New MethodInvoker(Sub()
                                                 tmrAutoConnect.Enabled = True
                                             End Sub))
        End Try
    End Sub
    'G-ALC接続ボタンクリック
    Private Sub btnGalcConnect_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGalcConnect.Click
        Dim thd As New Thread(AddressOf GALCConnect)
        thd.IsBackground = True
        thd.Start()

    End Sub
    Public ReadOnly Property IsConnected(Tcp As System.Net.Sockets.TcpClient) As Boolean
        Get

            Try

                If Tcp IsNot Nothing AndAlso Tcp.Client IsNot Nothing AndAlso Tcp.Client.Connected Then

                    If Tcp.Client.Poll(0, Net.Sockets.SelectMode.SelectRead) Then
                        Dim buff As Byte() = New Byte(0) {}

                        If Tcp.Client.Receive(buff, Net.Sockets.SocketFlags.Peek) = 0 Then
                            Return False
                        Else
                            Return True
                        End If
                    End If

                    Return True
                Else
                    Return False
                End If

            Catch
                Return False
            End Try
        End Get
    End Property
    Private Function pfunConnectionError(ByRef aSocketAsyncEvent As System.Net.Sockets.SocketAsyncEventArgs) As Boolean
        pfunConnectionError = False
        Try

            If aSocketAsyncEvent.SocketError <> Net.Sockets.SocketError.Success Then
                Select Case aSocketAsyncEvent.SocketError
                    Case Net.Sockets.SocketError.AccessDenied
                        '自動接続
                        '  tmrAutoConnect.Enabled = True
                        pInsErrMsg.psubErrMsgShow("Connection Error " & aSocketAsyncEvent.SocketError.ToString)
                        pfunConnectionError = True
                    Case Net.Sockets.SocketError.ConnectionAborted
                        '  tmrAutoConnect.Enabled = True
                        pInsErrMsg.psubErrMsgShow("Connection Error " & aSocketAsyncEvent.SocketError.ToString)
                        pfunConnectionError = True
                    Case Net.Sockets.SocketError.ConnectionRefused
                        ' tmrAutoConnect.Enabled = True
                        pInsErrMsg.psubErrMsgShow("Connection Error " & aSocketAsyncEvent.SocketError.ToString)
                        pfunConnectionError = True
                    Case Net.Sockets.SocketError.ConnectionReset
                        '  tmrAutoConnect.Enabled = True
                        pInsErrMsg.psubErrMsgShow("Connection Error " & aSocketAsyncEvent.SocketError.ToString)
                        pfunConnectionError = True
                    Case Net.Sockets.SocketError.Disconnecting
                        ' tmrAutoConnect.Enabled = True
                        pInsErrMsg.psubErrMsgShow("Connection Error " & aSocketAsyncEvent.SocketError.ToString)
                        pfunConnectionError = True
                    Case Net.Sockets.SocketError.Fault
                        ' tmrAutoConnect.Enabled = True
                        pInsErrMsg.psubErrMsgShow("Connection Error " & aSocketAsyncEvent.SocketError.ToString)
                        pfunConnectionError = True
                    Case Net.Sockets.SocketError.HostDown
                        '  tmrAutoConnect.Enabled = True
                        pInsErrMsg.psubErrMsgShow("Connection Error " & aSocketAsyncEvent.SocketError.ToString)
                        pfunConnectionError = True
                    Case Net.Sockets.SocketError.HostNotFound
                        ' tmrAutoConnect.Enabled = True
                        pInsErrMsg.psubErrMsgShow("Connection Error " & aSocketAsyncEvent.SocketError.ToString)
                        pfunConnectionError = True
                    Case Net.Sockets.SocketError.Interrupted
                        '  tmrAutoConnect.Enabled = True
                        pInsErrMsg.psubErrMsgShow("Connection Error " & aSocketAsyncEvent.SocketError.ToString)
                        pfunConnectionError = True
                    Case Net.Sockets.SocketError.IOPending
                        ' tmrAutoConnect.Enabled = True
                        pInsErrMsg.psubErrMsgShow("Connection Error " & aSocketAsyncEvent.SocketError.ToString)
                        pfunConnectionError = True
                    Case Net.Sockets.SocketError.NetworkDown
                        ' tmrAutoConnect.Enabled = True
                        pInsErrMsg.psubErrMsgShow("Connection Error " & aSocketAsyncEvent.SocketError.ToString)
                        pfunConnectionError = True
                    Case Net.Sockets.SocketError.NetworkReset
                        '  tmrAutoConnect.Enabled = True
                        pInsErrMsg.psubErrMsgShow("Connection Error " & aSocketAsyncEvent.SocketError.ToString)
                        pfunConnectionError = True
                        '                        Case Net.Sockets.SocketError.NotConnected
                        '                            MessageBox.Show("Connection Error " & SocketAsyncEvent.SocketError.ToString)
                        '                            Exit Do
                    Case Net.Sockets.SocketError.NotInitialized
                        '  tmrAutoConnect.Enabled = True
                        pInsErrMsg.psubErrMsgShow("Connection Error " & aSocketAsyncEvent.SocketError.ToString)
                        pfunConnectionError = True
                    Case Net.Sockets.SocketError.OperationAborted
                        '  tmrAutoConnect.Enabled = True
                        pInsErrMsg.psubErrMsgShow("Connection Error " & aSocketAsyncEvent.SocketError.ToString)
                        pfunConnectionError = True
                    Case Net.Sockets.SocketError.Shutdown
                        '  tmrAutoConnect.Enabled = True
                        pInsErrMsg.psubErrMsgShow("Connection Error " & aSocketAsyncEvent.SocketError.ToString)
                        pfunConnectionError = True
                    Case Net.Sockets.SocketError.SocketError
                        ' tmrAutoConnect.Enabled = True
                        pInsErrMsg.psubErrMsgShow("Connection Error " & aSocketAsyncEvent.SocketError.ToString)
                        pfunConnectionError = True
                    Case Net.Sockets.SocketError.TimedOut
                        ' tmrAutoConnect.Enabled = True
                        pInsErrMsg.psubErrMsgShow("Connection Error " & aSocketAsyncEvent.SocketError.ToString)
                        pfunConnectionError = True
                End Select

            End If
        Catch ex As Exception
            pInsErrMsg.psubErrMsgShow(ex, "frmMenu.pfunConnectionError")
        End Try

    End Function


    Private Sub btnGalcDisconnect_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGalcDisconnect.Click
        If MsgBox("Do you disconnect the connection?", MsgBoxStyle.OkCancel) =
                                               MsgBoxResult.Cancel Then
            'If MsgBox("Você terminar a comunicação?", MsgBoxStyle.OkCancel) = _
            '                                       MsgBoxResult.Cancel Then         'ﾎﾟﾙﾄｶﾞﾙ対応
            Exit Sub
        End If
        mdlCommon.pblnAlcLoop = False

    End Sub

    Private Sub tmrAutoConnect_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrAutoConnect.Tick
        tmrAutoConnect.Enabled = False
        '  btnGalcConnect.PerformClick()
        Call Me.btnGalcConnect_Click_1(sender, e)
    End Sub
    '自分のIPアドレス取得
    Public Function pfunLocalIpAdr() As String
        Dim HostIP() As System.Net.IPAddress
        Dim strHostName As String


        strHostName = System.Net.Dns.GetHostName()
        HostIP = System.Net.Dns.GetHostAddresses(strHostName)

        pfunLocalIpAdr = HostIP(0).ToString

        'MessageBox.Show(HostIP(0).ToString)
    End Function

    Private Sub txtBcNextRecv_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBcNextRecv.TextChanged

    End Sub

    Private Sub btnPlcDisconnect_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)
        mdlCommon.pblnPlcLoop = False
    End Sub

    Public Shared Sub subPlcSleep()
        'Thread.Sleep(100)
    End Sub

    Public waitCallback As New WaitCallback(AddressOf psubPlcMainLoop)
    Private Shared Sub psubPlcMainLoop(ByVal afrmMenu As Object)
        Dim intDummyCount As Integer    'DBﾀﾞﾐｰﾘｰﾄﾞの回数を減らす為のｶｳﾝﾀ
        'Dim timeoutLoop As New TimeSpan(0, 0, 0, 0)
        mdlCommon.pblnPlcLoop = True
        'Dim threadSleep As New Thread(AddressOf subPlcSleep)

        'threadSleep.SetApartmentState(ApartmentState.MTA)


        afrmMenu.lblPlcStatus.BackColor = Color.Yellow
        afrmMenu.lblPlcStatus.Text = "Waiting"
        'afrmMenu.lblPlcStatus.Text = "Espera"  'ポルトガル対応
        afrmMenu.lblPlcStatus.Refresh()
        afrmMenu.btnPlcConnect.Enabled = False
        afrmMenu.btnPlcDisconnect.Enabled = True

        Application.DoEvents()
        '通信開始
        If Not pClsPC10G.fConnectON(pstIni.strPlcIp, pstIni.strPlcPort) Then
            GoTo Disconnect_Label   '切断処理
        End If
        Try
            afrmMenu.lblPlcStatus.BackColor = Color.LightGreen
            afrmMenu.lblPlcStatus.Text = "Connect"
            'afrmMenu.lblPlcStatus.Text = "Conexão"    'ポルトガル対応
            'lblPlcStatus.Text = "送信中"
            'Call psubSetlblPlcStatus("送信中")
            '           Call psubSetlblPlcStatus("Connect")
            pClsPC10G.fSetPlcClock()                '時計を変更(PC→PLC)

            intDummyCount = 0
            pClsSql.pCmdDummy2 = New MySqlCommand("DESCRIBE tb_vehicle",
                                                  pClsSql.pCon)   'ﾀﾞﾐｰ用SQL
            'threadSleep.Start()
            Do 'ﾃﾞｰﾀ読み込み
                '処理中にｷｬﾝｾﾙされていないかを定期的にﾁｪｯｸする
                'If bgwPlcConnect.CancellationPending = True Then
                '    e.Cancel = True
                '    GoTo Disconnect_Label   '切断処理
                'End If
                Application.DoEvents()
                If mdlCommon.pblnPlcLoop = False Then
                    GoTo Disconnect_Label   '切断処理
                End If

                '                System.Threading.Thread.Sleep(100)
                System.Threading.Thread.Sleep(100)
                '<PLC送受信処理>
                Call frmMenu.psubPlcSendRecv()

                If intDummyCount > 60000 Then
                    'MySQLが8h放置で勝手にDisconnectの防止用ﾀﾞﾐｰSQL
                    Call pClsSql.sDummySQL2()
                    Application.DoEvents()
                    intDummyCount = 0
                Else
                    intDummyCount = intDummyCount + 1
                End If
            Loop
            If Not pClsSql.pCmdDummy2 Is Nothing Then
                'MySQLを起こしておく為のSQLｺﾏﾝﾄﾞをDispose
                pClsSql.pCmdDummy2.Dispose()
            End If
        Catch ex As Exception
            'mdlCommon.pblnEXFlg = True
            'mdlCommon.psysException = ex
            'mdlCommon.pstrErrorMsg = "frmMenu.btnPlcConnect"
            pInsErrMsg.psubErrMsgShow(ex, "frmMenu.btnPlcConnect")
        End Try

Disconnect_Label:
        afrmMenu.lblPlcStatus.BackColor = Color.Red
        afrmMenu.btnPlcConnect.Enabled = True
        '  afrmMenu.btnPlcDisconnect.Enabled = False
        'lblPlcStatus.Text = "送信停止"
        'Call psubSetlblPlcStatus("送信停止")
        'Call psubSetlblPlcStatus("Disconnect")
        afrmMenu.lblPlcStatus.Text = "Disconnect"
        '切断処理
        pClsPC10G.fConnectOFF()
        Application.DoEvents()
    End Sub


    Private Sub btnPlcConnect_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim intDummyCount As Integer    'DBﾀﾞﾐｰﾘｰﾄﾞの回数を減らす為のｶｳﾝﾀ
        'Dim timeoutLoop As New TimeSpan(0, 0, 0, 0)
        mdlCommon.pblnPlcLoop = True
        Dim threadSleep As New Thread(AddressOf subPlcSleep)

        'ThreadPool.QueueUserWorkItem(waitCallback, Me)
        'Exit Sub

        lblPlcStatus.BackColor = Color.Yellow
        lblPlcStatus.Text = "Waiting"
        'lblPlcStatus.Text = "Espera"    'ポルトガル対応
        lblPlcStatus.Refresh()
        btnPlcConnect.Enabled = False
        btnPlcDisconnect.Enabled = True
        Application.DoEvents()
        '通信開始
        If Not pClsPC10G.fConnectON(pstIni.strPlcIp, pstIni.strPlcPort) Then
            GoTo Disconnect_Label   '切断処理
        End If
        Try
            lblPlcStatus.BackColor = Color.LightGreen
            lblPlcStatus.Text = "Connect"
            'lblPlcStatus.Text = "Conexão"   'ポルトガル対応
            'lblPlcStatus.Text = "送信中"
            'Call psubSetlblPlcStatus("送信中")
            '           Call psubSetlblPlcStatus("Connect")
            pClsPC10G.fSetPlcClock()                '時計を変更(PC→PLC)

            intDummyCount = 0
            pClsSql.pCmdDummy2 = New MySqlCommand("DESCRIBE tb_vehicle",
                                                  pClsSql.pCon)   'ﾀﾞﾐｰ用SQL
            threadSleep.Start()
            Do 'ﾃﾞｰﾀ読み込み
                '処理中にｷｬﾝｾﾙされていないかを定期的にﾁｪｯｸする
                'If bgwPlcConnect.CancellationPending = True Then
                '    e.Cancel = True
                '    GoTo Disconnect_Label   '切断処理
                'End If
                Application.DoEvents()
                If mdlCommon.pblnPlcLoop = False Then
                    GoTo Disconnect_Label   '切断処理
                End If

                '                System.Threading.Thread.Sleep(100)
                System.Threading.Thread.CurrentThread.Join(100)
                '<PLC送受信処理>
                Call psubPlcSendRecv()

                If intDummyCount > 60000 Then
                    'MySQLが8h放置で勝手にDisconnectの防止用ﾀﾞﾐｰSQL
                    Call pClsSql.sDummySQL2()
                    Application.DoEvents()
                    intDummyCount = 0
                Else
                    intDummyCount = intDummyCount + 1
                End If
            Loop
            If Not pClsSql.pCmdDummy2 Is Nothing Then
                'MySQLを起こしておく為のSQLｺﾏﾝﾄﾞをDispose
                pClsSql.pCmdDummy2.Dispose()
            End If
        Catch ex As Exception
            'mdlCommon.pblnEXFlg = True
            'mdlCommon.psysException = ex
            'mdlCommon.pstrErrorMsg = "frmMenu.btnPlcConnect"
            pInsErrMsg.psubErrMsgShow(ex, "frmMenu.btnPlcConnect")
        End Try

Disconnect_Label:
        lblPlcStatus.BackColor = Color.Red
        btnPlcConnect.Enabled = True
        '  btnPlcDisconnect.Enabled = False

        'lblPlcStatus.Text = "送信停止"
        'Call psubSetlblPlcStatus("送信停止")
        'Call psubSetlblPlcStatus("Disconnect")
        lblPlcStatus.Text = "Disconnect"
        '切断処理
        pClsPC10G.fConnectOFF()
        Application.DoEvents()


    End Sub

    Public waitBtn1 As New WaitCallback(AddressOf psubBtnLoop)
    Public flgBtnLoop As Boolean
    Public Shared Sub psubBtnLoop()
        frmMenu.flgBtnLoop = True
        Do
            Application.DoEvents()
            Thread.Sleep(100)
            If frmMenu.flgBtnLoop = False Then
                Exit Sub
            End If
        Loop
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ThreadPool.QueueUserWorkItem(waitBtn1)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        flgBtnLoop = False
    End Sub


    Dim timer_checkstruck As Threading.Timer
    Dim Struck As String
    Sub CheckSTRUCK()
        timer_checkstruck.Change(Timeout.Infinite, Timeout.Infinite)
        Try
            Debug.WriteLine((My.Computer.Clock.TickCount - intCheckErrTimer))
            If intCheckErrTimer > 0 And (My.Computer.Clock.TickCount - intCheckErrTimer) > 30000 Then
                intCheckErrTimer = My.Computer.Clock.TickCount
                '  If lblGalcStatus.Text = "Connect" Then
                Debug.WriteLine("No Loop 30 sec")

                SaveMSG("No Loop 30 sec :" & Struck & ":" & GalcStatus, "ERR")
                '  End If
                'Me.BeginInvoke(New MethodInvoker(Sub()
                pInsErrMsg.psubErrMsgShow("No Loop 30 sec :" & Struck & ":" & GalcStatus)
                '                                     ' Application.Restart()
                '                                 End Sub))

            End If
        Catch ex As Exception

        Finally
            timer_checkstruck.Change(1000, 1000)
        End Try

    End Sub
    Private Sub Timer1s_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1s.Tick
        If pWatetimer <> 0 Then
            pWatetimer -= 1
        End If
        ShowERR()
        ReconnectPLC()


    End Sub




    Sub ShowERR()
        If pInsErrMsg.ShowERR > 0 Then
            ToolStripStatusLabel1.Visible = True
            ToolStripStatusLabel1.Text = pInsErrMsg.ErrMsg
            ToolStripStatusLabel1.BackColor = Color.Red
            pInsErrMsg.ShowERR -= 1
        Else
            ToolStripStatusLabel1.Visible = False

        End If

    End Sub


    Function ExistFrm(Of T As Form)()
        For Each f As Form In Application.OpenForms
            If TypeOf f Is T Then
                f.Activate()
                Return False
            End If
        Next

        Return True
    End Function
    Private Sub btnSequence_Click(sender As Object, e As EventArgs) Handles btnSequence.Click

        'If (ExistFrm(Of MixMonitorFrm)() = False) Then
        '    Exit Sub
        'End If
        'Dim frm As New MixMonitorFrm
        'frm.Show()

        Try


            Process.Start("Mixmonitor.exe")
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs)

    End Sub

    Private Sub frmMenu_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        Try
            SaveMSG("Close GALC", "ERR")
        Catch ex As Exception

        End Try
    End Sub
End Class


