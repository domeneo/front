﻿Public Class clsPublicIni


    Public Const pcstrIniFileName As String = "ENV.ini"

    Public Shared Sub psubIniLoad()

        Dim strIniFileName As String

        Try
            strIniFileName = Application.StartupPath & "\" & pcstrIniFileName
            pstIni.strAlcPort = ClsWinApi.GetIniString("SYS", "ALC_PORT", _
                                        strIniFileName, "30000")
            pstIni.strAlcIP = ClsWinApi.GetIniString("SYS", "ALC_IP", _
                                        strIniFileName, "127.0.0.1")
            pstIni.strPlcIp = ClsWinApi.GetIniString("SYS", "PLC_IP", _
                                        strIniFileName, "192.168.0.99")
            pstIni.strPlcPort = ClsWinApi.GetIniString("SYS", "PLC_PORT", _
                                        strIniFileName, "6000")
            pstIni.strChangeDay = ClsWinApi.GetIniString("SYS", "CHANGE_DAY", _
                                        strIniFileName, "5")
            pstIni.strBcNo = ClsWinApi.GetIniString("SYS", "BCNO",
                                        strIniFileName, "0")

            pstAlcRecv.strLastSEQ = ClsWinApi.GetIniString("SYS", "LASTSEQ",
                                        strIniFileName, "0")

            pstIni.strDbIp = ClsWinApi.GetIniString("DB", "IP", _
                                        strIniFileName, "localhost")
            pstIni.strDbName = ClsWinApi.GetIniString("DB", "NAME", _
                                        strIniFileName, "380A")
            pstIni.strDbPort = ClsWinApi.GetIniString("DB", "PORT", _
                                        strIniFileName, "3360")
            pstIni.strDbUser = ClsWinApi.GetIniString("DB", "USER", _
                                        strIniFileName, "root")
            pstIni.strDbPass = ClsWinApi.GetIniString("DB", "PASS", _
                                        strIniFileName, "")
            pstIni.strDirLog = ClsWinApi.GetIniString("DIR", "LOG", _
                                        strIniFileName, "C:\DATA\LOG")
            pstIni.strDebugMode = ClsWinApi.GetIniString("DEBUG", "MODE", _
                                        strIniFileName, "1")
            pstIni.strAlcConnD_PG = ClsWinApi.GetIniString("REG", _
                                        "ALC_CON_D_PG", strIniFileName, "P2")
            pstIni.strAlcConnD_AD = ClsWinApi.GetIniString("REG", _
                                        "ALC_CON_D_AD", strIniFileName, "L700")
            pstIni.strAlcTrnsD_PG = ClsWinApi.GetIniString("REG", _
                                        "ALC_TRN_D_PG", strIniFileName, "P2")
            pstIni.strAlcTrnsD_AD = ClsWinApi.GetIniString("REG", _
                                        "ALC_TRN_D_AD", strIniFileName, "L701")
            pstIni.strVehicleD_PG = ClsWinApi.GetIniString("REG", _
                                        "VEHICLE_D_PG", strIniFileName, "P2")
            pstIni.strVehicleD_AD = ClsWinApi.GetIniString("REG", _
                                        "VEHICLE_D_AD", strIniFileName, "L702")
            pstIni.strVehiName_PG = ClsWinApi.GetIniString("REG", _
                                        "VEHI_NAME_PG", strIniFileName, "P2")
            pstIni.strVehiName_AD = ClsWinApi.GetIniString("REG", _
                                        "VEHI_NAME_AD", strIniFileName, "H002")
            pstIni.strAlcTrnsC_PG = ClsWinApi.GetIniString("REG", _
                                        "ALC_TRN_C_PG", strIniFileName, "P2")
            pstIni.strAlcTrnsC_AD = ClsWinApi.GetIniString("REG", _
                                        "ALC_TRN_C_AD", strIniFileName, "L711")
            pstIni.strVehicleC_PG = ClsWinApi.GetIniString("REG", _
                                        "VEHICLE_C_PG", strIniFileName, "P2")
            pstIni.strVehicleC_AD = ClsWinApi.GetIniString("REG", _
                                        "VEHICLE_C_AD", strIniFileName, "L712")
            pstIni.strBCNo_PG = ClsWinApi.GetIniString("REG", "BCNO_PG", _
                                                       strIniFileName, "P2")
            pstIni.strBCNo_AD = ClsWinApi.GetIniString("REG", "BCNO_AD", _
                                                       strIniFileName, "R702")
            pstIni.strAssyNo_PG = ClsWinApi.GetIniString("REG", "ASSYNO_PG", _
                                                         strIniFileName, "P2")
            pstIni.strAssyNo_AD = ClsWinApi.GetIniString("REG", "ASSYNO_AD", _
                                                         strIniFileName, "R700")

            pstIni.strLOTcode_PG = ClsWinApi.GetIniString("REG", "LOTCODE_PG", _
                                                         strIniFileName, "P2")          'ﾛｯﾄｺｰﾄﾞ追加（2012/5）
            pstIni.strLOTcode_AD = ClsWinApi.GetIniString("REG", "LOTCODE_AD", _
                                                         strIniFileName, "R704")
            pstIni.strFamilycode_PG = ClsWinApi.GetIniString("REG", "FAMILYCODE_PG", _
                                                         strIniFileName, "P2")          'ﾌｧﾐﾘｰｺｰﾄﾞ追加（2012/5）
            pstIni.strFamilycode_AD = ClsWinApi.GetIniString("REG", "FAMILYCODE_AD", _
                                                         strIniFileName, "R706")
        Catch ex As Exception
            pInsErrMsg.psubErrMsgShow(ex, "clsPublicIni.psubIniLoad")
        End Try

    End Sub

    Public Shared Sub psubIniSave()

        Dim strIniFileName As String

        Try
            strIniFileName = Application.StartupPath & "\" & pcstrIniFileName
            ClsWinApi.SetIniString("SYS", "BCNO", pstAlcRecv.strBCNo.ToString,
                                   strIniFileName)

            ClsWinApi.SetIniString("SYS", "LASTSEQ", pstAlcRecv.strLastSEQ.ToString,
                               strIniFileName)
        Catch ex As Exception
            pInsErrMsg.psubErrMsgShow(ex, "clsPublicIni.psubIniSave")
        End Try

    End Sub

End Class
