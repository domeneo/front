﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDataDisp
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナで必要です。
    'Windows フォーム デザイナを使用して変更できます。  
    'コード エディタを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnClose = New System.Windows.Forms.Button
        Me.lblNoName00 = New System.Windows.Forms.Label
        Me.lblNo04 = New System.Windows.Forms.Label
        Me.lblNo03 = New System.Windows.Forms.Label
        Me.lblNo02 = New System.Windows.Forms.Label
        Me.lblNo01 = New System.Windows.Forms.Label
        Me.lblNo00 = New System.Windows.Forms.Label
        Me.lblBcNoName00 = New System.Windows.Forms.Label
        Me.lblVehicleName00 = New System.Windows.Forms.Label
        Me.lblLeft = New System.Windows.Forms.Label
        Me.lblNoName01 = New System.Windows.Forms.Label
        Me.lblNo09 = New System.Windows.Forms.Label
        Me.lblNo08 = New System.Windows.Forms.Label
        Me.lblNo07 = New System.Windows.Forms.Label
        Me.lblNo06 = New System.Windows.Forms.Label
        Me.lblNo05 = New System.Windows.Forms.Label
        Me.lblNo14 = New System.Windows.Forms.Label
        Me.lblNo13 = New System.Windows.Forms.Label
        Me.lblNo12 = New System.Windows.Forms.Label
        Me.lblNo11 = New System.Windows.Forms.Label
        Me.lblNo10 = New System.Windows.Forms.Label
        Me.lblNo19 = New System.Windows.Forms.Label
        Me.lblNo18 = New System.Windows.Forms.Label
        Me.lblNo17 = New System.Windows.Forms.Label
        Me.lblNo16 = New System.Windows.Forms.Label
        Me.lblNo15 = New System.Windows.Forms.Label
        Me.lblNo24 = New System.Windows.Forms.Label
        Me.lblNo23 = New System.Windows.Forms.Label
        Me.lblNo22 = New System.Windows.Forms.Label
        Me.lblNo21 = New System.Windows.Forms.Label
        Me.lblNo20 = New System.Windows.Forms.Label
        Me.lblVehicleName01 = New System.Windows.Forms.Label
        Me.lblVehicle04 = New System.Windows.Forms.Label
        Me.lblVehicle03 = New System.Windows.Forms.Label
        Me.lblVehicle02 = New System.Windows.Forms.Label
        Me.lblVehicle01 = New System.Windows.Forms.Label
        Me.lblVehicle00 = New System.Windows.Forms.Label
        Me.lblBcNoName01 = New System.Windows.Forms.Label
        Me.lblBcNo04 = New System.Windows.Forms.Label
        Me.lblBcNo03 = New System.Windows.Forms.Label
        Me.lblBcNo02 = New System.Windows.Forms.Label
        Me.lblBcNo01 = New System.Windows.Forms.Label
        Me.lblBcNo00 = New System.Windows.Forms.Label
        Me.lblVehicle09 = New System.Windows.Forms.Label
        Me.lblVehicle08 = New System.Windows.Forms.Label
        Me.lblVehicle07 = New System.Windows.Forms.Label
        Me.lblVehicle06 = New System.Windows.Forms.Label
        Me.lblVehicle05 = New System.Windows.Forms.Label
        Me.lblBcNo09 = New System.Windows.Forms.Label
        Me.lblBcNo08 = New System.Windows.Forms.Label
        Me.lblBcNo07 = New System.Windows.Forms.Label
        Me.lblBcNo06 = New System.Windows.Forms.Label
        Me.lblBcNo05 = New System.Windows.Forms.Label
        Me.lblVehicle19 = New System.Windows.Forms.Label
        Me.lblVehicle18 = New System.Windows.Forms.Label
        Me.lblVehicle17 = New System.Windows.Forms.Label
        Me.lblVehicle16 = New System.Windows.Forms.Label
        Me.lblVehicle15 = New System.Windows.Forms.Label
        Me.lblBcNo19 = New System.Windows.Forms.Label
        Me.lblBcNo18 = New System.Windows.Forms.Label
        Me.lblBcNo17 = New System.Windows.Forms.Label
        Me.lblBcNo16 = New System.Windows.Forms.Label
        Me.lblBcNo15 = New System.Windows.Forms.Label
        Me.lblVehicle14 = New System.Windows.Forms.Label
        Me.lblVehicle13 = New System.Windows.Forms.Label
        Me.lblVehicle12 = New System.Windows.Forms.Label
        Me.lblVehicle11 = New System.Windows.Forms.Label
        Me.lblVehicle10 = New System.Windows.Forms.Label
        Me.lblBcNo14 = New System.Windows.Forms.Label
        Me.lblBcNo13 = New System.Windows.Forms.Label
        Me.lblBcNo12 = New System.Windows.Forms.Label
        Me.lblBcNo11 = New System.Windows.Forms.Label
        Me.lblBcNo10 = New System.Windows.Forms.Label
        Me.chkDelFlg00 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg01 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg03 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg02 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg07 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg06 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg05 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg04 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg14 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg13 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg12 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg11 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg10 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg09 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg08 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg19 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg18 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg17 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg16 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg15 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg39 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg38 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg37 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg36 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg35 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg34 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg33 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg32 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg31 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg30 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg29 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg28 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg27 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg26 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg25 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg24 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg23 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg22 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg21 = New System.Windows.Forms.CheckBox
        Me.chkDelFlg20 = New System.Windows.Forms.CheckBox
        Me.lblVehicle39 = New System.Windows.Forms.Label
        Me.lblVehicle38 = New System.Windows.Forms.Label
        Me.lblVehicle37 = New System.Windows.Forms.Label
        Me.lblVehicle36 = New System.Windows.Forms.Label
        Me.lblVehicle35 = New System.Windows.Forms.Label
        Me.lblBcNo39 = New System.Windows.Forms.Label
        Me.lblBcNo38 = New System.Windows.Forms.Label
        Me.lblBcNo37 = New System.Windows.Forms.Label
        Me.lblBcNo36 = New System.Windows.Forms.Label
        Me.lblBcNo35 = New System.Windows.Forms.Label
        Me.lblVehicle34 = New System.Windows.Forms.Label
        Me.lblVehicle33 = New System.Windows.Forms.Label
        Me.lblVehicle32 = New System.Windows.Forms.Label
        Me.lblVehicle31 = New System.Windows.Forms.Label
        Me.lblVehicle30 = New System.Windows.Forms.Label
        Me.lblBcNo34 = New System.Windows.Forms.Label
        Me.lblBcNo33 = New System.Windows.Forms.Label
        Me.lblBcNo32 = New System.Windows.Forms.Label
        Me.lblBcNo31 = New System.Windows.Forms.Label
        Me.lblBcNo30 = New System.Windows.Forms.Label
        Me.lblVehicle29 = New System.Windows.Forms.Label
        Me.lblVehicle28 = New System.Windows.Forms.Label
        Me.lblVehicle27 = New System.Windows.Forms.Label
        Me.lblVehicle26 = New System.Windows.Forms.Label
        Me.lblVehicle25 = New System.Windows.Forms.Label
        Me.lblBcNo29 = New System.Windows.Forms.Label
        Me.lblBcNo28 = New System.Windows.Forms.Label
        Me.lblBcNo27 = New System.Windows.Forms.Label
        Me.lblBcNo26 = New System.Windows.Forms.Label
        Me.lblBcNo25 = New System.Windows.Forms.Label
        Me.lblVehicleName02 = New System.Windows.Forms.Label
        Me.lblVehicle24 = New System.Windows.Forms.Label
        Me.lblVehicle23 = New System.Windows.Forms.Label
        Me.lblVehicle22 = New System.Windows.Forms.Label
        Me.lblVehicle21 = New System.Windows.Forms.Label
        Me.lblVehicle20 = New System.Windows.Forms.Label
        Me.lblBcNoName02 = New System.Windows.Forms.Label
        Me.lblBcNo24 = New System.Windows.Forms.Label
        Me.lblBcNo23 = New System.Windows.Forms.Label
        Me.lblBcNo22 = New System.Windows.Forms.Label
        Me.lblBcNo21 = New System.Windows.Forms.Label
        Me.lblBcNo20 = New System.Windows.Forms.Label
        Me.lblNo44 = New System.Windows.Forms.Label
        Me.lblNo43 = New System.Windows.Forms.Label
        Me.lblNo42 = New System.Windows.Forms.Label
        Me.lblNo41 = New System.Windows.Forms.Label
        Me.lblNo40 = New System.Windows.Forms.Label
        Me.lblNo39 = New System.Windows.Forms.Label
        Me.lblNo38 = New System.Windows.Forms.Label
        Me.lblNo37 = New System.Windows.Forms.Label
        Me.lblNo36 = New System.Windows.Forms.Label
        Me.lblNo35 = New System.Windows.Forms.Label
        Me.lblNo34 = New System.Windows.Forms.Label
        Me.lblNo33 = New System.Windows.Forms.Label
        Me.lblNo32 = New System.Windows.Forms.Label
        Me.lblNo31 = New System.Windows.Forms.Label
        Me.lblNo30 = New System.Windows.Forms.Label
        Me.lblNoName02 = New System.Windows.Forms.Label
        Me.lblNo29 = New System.Windows.Forms.Label
        Me.lblNo28 = New System.Windows.Forms.Label
        Me.lblNo27 = New System.Windows.Forms.Label
        Me.lblNo26 = New System.Windows.Forms.Label
        Me.lblNo25 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.btnDelete = New System.Windows.Forms.Button
        Me.btnAllDelete = New System.Windows.Forms.Button
        Me.btnRefresh = New System.Windows.Forms.Button
        Me.lblAssyNoName00 = New System.Windows.Forms.Label
        Me.lblAssyNo00 = New System.Windows.Forms.Label
        Me.lblAssyNoName02 = New System.Windows.Forms.Label
        Me.lblVehicle44 = New System.Windows.Forms.Label
        Me.lblVehicle43 = New System.Windows.Forms.Label
        Me.lblVehicle42 = New System.Windows.Forms.Label
        Me.lblVehicle41 = New System.Windows.Forms.Label
        Me.lblVehicle40 = New System.Windows.Forms.Label
        Me.lblBcNo44 = New System.Windows.Forms.Label
        Me.lblBcNo43 = New System.Windows.Forms.Label
        Me.lblBcNo42 = New System.Windows.Forms.Label
        Me.lblBcNo41 = New System.Windows.Forms.Label
        Me.lblBcNo40 = New System.Windows.Forms.Label
        Me.lblAssyNo01 = New System.Windows.Forms.Label
        Me.lblAssyNo02 = New System.Windows.Forms.Label
        Me.lblAssyNo03 = New System.Windows.Forms.Label
        Me.lblAssyNo04 = New System.Windows.Forms.Label
        Me.lblAssyNoName01 = New System.Windows.Forms.Label
        Me.lblAssyNo05 = New System.Windows.Forms.Label
        Me.lblAssyNo06 = New System.Windows.Forms.Label
        Me.lblAssyNo07 = New System.Windows.Forms.Label
        Me.lblAssyNo08 = New System.Windows.Forms.Label
        Me.lblAssyNo09 = New System.Windows.Forms.Label
        Me.lblAssyNo10 = New System.Windows.Forms.Label
        Me.lblAssyNo11 = New System.Windows.Forms.Label
        Me.lblAssyNo12 = New System.Windows.Forms.Label
        Me.lblAssyNo13 = New System.Windows.Forms.Label
        Me.lblAssyNo14 = New System.Windows.Forms.Label
        Me.lblAssyNo15 = New System.Windows.Forms.Label
        Me.lblAssyNo16 = New System.Windows.Forms.Label
        Me.lblAssyNo17 = New System.Windows.Forms.Label
        Me.lblAssyNo18 = New System.Windows.Forms.Label
        Me.lblAssyNo19 = New System.Windows.Forms.Label
        Me.lblAssyNo20 = New System.Windows.Forms.Label
        Me.lblAssyNo21 = New System.Windows.Forms.Label
        Me.lblAssyNo22 = New System.Windows.Forms.Label
        Me.lblAssyNo23 = New System.Windows.Forms.Label
        Me.lblAssyNo24 = New System.Windows.Forms.Label
        Me.lblAssyNo25 = New System.Windows.Forms.Label
        Me.lblAssyNo26 = New System.Windows.Forms.Label
        Me.lblAssyNo27 = New System.Windows.Forms.Label
        Me.lblAssyNo28 = New System.Windows.Forms.Label
        Me.lblAssyNo29 = New System.Windows.Forms.Label
        Me.lblAssyNo30 = New System.Windows.Forms.Label
        Me.lblAssyNo31 = New System.Windows.Forms.Label
        Me.lblAssyNo32 = New System.Windows.Forms.Label
        Me.lblAssyNo33 = New System.Windows.Forms.Label
        Me.lblAssyNo34 = New System.Windows.Forms.Label
        Me.lblAssyNo35 = New System.Windows.Forms.Label
        Me.lblAssyNo36 = New System.Windows.Forms.Label
        Me.lblAssyNo37 = New System.Windows.Forms.Label
        Me.lblAssyNo38 = New System.Windows.Forms.Label
        Me.lblAssyNo39 = New System.Windows.Forms.Label
        Me.lblAssyNo40 = New System.Windows.Forms.Label
        Me.lblAssyNo41 = New System.Windows.Forms.Label
        Me.lblAssyNo42 = New System.Windows.Forms.Label
        Me.lblAssyNo43 = New System.Windows.Forms.Label
        Me.lblAssyNo44 = New System.Windows.Forms.Label
        Me.lblLOTcode00 = New System.Windows.Forms.Label
        Me.lblLOTcodeName00 = New System.Windows.Forms.Label
        Me.lblLOTcode01 = New System.Windows.Forms.Label
        Me.lblLOTcode02 = New System.Windows.Forms.Label
        Me.lblLOTcode03 = New System.Windows.Forms.Label
        Me.lblLOTcode04 = New System.Windows.Forms.Label
        Me.lblFamilycode00 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.lblFamilycode01 = New System.Windows.Forms.Label
        Me.lblFamilycode02 = New System.Windows.Forms.Label
        Me.lblFamilycode03 = New System.Windows.Forms.Label
        Me.lblFamilycode04 = New System.Windows.Forms.Label
        Me.lblLOTcode05 = New System.Windows.Forms.Label
        Me.lblFamilycode05 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.lblLOTcode06 = New System.Windows.Forms.Label
        Me.lblFamilycode06 = New System.Windows.Forms.Label
        Me.lblLOTcode07 = New System.Windows.Forms.Label
        Me.lblFamilycode07 = New System.Windows.Forms.Label
        Me.lblLOTcode08 = New System.Windows.Forms.Label
        Me.lblFamilycode08 = New System.Windows.Forms.Label
        Me.lblLOTcode09 = New System.Windows.Forms.Label
        Me.lblFamilycode09 = New System.Windows.Forms.Label
        Me.lblLOTcode10 = New System.Windows.Forms.Label
        Me.lblFamilycode10 = New System.Windows.Forms.Label
        Me.lblLOTcode11 = New System.Windows.Forms.Label
        Me.lblFamilycode11 = New System.Windows.Forms.Label
        Me.lblLOTcode12 = New System.Windows.Forms.Label
        Me.lblFamilycode12 = New System.Windows.Forms.Label
        Me.lblLOTcode13 = New System.Windows.Forms.Label
        Me.lblFamilycode13 = New System.Windows.Forms.Label
        Me.lblLOTcode14 = New System.Windows.Forms.Label
        Me.lblFamilycode14 = New System.Windows.Forms.Label
        Me.lblLOTcode15 = New System.Windows.Forms.Label
        Me.lblFamilycode15 = New System.Windows.Forms.Label
        Me.lblLOTcode16 = New System.Windows.Forms.Label
        Me.lblFamilycode16 = New System.Windows.Forms.Label
        Me.lblLOTcode17 = New System.Windows.Forms.Label
        Me.lblFamilycode17 = New System.Windows.Forms.Label
        Me.lblLOTcode18 = New System.Windows.Forms.Label
        Me.lblFamilycode18 = New System.Windows.Forms.Label
        Me.lblLOTcode19 = New System.Windows.Forms.Label
        Me.lblFamilycode19 = New System.Windows.Forms.Label
        Me.lblLOTcode20 = New System.Windows.Forms.Label
        Me.lblFamilycode20 = New System.Windows.Forms.Label
        Me.lblLOTcode21 = New System.Windows.Forms.Label
        Me.lblFamilycode21 = New System.Windows.Forms.Label
        Me.lblLOTcode22 = New System.Windows.Forms.Label
        Me.lblFamilycode22 = New System.Windows.Forms.Label
        Me.lblLOTcode23 = New System.Windows.Forms.Label
        Me.lblFamilycode23 = New System.Windows.Forms.Label
        Me.lblLOTcode24 = New System.Windows.Forms.Label
        Me.lblFamilycode24 = New System.Windows.Forms.Label
        Me.lblLOTcode25 = New System.Windows.Forms.Label
        Me.lblLOTcode30 = New System.Windows.Forms.Label
        Me.lblLOTcode35 = New System.Windows.Forms.Label
        Me.lblFamilycode25 = New System.Windows.Forms.Label
        Me.lblLOTcode40 = New System.Windows.Forms.Label
        Me.lblFamilycode30 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.lblFamilycode35 = New System.Windows.Forms.Label
        Me.lblFamilycode40 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.lblLOTcode26 = New System.Windows.Forms.Label
        Me.lblFamilycode26 = New System.Windows.Forms.Label
        Me.lblLOTcode31 = New System.Windows.Forms.Label
        Me.lblLOTcode36 = New System.Windows.Forms.Label
        Me.lblFamilycode31 = New System.Windows.Forms.Label
        Me.lblLOTcode41 = New System.Windows.Forms.Label
        Me.lblFamilycode36 = New System.Windows.Forms.Label
        Me.lblLOTcode27 = New System.Windows.Forms.Label
        Me.lblFamilycode41 = New System.Windows.Forms.Label
        Me.lblLOTcode32 = New System.Windows.Forms.Label
        Me.lblLOTcode37 = New System.Windows.Forms.Label
        Me.lblFamilycode27 = New System.Windows.Forms.Label
        Me.lblLOTcode42 = New System.Windows.Forms.Label
        Me.lblFamilycode32 = New System.Windows.Forms.Label
        Me.lblFamilycode37 = New System.Windows.Forms.Label
        Me.lblFamilycode42 = New System.Windows.Forms.Label
        Me.lblLOTcode28 = New System.Windows.Forms.Label
        Me.lblLOTcode33 = New System.Windows.Forms.Label
        Me.lblLOTcode38 = New System.Windows.Forms.Label
        Me.lblLOTcode43 = New System.Windows.Forms.Label
        Me.lblFamilycode28 = New System.Windows.Forms.Label
        Me.lblFamilycode33 = New System.Windows.Forms.Label
        Me.lblFamilycode38 = New System.Windows.Forms.Label
        Me.lblFamilycode43 = New System.Windows.Forms.Label
        Me.lblLOTcode29 = New System.Windows.Forms.Label
        Me.lblLOTcode34 = New System.Windows.Forms.Label
        Me.lblLOTcode39 = New System.Windows.Forms.Label
        Me.lblLOTcode44 = New System.Windows.Forms.Label
        Me.lblFamilycode29 = New System.Windows.Forms.Label
        Me.lblFamilycode34 = New System.Windows.Forms.Label
        Me.lblFamilycode39 = New System.Windows.Forms.Label
        Me.lblFamilycode44 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(12, 389)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(160, 32)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "Close(&X)"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lblNoName00
        '
        Me.lblNoName00.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNoName00.Location = New System.Drawing.Point(12, 19)
        Me.lblNoName00.Name = "lblNoName00"
        Me.lblNoName00.Size = New System.Drawing.Size(32, 18)
        Me.lblNoName00.TabIndex = 227
        Me.lblNoName00.Text = "No"
        Me.lblNoName00.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblNo04
        '
        Me.lblNo04.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo04.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo04.Location = New System.Drawing.Point(12, 109)
        Me.lblNo04.Name = "lblNo04"
        Me.lblNo04.Size = New System.Drawing.Size(32, 18)
        Me.lblNo04.TabIndex = 223
        Me.lblNo04.Text = "05"
        Me.lblNo04.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblNo04.Visible = False
        '
        'lblNo03
        '
        Me.lblNo03.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo03.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo03.Location = New System.Drawing.Point(12, 91)
        Me.lblNo03.Name = "lblNo03"
        Me.lblNo03.Size = New System.Drawing.Size(32, 18)
        Me.lblNo03.TabIndex = 222
        Me.lblNo03.Text = "04"
        Me.lblNo03.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblNo03.Visible = False
        '
        'lblNo02
        '
        Me.lblNo02.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo02.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo02.Location = New System.Drawing.Point(12, 73)
        Me.lblNo02.Name = "lblNo02"
        Me.lblNo02.Size = New System.Drawing.Size(32, 18)
        Me.lblNo02.TabIndex = 224
        Me.lblNo02.Text = "03"
        Me.lblNo02.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblNo02.Visible = False
        '
        'lblNo01
        '
        Me.lblNo01.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo01.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo01.Location = New System.Drawing.Point(12, 55)
        Me.lblNo01.Name = "lblNo01"
        Me.lblNo01.Size = New System.Drawing.Size(32, 18)
        Me.lblNo01.TabIndex = 225
        Me.lblNo01.Text = "02"
        Me.lblNo01.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblNo01.Visible = False
        '
        'lblNo00
        '
        Me.lblNo00.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo00.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo00.Location = New System.Drawing.Point(12, 37)
        Me.lblNo00.Name = "lblNo00"
        Me.lblNo00.Size = New System.Drawing.Size(32, 18)
        Me.lblNo00.TabIndex = 226
        Me.lblNo00.Text = "01"
        Me.lblNo00.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNoName00
        '
        Me.lblBcNoName00.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNoName00.Location = New System.Drawing.Point(44, 19)
        Me.lblBcNoName00.Name = "lblBcNoName00"
        Me.lblBcNoName00.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNoName00.TabIndex = 233
        Me.lblBcNoName00.Text = "BC No"
        Me.lblBcNoName00.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblVehicleName00
        '
        Me.lblVehicleName00.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicleName00.Location = New System.Drawing.Point(92, 19)
        Me.lblVehicleName00.Name = "lblVehicleName00"
        Me.lblVehicleName00.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicleName00.TabIndex = 239
        Me.lblVehicleName00.Text = "Vehicle Name"
        Me.lblVehicleName00.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblLeft
        '
        Me.lblLeft.AutoSize = True
        Me.lblLeft.Font = New System.Drawing.Font("MS UI Gothic", 26.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLeft.Location = New System.Drawing.Point(304, 29)
        Me.lblLeft.Name = "lblLeft"
        Me.lblLeft.Size = New System.Drawing.Size(51, 35)
        Me.lblLeft.TabIndex = 240
        Me.lblLeft.Text = "<="
        '
        'lblNoName01
        '
        Me.lblNoName01.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNoName01.Location = New System.Drawing.Point(368, 18)
        Me.lblNoName01.Name = "lblNoName01"
        Me.lblNoName01.Size = New System.Drawing.Size(32, 18)
        Me.lblNoName01.TabIndex = 246
        Me.lblNoName01.Text = "No"
        Me.lblNoName01.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblNo09
        '
        Me.lblNo09.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo09.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo09.Location = New System.Drawing.Point(368, 108)
        Me.lblNo09.Name = "lblNo09"
        Me.lblNo09.Size = New System.Drawing.Size(32, 18)
        Me.lblNo09.TabIndex = 242
        Me.lblNo09.Text = "05"
        Me.lblNo09.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo08
        '
        Me.lblNo08.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo08.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo08.Location = New System.Drawing.Point(368, 90)
        Me.lblNo08.Name = "lblNo08"
        Me.lblNo08.Size = New System.Drawing.Size(32, 18)
        Me.lblNo08.TabIndex = 241
        Me.lblNo08.Text = "04"
        Me.lblNo08.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo07
        '
        Me.lblNo07.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo07.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo07.Location = New System.Drawing.Point(368, 72)
        Me.lblNo07.Name = "lblNo07"
        Me.lblNo07.Size = New System.Drawing.Size(32, 18)
        Me.lblNo07.TabIndex = 243
        Me.lblNo07.Text = "03"
        Me.lblNo07.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo06
        '
        Me.lblNo06.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo06.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo06.Location = New System.Drawing.Point(368, 54)
        Me.lblNo06.Name = "lblNo06"
        Me.lblNo06.Size = New System.Drawing.Size(32, 18)
        Me.lblNo06.TabIndex = 244
        Me.lblNo06.Text = "02"
        Me.lblNo06.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo05
        '
        Me.lblNo05.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo05.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo05.Location = New System.Drawing.Point(368, 36)
        Me.lblNo05.Name = "lblNo05"
        Me.lblNo05.Size = New System.Drawing.Size(32, 18)
        Me.lblNo05.TabIndex = 245
        Me.lblNo05.Text = "01"
        Me.lblNo05.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo14
        '
        Me.lblNo14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo14.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo14.Location = New System.Drawing.Point(368, 206)
        Me.lblNo14.Name = "lblNo14"
        Me.lblNo14.Size = New System.Drawing.Size(32, 18)
        Me.lblNo14.TabIndex = 248
        Me.lblNo14.Text = "10"
        Me.lblNo14.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo13
        '
        Me.lblNo13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo13.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo13.Location = New System.Drawing.Point(368, 188)
        Me.lblNo13.Name = "lblNo13"
        Me.lblNo13.Size = New System.Drawing.Size(32, 18)
        Me.lblNo13.TabIndex = 247
        Me.lblNo13.Text = "09"
        Me.lblNo13.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo12
        '
        Me.lblNo12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo12.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo12.Location = New System.Drawing.Point(368, 170)
        Me.lblNo12.Name = "lblNo12"
        Me.lblNo12.Size = New System.Drawing.Size(32, 18)
        Me.lblNo12.TabIndex = 249
        Me.lblNo12.Text = "08"
        Me.lblNo12.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo11
        '
        Me.lblNo11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo11.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo11.Location = New System.Drawing.Point(368, 152)
        Me.lblNo11.Name = "lblNo11"
        Me.lblNo11.Size = New System.Drawing.Size(32, 18)
        Me.lblNo11.TabIndex = 250
        Me.lblNo11.Text = "07"
        Me.lblNo11.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo10
        '
        Me.lblNo10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo10.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo10.Location = New System.Drawing.Point(368, 134)
        Me.lblNo10.Name = "lblNo10"
        Me.lblNo10.Size = New System.Drawing.Size(32, 18)
        Me.lblNo10.TabIndex = 251
        Me.lblNo10.Text = "06"
        Me.lblNo10.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo19
        '
        Me.lblNo19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo19.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo19.Location = New System.Drawing.Point(368, 304)
        Me.lblNo19.Name = "lblNo19"
        Me.lblNo19.Size = New System.Drawing.Size(32, 18)
        Me.lblNo19.TabIndex = 253
        Me.lblNo19.Text = "15"
        Me.lblNo19.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo18
        '
        Me.lblNo18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo18.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo18.Location = New System.Drawing.Point(368, 286)
        Me.lblNo18.Name = "lblNo18"
        Me.lblNo18.Size = New System.Drawing.Size(32, 18)
        Me.lblNo18.TabIndex = 252
        Me.lblNo18.Text = "14"
        Me.lblNo18.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo17
        '
        Me.lblNo17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo17.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo17.Location = New System.Drawing.Point(368, 268)
        Me.lblNo17.Name = "lblNo17"
        Me.lblNo17.Size = New System.Drawing.Size(32, 18)
        Me.lblNo17.TabIndex = 254
        Me.lblNo17.Text = "13"
        Me.lblNo17.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo16
        '
        Me.lblNo16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo16.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo16.Location = New System.Drawing.Point(368, 250)
        Me.lblNo16.Name = "lblNo16"
        Me.lblNo16.Size = New System.Drawing.Size(32, 18)
        Me.lblNo16.TabIndex = 255
        Me.lblNo16.Text = "12"
        Me.lblNo16.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo15
        '
        Me.lblNo15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo15.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo15.Location = New System.Drawing.Point(368, 232)
        Me.lblNo15.Name = "lblNo15"
        Me.lblNo15.Size = New System.Drawing.Size(32, 18)
        Me.lblNo15.TabIndex = 256
        Me.lblNo15.Text = "11"
        Me.lblNo15.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo24
        '
        Me.lblNo24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo24.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo24.Location = New System.Drawing.Point(368, 402)
        Me.lblNo24.Name = "lblNo24"
        Me.lblNo24.Size = New System.Drawing.Size(32, 18)
        Me.lblNo24.TabIndex = 258
        Me.lblNo24.Text = "20"
        Me.lblNo24.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo23
        '
        Me.lblNo23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo23.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo23.Location = New System.Drawing.Point(368, 384)
        Me.lblNo23.Name = "lblNo23"
        Me.lblNo23.Size = New System.Drawing.Size(32, 18)
        Me.lblNo23.TabIndex = 257
        Me.lblNo23.Text = "19"
        Me.lblNo23.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo22
        '
        Me.lblNo22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo22.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo22.Location = New System.Drawing.Point(368, 366)
        Me.lblNo22.Name = "lblNo22"
        Me.lblNo22.Size = New System.Drawing.Size(32, 18)
        Me.lblNo22.TabIndex = 259
        Me.lblNo22.Text = "18"
        Me.lblNo22.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo21
        '
        Me.lblNo21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo21.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo21.Location = New System.Drawing.Point(368, 348)
        Me.lblNo21.Name = "lblNo21"
        Me.lblNo21.Size = New System.Drawing.Size(32, 18)
        Me.lblNo21.TabIndex = 260
        Me.lblNo21.Text = "17"
        Me.lblNo21.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo20
        '
        Me.lblNo20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo20.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo20.Location = New System.Drawing.Point(368, 330)
        Me.lblNo20.Name = "lblNo20"
        Me.lblNo20.Size = New System.Drawing.Size(32, 18)
        Me.lblNo20.TabIndex = 261
        Me.lblNo20.Text = "16"
        Me.lblNo20.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicleName01
        '
        Me.lblVehicleName01.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicleName01.Location = New System.Drawing.Point(448, 18)
        Me.lblVehicleName01.Name = "lblVehicleName01"
        Me.lblVehicleName01.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicleName01.TabIndex = 273
        Me.lblVehicleName01.Text = "Vehicle Name"
        Me.lblVehicleName01.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblVehicle04
        '
        Me.lblVehicle04.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle04.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle04.Location = New System.Drawing.Point(92, 109)
        Me.lblVehicle04.Name = "lblVehicle04"
        Me.lblVehicle04.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle04.TabIndex = 272
        Me.lblVehicle04.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblVehicle04.Visible = False
        '
        'lblVehicle03
        '
        Me.lblVehicle03.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle03.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle03.Location = New System.Drawing.Point(92, 91)
        Me.lblVehicle03.Name = "lblVehicle03"
        Me.lblVehicle03.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle03.TabIndex = 268
        Me.lblVehicle03.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblVehicle03.Visible = False
        '
        'lblVehicle02
        '
        Me.lblVehicle02.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle02.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle02.Location = New System.Drawing.Point(92, 73)
        Me.lblVehicle02.Name = "lblVehicle02"
        Me.lblVehicle02.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle02.TabIndex = 269
        Me.lblVehicle02.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblVehicle02.Visible = False
        '
        'lblVehicle01
        '
        Me.lblVehicle01.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle01.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle01.Location = New System.Drawing.Point(92, 55)
        Me.lblVehicle01.Name = "lblVehicle01"
        Me.lblVehicle01.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle01.TabIndex = 271
        Me.lblVehicle01.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblVehicle01.Visible = False
        '
        'lblVehicle00
        '
        Me.lblVehicle00.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle00.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle00.Location = New System.Drawing.Point(92, 37)
        Me.lblVehicle00.Name = "lblVehicle00"
        Me.lblVehicle00.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle00.TabIndex = 270
        Me.lblVehicle00.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNoName01
        '
        Me.lblBcNoName01.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNoName01.Location = New System.Drawing.Point(400, 18)
        Me.lblBcNoName01.Name = "lblBcNoName01"
        Me.lblBcNoName01.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNoName01.TabIndex = 267
        Me.lblBcNoName01.Text = "BC No"
        Me.lblBcNoName01.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblBcNo04
        '
        Me.lblBcNo04.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo04.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo04.Location = New System.Drawing.Point(44, 109)
        Me.lblBcNo04.Name = "lblBcNo04"
        Me.lblBcNo04.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo04.TabIndex = 265
        Me.lblBcNo04.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblBcNo04.Visible = False
        '
        'lblBcNo03
        '
        Me.lblBcNo03.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo03.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo03.Location = New System.Drawing.Point(44, 91)
        Me.lblBcNo03.Name = "lblBcNo03"
        Me.lblBcNo03.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo03.TabIndex = 264
        Me.lblBcNo03.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblBcNo03.Visible = False
        '
        'lblBcNo02
        '
        Me.lblBcNo02.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo02.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo02.Location = New System.Drawing.Point(44, 73)
        Me.lblBcNo02.Name = "lblBcNo02"
        Me.lblBcNo02.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo02.TabIndex = 266
        Me.lblBcNo02.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblBcNo02.Visible = False
        '
        'lblBcNo01
        '
        Me.lblBcNo01.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo01.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo01.Location = New System.Drawing.Point(44, 55)
        Me.lblBcNo01.Name = "lblBcNo01"
        Me.lblBcNo01.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo01.TabIndex = 262
        Me.lblBcNo01.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblBcNo01.Visible = False
        '
        'lblBcNo00
        '
        Me.lblBcNo00.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo00.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo00.Location = New System.Drawing.Point(44, 37)
        Me.lblBcNo00.Name = "lblBcNo00"
        Me.lblBcNo00.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo00.TabIndex = 263
        Me.lblBcNo00.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle09
        '
        Me.lblVehicle09.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle09.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle09.Location = New System.Drawing.Point(448, 108)
        Me.lblVehicle09.Name = "lblVehicle09"
        Me.lblVehicle09.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle09.TabIndex = 283
        Me.lblVehicle09.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle08
        '
        Me.lblVehicle08.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle08.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle08.Location = New System.Drawing.Point(448, 90)
        Me.lblVehicle08.Name = "lblVehicle08"
        Me.lblVehicle08.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle08.TabIndex = 279
        Me.lblVehicle08.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle07
        '
        Me.lblVehicle07.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle07.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle07.Location = New System.Drawing.Point(448, 72)
        Me.lblVehicle07.Name = "lblVehicle07"
        Me.lblVehicle07.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle07.TabIndex = 280
        Me.lblVehicle07.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle06
        '
        Me.lblVehicle06.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle06.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle06.Location = New System.Drawing.Point(448, 54)
        Me.lblVehicle06.Name = "lblVehicle06"
        Me.lblVehicle06.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle06.TabIndex = 282
        Me.lblVehicle06.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle05
        '
        Me.lblVehicle05.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle05.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle05.Location = New System.Drawing.Point(448, 36)
        Me.lblVehicle05.Name = "lblVehicle05"
        Me.lblVehicle05.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle05.TabIndex = 281
        Me.lblVehicle05.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo09
        '
        Me.lblBcNo09.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo09.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo09.Location = New System.Drawing.Point(400, 108)
        Me.lblBcNo09.Name = "lblBcNo09"
        Me.lblBcNo09.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo09.TabIndex = 277
        Me.lblBcNo09.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo08
        '
        Me.lblBcNo08.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo08.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo08.Location = New System.Drawing.Point(400, 90)
        Me.lblBcNo08.Name = "lblBcNo08"
        Me.lblBcNo08.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo08.TabIndex = 276
        Me.lblBcNo08.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo07
        '
        Me.lblBcNo07.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo07.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo07.Location = New System.Drawing.Point(400, 72)
        Me.lblBcNo07.Name = "lblBcNo07"
        Me.lblBcNo07.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo07.TabIndex = 278
        Me.lblBcNo07.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo06
        '
        Me.lblBcNo06.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo06.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo06.Location = New System.Drawing.Point(400, 54)
        Me.lblBcNo06.Name = "lblBcNo06"
        Me.lblBcNo06.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo06.TabIndex = 274
        Me.lblBcNo06.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo05
        '
        Me.lblBcNo05.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo05.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo05.Location = New System.Drawing.Point(400, 36)
        Me.lblBcNo05.Name = "lblBcNo05"
        Me.lblBcNo05.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo05.TabIndex = 275
        Me.lblBcNo05.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle19
        '
        Me.lblVehicle19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle19.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle19.Location = New System.Drawing.Point(448, 304)
        Me.lblVehicle19.Name = "lblVehicle19"
        Me.lblVehicle19.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle19.TabIndex = 303
        Me.lblVehicle19.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle18
        '
        Me.lblVehicle18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle18.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle18.Location = New System.Drawing.Point(448, 286)
        Me.lblVehicle18.Name = "lblVehicle18"
        Me.lblVehicle18.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle18.TabIndex = 299
        Me.lblVehicle18.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle17
        '
        Me.lblVehicle17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle17.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle17.Location = New System.Drawing.Point(448, 268)
        Me.lblVehicle17.Name = "lblVehicle17"
        Me.lblVehicle17.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle17.TabIndex = 300
        Me.lblVehicle17.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle16
        '
        Me.lblVehicle16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle16.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle16.Location = New System.Drawing.Point(448, 250)
        Me.lblVehicle16.Name = "lblVehicle16"
        Me.lblVehicle16.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle16.TabIndex = 302
        Me.lblVehicle16.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle15
        '
        Me.lblVehicle15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle15.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle15.Location = New System.Drawing.Point(448, 232)
        Me.lblVehicle15.Name = "lblVehicle15"
        Me.lblVehicle15.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle15.TabIndex = 301
        Me.lblVehicle15.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo19
        '
        Me.lblBcNo19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo19.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo19.Location = New System.Drawing.Point(400, 304)
        Me.lblBcNo19.Name = "lblBcNo19"
        Me.lblBcNo19.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo19.TabIndex = 297
        Me.lblBcNo19.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo18
        '
        Me.lblBcNo18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo18.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo18.Location = New System.Drawing.Point(400, 286)
        Me.lblBcNo18.Name = "lblBcNo18"
        Me.lblBcNo18.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo18.TabIndex = 296
        Me.lblBcNo18.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo17
        '
        Me.lblBcNo17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo17.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo17.Location = New System.Drawing.Point(400, 268)
        Me.lblBcNo17.Name = "lblBcNo17"
        Me.lblBcNo17.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo17.TabIndex = 298
        Me.lblBcNo17.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo16
        '
        Me.lblBcNo16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo16.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo16.Location = New System.Drawing.Point(400, 250)
        Me.lblBcNo16.Name = "lblBcNo16"
        Me.lblBcNo16.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo16.TabIndex = 294
        Me.lblBcNo16.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo15
        '
        Me.lblBcNo15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo15.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo15.Location = New System.Drawing.Point(400, 232)
        Me.lblBcNo15.Name = "lblBcNo15"
        Me.lblBcNo15.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo15.TabIndex = 295
        Me.lblBcNo15.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle14
        '
        Me.lblVehicle14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle14.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle14.Location = New System.Drawing.Point(448, 206)
        Me.lblVehicle14.Name = "lblVehicle14"
        Me.lblVehicle14.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle14.TabIndex = 293
        Me.lblVehicle14.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle13
        '
        Me.lblVehicle13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle13.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle13.Location = New System.Drawing.Point(448, 188)
        Me.lblVehicle13.Name = "lblVehicle13"
        Me.lblVehicle13.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle13.TabIndex = 289
        Me.lblVehicle13.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle12
        '
        Me.lblVehicle12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle12.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle12.Location = New System.Drawing.Point(448, 170)
        Me.lblVehicle12.Name = "lblVehicle12"
        Me.lblVehicle12.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle12.TabIndex = 290
        Me.lblVehicle12.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle11
        '
        Me.lblVehicle11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle11.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle11.Location = New System.Drawing.Point(448, 152)
        Me.lblVehicle11.Name = "lblVehicle11"
        Me.lblVehicle11.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle11.TabIndex = 292
        Me.lblVehicle11.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle10
        '
        Me.lblVehicle10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle10.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle10.Location = New System.Drawing.Point(448, 134)
        Me.lblVehicle10.Name = "lblVehicle10"
        Me.lblVehicle10.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle10.TabIndex = 291
        Me.lblVehicle10.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo14
        '
        Me.lblBcNo14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo14.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo14.Location = New System.Drawing.Point(400, 206)
        Me.lblBcNo14.Name = "lblBcNo14"
        Me.lblBcNo14.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo14.TabIndex = 287
        Me.lblBcNo14.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo13
        '
        Me.lblBcNo13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo13.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo13.Location = New System.Drawing.Point(400, 188)
        Me.lblBcNo13.Name = "lblBcNo13"
        Me.lblBcNo13.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo13.TabIndex = 286
        Me.lblBcNo13.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo12
        '
        Me.lblBcNo12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo12.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo12.Location = New System.Drawing.Point(400, 170)
        Me.lblBcNo12.Name = "lblBcNo12"
        Me.lblBcNo12.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo12.TabIndex = 288
        Me.lblBcNo12.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo11
        '
        Me.lblBcNo11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo11.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo11.Location = New System.Drawing.Point(400, 152)
        Me.lblBcNo11.Name = "lblBcNo11"
        Me.lblBcNo11.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo11.TabIndex = 284
        Me.lblBcNo11.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo10
        '
        Me.lblBcNo10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo10.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo10.Location = New System.Drawing.Point(400, 134)
        Me.lblBcNo10.Name = "lblBcNo10"
        Me.lblBcNo10.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo10.TabIndex = 285
        Me.lblBcNo10.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'chkDelFlg00
        '
        Me.chkDelFlg00.AutoSize = True
        Me.chkDelFlg00.Location = New System.Drawing.Point(350, 39)
        Me.chkDelFlg00.Name = "chkDelFlg00"
        Me.chkDelFlg00.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg00.TabIndex = 1
        Me.chkDelFlg00.UseVisualStyleBackColor = True
        '
        'chkDelFlg01
        '
        Me.chkDelFlg01.AutoSize = True
        Me.chkDelFlg01.Location = New System.Drawing.Point(350, 57)
        Me.chkDelFlg01.Name = "chkDelFlg01"
        Me.chkDelFlg01.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg01.TabIndex = 2
        Me.chkDelFlg01.UseVisualStyleBackColor = True
        '
        'chkDelFlg03
        '
        Me.chkDelFlg03.AutoSize = True
        Me.chkDelFlg03.Location = New System.Drawing.Point(350, 93)
        Me.chkDelFlg03.Name = "chkDelFlg03"
        Me.chkDelFlg03.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg03.TabIndex = 4
        Me.chkDelFlg03.UseVisualStyleBackColor = True
        '
        'chkDelFlg02
        '
        Me.chkDelFlg02.AutoSize = True
        Me.chkDelFlg02.Location = New System.Drawing.Point(350, 75)
        Me.chkDelFlg02.Name = "chkDelFlg02"
        Me.chkDelFlg02.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg02.TabIndex = 3
        Me.chkDelFlg02.UseVisualStyleBackColor = True
        '
        'chkDelFlg07
        '
        Me.chkDelFlg07.AutoSize = True
        Me.chkDelFlg07.Location = New System.Drawing.Point(350, 173)
        Me.chkDelFlg07.Name = "chkDelFlg07"
        Me.chkDelFlg07.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg07.TabIndex = 8
        Me.chkDelFlg07.UseVisualStyleBackColor = True
        '
        'chkDelFlg06
        '
        Me.chkDelFlg06.AutoSize = True
        Me.chkDelFlg06.Location = New System.Drawing.Point(350, 155)
        Me.chkDelFlg06.Name = "chkDelFlg06"
        Me.chkDelFlg06.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg06.TabIndex = 7
        Me.chkDelFlg06.UseVisualStyleBackColor = True
        '
        'chkDelFlg05
        '
        Me.chkDelFlg05.AutoSize = True
        Me.chkDelFlg05.Location = New System.Drawing.Point(350, 137)
        Me.chkDelFlg05.Name = "chkDelFlg05"
        Me.chkDelFlg05.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg05.TabIndex = 6
        Me.chkDelFlg05.UseVisualStyleBackColor = True
        '
        'chkDelFlg04
        '
        Me.chkDelFlg04.AutoSize = True
        Me.chkDelFlg04.Location = New System.Drawing.Point(350, 111)
        Me.chkDelFlg04.Name = "chkDelFlg04"
        Me.chkDelFlg04.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg04.TabIndex = 5
        Me.chkDelFlg04.UseVisualStyleBackColor = True
        '
        'chkDelFlg14
        '
        Me.chkDelFlg14.AutoSize = True
        Me.chkDelFlg14.Location = New System.Drawing.Point(350, 306)
        Me.chkDelFlg14.Name = "chkDelFlg14"
        Me.chkDelFlg14.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg14.TabIndex = 15
        Me.chkDelFlg14.UseVisualStyleBackColor = True
        '
        'chkDelFlg13
        '
        Me.chkDelFlg13.AutoSize = True
        Me.chkDelFlg13.Location = New System.Drawing.Point(350, 289)
        Me.chkDelFlg13.Name = "chkDelFlg13"
        Me.chkDelFlg13.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg13.TabIndex = 14
        Me.chkDelFlg13.UseVisualStyleBackColor = True
        '
        'chkDelFlg12
        '
        Me.chkDelFlg12.AutoSize = True
        Me.chkDelFlg12.Location = New System.Drawing.Point(350, 271)
        Me.chkDelFlg12.Name = "chkDelFlg12"
        Me.chkDelFlg12.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg12.TabIndex = 13
        Me.chkDelFlg12.UseVisualStyleBackColor = True
        '
        'chkDelFlg11
        '
        Me.chkDelFlg11.AutoSize = True
        Me.chkDelFlg11.Location = New System.Drawing.Point(350, 253)
        Me.chkDelFlg11.Name = "chkDelFlg11"
        Me.chkDelFlg11.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg11.TabIndex = 12
        Me.chkDelFlg11.UseVisualStyleBackColor = True
        '
        'chkDelFlg10
        '
        Me.chkDelFlg10.AutoSize = True
        Me.chkDelFlg10.Location = New System.Drawing.Point(350, 235)
        Me.chkDelFlg10.Name = "chkDelFlg10"
        Me.chkDelFlg10.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg10.TabIndex = 11
        Me.chkDelFlg10.UseVisualStyleBackColor = True
        '
        'chkDelFlg09
        '
        Me.chkDelFlg09.AutoSize = True
        Me.chkDelFlg09.Location = New System.Drawing.Point(350, 209)
        Me.chkDelFlg09.Name = "chkDelFlg09"
        Me.chkDelFlg09.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg09.TabIndex = 10
        Me.chkDelFlg09.UseVisualStyleBackColor = True
        '
        'chkDelFlg08
        '
        Me.chkDelFlg08.AutoSize = True
        Me.chkDelFlg08.Location = New System.Drawing.Point(350, 191)
        Me.chkDelFlg08.Name = "chkDelFlg08"
        Me.chkDelFlg08.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg08.TabIndex = 9
        Me.chkDelFlg08.UseVisualStyleBackColor = True
        '
        'chkDelFlg19
        '
        Me.chkDelFlg19.AutoSize = True
        Me.chkDelFlg19.Location = New System.Drawing.Point(350, 404)
        Me.chkDelFlg19.Name = "chkDelFlg19"
        Me.chkDelFlg19.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg19.TabIndex = 20
        Me.chkDelFlg19.UseVisualStyleBackColor = True
        '
        'chkDelFlg18
        '
        Me.chkDelFlg18.AutoSize = True
        Me.chkDelFlg18.Location = New System.Drawing.Point(350, 387)
        Me.chkDelFlg18.Name = "chkDelFlg18"
        Me.chkDelFlg18.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg18.TabIndex = 19
        Me.chkDelFlg18.UseVisualStyleBackColor = True
        '
        'chkDelFlg17
        '
        Me.chkDelFlg17.AutoSize = True
        Me.chkDelFlg17.Location = New System.Drawing.Point(350, 369)
        Me.chkDelFlg17.Name = "chkDelFlg17"
        Me.chkDelFlg17.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg17.TabIndex = 18
        Me.chkDelFlg17.UseVisualStyleBackColor = True
        '
        'chkDelFlg16
        '
        Me.chkDelFlg16.AutoSize = True
        Me.chkDelFlg16.Location = New System.Drawing.Point(350, 351)
        Me.chkDelFlg16.Name = "chkDelFlg16"
        Me.chkDelFlg16.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg16.TabIndex = 17
        Me.chkDelFlg16.UseVisualStyleBackColor = True
        '
        'chkDelFlg15
        '
        Me.chkDelFlg15.AutoSize = True
        Me.chkDelFlg15.Location = New System.Drawing.Point(350, 333)
        Me.chkDelFlg15.Name = "chkDelFlg15"
        Me.chkDelFlg15.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg15.TabIndex = 16
        Me.chkDelFlg15.UseVisualStyleBackColor = True
        '
        'chkDelFlg39
        '
        Me.chkDelFlg39.AutoSize = True
        Me.chkDelFlg39.Location = New System.Drawing.Point(684, 404)
        Me.chkDelFlg39.Name = "chkDelFlg39"
        Me.chkDelFlg39.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg39.TabIndex = 40
        Me.chkDelFlg39.UseVisualStyleBackColor = True
        '
        'chkDelFlg38
        '
        Me.chkDelFlg38.AutoSize = True
        Me.chkDelFlg38.Location = New System.Drawing.Point(684, 387)
        Me.chkDelFlg38.Name = "chkDelFlg38"
        Me.chkDelFlg38.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg38.TabIndex = 39
        Me.chkDelFlg38.UseVisualStyleBackColor = True
        '
        'chkDelFlg37
        '
        Me.chkDelFlg37.AutoSize = True
        Me.chkDelFlg37.Location = New System.Drawing.Point(684, 369)
        Me.chkDelFlg37.Name = "chkDelFlg37"
        Me.chkDelFlg37.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg37.TabIndex = 38
        Me.chkDelFlg37.UseVisualStyleBackColor = True
        '
        'chkDelFlg36
        '
        Me.chkDelFlg36.AutoSize = True
        Me.chkDelFlg36.Location = New System.Drawing.Point(684, 351)
        Me.chkDelFlg36.Name = "chkDelFlg36"
        Me.chkDelFlg36.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg36.TabIndex = 37
        Me.chkDelFlg36.UseVisualStyleBackColor = True
        '
        'chkDelFlg35
        '
        Me.chkDelFlg35.AutoSize = True
        Me.chkDelFlg35.Location = New System.Drawing.Point(684, 333)
        Me.chkDelFlg35.Name = "chkDelFlg35"
        Me.chkDelFlg35.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg35.TabIndex = 36
        Me.chkDelFlg35.UseVisualStyleBackColor = True
        '
        'chkDelFlg34
        '
        Me.chkDelFlg34.AutoSize = True
        Me.chkDelFlg34.Location = New System.Drawing.Point(684, 306)
        Me.chkDelFlg34.Name = "chkDelFlg34"
        Me.chkDelFlg34.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg34.TabIndex = 35
        Me.chkDelFlg34.UseVisualStyleBackColor = True
        '
        'chkDelFlg33
        '
        Me.chkDelFlg33.AutoSize = True
        Me.chkDelFlg33.Location = New System.Drawing.Point(684, 289)
        Me.chkDelFlg33.Name = "chkDelFlg33"
        Me.chkDelFlg33.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg33.TabIndex = 34
        Me.chkDelFlg33.UseVisualStyleBackColor = True
        '
        'chkDelFlg32
        '
        Me.chkDelFlg32.AutoSize = True
        Me.chkDelFlg32.Location = New System.Drawing.Point(684, 271)
        Me.chkDelFlg32.Name = "chkDelFlg32"
        Me.chkDelFlg32.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg32.TabIndex = 33
        Me.chkDelFlg32.UseVisualStyleBackColor = True
        '
        'chkDelFlg31
        '
        Me.chkDelFlg31.AutoSize = True
        Me.chkDelFlg31.Location = New System.Drawing.Point(684, 253)
        Me.chkDelFlg31.Name = "chkDelFlg31"
        Me.chkDelFlg31.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg31.TabIndex = 32
        Me.chkDelFlg31.UseVisualStyleBackColor = True
        '
        'chkDelFlg30
        '
        Me.chkDelFlg30.AutoSize = True
        Me.chkDelFlg30.Location = New System.Drawing.Point(684, 235)
        Me.chkDelFlg30.Name = "chkDelFlg30"
        Me.chkDelFlg30.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg30.TabIndex = 31
        Me.chkDelFlg30.UseVisualStyleBackColor = True
        '
        'chkDelFlg29
        '
        Me.chkDelFlg29.AutoSize = True
        Me.chkDelFlg29.Location = New System.Drawing.Point(684, 209)
        Me.chkDelFlg29.Name = "chkDelFlg29"
        Me.chkDelFlg29.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg29.TabIndex = 30
        Me.chkDelFlg29.UseVisualStyleBackColor = True
        '
        'chkDelFlg28
        '
        Me.chkDelFlg28.AutoSize = True
        Me.chkDelFlg28.Location = New System.Drawing.Point(684, 191)
        Me.chkDelFlg28.Name = "chkDelFlg28"
        Me.chkDelFlg28.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg28.TabIndex = 29
        Me.chkDelFlg28.UseVisualStyleBackColor = True
        '
        'chkDelFlg27
        '
        Me.chkDelFlg27.AutoSize = True
        Me.chkDelFlg27.Location = New System.Drawing.Point(684, 173)
        Me.chkDelFlg27.Name = "chkDelFlg27"
        Me.chkDelFlg27.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg27.TabIndex = 28
        Me.chkDelFlg27.UseVisualStyleBackColor = True
        '
        'chkDelFlg26
        '
        Me.chkDelFlg26.AutoSize = True
        Me.chkDelFlg26.Location = New System.Drawing.Point(684, 155)
        Me.chkDelFlg26.Name = "chkDelFlg26"
        Me.chkDelFlg26.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg26.TabIndex = 27
        Me.chkDelFlg26.UseVisualStyleBackColor = True
        '
        'chkDelFlg25
        '
        Me.chkDelFlg25.AutoSize = True
        Me.chkDelFlg25.Location = New System.Drawing.Point(684, 137)
        Me.chkDelFlg25.Name = "chkDelFlg25"
        Me.chkDelFlg25.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg25.TabIndex = 26
        Me.chkDelFlg25.UseVisualStyleBackColor = True
        '
        'chkDelFlg24
        '
        Me.chkDelFlg24.AutoSize = True
        Me.chkDelFlg24.Location = New System.Drawing.Point(684, 111)
        Me.chkDelFlg24.Name = "chkDelFlg24"
        Me.chkDelFlg24.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg24.TabIndex = 25
        Me.chkDelFlg24.UseVisualStyleBackColor = True
        '
        'chkDelFlg23
        '
        Me.chkDelFlg23.AutoSize = True
        Me.chkDelFlg23.Location = New System.Drawing.Point(684, 93)
        Me.chkDelFlg23.Name = "chkDelFlg23"
        Me.chkDelFlg23.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg23.TabIndex = 24
        Me.chkDelFlg23.UseVisualStyleBackColor = True
        '
        'chkDelFlg22
        '
        Me.chkDelFlg22.AutoSize = True
        Me.chkDelFlg22.Location = New System.Drawing.Point(684, 75)
        Me.chkDelFlg22.Name = "chkDelFlg22"
        Me.chkDelFlg22.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg22.TabIndex = 23
        Me.chkDelFlg22.UseVisualStyleBackColor = True
        '
        'chkDelFlg21
        '
        Me.chkDelFlg21.AutoSize = True
        Me.chkDelFlg21.Location = New System.Drawing.Point(684, 57)
        Me.chkDelFlg21.Name = "chkDelFlg21"
        Me.chkDelFlg21.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg21.TabIndex = 22
        Me.chkDelFlg21.UseVisualStyleBackColor = True
        '
        'chkDelFlg20
        '
        Me.chkDelFlg20.AutoSize = True
        Me.chkDelFlg20.Location = New System.Drawing.Point(684, 39)
        Me.chkDelFlg20.Name = "chkDelFlg20"
        Me.chkDelFlg20.Size = New System.Drawing.Size(15, 14)
        Me.chkDelFlg20.TabIndex = 21
        Me.chkDelFlg20.UseVisualStyleBackColor = True
        '
        'lblVehicle39
        '
        Me.lblVehicle39.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle39.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle39.Location = New System.Drawing.Point(782, 304)
        Me.lblVehicle39.Name = "lblVehicle39"
        Me.lblVehicle39.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle39.TabIndex = 386
        Me.lblVehicle39.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle38
        '
        Me.lblVehicle38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle38.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle38.Location = New System.Drawing.Point(782, 286)
        Me.lblVehicle38.Name = "lblVehicle38"
        Me.lblVehicle38.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle38.TabIndex = 382
        Me.lblVehicle38.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle37
        '
        Me.lblVehicle37.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle37.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle37.Location = New System.Drawing.Point(782, 268)
        Me.lblVehicle37.Name = "lblVehicle37"
        Me.lblVehicle37.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle37.TabIndex = 383
        Me.lblVehicle37.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle36
        '
        Me.lblVehicle36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle36.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle36.Location = New System.Drawing.Point(782, 250)
        Me.lblVehicle36.Name = "lblVehicle36"
        Me.lblVehicle36.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle36.TabIndex = 385
        Me.lblVehicle36.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle35
        '
        Me.lblVehicle35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle35.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle35.Location = New System.Drawing.Point(782, 232)
        Me.lblVehicle35.Name = "lblVehicle35"
        Me.lblVehicle35.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle35.TabIndex = 384
        Me.lblVehicle35.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo39
        '
        Me.lblBcNo39.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo39.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo39.Location = New System.Drawing.Point(734, 304)
        Me.lblBcNo39.Name = "lblBcNo39"
        Me.lblBcNo39.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo39.TabIndex = 380
        Me.lblBcNo39.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo38
        '
        Me.lblBcNo38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo38.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo38.Location = New System.Drawing.Point(734, 286)
        Me.lblBcNo38.Name = "lblBcNo38"
        Me.lblBcNo38.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo38.TabIndex = 379
        Me.lblBcNo38.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo37
        '
        Me.lblBcNo37.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo37.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo37.Location = New System.Drawing.Point(734, 268)
        Me.lblBcNo37.Name = "lblBcNo37"
        Me.lblBcNo37.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo37.TabIndex = 381
        Me.lblBcNo37.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo36
        '
        Me.lblBcNo36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo36.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo36.Location = New System.Drawing.Point(734, 250)
        Me.lblBcNo36.Name = "lblBcNo36"
        Me.lblBcNo36.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo36.TabIndex = 377
        Me.lblBcNo36.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo35
        '
        Me.lblBcNo35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo35.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo35.Location = New System.Drawing.Point(734, 232)
        Me.lblBcNo35.Name = "lblBcNo35"
        Me.lblBcNo35.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo35.TabIndex = 378
        Me.lblBcNo35.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle34
        '
        Me.lblVehicle34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle34.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle34.Location = New System.Drawing.Point(782, 206)
        Me.lblVehicle34.Name = "lblVehicle34"
        Me.lblVehicle34.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle34.TabIndex = 376
        Me.lblVehicle34.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle33
        '
        Me.lblVehicle33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle33.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle33.Location = New System.Drawing.Point(782, 188)
        Me.lblVehicle33.Name = "lblVehicle33"
        Me.lblVehicle33.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle33.TabIndex = 372
        Me.lblVehicle33.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle32
        '
        Me.lblVehicle32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle32.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle32.Location = New System.Drawing.Point(782, 170)
        Me.lblVehicle32.Name = "lblVehicle32"
        Me.lblVehicle32.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle32.TabIndex = 373
        Me.lblVehicle32.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle31
        '
        Me.lblVehicle31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle31.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle31.Location = New System.Drawing.Point(782, 152)
        Me.lblVehicle31.Name = "lblVehicle31"
        Me.lblVehicle31.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle31.TabIndex = 375
        Me.lblVehicle31.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle30
        '
        Me.lblVehicle30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle30.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle30.Location = New System.Drawing.Point(782, 134)
        Me.lblVehicle30.Name = "lblVehicle30"
        Me.lblVehicle30.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle30.TabIndex = 374
        Me.lblVehicle30.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo34
        '
        Me.lblBcNo34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo34.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo34.Location = New System.Drawing.Point(734, 206)
        Me.lblBcNo34.Name = "lblBcNo34"
        Me.lblBcNo34.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo34.TabIndex = 370
        Me.lblBcNo34.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo33
        '
        Me.lblBcNo33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo33.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo33.Location = New System.Drawing.Point(734, 188)
        Me.lblBcNo33.Name = "lblBcNo33"
        Me.lblBcNo33.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo33.TabIndex = 369
        Me.lblBcNo33.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo32
        '
        Me.lblBcNo32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo32.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo32.Location = New System.Drawing.Point(734, 170)
        Me.lblBcNo32.Name = "lblBcNo32"
        Me.lblBcNo32.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo32.TabIndex = 371
        Me.lblBcNo32.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo31
        '
        Me.lblBcNo31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo31.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo31.Location = New System.Drawing.Point(734, 152)
        Me.lblBcNo31.Name = "lblBcNo31"
        Me.lblBcNo31.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo31.TabIndex = 367
        Me.lblBcNo31.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo30
        '
        Me.lblBcNo30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo30.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo30.Location = New System.Drawing.Point(734, 134)
        Me.lblBcNo30.Name = "lblBcNo30"
        Me.lblBcNo30.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo30.TabIndex = 368
        Me.lblBcNo30.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle29
        '
        Me.lblVehicle29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle29.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle29.Location = New System.Drawing.Point(782, 108)
        Me.lblVehicle29.Name = "lblVehicle29"
        Me.lblVehicle29.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle29.TabIndex = 366
        Me.lblVehicle29.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle28
        '
        Me.lblVehicle28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle28.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle28.Location = New System.Drawing.Point(782, 90)
        Me.lblVehicle28.Name = "lblVehicle28"
        Me.lblVehicle28.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle28.TabIndex = 362
        Me.lblVehicle28.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle27
        '
        Me.lblVehicle27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle27.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle27.Location = New System.Drawing.Point(782, 72)
        Me.lblVehicle27.Name = "lblVehicle27"
        Me.lblVehicle27.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle27.TabIndex = 363
        Me.lblVehicle27.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle26
        '
        Me.lblVehicle26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle26.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle26.Location = New System.Drawing.Point(782, 54)
        Me.lblVehicle26.Name = "lblVehicle26"
        Me.lblVehicle26.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle26.TabIndex = 365
        Me.lblVehicle26.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle25
        '
        Me.lblVehicle25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle25.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle25.Location = New System.Drawing.Point(782, 36)
        Me.lblVehicle25.Name = "lblVehicle25"
        Me.lblVehicle25.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle25.TabIndex = 364
        Me.lblVehicle25.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo29
        '
        Me.lblBcNo29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo29.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo29.Location = New System.Drawing.Point(734, 108)
        Me.lblBcNo29.Name = "lblBcNo29"
        Me.lblBcNo29.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo29.TabIndex = 360
        Me.lblBcNo29.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo28
        '
        Me.lblBcNo28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo28.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo28.Location = New System.Drawing.Point(734, 90)
        Me.lblBcNo28.Name = "lblBcNo28"
        Me.lblBcNo28.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo28.TabIndex = 359
        Me.lblBcNo28.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo27
        '
        Me.lblBcNo27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo27.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo27.Location = New System.Drawing.Point(734, 72)
        Me.lblBcNo27.Name = "lblBcNo27"
        Me.lblBcNo27.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo27.TabIndex = 361
        Me.lblBcNo27.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo26
        '
        Me.lblBcNo26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo26.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo26.Location = New System.Drawing.Point(734, 54)
        Me.lblBcNo26.Name = "lblBcNo26"
        Me.lblBcNo26.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo26.TabIndex = 357
        Me.lblBcNo26.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo25
        '
        Me.lblBcNo25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo25.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo25.Location = New System.Drawing.Point(734, 36)
        Me.lblBcNo25.Name = "lblBcNo25"
        Me.lblBcNo25.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo25.TabIndex = 358
        Me.lblBcNo25.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicleName02
        '
        Me.lblVehicleName02.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicleName02.Location = New System.Drawing.Point(782, 18)
        Me.lblVehicleName02.Name = "lblVehicleName02"
        Me.lblVehicleName02.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicleName02.TabIndex = 356
        Me.lblVehicleName02.Text = "Vehicle Name"
        Me.lblVehicleName02.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblVehicle24
        '
        Me.lblVehicle24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle24.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle24.Location = New System.Drawing.Point(448, 402)
        Me.lblVehicle24.Name = "lblVehicle24"
        Me.lblVehicle24.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle24.TabIndex = 355
        Me.lblVehicle24.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle23
        '
        Me.lblVehicle23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle23.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle23.Location = New System.Drawing.Point(448, 384)
        Me.lblVehicle23.Name = "lblVehicle23"
        Me.lblVehicle23.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle23.TabIndex = 351
        Me.lblVehicle23.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle22
        '
        Me.lblVehicle22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle22.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle22.Location = New System.Drawing.Point(448, 366)
        Me.lblVehicle22.Name = "lblVehicle22"
        Me.lblVehicle22.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle22.TabIndex = 352
        Me.lblVehicle22.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle21
        '
        Me.lblVehicle21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle21.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle21.Location = New System.Drawing.Point(448, 348)
        Me.lblVehicle21.Name = "lblVehicle21"
        Me.lblVehicle21.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle21.TabIndex = 354
        Me.lblVehicle21.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle20
        '
        Me.lblVehicle20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle20.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle20.Location = New System.Drawing.Point(448, 330)
        Me.lblVehicle20.Name = "lblVehicle20"
        Me.lblVehicle20.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle20.TabIndex = 353
        Me.lblVehicle20.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNoName02
        '
        Me.lblBcNoName02.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNoName02.Location = New System.Drawing.Point(734, 18)
        Me.lblBcNoName02.Name = "lblBcNoName02"
        Me.lblBcNoName02.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNoName02.TabIndex = 350
        Me.lblBcNoName02.Text = "BC No"
        Me.lblBcNoName02.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblBcNo24
        '
        Me.lblBcNo24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo24.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo24.Location = New System.Drawing.Point(400, 402)
        Me.lblBcNo24.Name = "lblBcNo24"
        Me.lblBcNo24.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo24.TabIndex = 348
        Me.lblBcNo24.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo23
        '
        Me.lblBcNo23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo23.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo23.Location = New System.Drawing.Point(400, 384)
        Me.lblBcNo23.Name = "lblBcNo23"
        Me.lblBcNo23.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo23.TabIndex = 347
        Me.lblBcNo23.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo22
        '
        Me.lblBcNo22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo22.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo22.Location = New System.Drawing.Point(400, 366)
        Me.lblBcNo22.Name = "lblBcNo22"
        Me.lblBcNo22.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo22.TabIndex = 349
        Me.lblBcNo22.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo21
        '
        Me.lblBcNo21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo21.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo21.Location = New System.Drawing.Point(400, 348)
        Me.lblBcNo21.Name = "lblBcNo21"
        Me.lblBcNo21.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo21.TabIndex = 345
        Me.lblBcNo21.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo20
        '
        Me.lblBcNo20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo20.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo20.Location = New System.Drawing.Point(400, 330)
        Me.lblBcNo20.Name = "lblBcNo20"
        Me.lblBcNo20.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo20.TabIndex = 346
        Me.lblBcNo20.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo44
        '
        Me.lblNo44.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo44.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo44.Location = New System.Drawing.Point(702, 402)
        Me.lblNo44.Name = "lblNo44"
        Me.lblNo44.Size = New System.Drawing.Size(32, 18)
        Me.lblNo44.TabIndex = 341
        Me.lblNo44.Text = "40"
        Me.lblNo44.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo43
        '
        Me.lblNo43.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo43.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo43.Location = New System.Drawing.Point(702, 384)
        Me.lblNo43.Name = "lblNo43"
        Me.lblNo43.Size = New System.Drawing.Size(32, 18)
        Me.lblNo43.TabIndex = 340
        Me.lblNo43.Text = "39"
        Me.lblNo43.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo42
        '
        Me.lblNo42.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo42.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo42.Location = New System.Drawing.Point(702, 366)
        Me.lblNo42.Name = "lblNo42"
        Me.lblNo42.Size = New System.Drawing.Size(32, 18)
        Me.lblNo42.TabIndex = 342
        Me.lblNo42.Text = "38"
        Me.lblNo42.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo41
        '
        Me.lblNo41.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo41.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo41.Location = New System.Drawing.Point(702, 348)
        Me.lblNo41.Name = "lblNo41"
        Me.lblNo41.Size = New System.Drawing.Size(32, 18)
        Me.lblNo41.TabIndex = 343
        Me.lblNo41.Text = "37"
        Me.lblNo41.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo40
        '
        Me.lblNo40.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo40.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo40.Location = New System.Drawing.Point(702, 330)
        Me.lblNo40.Name = "lblNo40"
        Me.lblNo40.Size = New System.Drawing.Size(32, 18)
        Me.lblNo40.TabIndex = 344
        Me.lblNo40.Text = "36"
        Me.lblNo40.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo39
        '
        Me.lblNo39.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo39.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo39.Location = New System.Drawing.Point(702, 304)
        Me.lblNo39.Name = "lblNo39"
        Me.lblNo39.Size = New System.Drawing.Size(32, 18)
        Me.lblNo39.TabIndex = 336
        Me.lblNo39.Text = "35"
        Me.lblNo39.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo38
        '
        Me.lblNo38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo38.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo38.Location = New System.Drawing.Point(702, 286)
        Me.lblNo38.Name = "lblNo38"
        Me.lblNo38.Size = New System.Drawing.Size(32, 18)
        Me.lblNo38.TabIndex = 335
        Me.lblNo38.Text = "34"
        Me.lblNo38.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo37
        '
        Me.lblNo37.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo37.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo37.Location = New System.Drawing.Point(702, 268)
        Me.lblNo37.Name = "lblNo37"
        Me.lblNo37.Size = New System.Drawing.Size(32, 18)
        Me.lblNo37.TabIndex = 337
        Me.lblNo37.Text = "33"
        Me.lblNo37.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo36
        '
        Me.lblNo36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo36.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo36.Location = New System.Drawing.Point(702, 250)
        Me.lblNo36.Name = "lblNo36"
        Me.lblNo36.Size = New System.Drawing.Size(32, 18)
        Me.lblNo36.TabIndex = 338
        Me.lblNo36.Text = "32"
        Me.lblNo36.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo35
        '
        Me.lblNo35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo35.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo35.Location = New System.Drawing.Point(702, 232)
        Me.lblNo35.Name = "lblNo35"
        Me.lblNo35.Size = New System.Drawing.Size(32, 18)
        Me.lblNo35.TabIndex = 339
        Me.lblNo35.Text = "31"
        Me.lblNo35.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo34
        '
        Me.lblNo34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo34.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo34.Location = New System.Drawing.Point(702, 206)
        Me.lblNo34.Name = "lblNo34"
        Me.lblNo34.Size = New System.Drawing.Size(32, 18)
        Me.lblNo34.TabIndex = 331
        Me.lblNo34.Text = "30"
        Me.lblNo34.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo33
        '
        Me.lblNo33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo33.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo33.Location = New System.Drawing.Point(702, 188)
        Me.lblNo33.Name = "lblNo33"
        Me.lblNo33.Size = New System.Drawing.Size(32, 18)
        Me.lblNo33.TabIndex = 330
        Me.lblNo33.Text = "29"
        Me.lblNo33.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo32
        '
        Me.lblNo32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo32.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo32.Location = New System.Drawing.Point(702, 170)
        Me.lblNo32.Name = "lblNo32"
        Me.lblNo32.Size = New System.Drawing.Size(32, 18)
        Me.lblNo32.TabIndex = 332
        Me.lblNo32.Text = "28"
        Me.lblNo32.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo31
        '
        Me.lblNo31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo31.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo31.Location = New System.Drawing.Point(702, 152)
        Me.lblNo31.Name = "lblNo31"
        Me.lblNo31.Size = New System.Drawing.Size(32, 18)
        Me.lblNo31.TabIndex = 333
        Me.lblNo31.Text = "27"
        Me.lblNo31.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo30
        '
        Me.lblNo30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo30.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo30.Location = New System.Drawing.Point(702, 134)
        Me.lblNo30.Name = "lblNo30"
        Me.lblNo30.Size = New System.Drawing.Size(32, 18)
        Me.lblNo30.TabIndex = 334
        Me.lblNo30.Text = "26"
        Me.lblNo30.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNoName02
        '
        Me.lblNoName02.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNoName02.Location = New System.Drawing.Point(702, 18)
        Me.lblNoName02.Name = "lblNoName02"
        Me.lblNoName02.Size = New System.Drawing.Size(32, 18)
        Me.lblNoName02.TabIndex = 329
        Me.lblNoName02.Text = "No"
        Me.lblNoName02.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblNo29
        '
        Me.lblNo29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo29.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo29.Location = New System.Drawing.Point(702, 108)
        Me.lblNo29.Name = "lblNo29"
        Me.lblNo29.Size = New System.Drawing.Size(32, 18)
        Me.lblNo29.TabIndex = 325
        Me.lblNo29.Text = "25"
        Me.lblNo29.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo28
        '
        Me.lblNo28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo28.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo28.Location = New System.Drawing.Point(702, 90)
        Me.lblNo28.Name = "lblNo28"
        Me.lblNo28.Size = New System.Drawing.Size(32, 18)
        Me.lblNo28.TabIndex = 324
        Me.lblNo28.Text = "24"
        Me.lblNo28.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo27
        '
        Me.lblNo27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo27.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo27.Location = New System.Drawing.Point(702, 72)
        Me.lblNo27.Name = "lblNo27"
        Me.lblNo27.Size = New System.Drawing.Size(32, 18)
        Me.lblNo27.TabIndex = 326
        Me.lblNo27.Text = "23"
        Me.lblNo27.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo26
        '
        Me.lblNo26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo26.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo26.Location = New System.Drawing.Point(702, 54)
        Me.lblNo26.Name = "lblNo26"
        Me.lblNo26.Size = New System.Drawing.Size(32, 18)
        Me.lblNo26.TabIndex = 327
        Me.lblNo26.Text = "22"
        Me.lblNo26.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNo25
        '
        Me.lblNo25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNo25.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNo25.Location = New System.Drawing.Point(702, 36)
        Me.lblNo25.Name = "lblNo25"
        Me.lblNo25.Size = New System.Drawing.Size(32, 18)
        Me.lblNo25.TabIndex = 328
        Me.lblNo25.Text = "21"
        Me.lblNo25.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'GroupBox1
        '
        Me.GroupBox1.Location = New System.Drawing.Point(12, 157)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(160, 13)
        Me.GroupBox1.TabIndex = 408
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Deletion processing"
        Me.GroupBox1.Visible = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Location = New System.Drawing.Point(12, 290)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(160, 16)
        Me.GroupBox3.TabIndex = 409
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Display"
        Me.GroupBox3.Visible = False
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(12, 185)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(160, 28)
        Me.btnDelete.TabIndex = 41
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnAllDelete
        '
        Me.btnAllDelete.Location = New System.Drawing.Point(12, 229)
        Me.btnAllDelete.Name = "btnAllDelete"
        Me.btnAllDelete.Size = New System.Drawing.Size(160, 28)
        Me.btnAllDelete.TabIndex = 42
        Me.btnAllDelete.Text = "Delete All"
        Me.btnAllDelete.UseVisualStyleBackColor = True
        '
        'btnRefresh
        '
        Me.btnRefresh.Location = New System.Drawing.Point(12, 320)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(160, 28)
        Me.btnRefresh.TabIndex = 43
        Me.btnRefresh.Text = "Re-Display"
        Me.btnRefresh.UseVisualStyleBackColor = True
        '
        'lblAssyNoName00
        '
        Me.lblAssyNoName00.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNoName00.Location = New System.Drawing.Point(302, 19)
        Me.lblAssyNoName00.Name = "lblAssyNoName00"
        Me.lblAssyNoName00.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNoName00.TabIndex = 418
        Me.lblAssyNoName00.Text = "Assy"
        Me.lblAssyNoName00.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblAssyNoName00.Visible = False
        '
        'lblAssyNo00
        '
        Me.lblAssyNo00.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo00.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo00.Location = New System.Drawing.Point(302, 37)
        Me.lblAssyNo00.Name = "lblAssyNo00"
        Me.lblAssyNo00.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo00.TabIndex = 417
        Me.lblAssyNo00.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo00.Visible = False
        '
        'lblAssyNoName02
        '
        Me.lblAssyNoName02.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNoName02.Location = New System.Drawing.Point(992, 18)
        Me.lblAssyNoName02.Name = "lblAssyNoName02"
        Me.lblAssyNoName02.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNoName02.TabIndex = 439
        Me.lblAssyNoName02.Text = "Assy"
        Me.lblAssyNoName02.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblAssyNoName02.Visible = False
        '
        'lblVehicle44
        '
        Me.lblVehicle44.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle44.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle44.Location = New System.Drawing.Point(782, 402)
        Me.lblVehicle44.Name = "lblVehicle44"
        Me.lblVehicle44.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle44.TabIndex = 464
        Me.lblVehicle44.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle43
        '
        Me.lblVehicle43.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle43.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle43.Location = New System.Drawing.Point(782, 384)
        Me.lblVehicle43.Name = "lblVehicle43"
        Me.lblVehicle43.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle43.TabIndex = 460
        Me.lblVehicle43.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle42
        '
        Me.lblVehicle42.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle42.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle42.Location = New System.Drawing.Point(782, 366)
        Me.lblVehicle42.Name = "lblVehicle42"
        Me.lblVehicle42.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle42.TabIndex = 461
        Me.lblVehicle42.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle41
        '
        Me.lblVehicle41.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle41.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle41.Location = New System.Drawing.Point(782, 348)
        Me.lblVehicle41.Name = "lblVehicle41"
        Me.lblVehicle41.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle41.TabIndex = 463
        Me.lblVehicle41.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVehicle40
        '
        Me.lblVehicle40.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVehicle40.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblVehicle40.Location = New System.Drawing.Point(782, 330)
        Me.lblVehicle40.Name = "lblVehicle40"
        Me.lblVehicle40.Size = New System.Drawing.Size(80, 18)
        Me.lblVehicle40.TabIndex = 462
        Me.lblVehicle40.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo44
        '
        Me.lblBcNo44.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo44.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo44.Location = New System.Drawing.Point(734, 402)
        Me.lblBcNo44.Name = "lblBcNo44"
        Me.lblBcNo44.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo44.TabIndex = 458
        Me.lblBcNo44.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo43
        '
        Me.lblBcNo43.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo43.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo43.Location = New System.Drawing.Point(734, 384)
        Me.lblBcNo43.Name = "lblBcNo43"
        Me.lblBcNo43.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo43.TabIndex = 457
        Me.lblBcNo43.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo42
        '
        Me.lblBcNo42.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo42.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo42.Location = New System.Drawing.Point(734, 366)
        Me.lblBcNo42.Name = "lblBcNo42"
        Me.lblBcNo42.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo42.TabIndex = 459
        Me.lblBcNo42.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo41
        '
        Me.lblBcNo41.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo41.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo41.Location = New System.Drawing.Point(734, 348)
        Me.lblBcNo41.Name = "lblBcNo41"
        Me.lblBcNo41.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo41.TabIndex = 455
        Me.lblBcNo41.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBcNo40
        '
        Me.lblBcNo40.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBcNo40.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblBcNo40.Location = New System.Drawing.Point(734, 330)
        Me.lblBcNo40.Name = "lblBcNo40"
        Me.lblBcNo40.Size = New System.Drawing.Size(48, 18)
        Me.lblBcNo40.TabIndex = 456
        Me.lblBcNo40.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblAssyNo01
        '
        Me.lblAssyNo01.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo01.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo01.Location = New System.Drawing.Point(302, 55)
        Me.lblAssyNo01.Name = "lblAssyNo01"
        Me.lblAssyNo01.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo01.TabIndex = 465
        Me.lblAssyNo01.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo01.Visible = False
        '
        'lblAssyNo02
        '
        Me.lblAssyNo02.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo02.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo02.Location = New System.Drawing.Point(302, 73)
        Me.lblAssyNo02.Name = "lblAssyNo02"
        Me.lblAssyNo02.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo02.TabIndex = 466
        Me.lblAssyNo02.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo02.Visible = False
        '
        'lblAssyNo03
        '
        Me.lblAssyNo03.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo03.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo03.Location = New System.Drawing.Point(302, 91)
        Me.lblAssyNo03.Name = "lblAssyNo03"
        Me.lblAssyNo03.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo03.TabIndex = 467
        Me.lblAssyNo03.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo03.Visible = False
        '
        'lblAssyNo04
        '
        Me.lblAssyNo04.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo04.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo04.Location = New System.Drawing.Point(302, 109)
        Me.lblAssyNo04.Name = "lblAssyNo04"
        Me.lblAssyNo04.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo04.TabIndex = 468
        Me.lblAssyNo04.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo04.Visible = False
        '
        'lblAssyNoName01
        '
        Me.lblAssyNoName01.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNoName01.Location = New System.Drawing.Point(658, 18)
        Me.lblAssyNoName01.Name = "lblAssyNoName01"
        Me.lblAssyNoName01.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNoName01.TabIndex = 469
        Me.lblAssyNoName01.Text = "Assy"
        Me.lblAssyNoName01.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblAssyNoName01.Visible = False
        '
        'lblAssyNo05
        '
        Me.lblAssyNo05.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo05.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo05.Location = New System.Drawing.Point(658, 36)
        Me.lblAssyNo05.Name = "lblAssyNo05"
        Me.lblAssyNo05.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo05.TabIndex = 473
        Me.lblAssyNo05.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo05.Visible = False
        '
        'lblAssyNo06
        '
        Me.lblAssyNo06.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo06.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo06.Location = New System.Drawing.Point(658, 54)
        Me.lblAssyNo06.Name = "lblAssyNo06"
        Me.lblAssyNo06.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo06.TabIndex = 472
        Me.lblAssyNo06.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo06.Visible = False
        '
        'lblAssyNo07
        '
        Me.lblAssyNo07.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo07.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo07.Location = New System.Drawing.Point(658, 72)
        Me.lblAssyNo07.Name = "lblAssyNo07"
        Me.lblAssyNo07.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo07.TabIndex = 471
        Me.lblAssyNo07.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo07.Visible = False
        '
        'lblAssyNo08
        '
        Me.lblAssyNo08.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo08.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo08.Location = New System.Drawing.Point(658, 90)
        Me.lblAssyNo08.Name = "lblAssyNo08"
        Me.lblAssyNo08.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo08.TabIndex = 470
        Me.lblAssyNo08.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo08.Visible = False
        '
        'lblAssyNo09
        '
        Me.lblAssyNo09.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo09.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo09.Location = New System.Drawing.Point(658, 108)
        Me.lblAssyNo09.Name = "lblAssyNo09"
        Me.lblAssyNo09.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo09.TabIndex = 477
        Me.lblAssyNo09.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo09.Visible = False
        '
        'lblAssyNo10
        '
        Me.lblAssyNo10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo10.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo10.Location = New System.Drawing.Point(658, 134)
        Me.lblAssyNo10.Name = "lblAssyNo10"
        Me.lblAssyNo10.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo10.TabIndex = 476
        Me.lblAssyNo10.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo10.Visible = False
        '
        'lblAssyNo11
        '
        Me.lblAssyNo11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo11.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo11.Location = New System.Drawing.Point(658, 152)
        Me.lblAssyNo11.Name = "lblAssyNo11"
        Me.lblAssyNo11.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo11.TabIndex = 475
        Me.lblAssyNo11.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo11.Visible = False
        '
        'lblAssyNo12
        '
        Me.lblAssyNo12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo12.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo12.Location = New System.Drawing.Point(658, 170)
        Me.lblAssyNo12.Name = "lblAssyNo12"
        Me.lblAssyNo12.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo12.TabIndex = 474
        Me.lblAssyNo12.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo12.Visible = False
        '
        'lblAssyNo13
        '
        Me.lblAssyNo13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo13.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo13.Location = New System.Drawing.Point(658, 188)
        Me.lblAssyNo13.Name = "lblAssyNo13"
        Me.lblAssyNo13.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo13.TabIndex = 481
        Me.lblAssyNo13.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo13.Visible = False
        '
        'lblAssyNo14
        '
        Me.lblAssyNo14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo14.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo14.Location = New System.Drawing.Point(658, 206)
        Me.lblAssyNo14.Name = "lblAssyNo14"
        Me.lblAssyNo14.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo14.TabIndex = 480
        Me.lblAssyNo14.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo14.Visible = False
        '
        'lblAssyNo15
        '
        Me.lblAssyNo15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo15.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo15.Location = New System.Drawing.Point(658, 232)
        Me.lblAssyNo15.Name = "lblAssyNo15"
        Me.lblAssyNo15.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo15.TabIndex = 479
        Me.lblAssyNo15.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo15.Visible = False
        '
        'lblAssyNo16
        '
        Me.lblAssyNo16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo16.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo16.Location = New System.Drawing.Point(658, 250)
        Me.lblAssyNo16.Name = "lblAssyNo16"
        Me.lblAssyNo16.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo16.TabIndex = 478
        Me.lblAssyNo16.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo16.Visible = False
        '
        'lblAssyNo17
        '
        Me.lblAssyNo17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo17.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo17.Location = New System.Drawing.Point(658, 268)
        Me.lblAssyNo17.Name = "lblAssyNo17"
        Me.lblAssyNo17.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo17.TabIndex = 485
        Me.lblAssyNo17.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo17.Visible = False
        '
        'lblAssyNo18
        '
        Me.lblAssyNo18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo18.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo18.Location = New System.Drawing.Point(658, 286)
        Me.lblAssyNo18.Name = "lblAssyNo18"
        Me.lblAssyNo18.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo18.TabIndex = 484
        Me.lblAssyNo18.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo18.Visible = False
        '
        'lblAssyNo19
        '
        Me.lblAssyNo19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo19.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo19.Location = New System.Drawing.Point(658, 304)
        Me.lblAssyNo19.Name = "lblAssyNo19"
        Me.lblAssyNo19.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo19.TabIndex = 483
        Me.lblAssyNo19.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo19.Visible = False
        '
        'lblAssyNo20
        '
        Me.lblAssyNo20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo20.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo20.Location = New System.Drawing.Point(658, 330)
        Me.lblAssyNo20.Name = "lblAssyNo20"
        Me.lblAssyNo20.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo20.TabIndex = 482
        Me.lblAssyNo20.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo20.Visible = False
        '
        'lblAssyNo21
        '
        Me.lblAssyNo21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo21.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo21.Location = New System.Drawing.Point(658, 348)
        Me.lblAssyNo21.Name = "lblAssyNo21"
        Me.lblAssyNo21.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo21.TabIndex = 489
        Me.lblAssyNo21.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo21.Visible = False
        '
        'lblAssyNo22
        '
        Me.lblAssyNo22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo22.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo22.Location = New System.Drawing.Point(658, 366)
        Me.lblAssyNo22.Name = "lblAssyNo22"
        Me.lblAssyNo22.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo22.TabIndex = 488
        Me.lblAssyNo22.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo22.Visible = False
        '
        'lblAssyNo23
        '
        Me.lblAssyNo23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo23.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo23.Location = New System.Drawing.Point(658, 384)
        Me.lblAssyNo23.Name = "lblAssyNo23"
        Me.lblAssyNo23.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo23.TabIndex = 487
        Me.lblAssyNo23.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo23.Visible = False
        '
        'lblAssyNo24
        '
        Me.lblAssyNo24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo24.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo24.Location = New System.Drawing.Point(658, 402)
        Me.lblAssyNo24.Name = "lblAssyNo24"
        Me.lblAssyNo24.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo24.TabIndex = 486
        Me.lblAssyNo24.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo24.Visible = False
        '
        'lblAssyNo25
        '
        Me.lblAssyNo25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo25.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo25.Location = New System.Drawing.Point(992, 36)
        Me.lblAssyNo25.Name = "lblAssyNo25"
        Me.lblAssyNo25.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo25.TabIndex = 493
        Me.lblAssyNo25.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo25.Visible = False
        '
        'lblAssyNo26
        '
        Me.lblAssyNo26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo26.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo26.Location = New System.Drawing.Point(992, 54)
        Me.lblAssyNo26.Name = "lblAssyNo26"
        Me.lblAssyNo26.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo26.TabIndex = 492
        Me.lblAssyNo26.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo26.Visible = False
        '
        'lblAssyNo27
        '
        Me.lblAssyNo27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo27.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo27.Location = New System.Drawing.Point(992, 72)
        Me.lblAssyNo27.Name = "lblAssyNo27"
        Me.lblAssyNo27.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo27.TabIndex = 491
        Me.lblAssyNo27.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo27.Visible = False
        '
        'lblAssyNo28
        '
        Me.lblAssyNo28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo28.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo28.Location = New System.Drawing.Point(992, 90)
        Me.lblAssyNo28.Name = "lblAssyNo28"
        Me.lblAssyNo28.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo28.TabIndex = 490
        Me.lblAssyNo28.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo28.Visible = False
        '
        'lblAssyNo29
        '
        Me.lblAssyNo29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo29.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo29.Location = New System.Drawing.Point(992, 108)
        Me.lblAssyNo29.Name = "lblAssyNo29"
        Me.lblAssyNo29.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo29.TabIndex = 497
        Me.lblAssyNo29.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo29.Visible = False
        '
        'lblAssyNo30
        '
        Me.lblAssyNo30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo30.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo30.Location = New System.Drawing.Point(992, 134)
        Me.lblAssyNo30.Name = "lblAssyNo30"
        Me.lblAssyNo30.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo30.TabIndex = 496
        Me.lblAssyNo30.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo30.Visible = False
        '
        'lblAssyNo31
        '
        Me.lblAssyNo31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo31.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo31.Location = New System.Drawing.Point(992, 152)
        Me.lblAssyNo31.Name = "lblAssyNo31"
        Me.lblAssyNo31.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo31.TabIndex = 495
        Me.lblAssyNo31.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo31.Visible = False
        '
        'lblAssyNo32
        '
        Me.lblAssyNo32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo32.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo32.Location = New System.Drawing.Point(992, 170)
        Me.lblAssyNo32.Name = "lblAssyNo32"
        Me.lblAssyNo32.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo32.TabIndex = 494
        Me.lblAssyNo32.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo32.Visible = False
        '
        'lblAssyNo33
        '
        Me.lblAssyNo33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo33.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo33.Location = New System.Drawing.Point(992, 188)
        Me.lblAssyNo33.Name = "lblAssyNo33"
        Me.lblAssyNo33.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo33.TabIndex = 501
        Me.lblAssyNo33.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo33.Visible = False
        '
        'lblAssyNo34
        '
        Me.lblAssyNo34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo34.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo34.Location = New System.Drawing.Point(992, 206)
        Me.lblAssyNo34.Name = "lblAssyNo34"
        Me.lblAssyNo34.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo34.TabIndex = 500
        Me.lblAssyNo34.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo34.Visible = False
        '
        'lblAssyNo35
        '
        Me.lblAssyNo35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo35.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo35.Location = New System.Drawing.Point(992, 232)
        Me.lblAssyNo35.Name = "lblAssyNo35"
        Me.lblAssyNo35.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo35.TabIndex = 499
        Me.lblAssyNo35.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo35.Visible = False
        '
        'lblAssyNo36
        '
        Me.lblAssyNo36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo36.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo36.Location = New System.Drawing.Point(992, 250)
        Me.lblAssyNo36.Name = "lblAssyNo36"
        Me.lblAssyNo36.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo36.TabIndex = 498
        Me.lblAssyNo36.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo36.Visible = False
        '
        'lblAssyNo37
        '
        Me.lblAssyNo37.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo37.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo37.Location = New System.Drawing.Point(992, 268)
        Me.lblAssyNo37.Name = "lblAssyNo37"
        Me.lblAssyNo37.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo37.TabIndex = 505
        Me.lblAssyNo37.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo37.Visible = False
        '
        'lblAssyNo38
        '
        Me.lblAssyNo38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo38.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo38.Location = New System.Drawing.Point(992, 286)
        Me.lblAssyNo38.Name = "lblAssyNo38"
        Me.lblAssyNo38.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo38.TabIndex = 504
        Me.lblAssyNo38.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo38.Visible = False
        '
        'lblAssyNo39
        '
        Me.lblAssyNo39.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo39.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo39.Location = New System.Drawing.Point(992, 304)
        Me.lblAssyNo39.Name = "lblAssyNo39"
        Me.lblAssyNo39.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo39.TabIndex = 503
        Me.lblAssyNo39.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo39.Visible = False
        '
        'lblAssyNo40
        '
        Me.lblAssyNo40.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo40.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo40.Location = New System.Drawing.Point(992, 330)
        Me.lblAssyNo40.Name = "lblAssyNo40"
        Me.lblAssyNo40.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo40.TabIndex = 502
        Me.lblAssyNo40.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo40.Visible = False
        '
        'lblAssyNo41
        '
        Me.lblAssyNo41.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo41.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo41.Location = New System.Drawing.Point(992, 348)
        Me.lblAssyNo41.Name = "lblAssyNo41"
        Me.lblAssyNo41.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo41.TabIndex = 509
        Me.lblAssyNo41.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo41.Visible = False
        '
        'lblAssyNo42
        '
        Me.lblAssyNo42.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo42.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo42.Location = New System.Drawing.Point(992, 366)
        Me.lblAssyNo42.Name = "lblAssyNo42"
        Me.lblAssyNo42.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo42.TabIndex = 508
        Me.lblAssyNo42.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo42.Visible = False
        '
        'lblAssyNo43
        '
        Me.lblAssyNo43.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo43.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo43.Location = New System.Drawing.Point(992, 384)
        Me.lblAssyNo43.Name = "lblAssyNo43"
        Me.lblAssyNo43.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo43.TabIndex = 507
        Me.lblAssyNo43.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo43.Visible = False
        '
        'lblAssyNo44
        '
        Me.lblAssyNo44.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAssyNo44.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblAssyNo44.Location = New System.Drawing.Point(992, 402)
        Me.lblAssyNo44.Name = "lblAssyNo44"
        Me.lblAssyNo44.Size = New System.Drawing.Size(33, 18)
        Me.lblAssyNo44.TabIndex = 506
        Me.lblAssyNo44.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblAssyNo44.Visible = False
        '
        'lblLOTcode00
        '
        Me.lblLOTcode00.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode00.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode00.Location = New System.Drawing.Point(172, 37)
        Me.lblLOTcode00.Name = "lblLOTcode00"
        Me.lblLOTcode00.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode00.TabIndex = 417
        Me.lblLOTcode00.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcodeName00
        '
        Me.lblLOTcodeName00.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcodeName00.Location = New System.Drawing.Point(172, 19)
        Me.lblLOTcodeName00.Name = "lblLOTcodeName00"
        Me.lblLOTcodeName00.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcodeName00.TabIndex = 418
        Me.lblLOTcodeName00.Text = "LOTcode"
        Me.lblLOTcodeName00.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblLOTcode01
        '
        Me.lblLOTcode01.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode01.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode01.Location = New System.Drawing.Point(172, 55)
        Me.lblLOTcode01.Name = "lblLOTcode01"
        Me.lblLOTcode01.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode01.TabIndex = 465
        Me.lblLOTcode01.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblLOTcode01.Visible = False
        '
        'lblLOTcode02
        '
        Me.lblLOTcode02.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode02.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode02.Location = New System.Drawing.Point(172, 73)
        Me.lblLOTcode02.Name = "lblLOTcode02"
        Me.lblLOTcode02.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode02.TabIndex = 466
        Me.lblLOTcode02.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblLOTcode02.Visible = False
        '
        'lblLOTcode03
        '
        Me.lblLOTcode03.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode03.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode03.Location = New System.Drawing.Point(172, 91)
        Me.lblLOTcode03.Name = "lblLOTcode03"
        Me.lblLOTcode03.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode03.TabIndex = 467
        Me.lblLOTcode03.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblLOTcode03.Visible = False
        '
        'lblLOTcode04
        '
        Me.lblLOTcode04.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode04.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode04.Location = New System.Drawing.Point(172, 109)
        Me.lblLOTcode04.Name = "lblLOTcode04"
        Me.lblLOTcode04.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode04.TabIndex = 468
        Me.lblLOTcode04.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblLOTcode04.Visible = False
        '
        'lblFamilycode00
        '
        Me.lblFamilycode00.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode00.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode00.Location = New System.Drawing.Point(237, 37)
        Me.lblFamilycode00.Name = "lblFamilycode00"
        Me.lblFamilycode00.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode00.TabIndex = 417
        Me.lblFamilycode00.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label8
        '
        Me.Label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label8.Location = New System.Drawing.Point(237, 19)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(65, 18)
        Me.Label8.TabIndex = 418
        Me.Label8.Text = "Familycode"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblFamilycode01
        '
        Me.lblFamilycode01.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode01.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode01.Location = New System.Drawing.Point(237, 55)
        Me.lblFamilycode01.Name = "lblFamilycode01"
        Me.lblFamilycode01.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode01.TabIndex = 465
        Me.lblFamilycode01.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblFamilycode01.Visible = False
        '
        'lblFamilycode02
        '
        Me.lblFamilycode02.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode02.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode02.Location = New System.Drawing.Point(237, 73)
        Me.lblFamilycode02.Name = "lblFamilycode02"
        Me.lblFamilycode02.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode02.TabIndex = 466
        Me.lblFamilycode02.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblFamilycode02.Visible = False
        '
        'lblFamilycode03
        '
        Me.lblFamilycode03.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode03.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode03.Location = New System.Drawing.Point(237, 91)
        Me.lblFamilycode03.Name = "lblFamilycode03"
        Me.lblFamilycode03.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode03.TabIndex = 467
        Me.lblFamilycode03.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblFamilycode03.Visible = False
        '
        'lblFamilycode04
        '
        Me.lblFamilycode04.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode04.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode04.Location = New System.Drawing.Point(237, 109)
        Me.lblFamilycode04.Name = "lblFamilycode04"
        Me.lblFamilycode04.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode04.TabIndex = 468
        Me.lblFamilycode04.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblFamilycode04.Visible = False
        '
        'lblLOTcode05
        '
        Me.lblLOTcode05.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode05.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode05.Location = New System.Drawing.Point(528, 36)
        Me.lblLOTcode05.Name = "lblLOTcode05"
        Me.lblLOTcode05.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode05.TabIndex = 417
        Me.lblLOTcode05.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode05
        '
        Me.lblFamilycode05.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode05.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode05.Location = New System.Drawing.Point(593, 36)
        Me.lblFamilycode05.Name = "lblFamilycode05"
        Me.lblFamilycode05.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode05.TabIndex = 417
        Me.lblFamilycode05.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label14
        '
        Me.Label14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label14.Location = New System.Drawing.Point(528, 18)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(65, 18)
        Me.Label14.TabIndex = 418
        Me.Label14.Text = "LOTcode"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label15
        '
        Me.Label15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label15.Location = New System.Drawing.Point(593, 18)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(65, 18)
        Me.Label15.TabIndex = 418
        Me.Label15.Text = "Familycode"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblLOTcode06
        '
        Me.lblLOTcode06.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode06.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode06.Location = New System.Drawing.Point(528, 54)
        Me.lblLOTcode06.Name = "lblLOTcode06"
        Me.lblLOTcode06.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode06.TabIndex = 465
        Me.lblLOTcode06.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode06
        '
        Me.lblFamilycode06.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode06.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode06.Location = New System.Drawing.Point(593, 54)
        Me.lblFamilycode06.Name = "lblFamilycode06"
        Me.lblFamilycode06.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode06.TabIndex = 465
        Me.lblFamilycode06.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode07
        '
        Me.lblLOTcode07.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode07.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode07.Location = New System.Drawing.Point(528, 72)
        Me.lblLOTcode07.Name = "lblLOTcode07"
        Me.lblLOTcode07.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode07.TabIndex = 466
        Me.lblLOTcode07.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode07
        '
        Me.lblFamilycode07.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode07.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode07.Location = New System.Drawing.Point(593, 72)
        Me.lblFamilycode07.Name = "lblFamilycode07"
        Me.lblFamilycode07.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode07.TabIndex = 466
        Me.lblFamilycode07.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode08
        '
        Me.lblLOTcode08.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode08.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode08.Location = New System.Drawing.Point(528, 90)
        Me.lblLOTcode08.Name = "lblLOTcode08"
        Me.lblLOTcode08.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode08.TabIndex = 467
        Me.lblLOTcode08.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode08
        '
        Me.lblFamilycode08.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode08.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode08.Location = New System.Drawing.Point(593, 90)
        Me.lblFamilycode08.Name = "lblFamilycode08"
        Me.lblFamilycode08.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode08.TabIndex = 467
        Me.lblFamilycode08.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode09
        '
        Me.lblLOTcode09.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode09.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode09.Location = New System.Drawing.Point(528, 108)
        Me.lblLOTcode09.Name = "lblLOTcode09"
        Me.lblLOTcode09.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode09.TabIndex = 468
        Me.lblLOTcode09.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode09
        '
        Me.lblFamilycode09.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode09.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode09.Location = New System.Drawing.Point(593, 108)
        Me.lblFamilycode09.Name = "lblFamilycode09"
        Me.lblFamilycode09.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode09.TabIndex = 468
        Me.lblFamilycode09.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode10
        '
        Me.lblLOTcode10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode10.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode10.Location = New System.Drawing.Point(528, 134)
        Me.lblLOTcode10.Name = "lblLOTcode10"
        Me.lblLOTcode10.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode10.TabIndex = 417
        Me.lblLOTcode10.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode10
        '
        Me.lblFamilycode10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode10.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode10.Location = New System.Drawing.Point(593, 134)
        Me.lblFamilycode10.Name = "lblFamilycode10"
        Me.lblFamilycode10.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode10.TabIndex = 417
        Me.lblFamilycode10.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode11
        '
        Me.lblLOTcode11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode11.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode11.Location = New System.Drawing.Point(528, 152)
        Me.lblLOTcode11.Name = "lblLOTcode11"
        Me.lblLOTcode11.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode11.TabIndex = 465
        Me.lblLOTcode11.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode11
        '
        Me.lblFamilycode11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode11.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode11.Location = New System.Drawing.Point(593, 152)
        Me.lblFamilycode11.Name = "lblFamilycode11"
        Me.lblFamilycode11.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode11.TabIndex = 465
        Me.lblFamilycode11.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode12
        '
        Me.lblLOTcode12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode12.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode12.Location = New System.Drawing.Point(528, 170)
        Me.lblLOTcode12.Name = "lblLOTcode12"
        Me.lblLOTcode12.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode12.TabIndex = 466
        Me.lblLOTcode12.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode12
        '
        Me.lblFamilycode12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode12.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode12.Location = New System.Drawing.Point(593, 170)
        Me.lblFamilycode12.Name = "lblFamilycode12"
        Me.lblFamilycode12.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode12.TabIndex = 466
        Me.lblFamilycode12.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode13
        '
        Me.lblLOTcode13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode13.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode13.Location = New System.Drawing.Point(528, 188)
        Me.lblLOTcode13.Name = "lblLOTcode13"
        Me.lblLOTcode13.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode13.TabIndex = 467
        Me.lblLOTcode13.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode13
        '
        Me.lblFamilycode13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode13.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode13.Location = New System.Drawing.Point(593, 188)
        Me.lblFamilycode13.Name = "lblFamilycode13"
        Me.lblFamilycode13.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode13.TabIndex = 467
        Me.lblFamilycode13.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode14
        '
        Me.lblLOTcode14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode14.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode14.Location = New System.Drawing.Point(528, 206)
        Me.lblLOTcode14.Name = "lblLOTcode14"
        Me.lblLOTcode14.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode14.TabIndex = 468
        Me.lblLOTcode14.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode14
        '
        Me.lblFamilycode14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode14.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode14.Location = New System.Drawing.Point(593, 206)
        Me.lblFamilycode14.Name = "lblFamilycode14"
        Me.lblFamilycode14.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode14.TabIndex = 468
        Me.lblFamilycode14.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode15
        '
        Me.lblLOTcode15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode15.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode15.Location = New System.Drawing.Point(528, 232)
        Me.lblLOTcode15.Name = "lblLOTcode15"
        Me.lblLOTcode15.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode15.TabIndex = 417
        Me.lblLOTcode15.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode15
        '
        Me.lblFamilycode15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode15.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode15.Location = New System.Drawing.Point(593, 232)
        Me.lblFamilycode15.Name = "lblFamilycode15"
        Me.lblFamilycode15.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode15.TabIndex = 417
        Me.lblFamilycode15.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode16
        '
        Me.lblLOTcode16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode16.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode16.Location = New System.Drawing.Point(528, 250)
        Me.lblLOTcode16.Name = "lblLOTcode16"
        Me.lblLOTcode16.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode16.TabIndex = 465
        Me.lblLOTcode16.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode16
        '
        Me.lblFamilycode16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode16.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode16.Location = New System.Drawing.Point(593, 250)
        Me.lblFamilycode16.Name = "lblFamilycode16"
        Me.lblFamilycode16.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode16.TabIndex = 465
        Me.lblFamilycode16.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode17
        '
        Me.lblLOTcode17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode17.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode17.Location = New System.Drawing.Point(528, 268)
        Me.lblLOTcode17.Name = "lblLOTcode17"
        Me.lblLOTcode17.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode17.TabIndex = 466
        Me.lblLOTcode17.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode17
        '
        Me.lblFamilycode17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode17.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode17.Location = New System.Drawing.Point(593, 268)
        Me.lblFamilycode17.Name = "lblFamilycode17"
        Me.lblFamilycode17.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode17.TabIndex = 466
        Me.lblFamilycode17.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode18
        '
        Me.lblLOTcode18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode18.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode18.Location = New System.Drawing.Point(528, 286)
        Me.lblLOTcode18.Name = "lblLOTcode18"
        Me.lblLOTcode18.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode18.TabIndex = 467
        Me.lblLOTcode18.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode18
        '
        Me.lblFamilycode18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode18.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode18.Location = New System.Drawing.Point(593, 286)
        Me.lblFamilycode18.Name = "lblFamilycode18"
        Me.lblFamilycode18.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode18.TabIndex = 467
        Me.lblFamilycode18.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode19
        '
        Me.lblLOTcode19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode19.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode19.Location = New System.Drawing.Point(528, 304)
        Me.lblLOTcode19.Name = "lblLOTcode19"
        Me.lblLOTcode19.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode19.TabIndex = 468
        Me.lblLOTcode19.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode19
        '
        Me.lblFamilycode19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode19.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode19.Location = New System.Drawing.Point(593, 304)
        Me.lblFamilycode19.Name = "lblFamilycode19"
        Me.lblFamilycode19.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode19.TabIndex = 468
        Me.lblFamilycode19.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode20
        '
        Me.lblLOTcode20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode20.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode20.Location = New System.Drawing.Point(528, 330)
        Me.lblLOTcode20.Name = "lblLOTcode20"
        Me.lblLOTcode20.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode20.TabIndex = 417
        Me.lblLOTcode20.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode20
        '
        Me.lblFamilycode20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode20.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode20.Location = New System.Drawing.Point(593, 330)
        Me.lblFamilycode20.Name = "lblFamilycode20"
        Me.lblFamilycode20.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode20.TabIndex = 417
        Me.lblFamilycode20.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode21
        '
        Me.lblLOTcode21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode21.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode21.Location = New System.Drawing.Point(528, 348)
        Me.lblLOTcode21.Name = "lblLOTcode21"
        Me.lblLOTcode21.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode21.TabIndex = 465
        Me.lblLOTcode21.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode21
        '
        Me.lblFamilycode21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode21.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode21.Location = New System.Drawing.Point(593, 348)
        Me.lblFamilycode21.Name = "lblFamilycode21"
        Me.lblFamilycode21.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode21.TabIndex = 465
        Me.lblFamilycode21.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode22
        '
        Me.lblLOTcode22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode22.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode22.Location = New System.Drawing.Point(528, 366)
        Me.lblLOTcode22.Name = "lblLOTcode22"
        Me.lblLOTcode22.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode22.TabIndex = 466
        Me.lblLOTcode22.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode22
        '
        Me.lblFamilycode22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode22.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode22.Location = New System.Drawing.Point(593, 366)
        Me.lblFamilycode22.Name = "lblFamilycode22"
        Me.lblFamilycode22.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode22.TabIndex = 466
        Me.lblFamilycode22.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode23
        '
        Me.lblLOTcode23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode23.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode23.Location = New System.Drawing.Point(528, 384)
        Me.lblLOTcode23.Name = "lblLOTcode23"
        Me.lblLOTcode23.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode23.TabIndex = 467
        Me.lblLOTcode23.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode23
        '
        Me.lblFamilycode23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode23.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode23.Location = New System.Drawing.Point(593, 384)
        Me.lblFamilycode23.Name = "lblFamilycode23"
        Me.lblFamilycode23.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode23.TabIndex = 467
        Me.lblFamilycode23.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode24
        '
        Me.lblLOTcode24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode24.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode24.Location = New System.Drawing.Point(528, 402)
        Me.lblLOTcode24.Name = "lblLOTcode24"
        Me.lblLOTcode24.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode24.TabIndex = 468
        Me.lblLOTcode24.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode24
        '
        Me.lblFamilycode24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode24.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode24.Location = New System.Drawing.Point(593, 402)
        Me.lblFamilycode24.Name = "lblFamilycode24"
        Me.lblFamilycode24.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode24.TabIndex = 468
        Me.lblFamilycode24.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode25
        '
        Me.lblLOTcode25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode25.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode25.Location = New System.Drawing.Point(862, 36)
        Me.lblLOTcode25.Name = "lblLOTcode25"
        Me.lblLOTcode25.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode25.TabIndex = 417
        Me.lblLOTcode25.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode30
        '
        Me.lblLOTcode30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode30.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode30.Location = New System.Drawing.Point(862, 134)
        Me.lblLOTcode30.Name = "lblLOTcode30"
        Me.lblLOTcode30.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode30.TabIndex = 417
        Me.lblLOTcode30.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode35
        '
        Me.lblLOTcode35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode35.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode35.Location = New System.Drawing.Point(862, 232)
        Me.lblLOTcode35.Name = "lblLOTcode35"
        Me.lblLOTcode35.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode35.TabIndex = 417
        Me.lblLOTcode35.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode25
        '
        Me.lblFamilycode25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode25.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode25.Location = New System.Drawing.Point(927, 36)
        Me.lblFamilycode25.Name = "lblFamilycode25"
        Me.lblFamilycode25.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode25.TabIndex = 417
        Me.lblFamilycode25.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode40
        '
        Me.lblLOTcode40.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode40.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode40.Location = New System.Drawing.Point(862, 330)
        Me.lblLOTcode40.Name = "lblLOTcode40"
        Me.lblLOTcode40.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode40.TabIndex = 417
        Me.lblLOTcode40.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode30
        '
        Me.lblFamilycode30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode30.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode30.Location = New System.Drawing.Point(927, 134)
        Me.lblFamilycode30.Name = "lblFamilycode30"
        Me.lblFamilycode30.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode30.TabIndex = 417
        Me.lblFamilycode30.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label7
        '
        Me.Label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label7.Location = New System.Drawing.Point(862, 18)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(65, 18)
        Me.Label7.TabIndex = 418
        Me.Label7.Text = "LOTcode"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblFamilycode35
        '
        Me.lblFamilycode35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode35.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode35.Location = New System.Drawing.Point(927, 232)
        Me.lblFamilycode35.Name = "lblFamilycode35"
        Me.lblFamilycode35.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode35.TabIndex = 417
        Me.lblFamilycode35.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode40
        '
        Me.lblFamilycode40.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode40.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode40.Location = New System.Drawing.Point(927, 330)
        Me.lblFamilycode40.Name = "lblFamilycode40"
        Me.lblFamilycode40.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode40.TabIndex = 417
        Me.lblFamilycode40.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label11
        '
        Me.Label11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label11.Location = New System.Drawing.Point(927, 18)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(65, 18)
        Me.Label11.TabIndex = 418
        Me.Label11.Text = "Familycode"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblLOTcode26
        '
        Me.lblLOTcode26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode26.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode26.Location = New System.Drawing.Point(862, 54)
        Me.lblLOTcode26.Name = "lblLOTcode26"
        Me.lblLOTcode26.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode26.TabIndex = 465
        Me.lblLOTcode26.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode26
        '
        Me.lblFamilycode26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode26.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode26.Location = New System.Drawing.Point(927, 54)
        Me.lblFamilycode26.Name = "lblFamilycode26"
        Me.lblFamilycode26.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode26.TabIndex = 465
        Me.lblFamilycode26.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode31
        '
        Me.lblLOTcode31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode31.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode31.Location = New System.Drawing.Point(862, 152)
        Me.lblLOTcode31.Name = "lblLOTcode31"
        Me.lblLOTcode31.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode31.TabIndex = 465
        Me.lblLOTcode31.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode36
        '
        Me.lblLOTcode36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode36.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode36.Location = New System.Drawing.Point(862, 250)
        Me.lblLOTcode36.Name = "lblLOTcode36"
        Me.lblLOTcode36.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode36.TabIndex = 465
        Me.lblLOTcode36.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode31
        '
        Me.lblFamilycode31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode31.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode31.Location = New System.Drawing.Point(927, 152)
        Me.lblFamilycode31.Name = "lblFamilycode31"
        Me.lblFamilycode31.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode31.TabIndex = 465
        Me.lblFamilycode31.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode41
        '
        Me.lblLOTcode41.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode41.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode41.Location = New System.Drawing.Point(862, 348)
        Me.lblLOTcode41.Name = "lblLOTcode41"
        Me.lblLOTcode41.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode41.TabIndex = 465
        Me.lblLOTcode41.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode36
        '
        Me.lblFamilycode36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode36.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode36.Location = New System.Drawing.Point(927, 250)
        Me.lblFamilycode36.Name = "lblFamilycode36"
        Me.lblFamilycode36.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode36.TabIndex = 465
        Me.lblFamilycode36.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode27
        '
        Me.lblLOTcode27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode27.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode27.Location = New System.Drawing.Point(862, 72)
        Me.lblLOTcode27.Name = "lblLOTcode27"
        Me.lblLOTcode27.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode27.TabIndex = 466
        Me.lblLOTcode27.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode41
        '
        Me.lblFamilycode41.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode41.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode41.Location = New System.Drawing.Point(927, 348)
        Me.lblFamilycode41.Name = "lblFamilycode41"
        Me.lblFamilycode41.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode41.TabIndex = 465
        Me.lblFamilycode41.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode32
        '
        Me.lblLOTcode32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode32.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode32.Location = New System.Drawing.Point(862, 170)
        Me.lblLOTcode32.Name = "lblLOTcode32"
        Me.lblLOTcode32.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode32.TabIndex = 466
        Me.lblLOTcode32.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode37
        '
        Me.lblLOTcode37.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode37.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode37.Location = New System.Drawing.Point(862, 268)
        Me.lblLOTcode37.Name = "lblLOTcode37"
        Me.lblLOTcode37.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode37.TabIndex = 466
        Me.lblLOTcode37.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode27
        '
        Me.lblFamilycode27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode27.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode27.Location = New System.Drawing.Point(927, 72)
        Me.lblFamilycode27.Name = "lblFamilycode27"
        Me.lblFamilycode27.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode27.TabIndex = 466
        Me.lblFamilycode27.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode42
        '
        Me.lblLOTcode42.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode42.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode42.Location = New System.Drawing.Point(862, 366)
        Me.lblLOTcode42.Name = "lblLOTcode42"
        Me.lblLOTcode42.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode42.TabIndex = 466
        Me.lblLOTcode42.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode32
        '
        Me.lblFamilycode32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode32.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode32.Location = New System.Drawing.Point(927, 170)
        Me.lblFamilycode32.Name = "lblFamilycode32"
        Me.lblFamilycode32.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode32.TabIndex = 466
        Me.lblFamilycode32.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode37
        '
        Me.lblFamilycode37.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode37.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode37.Location = New System.Drawing.Point(927, 268)
        Me.lblFamilycode37.Name = "lblFamilycode37"
        Me.lblFamilycode37.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode37.TabIndex = 466
        Me.lblFamilycode37.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode42
        '
        Me.lblFamilycode42.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode42.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode42.Location = New System.Drawing.Point(927, 366)
        Me.lblFamilycode42.Name = "lblFamilycode42"
        Me.lblFamilycode42.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode42.TabIndex = 466
        Me.lblFamilycode42.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode28
        '
        Me.lblLOTcode28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode28.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode28.Location = New System.Drawing.Point(862, 90)
        Me.lblLOTcode28.Name = "lblLOTcode28"
        Me.lblLOTcode28.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode28.TabIndex = 467
        Me.lblLOTcode28.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode33
        '
        Me.lblLOTcode33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode33.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode33.Location = New System.Drawing.Point(862, 188)
        Me.lblLOTcode33.Name = "lblLOTcode33"
        Me.lblLOTcode33.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode33.TabIndex = 467
        Me.lblLOTcode33.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode38
        '
        Me.lblLOTcode38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode38.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode38.Location = New System.Drawing.Point(862, 286)
        Me.lblLOTcode38.Name = "lblLOTcode38"
        Me.lblLOTcode38.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode38.TabIndex = 467
        Me.lblLOTcode38.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode43
        '
        Me.lblLOTcode43.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode43.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode43.Location = New System.Drawing.Point(862, 384)
        Me.lblLOTcode43.Name = "lblLOTcode43"
        Me.lblLOTcode43.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode43.TabIndex = 467
        Me.lblLOTcode43.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode28
        '
        Me.lblFamilycode28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode28.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode28.Location = New System.Drawing.Point(927, 90)
        Me.lblFamilycode28.Name = "lblFamilycode28"
        Me.lblFamilycode28.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode28.TabIndex = 467
        Me.lblFamilycode28.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode33
        '
        Me.lblFamilycode33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode33.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode33.Location = New System.Drawing.Point(927, 188)
        Me.lblFamilycode33.Name = "lblFamilycode33"
        Me.lblFamilycode33.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode33.TabIndex = 467
        Me.lblFamilycode33.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode38
        '
        Me.lblFamilycode38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode38.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode38.Location = New System.Drawing.Point(927, 286)
        Me.lblFamilycode38.Name = "lblFamilycode38"
        Me.lblFamilycode38.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode38.TabIndex = 467
        Me.lblFamilycode38.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode43
        '
        Me.lblFamilycode43.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode43.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode43.Location = New System.Drawing.Point(927, 384)
        Me.lblFamilycode43.Name = "lblFamilycode43"
        Me.lblFamilycode43.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode43.TabIndex = 467
        Me.lblFamilycode43.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode29
        '
        Me.lblLOTcode29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode29.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode29.Location = New System.Drawing.Point(862, 108)
        Me.lblLOTcode29.Name = "lblLOTcode29"
        Me.lblLOTcode29.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode29.TabIndex = 468
        Me.lblLOTcode29.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode34
        '
        Me.lblLOTcode34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode34.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode34.Location = New System.Drawing.Point(862, 206)
        Me.lblLOTcode34.Name = "lblLOTcode34"
        Me.lblLOTcode34.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode34.TabIndex = 468
        Me.lblLOTcode34.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode39
        '
        Me.lblLOTcode39.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode39.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode39.Location = New System.Drawing.Point(862, 304)
        Me.lblLOTcode39.Name = "lblLOTcode39"
        Me.lblLOTcode39.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode39.TabIndex = 468
        Me.lblLOTcode39.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblLOTcode44
        '
        Me.lblLOTcode44.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLOTcode44.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblLOTcode44.Location = New System.Drawing.Point(862, 402)
        Me.lblLOTcode44.Name = "lblLOTcode44"
        Me.lblLOTcode44.Size = New System.Drawing.Size(65, 18)
        Me.lblLOTcode44.TabIndex = 468
        Me.lblLOTcode44.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode29
        '
        Me.lblFamilycode29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode29.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode29.Location = New System.Drawing.Point(927, 108)
        Me.lblFamilycode29.Name = "lblFamilycode29"
        Me.lblFamilycode29.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode29.TabIndex = 468
        Me.lblFamilycode29.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode34
        '
        Me.lblFamilycode34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode34.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode34.Location = New System.Drawing.Point(927, 206)
        Me.lblFamilycode34.Name = "lblFamilycode34"
        Me.lblFamilycode34.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode34.TabIndex = 468
        Me.lblFamilycode34.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode39
        '
        Me.lblFamilycode39.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode39.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode39.Location = New System.Drawing.Point(927, 304)
        Me.lblFamilycode39.Name = "lblFamilycode39"
        Me.lblFamilycode39.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode39.TabIndex = 468
        Me.lblFamilycode39.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblFamilycode44
        '
        Me.lblFamilycode44.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFamilycode44.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblFamilycode44.Location = New System.Drawing.Point(927, 402)
        Me.lblFamilycode44.Name = "lblFamilycode44"
        Me.lblFamilycode44.Size = New System.Drawing.Size(65, 18)
        Me.lblFamilycode44.TabIndex = 468
        Me.lblFamilycode44.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'frmDataDisp
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1005, 476)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblAssyNo41)
        Me.Controls.Add(Me.lblAssyNo42)
        Me.Controls.Add(Me.lblAssyNo43)
        Me.Controls.Add(Me.lblAssyNo44)
        Me.Controls.Add(Me.lblAssyNo37)
        Me.Controls.Add(Me.lblAssyNo38)
        Me.Controls.Add(Me.lblAssyNo39)
        Me.Controls.Add(Me.lblAssyNo40)
        Me.Controls.Add(Me.lblAssyNo33)
        Me.Controls.Add(Me.lblAssyNo34)
        Me.Controls.Add(Me.lblAssyNo35)
        Me.Controls.Add(Me.lblAssyNo36)
        Me.Controls.Add(Me.lblAssyNo29)
        Me.Controls.Add(Me.lblAssyNo30)
        Me.Controls.Add(Me.lblAssyNo31)
        Me.Controls.Add(Me.lblAssyNo32)
        Me.Controls.Add(Me.lblAssyNo25)
        Me.Controls.Add(Me.lblAssyNo26)
        Me.Controls.Add(Me.lblAssyNo27)
        Me.Controls.Add(Me.lblAssyNo28)
        Me.Controls.Add(Me.lblAssyNo21)
        Me.Controls.Add(Me.lblAssyNo22)
        Me.Controls.Add(Me.lblAssyNo23)
        Me.Controls.Add(Me.lblAssyNo24)
        Me.Controls.Add(Me.lblAssyNo17)
        Me.Controls.Add(Me.lblAssyNo18)
        Me.Controls.Add(Me.lblAssyNo19)
        Me.Controls.Add(Me.lblAssyNo20)
        Me.Controls.Add(Me.lblAssyNo13)
        Me.Controls.Add(Me.lblAssyNo14)
        Me.Controls.Add(Me.lblAssyNo15)
        Me.Controls.Add(Me.lblAssyNo16)
        Me.Controls.Add(Me.lblAssyNo09)
        Me.Controls.Add(Me.lblAssyNo10)
        Me.Controls.Add(Me.lblAssyNo11)
        Me.Controls.Add(Me.lblAssyNo12)
        Me.Controls.Add(Me.lblAssyNo05)
        Me.Controls.Add(Me.lblAssyNo06)
        Me.Controls.Add(Me.lblAssyNo07)
        Me.Controls.Add(Me.lblAssyNo08)
        Me.Controls.Add(Me.lblAssyNoName01)
        Me.Controls.Add(Me.lblFamilycode44)
        Me.Controls.Add(Me.lblFamilycode24)
        Me.Controls.Add(Me.lblFamilycode39)
        Me.Controls.Add(Me.lblFamilycode19)
        Me.Controls.Add(Me.lblFamilycode34)
        Me.Controls.Add(Me.lblFamilycode14)
        Me.Controls.Add(Me.lblFamilycode29)
        Me.Controls.Add(Me.lblFamilycode09)
        Me.Controls.Add(Me.lblFamilycode04)
        Me.Controls.Add(Me.lblLOTcode44)
        Me.Controls.Add(Me.lblLOTcode24)
        Me.Controls.Add(Me.lblLOTcode39)
        Me.Controls.Add(Me.lblLOTcode19)
        Me.Controls.Add(Me.lblLOTcode34)
        Me.Controls.Add(Me.lblLOTcode14)
        Me.Controls.Add(Me.lblLOTcode29)
        Me.Controls.Add(Me.lblLOTcode09)
        Me.Controls.Add(Me.lblLOTcode04)
        Me.Controls.Add(Me.lblFamilycode43)
        Me.Controls.Add(Me.lblFamilycode23)
        Me.Controls.Add(Me.lblFamilycode38)
        Me.Controls.Add(Me.lblFamilycode18)
        Me.Controls.Add(Me.lblFamilycode33)
        Me.Controls.Add(Me.lblFamilycode13)
        Me.Controls.Add(Me.lblAssyNo04)
        Me.Controls.Add(Me.lblFamilycode28)
        Me.Controls.Add(Me.lblFamilycode08)
        Me.Controls.Add(Me.lblLOTcode43)
        Me.Controls.Add(Me.lblLOTcode23)
        Me.Controls.Add(Me.lblLOTcode38)
        Me.Controls.Add(Me.lblLOTcode18)
        Me.Controls.Add(Me.lblLOTcode33)
        Me.Controls.Add(Me.lblLOTcode13)
        Me.Controls.Add(Me.lblFamilycode03)
        Me.Controls.Add(Me.lblLOTcode28)
        Me.Controls.Add(Me.lblLOTcode08)
        Me.Controls.Add(Me.lblFamilycode42)
        Me.Controls.Add(Me.lblFamilycode22)
        Me.Controls.Add(Me.lblLOTcode03)
        Me.Controls.Add(Me.lblFamilycode37)
        Me.Controls.Add(Me.lblFamilycode17)
        Me.Controls.Add(Me.lblFamilycode32)
        Me.Controls.Add(Me.lblFamilycode12)
        Me.Controls.Add(Me.lblAssyNo03)
        Me.Controls.Add(Me.lblLOTcode42)
        Me.Controls.Add(Me.lblLOTcode22)
        Me.Controls.Add(Me.lblFamilycode27)
        Me.Controls.Add(Me.lblFamilycode07)
        Me.Controls.Add(Me.lblLOTcode37)
        Me.Controls.Add(Me.lblLOTcode17)
        Me.Controls.Add(Me.lblLOTcode32)
        Me.Controls.Add(Me.lblLOTcode12)
        Me.Controls.Add(Me.lblFamilycode02)
        Me.Controls.Add(Me.lblFamilycode41)
        Me.Controls.Add(Me.lblFamilycode21)
        Me.Controls.Add(Me.lblLOTcode27)
        Me.Controls.Add(Me.lblLOTcode07)
        Me.Controls.Add(Me.lblFamilycode36)
        Me.Controls.Add(Me.lblFamilycode16)
        Me.Controls.Add(Me.lblLOTcode02)
        Me.Controls.Add(Me.lblLOTcode41)
        Me.Controls.Add(Me.lblLOTcode21)
        Me.Controls.Add(Me.lblFamilycode31)
        Me.Controls.Add(Me.lblFamilycode11)
        Me.Controls.Add(Me.lblLOTcode36)
        Me.Controls.Add(Me.lblLOTcode16)
        Me.Controls.Add(Me.lblAssyNo02)
        Me.Controls.Add(Me.lblLOTcode31)
        Me.Controls.Add(Me.lblLOTcode11)
        Me.Controls.Add(Me.lblFamilycode26)
        Me.Controls.Add(Me.lblFamilycode06)
        Me.Controls.Add(Me.lblLOTcode26)
        Me.Controls.Add(Me.lblLOTcode06)
        Me.Controls.Add(Me.lblFamilycode01)
        Me.Controls.Add(Me.lblLOTcode01)
        Me.Controls.Add(Me.lblAssyNo01)
        Me.Controls.Add(Me.lblVehicle44)
        Me.Controls.Add(Me.lblVehicle43)
        Me.Controls.Add(Me.lblVehicle42)
        Me.Controls.Add(Me.lblVehicle41)
        Me.Controls.Add(Me.lblVehicle40)
        Me.Controls.Add(Me.lblBcNo44)
        Me.Controls.Add(Me.lblBcNo43)
        Me.Controls.Add(Me.lblBcNo42)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.lblBcNo41)
        Me.Controls.Add(Me.lblFamilycode40)
        Me.Controls.Add(Me.lblFamilycode20)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.lblFamilycode35)
        Me.Controls.Add(Me.lblFamilycode15)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.lblFamilycode30)
        Me.Controls.Add(Me.lblFamilycode10)
        Me.Controls.Add(Me.lblBcNo40)
        Me.Controls.Add(Me.lblLOTcode40)
        Me.Controls.Add(Me.lblLOTcode20)
        Me.Controls.Add(Me.lblFamilycode25)
        Me.Controls.Add(Me.lblLOTcode35)
        Me.Controls.Add(Me.lblFamilycode05)
        Me.Controls.Add(Me.lblLOTcode15)
        Me.Controls.Add(Me.lblLOTcode30)
        Me.Controls.Add(Me.lblLOTcodeName00)
        Me.Controls.Add(Me.lblLOTcode10)
        Me.Controls.Add(Me.lblLOTcode25)
        Me.Controls.Add(Me.lblFamilycode00)
        Me.Controls.Add(Me.lblLOTcode05)
        Me.Controls.Add(Me.lblAssyNoName02)
        Me.Controls.Add(Me.lblLOTcode00)
        Me.Controls.Add(Me.lblAssyNoName00)
        Me.Controls.Add(Me.lblAssyNo00)
        Me.Controls.Add(Me.btnRefresh)
        Me.Controls.Add(Me.btnAllDelete)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.chkDelFlg39)
        Me.Controls.Add(Me.chkDelFlg38)
        Me.Controls.Add(Me.chkDelFlg37)
        Me.Controls.Add(Me.chkDelFlg36)
        Me.Controls.Add(Me.chkDelFlg35)
        Me.Controls.Add(Me.chkDelFlg34)
        Me.Controls.Add(Me.chkDelFlg33)
        Me.Controls.Add(Me.chkDelFlg32)
        Me.Controls.Add(Me.chkDelFlg31)
        Me.Controls.Add(Me.chkDelFlg30)
        Me.Controls.Add(Me.chkDelFlg29)
        Me.Controls.Add(Me.chkDelFlg28)
        Me.Controls.Add(Me.chkDelFlg27)
        Me.Controls.Add(Me.chkDelFlg26)
        Me.Controls.Add(Me.chkDelFlg25)
        Me.Controls.Add(Me.chkDelFlg24)
        Me.Controls.Add(Me.chkDelFlg23)
        Me.Controls.Add(Me.chkDelFlg22)
        Me.Controls.Add(Me.chkDelFlg21)
        Me.Controls.Add(Me.chkDelFlg20)
        Me.Controls.Add(Me.lblVehicle39)
        Me.Controls.Add(Me.lblVehicle38)
        Me.Controls.Add(Me.lblVehicle37)
        Me.Controls.Add(Me.lblVehicle36)
        Me.Controls.Add(Me.lblVehicle35)
        Me.Controls.Add(Me.lblBcNo39)
        Me.Controls.Add(Me.lblBcNo38)
        Me.Controls.Add(Me.lblBcNo37)
        Me.Controls.Add(Me.lblBcNo36)
        Me.Controls.Add(Me.lblBcNo35)
        Me.Controls.Add(Me.lblVehicle34)
        Me.Controls.Add(Me.lblVehicle33)
        Me.Controls.Add(Me.lblVehicle32)
        Me.Controls.Add(Me.lblVehicle31)
        Me.Controls.Add(Me.lblVehicle30)
        Me.Controls.Add(Me.lblBcNo34)
        Me.Controls.Add(Me.lblBcNo33)
        Me.Controls.Add(Me.lblBcNo32)
        Me.Controls.Add(Me.lblBcNo31)
        Me.Controls.Add(Me.lblBcNo30)
        Me.Controls.Add(Me.lblVehicle29)
        Me.Controls.Add(Me.lblVehicle28)
        Me.Controls.Add(Me.lblVehicle27)
        Me.Controls.Add(Me.lblVehicle26)
        Me.Controls.Add(Me.lblVehicle25)
        Me.Controls.Add(Me.lblBcNo29)
        Me.Controls.Add(Me.lblBcNo28)
        Me.Controls.Add(Me.lblBcNo27)
        Me.Controls.Add(Me.lblBcNo26)
        Me.Controls.Add(Me.lblBcNo25)
        Me.Controls.Add(Me.lblVehicleName02)
        Me.Controls.Add(Me.lblVehicle24)
        Me.Controls.Add(Me.lblVehicle23)
        Me.Controls.Add(Me.lblVehicle22)
        Me.Controls.Add(Me.lblVehicle21)
        Me.Controls.Add(Me.lblVehicle20)
        Me.Controls.Add(Me.lblBcNoName02)
        Me.Controls.Add(Me.lblBcNo24)
        Me.Controls.Add(Me.lblBcNo23)
        Me.Controls.Add(Me.lblBcNo22)
        Me.Controls.Add(Me.lblBcNo21)
        Me.Controls.Add(Me.lblBcNo20)
        Me.Controls.Add(Me.lblNo44)
        Me.Controls.Add(Me.lblNo43)
        Me.Controls.Add(Me.lblNo42)
        Me.Controls.Add(Me.lblNo41)
        Me.Controls.Add(Me.lblNo40)
        Me.Controls.Add(Me.lblNo39)
        Me.Controls.Add(Me.lblNo38)
        Me.Controls.Add(Me.lblNo37)
        Me.Controls.Add(Me.lblNo36)
        Me.Controls.Add(Me.lblNo35)
        Me.Controls.Add(Me.lblNo34)
        Me.Controls.Add(Me.lblNo33)
        Me.Controls.Add(Me.lblNo32)
        Me.Controls.Add(Me.lblNo31)
        Me.Controls.Add(Me.lblNo30)
        Me.Controls.Add(Me.lblNoName02)
        Me.Controls.Add(Me.lblNo29)
        Me.Controls.Add(Me.lblNo28)
        Me.Controls.Add(Me.lblNo27)
        Me.Controls.Add(Me.lblNo26)
        Me.Controls.Add(Me.lblNo25)
        Me.Controls.Add(Me.chkDelFlg19)
        Me.Controls.Add(Me.chkDelFlg18)
        Me.Controls.Add(Me.chkDelFlg17)
        Me.Controls.Add(Me.chkDelFlg16)
        Me.Controls.Add(Me.chkDelFlg15)
        Me.Controls.Add(Me.chkDelFlg14)
        Me.Controls.Add(Me.chkDelFlg13)
        Me.Controls.Add(Me.chkDelFlg12)
        Me.Controls.Add(Me.chkDelFlg11)
        Me.Controls.Add(Me.chkDelFlg10)
        Me.Controls.Add(Me.chkDelFlg09)
        Me.Controls.Add(Me.chkDelFlg08)
        Me.Controls.Add(Me.chkDelFlg07)
        Me.Controls.Add(Me.chkDelFlg06)
        Me.Controls.Add(Me.chkDelFlg05)
        Me.Controls.Add(Me.chkDelFlg04)
        Me.Controls.Add(Me.chkDelFlg03)
        Me.Controls.Add(Me.chkDelFlg02)
        Me.Controls.Add(Me.chkDelFlg01)
        Me.Controls.Add(Me.chkDelFlg00)
        Me.Controls.Add(Me.lblVehicle19)
        Me.Controls.Add(Me.lblVehicle18)
        Me.Controls.Add(Me.lblVehicle17)
        Me.Controls.Add(Me.lblVehicle16)
        Me.Controls.Add(Me.lblVehicle15)
        Me.Controls.Add(Me.lblBcNo19)
        Me.Controls.Add(Me.lblBcNo18)
        Me.Controls.Add(Me.lblBcNo17)
        Me.Controls.Add(Me.lblBcNo16)
        Me.Controls.Add(Me.lblBcNo15)
        Me.Controls.Add(Me.lblVehicle14)
        Me.Controls.Add(Me.lblVehicle13)
        Me.Controls.Add(Me.lblVehicle12)
        Me.Controls.Add(Me.lblVehicle11)
        Me.Controls.Add(Me.lblVehicle10)
        Me.Controls.Add(Me.lblBcNo14)
        Me.Controls.Add(Me.lblBcNo13)
        Me.Controls.Add(Me.lblBcNo12)
        Me.Controls.Add(Me.lblBcNo11)
        Me.Controls.Add(Me.lblBcNo10)
        Me.Controls.Add(Me.lblVehicle09)
        Me.Controls.Add(Me.lblVehicle08)
        Me.Controls.Add(Me.lblVehicle07)
        Me.Controls.Add(Me.lblVehicle06)
        Me.Controls.Add(Me.lblVehicle05)
        Me.Controls.Add(Me.lblBcNo09)
        Me.Controls.Add(Me.lblBcNo08)
        Me.Controls.Add(Me.lblBcNo07)
        Me.Controls.Add(Me.lblBcNo06)
        Me.Controls.Add(Me.lblBcNo05)
        Me.Controls.Add(Me.lblVehicleName01)
        Me.Controls.Add(Me.lblVehicle04)
        Me.Controls.Add(Me.lblVehicle03)
        Me.Controls.Add(Me.lblVehicle02)
        Me.Controls.Add(Me.lblVehicle01)
        Me.Controls.Add(Me.lblVehicle00)
        Me.Controls.Add(Me.lblBcNoName01)
        Me.Controls.Add(Me.lblBcNo04)
        Me.Controls.Add(Me.lblBcNo03)
        Me.Controls.Add(Me.lblBcNo02)
        Me.Controls.Add(Me.lblBcNo01)
        Me.Controls.Add(Me.lblBcNo00)
        Me.Controls.Add(Me.lblNo24)
        Me.Controls.Add(Me.lblNo23)
        Me.Controls.Add(Me.lblNo22)
        Me.Controls.Add(Me.lblNo21)
        Me.Controls.Add(Me.lblNo20)
        Me.Controls.Add(Me.lblNo19)
        Me.Controls.Add(Me.lblNo18)
        Me.Controls.Add(Me.lblNo17)
        Me.Controls.Add(Me.lblNo16)
        Me.Controls.Add(Me.lblNo15)
        Me.Controls.Add(Me.lblNo14)
        Me.Controls.Add(Me.lblNo13)
        Me.Controls.Add(Me.lblNo12)
        Me.Controls.Add(Me.lblNo11)
        Me.Controls.Add(Me.lblNo10)
        Me.Controls.Add(Me.lblNoName01)
        Me.Controls.Add(Me.lblNo09)
        Me.Controls.Add(Me.lblNo08)
        Me.Controls.Add(Me.lblNo07)
        Me.Controls.Add(Me.lblNo06)
        Me.Controls.Add(Me.lblNo05)
        Me.Controls.Add(Me.lblLeft)
        Me.Controls.Add(Me.lblVehicleName00)
        Me.Controls.Add(Me.lblBcNoName00)
        Me.Controls.Add(Me.lblNoName00)
        Me.Controls.Add(Me.lblNo04)
        Me.Controls.Add(Me.lblNo03)
        Me.Controls.Add(Me.lblNo02)
        Me.Controls.Add(Me.lblNo01)
        Me.Controls.Add(Me.lblNo00)
        Me.Controls.Add(Me.btnClose)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDataDisp"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Data Display"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents lblNoName00 As System.Windows.Forms.Label
    Friend WithEvents lblNo04 As System.Windows.Forms.Label
    Friend WithEvents lblNo03 As System.Windows.Forms.Label
    Friend WithEvents lblNo02 As System.Windows.Forms.Label
    Friend WithEvents lblNo01 As System.Windows.Forms.Label
    Friend WithEvents lblNo00 As System.Windows.Forms.Label
    Friend WithEvents lblBcNoName00 As System.Windows.Forms.Label
    Friend WithEvents lblVehicleName00 As System.Windows.Forms.Label
    Friend WithEvents lblLeft As System.Windows.Forms.Label
    Friend WithEvents lblNoName01 As System.Windows.Forms.Label
    Friend WithEvents lblNo09 As System.Windows.Forms.Label
    Friend WithEvents lblNo08 As System.Windows.Forms.Label
    Friend WithEvents lblNo07 As System.Windows.Forms.Label
    Friend WithEvents lblNo06 As System.Windows.Forms.Label
    Friend WithEvents lblNo05 As System.Windows.Forms.Label
    Friend WithEvents lblNo14 As System.Windows.Forms.Label
    Friend WithEvents lblNo13 As System.Windows.Forms.Label
    Friend WithEvents lblNo12 As System.Windows.Forms.Label
    Friend WithEvents lblNo11 As System.Windows.Forms.Label
    Friend WithEvents lblNo10 As System.Windows.Forms.Label
    Friend WithEvents lblNo19 As System.Windows.Forms.Label
    Friend WithEvents lblNo18 As System.Windows.Forms.Label
    Friend WithEvents lblNo17 As System.Windows.Forms.Label
    Friend WithEvents lblNo16 As System.Windows.Forms.Label
    Friend WithEvents lblNo15 As System.Windows.Forms.Label
    Friend WithEvents lblNo24 As System.Windows.Forms.Label
    Friend WithEvents lblNo23 As System.Windows.Forms.Label
    Friend WithEvents lblNo22 As System.Windows.Forms.Label
    Friend WithEvents lblNo21 As System.Windows.Forms.Label
    Friend WithEvents lblNo20 As System.Windows.Forms.Label
    Friend WithEvents lblVehicleName01 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle04 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle03 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle02 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle01 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle00 As System.Windows.Forms.Label
    Friend WithEvents lblBcNoName01 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo04 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo03 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo02 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo01 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo00 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle09 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle08 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle07 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle06 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle05 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo09 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo08 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo07 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo06 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo05 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle19 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle18 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle17 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle16 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle15 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo19 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo18 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo17 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo16 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo15 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle14 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle13 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle12 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle11 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle10 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo14 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo13 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo12 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo11 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo10 As System.Windows.Forms.Label
    Friend WithEvents chkDelFlg00 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg01 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg03 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg02 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg07 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg06 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg05 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg04 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg14 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg13 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg12 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg11 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg10 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg09 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg08 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg19 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg18 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg17 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg16 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg15 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg39 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg38 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg37 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg36 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg35 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg34 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg33 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg32 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg31 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg30 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg29 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg28 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg27 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg26 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg25 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg24 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg23 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg22 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg21 As System.Windows.Forms.CheckBox
    Friend WithEvents chkDelFlg20 As System.Windows.Forms.CheckBox
    Friend WithEvents lblVehicle39 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle38 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle37 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle36 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle35 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo39 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo38 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo37 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo36 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo35 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle34 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle33 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle32 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle31 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle30 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo34 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo33 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo32 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo31 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo30 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle29 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle28 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle27 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle26 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle25 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo29 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo28 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo27 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo26 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo25 As System.Windows.Forms.Label
    Friend WithEvents lblVehicleName02 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle24 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle23 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle22 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle21 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle20 As System.Windows.Forms.Label
    Friend WithEvents lblBcNoName02 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo24 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo23 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo22 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo21 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo20 As System.Windows.Forms.Label
    Friend WithEvents lblNo44 As System.Windows.Forms.Label
    Friend WithEvents lblNo43 As System.Windows.Forms.Label
    Friend WithEvents lblNo42 As System.Windows.Forms.Label
    Friend WithEvents lblNo41 As System.Windows.Forms.Label
    Friend WithEvents lblNo40 As System.Windows.Forms.Label
    Friend WithEvents lblNo39 As System.Windows.Forms.Label
    Friend WithEvents lblNo38 As System.Windows.Forms.Label
    Friend WithEvents lblNo37 As System.Windows.Forms.Label
    Friend WithEvents lblNo36 As System.Windows.Forms.Label
    Friend WithEvents lblNo35 As System.Windows.Forms.Label
    Friend WithEvents lblNo34 As System.Windows.Forms.Label
    Friend WithEvents lblNo33 As System.Windows.Forms.Label
    Friend WithEvents lblNo32 As System.Windows.Forms.Label
    Friend WithEvents lblNo31 As System.Windows.Forms.Label
    Friend WithEvents lblNo30 As System.Windows.Forms.Label
    Friend WithEvents lblNoName02 As System.Windows.Forms.Label
    Friend WithEvents lblNo29 As System.Windows.Forms.Label
    Friend WithEvents lblNo28 As System.Windows.Forms.Label
    Friend WithEvents lblNo27 As System.Windows.Forms.Label
    Friend WithEvents lblNo26 As System.Windows.Forms.Label
    Friend WithEvents lblNo25 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnAllDelete As System.Windows.Forms.Button
    Friend WithEvents btnRefresh As System.Windows.Forms.Button
    Friend WithEvents lblAssyNoName00 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo00 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNoName02 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle44 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle43 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle42 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle41 As System.Windows.Forms.Label
    Friend WithEvents lblVehicle40 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo44 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo43 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo42 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo41 As System.Windows.Forms.Label
    Friend WithEvents lblBcNo40 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo01 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo02 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo03 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo04 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNoName01 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo05 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo06 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo07 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo08 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo09 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo10 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo11 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo12 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo13 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo14 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo15 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo16 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo17 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo18 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo19 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo20 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo21 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo22 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo23 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo24 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo25 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo26 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo27 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo28 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo29 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo30 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo31 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo32 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo33 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo34 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo35 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo36 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo37 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo38 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo39 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo40 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo41 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo42 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo43 As System.Windows.Forms.Label
    Friend WithEvents lblAssyNo44 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode00 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcodeName00 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode01 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode02 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode03 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode04 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode00 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode01 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode02 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode03 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode04 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode05 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode05 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode06 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode06 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode07 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode07 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode08 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode08 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode09 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode09 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode10 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode10 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode11 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode11 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode12 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode12 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode13 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode13 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode14 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode14 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode15 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode15 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode16 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode16 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode17 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode17 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode18 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode18 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode19 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode19 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode20 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode20 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode21 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode21 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode22 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode22 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode23 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode23 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode24 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode24 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode25 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode30 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode35 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode25 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode40 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode30 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode35 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode40 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode26 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode26 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode31 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode36 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode31 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode41 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode36 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode27 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode41 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode32 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode37 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode27 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode42 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode32 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode37 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode42 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode28 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode33 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode38 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode43 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode28 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode33 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode38 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode43 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode29 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode34 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode39 As System.Windows.Forms.Label
    Friend WithEvents lblLOTcode44 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode29 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode34 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode39 As System.Windows.Forms.Label
    Friend WithEvents lblFamilycode44 As System.Windows.Forms.Label

End Class
