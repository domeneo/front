﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMenu
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナで必要です。
    'Windows フォーム デザイナを使用して変更できます。  
    'コード エディタを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim PictureBox1 As System.Windows.Forms.PictureBox
        Me.BottomToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.TopToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.RightToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.LeftToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.ContentPanel = New System.Windows.Forms.ToolStripContentPanel()
        Me.grpGalc = New System.Windows.Forms.GroupBox()
        Me.btnGalcDisconnect = New System.Windows.Forms.Button()
        Me.btnGalcConnect = New System.Windows.Forms.Button()
        Me.lblGalcStatus = New System.Windows.Forms.Label()
        Me.btnGalcDisconnect_kako = New System.Windows.Forms.Button()
        Me.btnGalcConnect_kako = New System.Windows.Forms.Button()
        Me.grpPlc = New System.Windows.Forms.GroupBox()
        Me.btnPlcDisconnect = New System.Windows.Forms.Button()
        Me.btnPlcConnect = New System.Windows.Forms.Button()
        Me.lblPlcStatus = New System.Windows.Forms.Label()
        Me.grpBCNo = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtLastSEQ = New System.Windows.Forms.TextBox()
        Me.lblBcRecv = New System.Windows.Forms.Label()
        Me.lblNextRecvName = New System.Windows.Forms.Label()
        Me.lblRecvName = New System.Windows.Forms.Label()
        Me.txtBcNextRecv = New System.Windows.Forms.TextBox()
        Me.btnRecvDataDisp = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.ToolStripContainer1 = New System.Windows.Forms.ToolStripContainer()
        Me.btnSequence = New System.Windows.Forms.Button()
        Me.stsMessage = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.bgwGalcConnect = New System.ComponentModel.BackgroundWorker()
        Me.bgwPlcConnect = New System.ComponentModel.BackgroundWorker()
        Me.bgwVehicleCheck = New System.ComponentModel.BackgroundWorker()
        Me.tmrAutoConnect = New System.Windows.Forms.Timer(Me.components)
        Me.Timer1s = New System.Windows.Forms.Timer(Me.components)
        PictureBox1 = New System.Windows.Forms.PictureBox()
        CType(PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpGalc.SuspendLayout()
        Me.grpPlc.SuspendLayout()
        Me.grpBCNo.SuspendLayout()
        Me.ToolStripContainer1.ContentPanel.SuspendLayout()
        Me.ToolStripContainer1.SuspendLayout()
        Me.stsMessage.SuspendLayout()
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        PictureBox1.Image = Global._800L_G_ALC.My.Resources.Resources.fukaden03
        PictureBox1.Location = New System.Drawing.Point(160, 286)
        PictureBox1.Name = "PictureBox1"
        PictureBox1.Size = New System.Drawing.Size(139, 38)
        PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        PictureBox1.TabIndex = 7
        PictureBox1.TabStop = False
        '
        'BottomToolStripPanel
        '
        Me.BottomToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.BottomToolStripPanel.Name = "BottomToolStripPanel"
        Me.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.BottomToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.BottomToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'TopToolStripPanel
        '
        Me.TopToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.TopToolStripPanel.Name = "TopToolStripPanel"
        Me.TopToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.TopToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.TopToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'RightToolStripPanel
        '
        Me.RightToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.RightToolStripPanel.Name = "RightToolStripPanel"
        Me.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.RightToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.RightToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'LeftToolStripPanel
        '
        Me.LeftToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.LeftToolStripPanel.Name = "LeftToolStripPanel"
        Me.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.LeftToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.LeftToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'ContentPanel
        '
        Me.ContentPanel.AutoScroll = True
        Me.ContentPanel.Size = New System.Drawing.Size(427, 291)
        '
        'grpGalc
        '
        Me.grpGalc.Controls.Add(Me.btnGalcDisconnect)
        Me.grpGalc.Controls.Add(Me.btnGalcConnect)
        Me.grpGalc.Controls.Add(Me.lblGalcStatus)
        Me.grpGalc.Location = New System.Drawing.Point(12, 15)
        Me.grpGalc.Name = "grpGalc"
        Me.grpGalc.Size = New System.Drawing.Size(139, 241)
        Me.grpGalc.TabIndex = 0
        Me.grpGalc.TabStop = False
        Me.grpGalc.Text = "G-ALC→PC"
        '
        'btnGalcDisconnect
        '
        Me.btnGalcDisconnect.Enabled = False
        Me.btnGalcDisconnect.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnGalcDisconnect.Location = New System.Drawing.Point(16, 165)
        Me.btnGalcDisconnect.Name = "btnGalcDisconnect"
        Me.btnGalcDisconnect.Size = New System.Drawing.Size(112, 43)
        Me.btnGalcDisconnect.TabIndex = 4
        Me.btnGalcDisconnect.Text = "Disconnect"
        Me.btnGalcDisconnect.UseVisualStyleBackColor = True
        '
        'btnGalcConnect
        '
        Me.btnGalcConnect.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnGalcConnect.Location = New System.Drawing.Point(16, 104)
        Me.btnGalcConnect.Name = "btnGalcConnect"
        Me.btnGalcConnect.Size = New System.Drawing.Size(112, 43)
        Me.btnGalcConnect.TabIndex = 3
        Me.btnGalcConnect.Text = "Connect"
        Me.btnGalcConnect.UseVisualStyleBackColor = True
        '
        'lblGalcStatus
        '
        Me.lblGalcStatus.BackColor = System.Drawing.Color.Red
        Me.lblGalcStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblGalcStatus.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblGalcStatus.Location = New System.Drawing.Point(14, 41)
        Me.lblGalcStatus.Name = "lblGalcStatus"
        Me.lblGalcStatus.Size = New System.Drawing.Size(111, 38)
        Me.lblGalcStatus.TabIndex = 0
        Me.lblGalcStatus.Text = "Disconnect"
        Me.lblGalcStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnGalcDisconnect_kako
        '
        Me.btnGalcDisconnect_kako.AutoSize = True
        Me.btnGalcDisconnect_kako.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnGalcDisconnect_kako.Location = New System.Drawing.Point(136, 312)
        Me.btnGalcDisconnect_kako.Name = "btnGalcDisconnect_kako"
        Me.btnGalcDisconnect_kako.Size = New System.Drawing.Size(92, 35)
        Me.btnGalcDisconnect_kako.TabIndex = 2
        Me.btnGalcDisconnect_kako.Text = "Disconnect"
        Me.btnGalcDisconnect_kako.UseVisualStyleBackColor = True
        Me.btnGalcDisconnect_kako.Visible = False
        '
        'btnGalcConnect_kako
        '
        Me.btnGalcConnect_kako.AutoSize = True
        Me.btnGalcConnect_kako.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnGalcConnect_kako.Enabled = False
        Me.btnGalcConnect_kako.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnGalcConnect_kako.Location = New System.Drawing.Point(136, 260)
        Me.btnGalcConnect_kako.Name = "btnGalcConnect_kako"
        Me.btnGalcConnect_kako.Size = New System.Drawing.Size(91, 38)
        Me.btnGalcConnect_kako.TabIndex = 1
        Me.btnGalcConnect_kako.Text = "Connect"
        Me.btnGalcConnect_kako.UseVisualStyleBackColor = True
        Me.btnGalcConnect_kako.Visible = False
        '
        'grpPlc
        '
        Me.grpPlc.Controls.Add(Me.btnPlcDisconnect)
        Me.grpPlc.Controls.Add(Me.btnPlcConnect)
        Me.grpPlc.Controls.Add(Me.lblPlcStatus)
        Me.grpPlc.Location = New System.Drawing.Point(158, 15)
        Me.grpPlc.Name = "grpPlc"
        Me.grpPlc.Size = New System.Drawing.Size(139, 241)
        Me.grpPlc.TabIndex = 1
        Me.grpPlc.TabStop = False
        Me.grpPlc.Text = "PC→PIS"
        '
        'btnPlcDisconnect
        '
        Me.btnPlcDisconnect.AutoSize = True
        Me.btnPlcDisconnect.Enabled = False
        Me.btnPlcDisconnect.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnPlcDisconnect.Location = New System.Drawing.Point(16, 165)
        Me.btnPlcDisconnect.Name = "btnPlcDisconnect"
        Me.btnPlcDisconnect.Size = New System.Drawing.Size(110, 41)
        Me.btnPlcDisconnect.TabIndex = 4
        Me.btnPlcDisconnect.Text = "Disconnect"
        Me.btnPlcDisconnect.UseVisualStyleBackColor = True
        '
        'btnPlcConnect
        '
        Me.btnPlcConnect.AutoSize = True
        Me.btnPlcConnect.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnPlcConnect.Location = New System.Drawing.Point(16, 104)
        Me.btnPlcConnect.Name = "btnPlcConnect"
        Me.btnPlcConnect.Size = New System.Drawing.Size(112, 43)
        Me.btnPlcConnect.TabIndex = 3
        Me.btnPlcConnect.Text = "Connect"
        Me.btnPlcConnect.UseVisualStyleBackColor = True
        '
        'lblPlcStatus
        '
        Me.lblPlcStatus.BackColor = System.Drawing.Color.Red
        Me.lblPlcStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblPlcStatus.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblPlcStatus.Location = New System.Drawing.Point(14, 41)
        Me.lblPlcStatus.Name = "lblPlcStatus"
        Me.lblPlcStatus.Size = New System.Drawing.Size(111, 38)
        Me.lblPlcStatus.TabIndex = 0
        Me.lblPlcStatus.Text = "Disconnect"
        Me.lblPlcStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'grpBCNo
        '
        Me.grpBCNo.Controls.Add(Me.Label1)
        Me.grpBCNo.Controls.Add(Me.txtLastSEQ)
        Me.grpBCNo.Controls.Add(Me.lblBcRecv)
        Me.grpBCNo.Controls.Add(Me.lblNextRecvName)
        Me.grpBCNo.Controls.Add(Me.lblRecvName)
        Me.grpBCNo.Controls.Add(Me.txtBcNextRecv)
        Me.grpBCNo.Location = New System.Drawing.Point(304, 15)
        Me.grpBCNo.Name = "grpBCNo"
        Me.grpBCNo.Size = New System.Drawing.Size(139, 241)
        Me.grpBCNo.TabIndex = 2
        Me.grpBCNo.TabStop = False
        Me.grpBCNo.Text = "BC No"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 199)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 13)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Last Data"
        '
        'txtLastSEQ
        '
        Me.txtLastSEQ.Location = New System.Drawing.Point(16, 215)
        Me.txtLastSEQ.MaxLength = 3
        Me.txtLastSEQ.Name = "txtLastSEQ"
        Me.txtLastSEQ.Size = New System.Drawing.Size(111, 20)
        Me.txtLastSEQ.TabIndex = 6
        Me.txtLastSEQ.Text = "999"
        Me.txtLastSEQ.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblBcRecv
        '
        Me.lblBcRecv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblBcRecv.Location = New System.Drawing.Point(16, 78)
        Me.lblBcRecv.Name = "lblBcRecv"
        Me.lblBcRecv.Size = New System.Drawing.Size(111, 21)
        Me.lblBcRecv.TabIndex = 4
        Me.lblBcRecv.Text = "999"
        Me.lblBcRecv.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblNextRecvName
        '
        Me.lblNextRecvName.AutoSize = True
        Me.lblNextRecvName.Location = New System.Drawing.Point(9, 143)
        Me.lblNextRecvName.Name = "lblNextRecvName"
        Me.lblNextRecvName.Size = New System.Drawing.Size(63, 13)
        Me.lblNextRecvName.TabIndex = 3
        Me.lblNextRecvName.Text = "Next BC No"
        '
        'lblRecvName
        '
        Me.lblRecvName.AutoSize = True
        Me.lblRecvName.Location = New System.Drawing.Point(9, 52)
        Me.lblRecvName.Name = "lblRecvName"
        Me.lblRecvName.Size = New System.Drawing.Size(87, 13)
        Me.lblRecvName.TabIndex = 2
        Me.lblRecvName.Text = "Received BC No"
        '
        'txtBcNextRecv
        '
        Me.txtBcNextRecv.Location = New System.Drawing.Point(16, 165)
        Me.txtBcNextRecv.MaxLength = 3
        Me.txtBcNextRecv.Name = "txtBcNextRecv"
        Me.txtBcNextRecv.Size = New System.Drawing.Size(111, 20)
        Me.txtBcNextRecv.TabIndex = 5
        Me.txtBcNextRecv.Text = "999"
        Me.txtBcNextRecv.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnRecvDataDisp
        '
        Me.btnRecvDataDisp.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnRecvDataDisp.Location = New System.Drawing.Point(12, 262)
        Me.btnRecvDataDisp.Name = "btnRecvDataDisp"
        Me.btnRecvDataDisp.Size = New System.Drawing.Size(128, 38)
        Me.btnRecvDataDisp.TabIndex = 6
        Me.btnRecvDataDisp.Text = "Data Display"
        Me.btnRecvDataDisp.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnClose.Location = New System.Drawing.Point(312, 286)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(131, 38)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "Close(&X)"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'ToolStripContainer1
        '
        Me.ToolStripContainer1.BottomToolStripPanelVisible = False
        '
        'ToolStripContainer1.ContentPanel
        '
        Me.ToolStripContainer1.ContentPanel.AutoScroll = True
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.btnSequence)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(PictureBox1)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.stsMessage)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.btnGalcConnect_kako)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.btnGalcDisconnect_kako)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.btnClose)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.btnRecvDataDisp)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.grpBCNo)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.grpPlc)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.grpGalc)
        Me.ToolStripContainer1.ContentPanel.Size = New System.Drawing.Size(457, 374)
        Me.ToolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ToolStripContainer1.LeftToolStripPanelVisible = False
        Me.ToolStripContainer1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStripContainer1.Name = "ToolStripContainer1"
        Me.ToolStripContainer1.RightToolStripPanelVisible = False
        Me.ToolStripContainer1.Size = New System.Drawing.Size(457, 374)
        Me.ToolStripContainer1.TabIndex = 7
        Me.ToolStripContainer1.Text = "ToolStripContainer1"
        Me.ToolStripContainer1.TopToolStripPanelVisible = False
        '
        'btnSequence
        '
        Me.btnSequence.BackColor = System.Drawing.Color.SkyBlue
        Me.btnSequence.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSequence.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnSequence.Location = New System.Drawing.Point(12, 306)
        Me.btnSequence.Name = "btnSequence"
        Me.btnSequence.Size = New System.Drawing.Size(128, 38)
        Me.btnSequence.TabIndex = 8
        Me.btnSequence.Text = "Sequence"
        Me.btnSequence.UseVisualStyleBackColor = False
        '
        'stsMessage
        '
        Me.stsMessage.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.stsMessage.Location = New System.Drawing.Point(0, 352)
        Me.stsMessage.Name = "stsMessage"
        Me.stsMessage.Size = New System.Drawing.Size(457, 22)
        Me.stsMessage.SizingGrip = False
        Me.stsMessage.TabIndex = 6
        Me.stsMessage.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.AutoSize = False
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(425, 17)
        Me.ToolStripStatusLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'bgwGalcConnect
        '
        Me.bgwGalcConnect.WorkerReportsProgress = True
        Me.bgwGalcConnect.WorkerSupportsCancellation = True
        '
        'bgwPlcConnect
        '
        Me.bgwPlcConnect.WorkerReportsProgress = True
        Me.bgwPlcConnect.WorkerSupportsCancellation = True
        '
        'bgwVehicleCheck
        '
        Me.bgwVehicleCheck.WorkerReportsProgress = True
        Me.bgwVehicleCheck.WorkerSupportsCancellation = True
        '
        'tmrAutoConnect
        '
        Me.tmrAutoConnect.Enabled = True
        Me.tmrAutoConnect.Interval = 10000
        '
        'Timer1s
        '
        Me.Timer1s.Enabled = True
        Me.Timer1s.Interval = 1000
        '
        'frmMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(457, 374)
        Me.ControlBox = False
        Me.Controls.Add(Me.ToolStripContainer1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMenu"
        Me.Text = "G-ALC System 30/10/2018"
        CType(PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpGalc.ResumeLayout(False)
        Me.grpPlc.ResumeLayout(False)
        Me.grpPlc.PerformLayout()
        Me.grpBCNo.ResumeLayout(False)
        Me.grpBCNo.PerformLayout()
        Me.ToolStripContainer1.ContentPanel.ResumeLayout(False)
        Me.ToolStripContainer1.ContentPanel.PerformLayout()
        Me.ToolStripContainer1.ResumeLayout(False)
        Me.ToolStripContainer1.PerformLayout()
        Me.stsMessage.ResumeLayout(False)
        Me.stsMessage.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BottomToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents TopToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents RightToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents LeftToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents ContentPanel As System.Windows.Forms.ToolStripContentPanel
    Friend WithEvents grpGalc As System.Windows.Forms.GroupBox
    Friend WithEvents btnGalcDisconnect_kako As System.Windows.Forms.Button
    Friend WithEvents btnGalcConnect_kako As System.Windows.Forms.Button
    Friend WithEvents lblGalcStatus As System.Windows.Forms.Label
    Friend WithEvents grpPlc As System.Windows.Forms.GroupBox
    Friend WithEvents btnPlcDisconnect As System.Windows.Forms.Button
    Friend WithEvents lblPlcStatus As System.Windows.Forms.Label
    Friend WithEvents grpBCNo As System.Windows.Forms.GroupBox
    Friend WithEvents lblBcRecv As System.Windows.Forms.Label
    Friend WithEvents lblNextRecvName As System.Windows.Forms.Label
    Friend WithEvents lblRecvName As System.Windows.Forms.Label
    Friend WithEvents txtBcNextRecv As System.Windows.Forms.TextBox
    Friend WithEvents btnRecvDataDisp As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents ToolStripContainer1 As System.Windows.Forms.ToolStripContainer
    Friend WithEvents stsMessage As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Public WithEvents bgwGalcConnect As System.ComponentModel.BackgroundWorker
    Public WithEvents bgwPlcConnect As System.ComponentModel.BackgroundWorker
    Public WithEvents bgwVehicleCheck As System.ComponentModel.BackgroundWorker
    Friend WithEvents btnGalcDisconnect As System.Windows.Forms.Button
    Friend WithEvents btnGalcConnect As System.Windows.Forms.Button
    Friend WithEvents tmrAutoConnect As System.Windows.Forms.Timer
    Friend WithEvents btnPlcConnect As System.Windows.Forms.Button
    Friend WithEvents Timer1s As System.Windows.Forms.Timer
    Friend WithEvents btnSequence As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents txtLastSEQ As TextBox
End Class
