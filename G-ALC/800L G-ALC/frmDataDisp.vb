﻿Public Class frmDataDisp

    Delegate Sub SetTextCallback(ByVal strText As String, _
                 ByRef lblLabel As Label(), ByVal intArray As Integer)
    Delegate Sub SetCheckCallback(ByVal intCheckBoxState As Integer, _
                 ByRef chkCheckBox As CheckBox(), ByVal intArray As Integer)

    Public lblNoName(3) As Label
    Public lblBcNoName(3) As Label
    Public lblVehicleName(3) As Label
    Public lblAssyNoName(3) As Label
    Public lblNo(44) As Label
    Public lblBcNo(44) As Label
    Public lblVehicle(44) As Label
    Public lblAssyNo(44) As Label
    Public lblLOTcode(44) As Label      '2012/5/22
    Public lblFamilycode(44) As Label   '2012/5/22
    Public chkDelFlg(40) As CheckBox

    Public Sub New()

        ' この呼び出しは、Windows フォーム デザイナで必要です。
        InitializeComponent()

        ' InitializeComponent() 呼び出しの後で初期化を追加します。

        lblNoName(0) = lblNoName00
        lblNoName(1) = lblNoName01
        lblNoName(2) = lblNoName02

        lblBcNoName(0) = lblBcNoName00
        lblBcNoName(1) = lblBcNoName01
        lblBcNoName(2) = lblBcNoName02

        lblVehicleName(0) = lblVehicleName00
        lblVehicleName(1) = lblVehicleName01
        lblVehicleName(2) = lblVehicleName02

        lblAssyNoName(0) = lblAssyNoName00
        lblAssyNoName(1) = lblAssyNoName01
        lblAssyNoName(2) = lblAssyNoName02

        lblNo(0) = lblNo00
        lblNo(1) = lblNo01
        lblNo(2) = lblNo02
        lblNo(3) = lblNo03
        lblNo(4) = lblNo04
        lblNo(5) = lblNo05
        lblNo(6) = lblNo06
        lblNo(7) = lblNo07
        lblNo(8) = lblNo08
        lblNo(9) = lblNo09
        lblNo(10) = lblNo10
        lblNo(11) = lblNo11
        lblNo(12) = lblNo12
        lblNo(13) = lblNo13
        lblNo(14) = lblNo14
        lblNo(15) = lblNo15
        lblNo(16) = lblNo16
        lblNo(17) = lblNo17
        lblNo(18) = lblNo18
        lblNo(19) = lblNo19
        lblNo(20) = lblNo20
        lblNo(21) = lblNo21
        lblNo(22) = lblNo22
        lblNo(23) = lblNo23
        lblNo(24) = lblNo24
        lblNo(25) = lblNo25
        lblNo(26) = lblNo26
        lblNo(27) = lblNo27
        lblNo(28) = lblNo28
        lblNo(29) = lblNo29
        lblNo(30) = lblNo30
        lblNo(31) = lblNo31
        lblNo(32) = lblNo32
        lblNo(33) = lblNo33
        lblNo(34) = lblNo34
        lblNo(35) = lblNo35
        lblNo(36) = lblNo36
        lblNo(37) = lblNo37
        lblNo(38) = lblNo38
        lblNo(39) = lblNo39
        lblNo(40) = lblNo40
        lblNo(41) = lblNo41
        lblNo(42) = lblNo42
        lblNo(43) = lblNo43
        lblNo(44) = lblNo44

        lblBcNo(0) = lblBcNo00
        lblBcNo(1) = lblBcNo01
        lblBcNo(2) = lblBcNo02
        lblBcNo(3) = lblBcNo03
        lblBcNo(4) = lblBcNo04
        lblBcNo(5) = lblBcNo05
        lblBcNo(6) = lblBcNo06
        lblBcNo(7) = lblBcNo07
        lblBcNo(8) = lblBcNo08
        lblBcNo(9) = lblBcNo09
        lblBcNo(10) = lblBcNo10
        lblBcNo(11) = lblBcNo11
        lblBcNo(12) = lblBcNo12
        lblBcNo(13) = lblBcNo13
        lblBcNo(14) = lblBcNo14
        lblBcNo(15) = lblBcNo15
        lblBcNo(16) = lblBcNo16
        lblBcNo(17) = lblBcNo17
        lblBcNo(18) = lblBcNo18
        lblBcNo(19) = lblBcNo19
        lblBcNo(20) = lblBcNo20
        lblBcNo(21) = lblBcNo21
        lblBcNo(22) = lblBcNo22
        lblBcNo(23) = lblBcNo23
        lblBcNo(24) = lblBcNo24
        lblBcNo(25) = lblBcNo25
        lblBcNo(26) = lblBcNo26
        lblBcNo(27) = lblBcNo27
        lblBcNo(28) = lblBcNo28
        lblBcNo(29) = lblBcNo29
        lblBcNo(30) = lblBcNo30
        lblBcNo(31) = lblBcNo31
        lblBcNo(32) = lblBcNo32
        lblBcNo(33) = lblBcNo33
        lblBcNo(34) = lblBcNo34
        lblBcNo(35) = lblBcNo35
        lblBcNo(36) = lblBcNo36
        lblBcNo(37) = lblBcNo37
        lblBcNo(38) = lblBcNo38
        lblBcNo(39) = lblBcNo39
        lblBcNo(40) = lblBcNo40
        lblBcNo(41) = lblBcNo41
        lblBcNo(42) = lblBcNo42
        lblBcNo(43) = lblBcNo43
        lblBcNo(44) = lblBcNo44

        lblVehicle(0) = lblVehicle00
        lblVehicle(1) = lblVehicle01
        lblVehicle(2) = lblVehicle02
        lblVehicle(3) = lblVehicle03
        lblVehicle(4) = lblVehicle04
        lblVehicle(5) = lblVehicle05
        lblVehicle(6) = lblVehicle06
        lblVehicle(7) = lblVehicle07
        lblVehicle(8) = lblVehicle08
        lblVehicle(9) = lblVehicle09
        lblVehicle(10) = lblVehicle10
        lblVehicle(11) = lblVehicle11
        lblVehicle(12) = lblVehicle12
        lblVehicle(13) = lblVehicle13
        lblVehicle(14) = lblVehicle14
        lblVehicle(15) = lblVehicle15
        lblVehicle(16) = lblVehicle16
        lblVehicle(17) = lblVehicle17
        lblVehicle(18) = lblVehicle18
        lblVehicle(19) = lblVehicle19
        lblVehicle(20) = lblVehicle20
        lblVehicle(21) = lblVehicle21
        lblVehicle(22) = lblVehicle22
        lblVehicle(23) = lblVehicle23
        lblVehicle(24) = lblVehicle24
        lblVehicle(25) = lblVehicle25
        lblVehicle(26) = lblVehicle26
        lblVehicle(27) = lblVehicle27
        lblVehicle(28) = lblVehicle28
        lblVehicle(29) = lblVehicle29
        lblVehicle(30) = lblVehicle30
        lblVehicle(31) = lblVehicle31
        lblVehicle(32) = lblVehicle32
        lblVehicle(33) = lblVehicle33
        lblVehicle(34) = lblVehicle34
        lblVehicle(35) = lblVehicle35
        lblVehicle(36) = lblVehicle36
        lblVehicle(37) = lblVehicle37
        lblVehicle(38) = lblVehicle38
        lblVehicle(39) = lblVehicle39
        lblVehicle(40) = lblVehicle40
        lblVehicle(41) = lblVehicle41
        lblVehicle(42) = lblVehicle42
        lblVehicle(43) = lblVehicle43
        lblVehicle(44) = lblVehicle44

        lblAssyNo(0) = lblAssyNo00
        lblAssyNo(1) = lblAssyNo01
        lblAssyNo(2) = lblAssyNo02
        lblAssyNo(3) = lblAssyNo03
        lblAssyNo(4) = lblAssyNo04
        lblAssyNo(5) = lblAssyNo05
        lblAssyNo(6) = lblAssyNo06
        lblAssyNo(7) = lblAssyNo07
        lblAssyNo(8) = lblAssyNo08
        lblAssyNo(9) = lblAssyNo09
        lblAssyNo(10) = lblAssyNo10
        lblAssyNo(11) = lblAssyNo11
        lblAssyNo(12) = lblAssyNo12
        lblAssyNo(13) = lblAssyNo13
        lblAssyNo(14) = lblAssyNo14
        lblAssyNo(15) = lblAssyNo15
        lblAssyNo(16) = lblAssyNo16
        lblAssyNo(17) = lblAssyNo17
        lblAssyNo(18) = lblAssyNo18
        lblAssyNo(19) = lblAssyNo19
        lblAssyNo(20) = lblAssyNo20
        lblAssyNo(21) = lblAssyNo21
        lblAssyNo(22) = lblAssyNo22
        lblAssyNo(23) = lblAssyNo23
        lblAssyNo(24) = lblAssyNo24
        lblAssyNo(25) = lblAssyNo25
        lblAssyNo(26) = lblAssyNo26
        lblAssyNo(27) = lblAssyNo27
        lblAssyNo(28) = lblAssyNo28
        lblAssyNo(29) = lblAssyNo29
        lblAssyNo(30) = lblAssyNo30
        lblAssyNo(31) = lblAssyNo31
        lblAssyNo(32) = lblAssyNo32
        lblAssyNo(33) = lblAssyNo33
        lblAssyNo(34) = lblAssyNo34
        lblAssyNo(35) = lblAssyNo35
        lblAssyNo(36) = lblAssyNo36
        lblAssyNo(37) = lblAssyNo37
        lblAssyNo(38) = lblAssyNo38
        lblAssyNo(39) = lblAssyNo39
        lblAssyNo(40) = lblAssyNo40
        lblAssyNo(41) = lblAssyNo41
        lblAssyNo(42) = lblAssyNo42
        lblAssyNo(43) = lblAssyNo43
        lblAssyNo(44) = lblAssyNo44

        'ﾛｯﾄｺｰﾄﾞ（'2012/5/22）
        lblLOTcode(0) = lblLOTcode00
        lblLOTcode(1) = lblLOTcode01
        lblLOTcode(2) = lblLOTcode02
        lblLOTcode(3) = lblLOTcode03
        lblLOTcode(4) = lblLOTcode04
        lblLOTcode(5) = lblLOTcode05
        lblLOTcode(6) = lblLOTcode06
        lblLOTcode(7) = lblLOTcode07
        lblLOTcode(8) = lblLOTcode08
        lblLOTcode(9) = lblLOTcode09
        lblLOTcode(10) = lblLOTcode10
        lblLOTcode(11) = lblLOTcode11
        lblLOTcode(12) = lblLOTcode12
        lblLOTcode(13) = lblLOTcode13
        lblLOTcode(14) = lblLOTcode14
        lblLOTcode(15) = lblLOTcode15
        lblLOTcode(16) = lblLOTcode16
        lblLOTcode(17) = lblLOTcode17
        lblLOTcode(18) = lblLOTcode18
        lblLOTcode(19) = lblLOTcode19
        lblLOTcode(20) = lblLOTcode20
        lblLOTcode(21) = lblLOTcode21
        lblLOTcode(22) = lblLOTcode22
        lblLOTcode(23) = lblLOTcode23
        lblLOTcode(24) = lblLOTcode24
        lblLOTcode(25) = lblLOTcode25
        lblLOTcode(26) = lblLOTcode26
        lblLOTcode(27) = lblLOTcode27
        lblLOTcode(28) = lblLOTcode28
        lblLOTcode(29) = lblLOTcode29
        lblLOTcode(30) = lblLOTcode30
        lblLOTcode(31) = lblLOTcode31
        lblLOTcode(32) = lblLOTcode32
        lblLOTcode(33) = lblLOTcode33
        lblLOTcode(34) = lblLOTcode34
        lblLOTcode(35) = lblLOTcode35
        lblLOTcode(36) = lblLOTcode36
        lblLOTcode(37) = lblLOTcode37
        lblLOTcode(38) = lblLOTcode38
        lblLOTcode(39) = lblLOTcode39
        lblLOTcode(40) = lblLOTcode40
        lblLOTcode(41) = lblLOTcode41
        lblLOTcode(42) = lblLOTcode42
        lblLOTcode(43) = lblLOTcode43
        lblLOTcode(44) = lblLOTcode44

        'ﾌｧﾐﾘｰｺｰﾄﾞ（'2012/5/22）
        lblFamilycode(0) = lblFamilycode00
        lblFamilycode(1) = lblFamilycode01
        lblFamilycode(2) = lblFamilycode02
        lblFamilycode(3) = lblFamilycode03
        lblFamilycode(4) = lblFamilycode04
        lblFamilycode(5) = lblFamilycode05
        lblFamilycode(6) = lblFamilycode06
        lblFamilycode(7) = lblFamilycode07
        lblFamilycode(8) = lblFamilycode08
        lblFamilycode(9) = lblFamilycode09
        lblFamilycode(10) = lblFamilycode10
        lblFamilycode(11) = lblFamilycode11
        lblFamilycode(12) = lblFamilycode12
        lblFamilycode(13) = lblFamilycode13
        lblFamilycode(14) = lblFamilycode14
        lblFamilycode(15) = lblFamilycode15
        lblFamilycode(16) = lblFamilycode16
        lblFamilycode(17) = lblFamilycode17
        lblFamilycode(18) = lblFamilycode18
        lblFamilycode(19) = lblFamilycode19
        lblFamilycode(20) = lblFamilycode20
        lblFamilycode(21) = lblFamilycode21
        lblFamilycode(22) = lblFamilycode22
        lblFamilycode(23) = lblFamilycode23
        lblFamilycode(24) = lblFamilycode24
        lblFamilycode(25) = lblFamilycode25
        lblFamilycode(26) = lblFamilycode26
        lblFamilycode(27) = lblFamilycode27
        lblFamilycode(28) = lblFamilycode28
        lblFamilycode(29) = lblFamilycode29
        lblFamilycode(30) = lblFamilycode30
        lblFamilycode(31) = lblFamilycode31
        lblFamilycode(32) = lblFamilycode32
        lblFamilycode(33) = lblFamilycode33
        lblFamilycode(34) = lblFamilycode34
        lblFamilycode(35) = lblFamilycode35
        lblFamilycode(36) = lblFamilycode36
        lblFamilycode(37) = lblFamilycode37
        lblFamilycode(38) = lblFamilycode38
        lblFamilycode(39) = lblFamilycode39
        lblFamilycode(40) = lblFamilycode40
        lblFamilycode(41) = lblFamilycode41
        lblFamilycode(42) = lblFamilycode42
        lblFamilycode(43) = lblFamilycode43
        lblFamilycode(44) = lblFamilycode44

        chkDelFlg(0) = chkDelFlg00
        chkDelFlg(1) = chkDelFlg01
        chkDelFlg(2) = chkDelFlg02
        chkDelFlg(3) = chkDelFlg03
        chkDelFlg(4) = chkDelFlg04
        chkDelFlg(5) = chkDelFlg05
        chkDelFlg(6) = chkDelFlg06
        chkDelFlg(7) = chkDelFlg07
        chkDelFlg(8) = chkDelFlg08
        chkDelFlg(9) = chkDelFlg09
        chkDelFlg(10) = chkDelFlg10
        chkDelFlg(11) = chkDelFlg11
        chkDelFlg(12) = chkDelFlg12
        chkDelFlg(13) = chkDelFlg13
        chkDelFlg(14) = chkDelFlg14
        chkDelFlg(15) = chkDelFlg15
        chkDelFlg(16) = chkDelFlg16
        chkDelFlg(17) = chkDelFlg17
        chkDelFlg(18) = chkDelFlg18
        chkDelFlg(19) = chkDelFlg19
        chkDelFlg(20) = chkDelFlg20
        chkDelFlg(21) = chkDelFlg21
        chkDelFlg(22) = chkDelFlg22
        chkDelFlg(23) = chkDelFlg23
        chkDelFlg(24) = chkDelFlg24
        chkDelFlg(25) = chkDelFlg25
        chkDelFlg(26) = chkDelFlg26
        chkDelFlg(27) = chkDelFlg27
        chkDelFlg(28) = chkDelFlg28
        chkDelFlg(29) = chkDelFlg29
        chkDelFlg(30) = chkDelFlg30
        chkDelFlg(31) = chkDelFlg31
        chkDelFlg(32) = chkDelFlg32
        chkDelFlg(33) = chkDelFlg33
        chkDelFlg(34) = chkDelFlg34
        chkDelFlg(35) = chkDelFlg35
        chkDelFlg(36) = chkDelFlg36
        chkDelFlg(37) = chkDelFlg37
        chkDelFlg(38) = chkDelFlg38
        chkDelFlg(39) = chkDelFlg39

    End Sub

    Private Sub frmDataDisp_Load(ByVal sender As System.Object, _
                ByVal e As System.EventArgs) Handles MyBase.Load

        Call psubdispRefresh()

    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, _
                ByVal e As System.EventArgs) Handles btnDelete.Click

        Dim intCount As Integer

        If MsgBox("Data is deleted." + vbCrLf + "Is it OK?", _
                  MsgBoxStyle.OkCancel) = MsgBoxResult.Cancel Then
            'If MsgBox("Posso apagar os dados?", _
            '          MsgBoxStyle.OkCancel) = MsgBoxResult.Cancel Then      'ﾎﾟﾙﾄｶﾞﾙ対応
            Exit Sub
        End If

        For intCount = 0 To 39 Step 1
            If chkDelFlg(intCount).CheckState = CheckState.Checked Then
                If pstAlcData.Length > intCount + 1 Then
                    Call pClsSql.fDeleteAlcData( _
                         pstAlcData(intCount + 1).id.ToString)
                End If
            End If
        Next intCount
        Call psubdispRefresh()

    End Sub

    Private Sub btnAllDelete_Click(ByVal sender As System.Object, _
                ByVal e As System.EventArgs) Handles btnAllDelete.Click

        If MsgBox("All data is deleted." + vbCrLf + "Is it OK?", _
                  MsgBoxStyle.OkCancel) = MsgBoxResult.Cancel Then
            'If MsgBox("Posso apagar todos os dados?", _
            '          MsgBoxStyle.OkCancel) = MsgBoxResult.Cancel Then      'ﾎﾟﾙﾄｶﾞﾙ対応         
            Exit Sub
        End If

        Call pClsSql.fDeleteAlcData("ALL")
        Call psubdispRefresh()

    End Sub

    Private Sub btnRefresh_Click(ByVal sender As System.Object, _
                ByVal e As System.EventArgs) Handles btnRefresh.Click

        btnRefresh.Enabled = False
        Call psubdispRefresh()
        btnRefresh.Enabled = True

    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, _
                ByVal e As System.EventArgs) Handles btnClose.Click

        'If MsgBox("Is this screen shut?", MsgBoxStyle.OkCancel) = _
        '                                               MsgBoxResult.Cancel Then
        '    Exit Sub
        'End If

        '表示している画面を終了
        Me.Hide()

    End Sub

    Public Sub psubdispRefresh()

        Dim intCount As Integer     'ﾗﾍﾞﾙ配列のｲﾝﾃﾞｯｸｽ(0～44)
        Dim intDataCount As Integer 'ALC受信ﾃﾞｰﾀのｲﾝﾃﾞｯｸｽ(1～…)

        If pClsSql.fGetAlcDataDisp() <> pCSuccess Then 'ALC受信ﾃﾞｰﾀ取り込み
            Exit Sub
        End If

        For intCount = 0 To 44 Step 1
            '表示枠初期化
            Call psublblRefresh("", lblBcNo, intCount)          'BC連番
            Call psublblRefresh("", lblVehicle, intCount)       '車種名
            Call psublblRefresh("", lblAssyNo, intCount)        '車種番号
            Call psublblRefresh("", lblLOTcode, intCount)       'ﾛｯﾄｺｰﾄﾞ（2012/5/22）
            Call psublblRefresh("", lblFamilycode, intCount)    'ﾌｧﾐﾘｰｺｰﾄﾞ（2012/5/22）
            If intCount < 40 Then
                '削除CheckBoxをOFF
                Call psubchkRefresh(CheckState.Unchecked, chkDelFlg, intCount)
            End If
        Next intCount
        '次回送信予定ﾃﾞｰﾀ
        If pintAlcDataCount > 0 Then
            'BC連番
            Call psublblRefresh(pstAlcData(0).strBCNo, lblBcNo, 0)
            '車種名
            Call psublblRefresh(pstAlcData(0).strVehicleName, lblVehicle, 0)
            '車種番号
            Call psublblRefresh(pstAlcData(0).strAssyNo, lblAssyNo, 0)
            'ﾛｯﾄｺｰﾄﾞ
            Call psublblRefresh(pstAlcData(0).strLOTcode, lblLOTcode, 0)        '2012/5/22
            'ﾌｧﾐﾘｰｺｰﾄﾞ
            Call psublblRefresh(pstAlcData(0).strFamilycode, lblFamilycode, 0)  '2012/5/22
        End If
        'それ以降の受信ﾃﾞｰﾀ
        For intCount = 5 To 44 Step 1
            '受信ﾃﾞｰﾀをDBから読みだす
            intDataCount = intCount - 4
            If intDataCount < pintAlcDataCount Then
                Call psublblRefresh(pstAlcData(intDataCount).strBCNo, lblBcNo, intCount)                'BC連番
                If pstAlcData(intDataCount).intDelFlg = 1 Then
                    Call psubchkRefresh(CheckState.Checked, chkDelFlg, intCount)    '削除CheckBox
                End If
                Call psublblRefresh(pstAlcData(intDataCount).strVehicleName, lblVehicle, intCount)      '車種名
                Call psublblRefresh(pstAlcData(intDataCount).strAssyNo, lblAssyNo, intCount)            '車種番号
                Call psublblRefresh(pstAlcData(intDataCount).strLOTcode, lblLOTcode, intCount)          'ﾛｯﾄｺｰﾄﾞ（2012/5/22）
                Call psublblRefresh(pstAlcData(intDataCount).strFamilycode, lblFamilycode, intCount)    'ﾌｧﾐﾘｰｺｰﾄﾞ（2012/5/22）
            Else
                Exit For
            End If
        Next intCount
        Me.Refresh()
    End Sub
    Public Sub psubAlcdispRefresh()

        Dim intCount As Integer     'ﾗﾍﾞﾙ配列のｲﾝﾃﾞｯｸｽ(0～44)
        Dim intDataCount As Integer 'ALC受信ﾃﾞｰﾀのｲﾝﾃﾞｯｸｽ(1～…)

        '        If pClsSql.fGetAlcDataDisp() <> pCSuccess Then 'ALC受信ﾃﾞｰﾀ取り込み
        If pClsSql.fGetAlcLoopDataDisp() <> pCSuccess Then 'ALC受信ﾃﾞｰﾀ取り込み
            Exit Sub
        End If

        For intCount = 0 To 44 Step 1
            '表示枠初期化
            lblBcNo(intCount).Text = ""
            lblVehicle(intCount).Text = ""
            lblAssyNo(intCount).Text = ""
            'If intCount < 40 Then
            '    '削除CheckBoxをOFF
            '    Call psubchkRefresh(CheckState.Unchecked, chkDelFlg, intCount)
            'End If
        Next intCount
        '次回送信予定ﾃﾞｰﾀ
        If pintAlcDataCount > 0 Then
            'BC連番
            lblBcNo(0).Text = pstAlcLoopData(0).strBCNo
            '車種名
            lblVehicle(0).Text = pstAlcLoopData(0).strVehicleName
            '車種番号
            lblAssyNo(0).Text = pstAlcLoopData(0).strAssyNo
            'ﾛｯﾄｺｰﾄﾞ
            lblLOTcode(0).Text = pstAlcLoopData(0).strLOTcode
            'ﾌｧﾐﾘｰｺｰﾄﾞ
            lblFamilycode(0).Text = pstAlcLoopData(0).strFamilycode
        End If
        'それ以降の受信ﾃﾞｰﾀ

        pintAlcLoopDataCount = pstAlcLoopData.Count
        For intCount = 5 To 44 Step 1
            '受信ﾃﾞｰﾀをDBから読みだす
            intDataCount = intCount - 4
            If intDataCount < pintAlcLoopDataCount Then

                lblBcNo(intCount).Text = pstAlcLoopData(intDataCount).strBCNo
                'If pstAlcData(intDataCount).intDelFlg = 1 Then
                '    Call psubchkRefresh(CheckState.Checked, chkDelFlg, _
                '                        intCount)  '削除CheckBox
                'End If
                lblVehicle(intCount).Text = pstAlcLoopData(intDataCount).strVehicleName
                lblAssyNo(intCount).Text = pstAlcLoopData(intDataCount).strAssyNo
                lblLOTcode(intCount).Text = pstAlcLoopData(intDataCount).strLOTcode         'ﾛｯﾄｺｰﾄﾞ（2012/6/7）
                lblFamilycode(intCount).Text = pstAlcLoopData(intDataCount).strFamilycode   'ﾌｧﾐﾘｰｺｰﾄﾞ（2012/6/7）
            Else
                Exit For
            End If
        Next intCount
        Me.Refresh()
    End Sub

    'BackGroundWorks動作時に違うｽﾚｯﾄﾞよりﾗﾍﾞﾙのﾃｷｽﾄを変更する為のﾌﾟﾛｼｰｼﾞｬ
    '<引数>
    'strText…表示する文字列
    'lblLabel…変更するﾗﾍﾞﾙｵﾌﾞｼﾞｪｸﾄ
    'intArray…変更するﾗﾍﾞﾙの配列ｲﾝﾃﾞｯｸｽ
    Private Sub psublblRefresh(ByVal strText As String, _
                ByRef lblLabel As Label(), ByVal intArray As Integer)

        If lblLabel(intArray).InvokeRequired Then
            'ﾃﾞﾘｹﾞｰﾄ用変数定義
            Dim d As New SetTextCallback(AddressOf psublblRefresh)
            Me.Invoke(d, New Object() {strText})    'ﾃﾞﾘｹﾞｰﾄ実行
        Else                                'BackGroundWorksが動作してない時
            lblLabel(intArray).Text = strText
            'Me.Refresh()
        End If

    End Sub

    'BackGroundWorks動作時に違うｽﾚｯﾄﾞよりﾁｪｯｸﾎﾞｯｸｽのﾁｪｯｸｽﾃｰﾄを変更する為のﾌﾟﾛｼｰｼﾞｬ
    '<引数>
    'intCheckBoxState…ﾁｪｯｸ有:CheckState.Checked,ﾁｪｯｸ無:CheckState.Unchecked
    'chkCheckBox…変更するﾁｪｯｸﾎﾞｯｸｽｵﾌﾞｼﾞｪｸﾄ
    'intArray…変更するﾁｪｯｸﾎﾞｯｸｽの配列ｲﾝﾃﾞｯｸｽ
    Private Sub psubchkRefresh(ByVal intCheckBoxState As Integer, _
                ByRef chkCheckBox As CheckBox(), ByVal intArray As Integer)

        If chkCheckBox(intArray).InvokeRequired Then
            'ﾃﾞﾘｹﾞｰﾄ用変数定義
            Dim d As New SetCheckCallback(AddressOf psubchkRefresh)
            Me.Invoke(d, New Object() {intCheckBoxState})   'ﾃﾞﾘｹﾞｰﾄ実行
        Else                                'BackGroundWorksが動作してない時
            chkCheckBox(intArray).CheckState = intCheckBoxState
            'Me.Refresh()
        End If

    End Sub

    Private Sub chkDelFlg13_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDelFlg13.CheckedChanged

    End Sub
End Class