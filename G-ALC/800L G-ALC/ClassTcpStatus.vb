﻿Imports System.Runtime.InteropServices
Imports System.Net
Imports System.Net.Sockets

Public Class ClassTcpStatus
    Public Enum TCP_STATUS As Short
        MIB_TCP_STATE_ERROR = -1
        MIB_TCP_STATE_CLOSED = 1
        MIB_TCP_STATE_LISTEN = 2
        MIB_TCP_STATE_SYN_SENT = 3
        MIB_TCP_STATE_SYN_RCVD = 4
        MIB_TCP_STATE_ESTAB = 5
        MIB_TCP_STATE_FIN_WAIT1 = 6
        MIB_TCP_STATE_FIN_WAIT2 = 7
        MIB_TCP_STATE_CLOSE_WAIT = 8
        MIB_TCP_STATE_CLOSING = 9
        MIB_TCP_STATE_LAST_ACK = 10
        MIB_TCP_STATE_TIME_WAIT = 11
        MIB_TCP_STATE_DELETE_TCB = 12
    End Enum
    '
    Declare Function GetTcpTable Lib "Iphlpapi" (ByVal pTcpTable As IntPtr, ByRef pdwSize As Integer, ByVal bOrder As Boolean) As Integer

    <StructLayout(LayoutKind.Sequential)> Public Class MIB_TCPROW
        Public dwState As Integer
        Public dwLocalAddr As Integer
        Public dwLocalPort As Integer
        Public dwRemoteAddr As Integer
        Public dwRemotePort As Integer
    End Class

    Public Function GetMyIpAddresses() As List(Of IPAddress)
        Dim IP As IPHostEntry = Dns.GetHostEntry(Dns.GetHostName)

        Return IP.AddressList.ToList

    End Function

    Public Function GetIpAddr(ByVal ip As Integer) As String
        Dim s As String
        Dim i As Integer

        Dim b() As Byte = BitConverter.GetBytes(ip)

        s = b(0).ToString
        For i = 1 To 3
            s = s + "." + b(i).ToString
        Next
        Return s
    End Function

    Public Function GetPort(ByVal port As Integer) As Integer
        Return port / 256 + (port Mod 256) * 256
    End Function

    Public Function GetPortStatus(ByVal ip As String, ByVal port As Integer) As Integer
        Dim pdwSize As Integer
        Dim iRetVal As Integer
        Dim i As Integer
        Dim TcpTableRow As MIB_TCPROW
        Dim pStructPointer As IntPtr = IntPtr.Zero
        Dim iNumberOfStructures As Integer
        Dim Status As Integer = -1
        Dim PortNo As Integer
        Dim IpAdr As String

        iRetVal = GetTcpTable(pStructPointer, pdwSize, 0)

        pStructPointer = Marshal.AllocHGlobal(pdwSize)
        iRetVal = GetTcpTable(pStructPointer, pdwSize, 0)
        iNumberOfStructures = Math.Ceiling((pdwSize - 4) / Marshal.SizeOf(GetType(MIB_TCPROW)))

        For i = 0 To iNumberOfStructures - 1
            Dim pStructPointerTemp As IntPtr = New IntPtr(pStructPointer.ToInt32() + 4 + (i * Marshal.SizeOf(GetType(MIB_TCPROW))))

            TcpTableRow = New MIB_TCPROW()
            With TcpTableRow
                .dwLocalAddr = 0
                .dwState = 0
                .dwLocalPort = 0
                .dwRemoteAddr = 0
                .dwRemotePort = 0
            End With

            TcpTableRow = CType(Marshal.PtrToStructure(pStructPointerTemp, GetType(MIB_TCPROW)), MIB_TCPROW)

            With TcpTableRow
                PortNo = GetPort(.dwLocalPort)
                IpAdr = GetIpAddr(.dwLocalAddr)

                'If IpAdr = "192.168.29.109" Then
                '    Debug.Print("")
                'End If
                'If .dwState = 5 Then
                '    Debug.Print(IpAdr)
                '    Debug.Print(PortNo.ToString)
                'End If

                If PortNo = port And ip = ipadr Then
                    Status = .dwState
                    Exit For
                End If
            End With
        Next

        Marshal.FreeHGlobal(pStructPointer)

        Return Status
    End Function

End Class
