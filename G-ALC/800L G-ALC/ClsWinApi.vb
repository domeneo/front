﻿Imports System
Imports System.Text
Imports System.Runtime.InteropServices

Public Class ClsWinApi
    '* DEFINE
    Private Const BUFF_LEN As Integer = 256 '256文字
    '* メンバ変数
    Public Shared m_strIniFileName As String

    '* API宣言
    '--- iniﾌｧｲﾙ読込み
    '引数
    'lpApplicationName…ｾｸｼｮﾝ名
    'lpKeyName…ｷｰ名
    'lpDefault…既定の文字列
    'lpReturnedString…情報が格納されるﾊﾞｯﾌｧ
    'nSize…情報ﾊﾞｯﾌｧのｻｲｽﾞ
    'lpFilename….iniﾌｧｲﾙの名前
    Private Declare Auto Function GetPrivateProfileString Lib "kernel32.dll" _
        Alias "GetPrivateProfileString" ( _
        <MarshalAs(UnmanagedType.LPTStr)> ByVal lpApplicationName As String, _
        <MarshalAs(UnmanagedType.LPTStr)> ByVal lpKeyName As String, _
        <MarshalAs(UnmanagedType.LPTStr)> ByVal lpDefault As String, _
        <MarshalAs(UnmanagedType.LPTStr)> ByVal lpReturnedString As  _
                           StringBuilder, ByVal nSize As UInt32, _
        <MarshalAs(UnmanagedType.LPTStr)> ByVal lpFileName As String) As UInt32

    '--- iniﾌｧｲﾙ書込み
    '引数
    'lpApplicationName…ｾｸｼｮﾝ名
    'lpKeyName…ｷｰ名
    'lpString…追加するべき文字列
    'lpFilename….iniﾌｧｲﾙ
    <DllImport("kernel32.dll")> _
    Private Shared Function WritePrivateProfileString( _
        ByVal lpApplicationName As String, ByVal lpKeyName As String, _
        ByVal lpString As String, ByVal lpFilename As String) As Boolean

    End Function

    '--- リソースを手動で開放
    Public Sub Close()

        Call Finalize()

    End Sub

    '--- リソースを解放(必要が無くなったら、自動的に開放される)
    Protected Overrides Sub Finalize()

        MyBase.Finalize()

    End Sub

    '--- Get ini String情報
    Public Shared Function GetIniString(ByVal lpszSection As String, _
                    ByVal lpszEntry As String, ByVal lpszFileNm As String, _
                    Optional ByVal lpszDefault As String = Nothing) As String

        Dim sb As StringBuilder = New StringBuilder(BUFF_LEN)
        Dim ret As UInt32 = GetPrivateProfileString(lpszSection, lpszEntry, _
                    lpszDefault, sb, Convert.ToUInt32(sb.Capacity), lpszFileNm)

        Return sb.ToString

    End Function

    '--- Set ini String情報
    Public Shared Function SetIniString(ByVal lpszSection As String, _
                        ByVal lpszEntry As String, ByVal lpszString As String, _
                        ByVal lpszFileNm As String) As Boolean

        Return WritePrivateProfileString(lpszSection, lpszEntry, lpszString, _
                                         lpszFileNm)

    End Function

    Public Shared Function GetAppPath()

        Return System.IO.Path.GetDirectoryName( _
                    System.Reflection.Assembly.GetExecutingAssembly().Location)

    End Function

End Class
