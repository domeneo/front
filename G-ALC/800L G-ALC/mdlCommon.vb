﻿Imports System.Text
Module mdlErrorCode
    Public pCSuccess As Integer = 0
    Public pCErrOther As Integer = -1
    Public pCErrDisConnect As Integer = -2
    Public pCErrAddressOver As Integer = -3
    Public pCErrDataRead As Integer = -4
    Public pCErrDataWrite As Integer = -5

End Module

Module mdlConst

    Public pCstEnvFile As String = "ENV.INI"
    Public pCBitUp As Integer = 0

    Public Enum eChkStr
        Control = 0
        Sign = 1
        Numeric = 2
        AlphabetB = 3
        AlphabetS = 4
        Kana = 5
    End Enum

    Public Enum eAlc

        'id = 0
        'strBCNo = 1
        'strAssyNo = 2
        'strVehicleName = 3
        'intStatus = 4
        'intDelFlg = 5
        'strRecvDate = 6

        id = 0
        strBCNo = 1
        strAssyNo = 2
        strVehicleName = 3
        strLOTcode = 4      'ﾛｯﾄｺｰﾄﾞ追加（2012/5）
        strFamilycode = 5   'ﾌｧﾐﾘｰｺｰﾄﾞ追加（2012/5）
        intStatus = 6
        intDelFlg = 7
        strRecvDate = 8

    End Enum

    Public Enum eVehicle

        id = 0
        AssyNo = 1
        VehicleName = 2

    End Enum

End Module

Module mdlParam

    Public Structure ST_INI

        Public strAlcPort As String     '[SYS]ALC_PORT      接続待ちﾎﾟｰﾄ番号
        Public strAlcIP As String       '[SYS]ALC_IP        接続待ちIPｱﾄﾞﾚｽ
        Public strPlcIp As String       '[SYS]PLC_IP        生産指示のIPｱﾄﾞﾚｽ
        Public strPlcPort As String     '[SYS]PLC_PORT      生産指示のﾎﾟｰﾄ番号
        Public strChangeDay As String   '[SYS]CHANGE_DAY    日付切替時刻(5時)
        Public strBcNo As String        '[SYS]BCNO          受信済BC連番
        Public strDbIp As String        '[DB]IP             ﾃﾞｰﾀﾍﾞｰｽのIPｱﾄﾞﾚｽ
        Public strDbName As String      '[DB]NAME           ﾃﾞｰﾀﾍﾞｰｽのｽｷｰﾏ名
        Public strDbPort As String      '[DB]PORT           ﾃﾞｰﾀﾍﾞｰｽのﾎﾟｰﾄ番号
        Public strDbUser As String      '[DB]USER           User ID
        Public strDbPass As String      '[DB]PASS           Password
        Public strDirLog As String      '[DIR]LOG           ﾛｸﾞﾌｧｲﾙの置き場所
        Public strDebugMode As String   '[DEBUG]MODE        ﾃﾞﾊﾞｯｸﾞﾓｰﾄﾞ
        Public strAlcConnD_PG As String '[REG]ALC_CON_D_PG  ALC接続要求PG番号
        Public strAlcConnD_TY As String '[REG]ALC_CON_D_TY  ALC接続要求ﾀｲﾌﾟ
        Public strAlcConnD_AD As String '[REG]ALC_CON_D_AD  ALC接続要求ｱﾄﾞﾚｽ
        Public strAlcTrnsD_PG As String '[REG]ALC_TRN_D_PG  ALC情報要求PG番号
        Public strAlcTrnsD_TY As String '[REG]ALC_TRN_D_TY  ALC情報要求ﾀｲﾌﾟ
        Public strAlcTrnsD_AD As String '[REG]ALC_TRN_D_AD  ALC情報要求ｱﾄﾞﾚｽ
        Public strVehicleD_PG As String '[REG]VEHICLE_D_PG  車種取込要求PG番号
        Public strVehicleD_TY As String '[REG]VEHICLE_D_TY  車種取込要求ﾀｲﾌﾟ
        Public strVehicleD_AD As String '[REG]VEHICLE_D_AD  車種取込要求ｱﾄﾞﾚｽ
        Public strVehiName_PG As String '[REG]VEHI_NAME_PG  車種名PG番号
        Public strVehiName_TY As String '[REG]VEHI_NAME_TY  車種名ﾀｲﾌﾟ
        Public strVehiName_AD As String '[REG]VEHI_NAME_AD  車種名ｱﾄﾞﾚｽ
        Public strAlcTrnsC_PG As String '[REG]ALC_TRN_C_PG  ALC情報完了PG番号
        Public strAlcTrnsC_TY As String '[REG]ALC_TRN_C_TY  ALC情報完了ﾀｲﾌﾟ
        Public strAlcTrnsC_AD As String '[REG]ALC_TRN_C_AD  ALC情報完了ｱﾄﾞﾚｽ
        Public strVehicleC_PG As String '[REG]VEHICLE_C_PG  車種取込完了PG番号
        Public strVehicleC_TY As String '[REG]VEHICLE_C_TY  車種取込完了ﾀｲﾌﾟ
        Public strVehicleC_AD As String '[REG]VEHICLE_C_AD  車種取込完了ｱﾄﾞﾚｽ
        Public strBCNo_PG As String     '[REG]BCNO_PG       BC連番PG番号
        Public strBCNo_TY As String     '[REG]BCNO_TY       BC連番ﾀｲﾌﾟ
        Public strBCNo_AD As String     '[REG]BCNO_AD       BC連番ｱﾄﾞﾚｽ
        Public strAssyNo_PG As String   '[REG]ASSYNO_PG     車種番号PG番号
        Public strAssyNo_TY As String   '[REG]ASSYNO_TY     車種番号ﾀｲﾌﾟ
        Public strAssyNo_AD As String   '[REG]ASSYNO_AD     車種番号ｱﾄﾞﾚｽ
        Public strLOTcode_PG As String  '[REG]LOTCODE_PG    ﾛｯﾄｺｰﾄﾞPG番号           
        Public strLOTcode_TY As String  '[REG]LOTCODE_TY    ﾛｯﾄｺｰﾄﾞﾀｲﾌﾟ           
        Public strLOTcode_AD As String  '[REG]LOTCODE_AD    ﾛｯﾄｺｰﾄﾞｱﾄﾞﾚｽ            'ﾛｯﾄｺｰﾄﾞ追加（2012/5）
        Public strFamilycode_PG As String  '[REG]FAMILYCODE_PG    ﾌｧﾐﾘｰｺｰﾄﾞPG番号   
        Public strFamilycode_TY As String  '[REG]FAMILYCODE_TY    ﾌｧﾐﾘｰｺｰﾄﾞﾀｲﾌﾟ   
        Public strFamilycode_AD As String  '[REG]FAMILYCODE_AD    ﾌｧﾐﾘｰｺｰﾄﾞｱﾄﾞﾚｽ    'ﾌｧﾐﾘｰｺｰﾄﾞ追加（2012/5）

    End Structure

    Public Structure ST_ALC_SEND            'ALC送信ﾃﾞｰﾀ
        Public strToLogicalNama As String   '送信先論理名
        Public strFromLogicalName As String '送信元論理名
        Public strSerialNo As String        'ｼﾘｱﾙ№
        Public strMode As String            'ﾓｰﾄﾞ
        Public strDataLength As String      'ﾃﾞｰﾀ長
        Public strProcess As String         '処理区分
        Public strProcessResult As String   '処理結果
    End Structure

    Public Structure ST_ALC_RECV            'ALC受信ﾃﾞｰﾀ
        Public strToLogicalName As String   '送信先論理名
        Public strFromLogicalName As String '送信元論理名
        Public strSerialNo As String        'ｼﾘｱﾙ№
        Public strMode As String            'ﾓｰﾄﾞ
        Public strDataLength As String      'ﾃﾞｰﾀ長
        Public strProcess As String         '処理区分
        Public strProcessResult As String   '処理結果
        Public strLine As String            'ﾗｲﾝ
        Public strTP As String              'T/P
        Public strKumitateNo As String      'BC連番
        Public strBodyNo As String          'ﾎﾞﾃﾞｰ№
        Public strBCNo As String            'BC連番
        Public strAssyNo As String          '車種番号
        Public strLOTCode As String         'ﾛｯﾄｺｰﾄﾞ                'ﾛｯﾄｺｰﾄﾞ追加（2012/5）
        Public strFamilyCode As String      '号口ｺｰﾄﾞ（ﾌｧﾐﾘｰｺｰﾄﾞ）  'ﾌｧﾐﾘｰｺｰﾄﾞ追加（2012/5）
        Public strLastSEQ As String
    End Structure

    Public Structure ST_SENDDATA            'ALC送信済ﾃｰﾌﾞﾙ
        Public id As Integer                'ｼｽﾃﾑ連番
        Public strAssyNo As String          '車種番号
        Public strVehicleName As String     '車種名
        Public intDelFlg As Integer         '削除ﾌﾗｸﾞ
        Public strSendDate As String        '送信日時
    End Structure

    Public Structure ST_ALC                 'ALC受信ﾃｰﾌﾞﾙ
        Public id As Integer                'ｼｽﾃﾑ連番
        Public strBCNo As String            'BC連番
        Public strAssyNo As String          '車種番号
        Public strVehicleName As String     '車種名
        Public strLOTcode As String         'ﾛｯﾄｺｰﾄﾞ        'ﾛｯﾄｺｰﾄﾞ追加（2012/5）
        Public strFamilycode As String      'ﾌｧﾐﾘｰｺｰﾄﾞ      'ﾌｧﾐﾘｰｺｰﾄﾞ追加（2012/5）
        Public intStatus As Integer         'ﾃﾞｰﾀｽﾃｰﾀｽ
        Public intDelFlg As Integer         '削除ﾌﾗｸﾞ
        Public strRecvDate As String        '受信日時
    End Structure

    Public Structure ST_VEHICLE             '車種ﾃｰﾌﾞﾙ
        Public id As Integer                'ｼｽﾃﾑ連番
        Public strAssyNo As String          '車種番号
        Public strVehicleName As String     '車種名
    End Structure

End Module

Module mdlCommon

    Public pstrErrorMsg As String               'ｴﾗｰﾒｯｾｰｼﾞ
    Public pblnEXFlg As Boolean                 'CatchされたらON(True)
    Public psysException As System.Exception    'Catchした場合のExceptionを格納
    Public pstIni As ST_INI
    Public pstAlcSend As ST_ALC_SEND            ' ALC送信電文
    Public pstAlcRecv As ST_ALC_RECV            ' ALC受信電文
    Public pstAlcDataSend As ST_ALC             ' ALCﾃﾞｰﾀ構造体(DB用)PIS送信ﾃﾞｰﾀ
    Public pstAlcData() As ST_ALC               ' ALCﾃﾞｰﾀ構造体(DB用)DataDisp用
    Public pstAlcLoopData() As ST_ALC               ' ALCﾃﾞｰﾀ構造体(DB用)AlcLoop内表示用
    Public pstAlcData2PLC As ST_ALC             ' ALCﾃﾞｰﾀ構造体(DB用)Send時
    Public pstAlcRecvData2PLC As ST_ALC             ' ALCﾃﾞｰﾀ構造体(DB用)Receive時
    Public pblnAlcDataFlg As Boolean            ' ALCﾃﾞｰﾀが存在する場合TRUE
    Public pstVehicleData() As ST_VEHICLE       ' 車種ﾃｰﾌﾞﾙ構造体
    Public pintVehicleCount As Integer          ' 車種ﾃｰﾌﾞﾙ登録件数
    Public pintRecvSize As Integer              ' 受信電文長
    Public pInsfrmMsg As New frmMsg
    Public pInsErrMsg As New clsErrMsg
    Public pfrmDataDisp As New frmDataDisp
    Public pClsPC10G As New clsPc10G
    Public pClsSql As New clsSQL
    Public pintAlcDataCount As Integer
    Public pintAlcLoopDataCount As Integer
    Public pblnAlcLoop As Boolean               'ALCのループフラグ,TrueならALC処理中
    Public pblnPlcLoop As Boolean               'PLC(生産指示)のループフラグ,TrueならPLC処理中
    Public pinsTcpState As New clsTcpState
    Public cstrFileMidasi As String = "Date,Destination Logical Name," + _
           "Transmission Former Logical Name,Serial No,Mode,Data Length," + _
           "Processing Division,Processing Result,Line,T/P," + _
           "BC Sequential No,Body No,BC Sequential No,Assy No"
    'Public cstrPlcFileMidasi As String = "Date,strAssyNo,strVehicleName,strSendDate,strBCNo"
    Public cstrPlcFileMidasi As String = "Date,AssyNo,VehicleName,BCNo,LOTcode,Familycode"      '2012/5/8 kasuga

    Public pWatetimer As Integer                '2011/10/14 Kasuga
    Public clsTCPStatus As New ClassTcpStatus   '2011/10/14 Kasuga


    Public Function CmnBitUpDown(ByVal intBitFlg As Integer, _
                    ByVal intBitNo As Integer, ByVal lngBitData As Integer) _
                                                                    As Integer

        Dim lngTempBitData As Integer

        CmnBitUpDown = -1
        Try
            lngTempBitData = 2 ^ intBitNo

            If intBitFlg = pCBitUp Then
                ' 対象ビットをＯＮする
                lngBitData = lngBitData Or lngTempBitData
            Else
                '  対象ビットをＯＦＦする
                lngBitData = lngBitData Xor lngTempBitData
            End If
            CmnBitUpDown = lngBitData
        Catch ex As Exception
        End Try

    End Function


    Public Sub CmnLng2IntInt(ByVal lngData As Integer, _
               ByRef intLower As Short, ByRef intUpper As Short)

        Dim lngTempLower As Short
        Dim lngTempUpper As Short

        Try
            ' Lowerデータ算出
            lngTempLower = lngData Mod 256
            ' Upperデータ算出
            lngTempUpper = (lngData - lngTempLower) / 256

            intLower = lngTempLower
            intUpper = lngTempUpper
        Catch ex As Exception
        End Try

    End Sub

    Public Sub CmnIntInt2Lng(ByRef lngData As Integer, _
                             ByVal intLower As Short, ByVal intUpper As Short)

        Try
            lngData = intUpper * 256 + intLower
        Catch ex As Exception
        End Try

    End Sub

    Public Function CmnCheckChar(ByVal iAsc As Integer) As Integer
        CmnCheckChar = eChkStr.Control

        Select Case iAsc
            Case &H20 To &H2F, &H3A To &H40, _
                 &H5B To &H60, &H7B To &H7E     ' 記号
                CmnCheckChar = eChkStr.Sign
            Case &H30 To &H39                   ' 数字
                CmnCheckChar = eChkStr.Numeric
            Case &H41 To &H5A                   ' 大文字
                CmnCheckChar = eChkStr.AlphabetB
            Case &H61 To &H7A                   ' 小文字
                CmnCheckChar = eChkStr.AlphabetS
            Case &HA1 To &HDF                   ' 半角カナ
                CmnCheckChar = eChkStr.Kana
            Case Else                           ' 制御記号
                CmnCheckChar = eChkStr.Control
        End Select

    End Function

    ' 10進数→BCDコード変換関数
    ''' <summary>
    ''' 対象の10進数データをBCDコードに変換する
    ''' </summary>
    ''' <param name="iNum">対象10進数データ</param>
    ''' <returns>
    ''' 成功：BCDコード化した10進数データ
    ''' 異常：0xFFFF
    ''' </returns>
    ''' <remarks>
    ''' 別名2進化10進数のことを表す。
    ''' 2進化10進数とは、2進数4桁を用いて10進数1桁を表現するコート体系です。
    ''' 
    ''' よって、10進数95をBCDで表すと10010101となり、
    ''' これを16進数で表現すると0x95となる
    ''' </remarks>
    ''' 
    Public Function CmnInt2BCD(ByVal iNum As Integer) As Integer

        Dim strNum As String
        Dim i As Integer
        Dim iBCD As Integer = 0

        Try
            ' 10進数データの桁数を取得
            strNum = iNum.ToString()

            ' 1桁づつBCDコード化していく
            For i = 1 To strNum.Length
                iBCD = iBCD + Convert.ToInt32(Mid(strNum, i, 1)) _
                            * Math.Pow(16.0, CDbl(strNum.Length - i))
            Next i

        Catch
            iBCD = &HFFFF
        End Try

        CmnInt2BCD = iBCD

    End Function

    ' BCDコード→10進数変換関数
    ''' <summary>
    ''' 対象のBCDコードを10進数に変換する
    ''' </summary>
    ''' <param name="iBCD">対象BCDコード</param>
    ''' <returns>
    ''' 成功：10進数化データ
    ''' 異常：0xFFFF
    ''' </returns>
    ''' <remarks></remarks>
    ''' 
    Public Function CmnBCD2Int(ByVal iBCD As Integer) As Integer

        Dim iDecimal As Integer

        Try
            iDecimal = CInt(Convert.ToString(iBCD, 16))
        Catch
            iDecimal = &HFFFF
        End Try

        CmnBCD2Int = iDecimal

    End Function

    'G-ALC受信ﾃﾞｰﾀをﾃｷｽﾄに保存
    Public Sub CmnAlcRecvDataLog(ByVal strData As String)

        Dim dtDate As Date
        Dim strFiles As String

        dtDate = Now

        dtDate = Now
        If dtDate.Hour < 5 Then
            dtDate = dtDate.AddDays(-1)
        End If

        strFiles = pstIni.strDirLog + "\AlcRecvData" + _
        dtDate.Year.ToString("0000") + _
        dtDate.Month.ToString("00") + _
        dtDate.Day.ToString("00") + ".txt"

        If System.IO.File.Exists(strFiles) = False Then
            My.Computer.FileSystem.WriteAllText(strFiles, _
                                    cstrFileMidasi & ControlChars.CrLf, True)
        End If

        My.Computer.FileSystem.WriteAllText(strFiles, Now.ToString + ":" + _
                                            strData & ControlChars.CrLf, True)

    End Sub
    'G-ALC受信ﾃﾞｰﾀをﾃｷｽﾄに保存
    Public Sub CmnPlcSendDataLog(ByVal strData As String)

        Dim dtDate As Date
        Dim strFiles As String

        dtDate = Now

        dtDate = Now
        If dtDate.Hour < 5 Then
            dtDate = dtDate.AddDays(-1)
        End If

        strFiles = pstIni.strDirLog + "\PlcSendData" + _
        dtDate.Year.ToString("0000") + _
        dtDate.Month.ToString("00") + _
        dtDate.Day.ToString("00") + ".txt"

        '見出し作成
        If System.IO.File.Exists(strFiles) = False Then
            My.Computer.FileSystem.WriteAllText(strFiles, _
                                    cstrPlcFileMidasi & ControlChars.CrLf, True)
        End If

        My.Computer.FileSystem.WriteAllText(strFiles, Now.ToString + ":" + _
                                            strData & ControlChars.CrLf, True)

    End Sub

    Public Function CmnGetNow(ByVal pdtDateTime As DateTime) As String

        Dim intDD As Integer
        Dim intHH As Integer

        Try
            CmnGetNow = Format(pdtDateTime, "yyyy/MM/dd HH:MM:ss")
            '5時(日付切替時刻)以前の場合､日付を前日に､時刻を24時以降の時間にする
            If DateAndTime.Hour(pdtDateTime) < pstIni.strChangeDay Then
                intDD = CInt(Mid(CmnGetNow, 9, 2)) - 1
                intHH = CInt(Mid(CmnGetNow, 12, 2)) + 24
                CmnGetNow = Mid(CmnGetNow, 1, 8) & intDD.ToString & _
                            intHH.ToString & Mid(CmnGetNow, 14, 6)
            End If
        Catch ex As Exception
            Return ("")
        End Try

    End Function
    Public Function IPReady(ByVal serverName As String) As Boolean
        Try


            Dim bReturnStatus As Boolean = False


            Dim timeout As Integer = 1

            Dim pingSender As New System.Net.NetworkInformation.Ping()
            Dim options As New System.Net.NetworkInformation.PingOptions()

            options.DontFragment = True

            Dim ipAddressOrHostName As String = serverName
            If ipAddressOrHostName = "" Then
                Exit Function
            End If
            Dim data As String = "zz"
            Dim buffer As Byte() = System.Text.Encoding.ASCII.GetBytes(data)
            Dim reply As System.Net.NetworkInformation.PingReply =
                        pingSender.Send(ipAddressOrHostName, timeout, buffer, options)

            If reply.Status = Net.NetworkInformation.IPStatus.Success Then
                bReturnStatus = True
                'MsgBox(reply.RoundtripTime) 'show ping time by milliseccond
            End If

            Return bReturnStatus

        Catch ex As Exception
            Return False
            Exit Function
        End Try

    End Function
End Module
