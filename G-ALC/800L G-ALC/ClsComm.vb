﻿Public Class ClsComm

    Public ComPort As System.IO.Ports.SerialPort = Nothing


    Public Function GetCommPortAll() As String()
        Dim strPortName As String()

        GetCommPortAll = Nothing

        Try
            strPortName = IO.Ports.SerialPort.GetPortNames()

            Return strPortName
        Catch ex As Exception
        End Try
    End Function

    Public Function Comm(ByVal iNum As Integer) As String
        Dim strComm As String()

        strComm = GetCommPortAll()

        Return strComm(iNum)
    End Function

    Public Function Bps(ByVal iNum) As Integer
        Select Case iNum
            Case 0
                Return 300
            Case 1
                Return 600
            Case 2
                Return 1200
            Case 3
                Return 2400
            Case 4
                Return 4800
            Case 5
                Return 9600
            Case 6
                Return 19200
            Case 7
                Return 38400
            Case 8
                Return 57600
            Case 9
                Return 115200
            Case Else
                Return 9600
        End Select
    End Function

    Public Function DataLen(ByVal iNum As Integer) As Integer
        Select Case iNum
            Case 0
                Return 8
            Case 1
                Return 7
            Case 2
                Return 6
            Case 3
                Return 5
            Case 4
                Return 4
            Case Else
                Return 8
        End Select
    End Function

    Public Function Parity(ByVal iNum As Integer) As String
        Select Case iNum
            Case 0
                Return "None"
            Case 1
                Return "Odd"
            Case 2
                Return "Even"
            Case 3
                Return "Mark"
            Case 4
                Return "Space"
            Case Else
                Return "None"
        End Select
    End Function

    Public Function Handshake(ByVal iNum As Integer) As String
        Select Case iNum
            Case 0
                Return "None"
            Case 1
                Return "RequestToSend"
            Case 2
                Return "XOnXOff"
            Case 3
                Return "RequestToSendXOnXOff"
            Case Else
                Return "None"
        End Select
    End Function

    Public Function StopBits(ByVal iNum As Integer) As String
        Select Case iNum
            Case 0
                Return "None"
            Case 1
                Return "One"
            Case 2
                Return "OnePointFive"
            Case 3
                Return "Two"
            Case Else
                Return "None"
        End Select
    End Function

End Class
