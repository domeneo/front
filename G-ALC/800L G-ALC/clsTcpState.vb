﻿Imports System.Runtime.InteropServices

Public Class clsTcpState

    <StructLayout(LayoutKind.Sequential)> Public Class MIB_TCPROW
        Public dwState As Integer
        Public dwLocalAddr As Integer
        Public dwLocalPort As Integer
        Public dwRemoteAddr As Integer
        Public dwRemotePort As Integer
    End Class

    Private Const ERROR_BUFFER_OVERFLOW As Short = 111
    Private Const ERROR_INVALID_PARAMETER As Short = 87
    Private Const ERROR_NO_DATA As Short = 232
    Private Const ERROR_NOT_SUPPORTED As Short = 50
    Private Const ERROR_SUCCESS As Short = 0
    '
    Private Const MIB_TCP_STATE_CLOSED As Short = 1
    Private Const MIB_TCP_STATE_LISTEN As Short = 2
    Private Const MIB_TCP_STATE_SYN_SENT As Short = 3
    Private Const MIB_TCP_STATE_SYN_RCVD As Short = 4
    Private Const MIB_TCP_STATE_ESTAB As Short = 5
    Private Const MIB_TCP_STATE_FIN_WAIT1 As Short = 6
    Private Const MIB_TCP_STATE_FIN_WAIT2 As Short = 7
    Private Const MIB_TCP_STATE_CLOSE_WAIT As Short = 8
    Private Const MIB_TCP_STATE_CLOSING As Short = 9
    Private Const MIB_TCP_STATE_LAST_ACK As Short = 10
    Private Const MIB_TCP_STATE_TIME_WAIT As Short = 11
    Private Const MIB_TCP_STATE_DELETE_TCB As Short = 12
    '
    Declare Function GetTcpTable Lib "Iphlpapi" (ByVal pTcpTable As IntPtr, ByRef pdwSize As Integer, ByVal bOrder As Boolean) As Integer

    Public Function GetState(ByRef lngState As Integer) As String
        GetState = "Fumei"
        Select Case lngState
            Case MIB_TCP_STATE_CLOSED : GetState = "CLOSED"
            Case MIB_TCP_STATE_LISTEN : GetState = "LISTEN"
            Case MIB_TCP_STATE_SYN_SENT : GetState = "SYN_SENT"
            Case MIB_TCP_STATE_SYN_RCVD : GetState = "SYN_RCVD"
            Case MIB_TCP_STATE_ESTAB : GetState = "ESTAB"
            Case MIB_TCP_STATE_FIN_WAIT1 : GetState = "FIN_WAIT1"
            Case MIB_TCP_STATE_FIN_WAIT2 : GetState = "FIN_WAIT2"
            Case MIB_TCP_STATE_CLOSE_WAIT : GetState = "CLOSE_WAIT"
            Case MIB_TCP_STATE_CLOSING : GetState = "CLOSING"
            Case MIB_TCP_STATE_LAST_ACK : GetState = "LAST_ACK"
            Case MIB_TCP_STATE_TIME_WAIT : GetState = "TIME_WAIT"
            Case MIB_TCP_STATE_DELETE_TCB : GetState = "DELETE_TCB"
        End Select
        ''
    End Function

    Private Function GetTcpPortNumber(ByRef DWord As Integer) As Integer
        Dim lLowNum As Long
        Dim lHighNum As Long

        lLowNum = DWord \ 256
        lHighNum = DWord Mod 256

        GetTcpPortNumber = lLowNum + lHighNum * 256
        'GetTcpPortNumber = DWord / 256 + (DWord Mod 256) * 256
    End Function

    Private Function GetIpFromLong(ByRef lngIPAddress As Integer) As String
        '
        Dim arrIpParts() As Byte = BitConverter.GetBytes(lngIPAddress)
        GetIpFromLong = CStr(arrIpParts(0)) & "." & CStr(arrIpParts(1)) & "." & CStr(arrIpParts(2)) & "." & CStr(arrIpParts(3))
        ''
    End Function

    '※LANとつながっている状態でClient(ALC)側をLocalhostとして接続するとFumeiを返します。
    'localhostでつなげるとき(主にテストなどで)はケーブルを抜いてください。
    Public Function pfunPortState(ByVal strIpAdr As String, ByVal strPort As String) As String
        Dim pdwSize As Integer
        Dim iRetVal As Integer
        Dim pStructPointer As IntPtr = IntPtr.Zero
        Dim iNumberOfStructures As Integer
        Dim TcpTableRow As MIB_TCPROW
        Dim i As Integer

        Try


            pfunPortState = "Fumei"

            iRetVal = GetTcpTable(pStructPointer, pdwSize, 0)
            pStructPointer = Marshal.AllocHGlobal(pdwSize)
            iRetVal = GetTcpTable(pStructPointer, pdwSize, 0)
            iNumberOfStructures = Math.Ceiling((pdwSize - 4) / Marshal.SizeOf(GetType(MIB_TCPROW)))

            For i = 0 To iNumberOfStructures - 1
                Dim pStructPointerTemp As IntPtr = New IntPtr(pStructPointer.ToInt32() + 4 + (i * Marshal.SizeOf(GetType(MIB_TCPROW))))

                TcpTableRow = New MIB_TCPROW()
                With TcpTableRow
                    .dwLocalAddr = 0
                    .dwState = 0
                    .dwLocalPort = 0
                    .dwRemoteAddr = 0
                    .dwRemotePort = 0
                End With
                TcpTableRow = CType(Marshal.PtrToStructure(pStructPointerTemp, GetType(MIB_TCPROW)), MIB_TCPROW)
                With TcpTableRow
                    'ポートが合致した物のステータスを返す
                    If CStr(GetTcpPortNumber(.dwLocalPort)) = strPort Then
                        'MessageBox.Show(GetIpFromLong(.dwLocalAddr))

                        'If GetIpFromLong(.dwLocalAddr) = strIpAdr And CStr(GetTcpPortNumber(.dwLocalPort)) = strPort Then
                        pfunPortState = GetState(.dwState)
                        '                    pfunPortState = .dwState
                        'MessageBox.Show(.dwState.ToString())
                        Exit For
                    End If
                End With
            Next
            Marshal.FreeHGlobal(pStructPointer)

        Catch ex As Exception
            pInsErrMsg.psubErrMsgShow(ex, "clsTcpState.pfunPortState")
            pfunPortState = "Error"
        End Try
    End Function

End Class

