﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class SendSeqUC
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtBuffer = New System.Windows.Forms.Label()
        Me.CBgetComplete_Buffer = New System.Windows.Forms.CheckBox()
        Me.CBgetComplete_Change = New System.Windows.Forms.CheckBox()
        Me.btnSetBuffer = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtCURPIS = New System.Windows.Forms.TextBox()
        Me.txtOLDPIS = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.DGVComplete = New System.Windows.Forms.DataGridView()
        Me.Complete_ADDR = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.complete_type = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Complete_Lenght = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Complete_value = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnSavePis = New System.Windows.Forms.Button()
        Me.txtPISip = New System.Windows.Forms.TextBox()
        Me.txtPisPort = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtCCRPort = New System.Windows.Forms.TextBox()
        Me.txtCCRip = New System.Windows.Forms.TextBox()
        Me.btnFsend = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtBCdata = New System.Windows.Forms.TextBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.btnConnect = New System.Windows.Forms.Button()
        Me.lblPlant = New System.Windows.Forms.Label()
        Me.lblSEQ = New System.Windows.Forms.Label()
        Me.lblLOtcode = New System.Windows.Forms.Label()
        Me.lblProcess = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.lblJobState = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.lblVersion = New System.Windows.Forms.Label()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.lblStatus = New System.Windows.Forms.ToolStripStatusLabel()
        Me.btnProcessConfig = New System.Windows.Forms.Button()
        Me.lbllen = New System.Windows.Forms.Label()
        Me.btnSaveCCR = New System.Windows.Forms.Button()
        Me.btnserverConfig = New System.Windows.Forms.Button()
        Me.btnCancelCCR = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.CBSendCCR = New System.Windows.Forms.CheckBox()
        Me.CBDeleteExport = New System.Windows.Forms.CheckBox()
        Me.btnStart = New System.Windows.Forms.Button()
        Me.CBCheckComplete = New System.Windows.Forms.CheckBox()
        Me.CBgetExport = New System.Windows.Forms.CheckBox()
        Me.DGVbuffer = New System.Windows.Forms.DataGridView()
        Me.No = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LOT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SEQ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.address = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtbuffer_LOT = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtbuffer_SEQ = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.btnGetBuffer = New System.Windows.Forms.CheckBox()
        Me.txtbuffer_LOTLEN = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.txtbuffer_SEQLEN = New System.Windows.Forms.TextBox()
        Me.PNExport = New System.Windows.Forms.Panel()
        Me.btnResend = New System.Windows.Forms.Button()
        Me.ServerPN = New System.Windows.Forms.Panel()
        Me.Label75 = New System.Windows.Forms.Label()
        Me.ServerPingPN = New System.Windows.Forms.Panel()
        Me.lblServer1 = New System.Windows.Forms.Label()
        Me.ServerPingPN2 = New System.Windows.Forms.Panel()
        Me.lblServer2 = New System.Windows.Forms.Label()
        Me.Label74 = New System.Windows.Forms.Label()
        Me.ServerLinkPN = New System.Windows.Forms.Panel()
        Me.ServerLinkPN2 = New System.Windows.Forms.Panel()
        Me.TimerServerLink = New System.Windows.Forms.Timer(Me.components)
        Me.CBSendQ = New System.Windows.Forms.CheckBox()
        Me.CBSetLamp = New System.Windows.Forms.CheckBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtReplace000 = New System.Windows.Forms.TextBox()
        Me.PNmanualCCR = New System.Windows.Forms.Panel()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.PNDebug = New System.Windows.Forms.Panel()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.btnCancelBuffer = New System.Windows.Forms.Button()
        Me.btnSaveBuffer = New System.Windows.Forms.Button()
        Me.btnDeleteFirst = New System.Windows.Forms.Button()
        Me.CBDeleteF = New System.Windows.Forms.CheckBox()
        Me.txtDeleteFaddr = New System.Windows.Forms.TextBox()
        Me.txtDeleteFlen = New System.Windows.Forms.TextBox()
        Me.btnDisconnect = New System.Windows.Forms.Button()
        Me.lblRetryresend = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DGVComplete, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.DGVbuffer, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ServerPN.SuspendLayout()
        Me.PNmanualCCR.SuspendLayout()
        Me.PNDebug.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtBuffer)
        Me.GroupBox1.Controls.Add(Me.CBgetComplete_Buffer)
        Me.GroupBox1.Controls.Add(Me.CBgetComplete_Change)
        Me.GroupBox1.Controls.Add(Me.btnSetBuffer)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtCURPIS)
        Me.GroupBox1.Controls.Add(Me.txtOLDPIS)
        Me.GroupBox1.Controls.Add(Me.Label20)
        Me.GroupBox1.Controls.Add(Me.DGVComplete)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.btnSavePis)
        Me.GroupBox1.Controls.Add(Me.txtPISip)
        Me.GroupBox1.Controls.Add(Me.txtPisPort)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(3, 149)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(331, 208)
        Me.GroupBox1.TabIndex = 39
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "GetComplete"
        '
        'txtBuffer
        '
        Me.txtBuffer.AutoSize = True
        Me.txtBuffer.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtBuffer.Location = New System.Drawing.Point(76, 174)
        Me.txtBuffer.Name = "txtBuffer"
        Me.txtBuffer.Size = New System.Drawing.Size(26, 18)
        Me.txtBuffer.TabIndex = 213
        Me.txtBuffer.Text = "30"
        '
        'CBgetComplete_Buffer
        '
        Me.CBgetComplete_Buffer.AutoSize = True
        Me.CBgetComplete_Buffer.Location = New System.Drawing.Point(6, 178)
        Me.CBgetComplete_Buffer.Name = "CBgetComplete_Buffer"
        Me.CBgetComplete_Buffer.Size = New System.Drawing.Size(15, 14)
        Me.CBgetComplete_Buffer.TabIndex = 212
        Me.CBgetComplete_Buffer.UseVisualStyleBackColor = True
        '
        'CBgetComplete_Change
        '
        Me.CBgetComplete_Change.AutoSize = True
        Me.CBgetComplete_Change.Location = New System.Drawing.Point(6, 148)
        Me.CBgetComplete_Change.Name = "CBgetComplete_Change"
        Me.CBgetComplete_Change.Size = New System.Drawing.Size(15, 14)
        Me.CBgetComplete_Change.TabIndex = 211
        Me.CBgetComplete_Change.UseVisualStyleBackColor = True
        '
        'btnSetBuffer
        '
        Me.btnSetBuffer.BackColor = System.Drawing.Color.DimGray
        Me.btnSetBuffer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSetBuffer.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnSetBuffer.ForeColor = System.Drawing.Color.White
        Me.btnSetBuffer.Location = New System.Drawing.Point(115, 171)
        Me.btnSetBuffer.Name = "btnSetBuffer"
        Me.btnSetBuffer.Size = New System.Drawing.Size(43, 23)
        Me.btnSetBuffer.TabIndex = 209
        Me.btnSetBuffer.Text = "Set"
        Me.btnSetBuffer.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.Location = New System.Drawing.Point(25, 178)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(45, 13)
        Me.Label1.TabIndex = 210
        Me.Label1.Text = "Buffer:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(96, 129)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(53, 13)
        Me.Label6.TabIndex = 204
        Me.Label6.Text = "CURPIS"
        '
        'txtCURPIS
        '
        Me.txtCURPIS.Location = New System.Drawing.Point(91, 145)
        Me.txtCURPIS.Name = "txtCURPIS"
        Me.txtCURPIS.Size = New System.Drawing.Size(63, 20)
        Me.txtCURPIS.TabIndex = 203
        '
        'txtOLDPIS
        '
        Me.txtOLDPIS.Location = New System.Drawing.Point(23, 145)
        Me.txtOLDPIS.Name = "txtOLDPIS"
        Me.txtOLDPIS.Size = New System.Drawing.Size(62, 20)
        Me.txtOLDPIS.TabIndex = 201
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(30, 129)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(52, 13)
        Me.Label20.TabIndex = 202
        Me.Label20.Text = "OLDPIS"
        '
        'DGVComplete
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.DeepSkyBlue
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGVComplete.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DGVComplete.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVComplete.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Complete_ADDR, Me.complete_type, Me.Complete_Lenght, Me.Complete_value})
        Me.DGVComplete.EnableHeadersVisualStyles = False
        Me.DGVComplete.Location = New System.Drawing.Point(6, 39)
        Me.DGVComplete.Name = "DGVComplete"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGVComplete.RowHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.DGVComplete.RowHeadersWidth = 20
        Me.DGVComplete.Size = New System.Drawing.Size(285, 88)
        Me.DGVComplete.TabIndex = 188
        '
        'Complete_ADDR
        '
        Me.Complete_ADDR.DataPropertyName = "Address"
        Me.Complete_ADDR.HeaderText = "Address"
        Me.Complete_ADDR.Name = "Complete_ADDR"
        Me.Complete_ADDR.Width = 80
        '
        'complete_type
        '
        Me.complete_type.DataPropertyName = "type"
        Me.complete_type.HeaderText = "Type"
        Me.complete_type.Name = "complete_type"
        Me.complete_type.Width = 60
        '
        'Complete_Lenght
        '
        Me.Complete_Lenght.DataPropertyName = "lenght"
        Me.Complete_Lenght.HeaderText = "Lenght"
        Me.Complete_Lenght.Name = "Complete_Lenght"
        Me.Complete_Lenght.Width = 30
        '
        'Complete_value
        '
        Me.Complete_value.DataPropertyName = "Value"
        Me.Complete_value.HeaderText = "Value"
        Me.Complete_value.Name = "Complete_value"
        Me.Complete_value.Width = 80
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DimGray
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(233, 143)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(65, 23)
        Me.Button1.TabIndex = 187
        Me.Button1.Text = "Cancel"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'btnSavePis
        '
        Me.btnSavePis.BackColor = System.Drawing.Color.DimGray
        Me.btnSavePis.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSavePis.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnSavePis.ForeColor = System.Drawing.Color.White
        Me.btnSavePis.Location = New System.Drawing.Point(160, 143)
        Me.btnSavePis.Name = "btnSavePis"
        Me.btnSavePis.Size = New System.Drawing.Size(67, 23)
        Me.btnSavePis.TabIndex = 53
        Me.btnSavePis.Text = "Save"
        Me.btnSavePis.UseVisualStyleBackColor = False
        '
        'txtPISip
        '
        Me.txtPISip.Location = New System.Drawing.Point(39, 13)
        Me.txtPISip.Name = "txtPISip"
        Me.txtPISip.Size = New System.Drawing.Size(100, 20)
        Me.txtPISip.TabIndex = 19
        Me.txtPISip.Text = "192.168.0.99"
        '
        'txtPisPort
        '
        Me.txtPisPort.Location = New System.Drawing.Point(145, 13)
        Me.txtPisPort.Name = "txtPisPort"
        Me.txtPisPort.Size = New System.Drawing.Size(50, 20)
        Me.txtPisPort.TabIndex = 20
        Me.txtPisPort.Text = "10000"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(10, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(27, 13)
        Me.Label4.TabIndex = 21
        Me.Label4.Text = "IP :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 93)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(23, 13)
        Me.Label5.TabIndex = 38
        Me.Label5.Text = "IP :"
        '
        'txtCCRPort
        '
        Me.txtCCRPort.Location = New System.Drawing.Point(180, 90)
        Me.txtCCRPort.Name = "txtCCRPort"
        Me.txtCCRPort.Size = New System.Drawing.Size(47, 20)
        Me.txtCCRPort.TabIndex = 3
        Me.txtCCRPort.Text = "26528"
        '
        'txtCCRip
        '
        Me.txtCCRip.Location = New System.Drawing.Point(35, 90)
        Me.txtCCRip.Name = "txtCCRip"
        Me.txtCCRip.Size = New System.Drawing.Size(100, 20)
        Me.txtCCRip.TabIndex = 2
        Me.txtCCRip.Text = "localhost"
        '
        'btnFsend
        '
        Me.btnFsend.BackColor = System.Drawing.Color.DimGray
        Me.btnFsend.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFsend.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnFsend.ForeColor = System.Drawing.Color.White
        Me.btnFsend.Location = New System.Drawing.Point(398, 3)
        Me.btnFsend.Name = "btnFsend"
        Me.btnFsend.Size = New System.Drawing.Size(77, 23)
        Me.btnFsend.TabIndex = 35
        Me.btnFsend.Text = "Manual Send"
        Me.btnFsend.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(5, 8)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 13)
        Me.Label3.TabIndex = 34
        Me.Label3.Text = "Bcdata"
        '
        'txtBCdata
        '
        Me.txtBCdata.Location = New System.Drawing.Point(52, 5)
        Me.txtBCdata.Name = "txtBCdata"
        Me.txtBCdata.Size = New System.Drawing.Size(321, 20)
        Me.txtBCdata.TabIndex = 33
        Me.txtBCdata.Text = "LSA110OUTP_P003810001800  1L02803215149812DKEB687W"
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 1000
        '
        'btnConnect
        '
        Me.btnConnect.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnConnect.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnConnect.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnConnect.ForeColor = System.Drawing.Color.White
        Me.btnConnect.Location = New System.Drawing.Point(236, 88)
        Me.btnConnect.Name = "btnConnect"
        Me.btnConnect.Size = New System.Drawing.Size(60, 23)
        Me.btnConnect.TabIndex = 40
        Me.btnConnect.Text = "Connect"
        Me.btnConnect.UseVisualStyleBackColor = False
        '
        'lblPlant
        '
        Me.lblPlant.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.lblPlant.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblPlant.Location = New System.Drawing.Point(17, 20)
        Me.lblPlant.Name = "lblPlant"
        Me.lblPlant.Size = New System.Drawing.Size(64, 48)
        Me.lblPlant.TabIndex = 42
        Me.lblPlant.Text = "E"
        Me.lblPlant.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblSEQ
        '
        Me.lblSEQ.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.lblSEQ.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblSEQ.Location = New System.Drawing.Point(87, 20)
        Me.lblSEQ.Name = "lblSEQ"
        Me.lblSEQ.Size = New System.Drawing.Size(91, 48)
        Me.lblSEQ.TabIndex = 43
        Me.lblSEQ.Text = "000"
        Me.lblSEQ.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblLOtcode
        '
        Me.lblLOtcode.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.lblLOtcode.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblLOtcode.Location = New System.Drawing.Point(184, 20)
        Me.lblLOtcode.Name = "lblLOtcode"
        Me.lblLOtcode.Size = New System.Drawing.Size(121, 48)
        Me.lblLOtcode.TabIndex = 44
        Me.lblLOtcode.Text = "RQVB"
        Me.lblLOtcode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblProcess
        '
        Me.lblProcess.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.lblProcess.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblProcess.Location = New System.Drawing.Point(311, 20)
        Me.lblProcess.Name = "lblProcess"
        Me.lblProcess.Size = New System.Drawing.Size(212, 23)
        Me.lblProcess.TabIndex = 45
        Me.lblProcess.Text = "Fr ,P2 ,S1"
        Me.lblProcess.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Azure
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.lblJobState)
        Me.Panel1.Controls.Add(Me.Label14)
        Me.Panel1.Controls.Add(Me.Label13)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.lblPlant)
        Me.Panel1.Controls.Add(Me.lblProcess)
        Me.Panel1.Controls.Add(Me.lblSEQ)
        Me.Panel1.Controls.Add(Me.lblLOtcode)
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(556, 82)
        Me.Panel1.TabIndex = 46
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label8.Location = New System.Drawing.Point(311, 52)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(43, 13)
        Me.Label8.TabIndex = 52
        Me.Label8.Text = "Status :"
        '
        'lblJobState
        '
        Me.lblJobState.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.lblJobState.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblJobState.Location = New System.Drawing.Point(360, 45)
        Me.lblJobState.Name = "lblJobState"
        Me.lblJobState.Size = New System.Drawing.Size(162, 23)
        Me.lblJobState.TabIndex = 51
        Me.lblJobState.Text = "WORKING"
        Me.lblJobState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label14.Location = New System.Drawing.Point(363, 4)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(45, 13)
        Me.Label14.TabIndex = 50
        Me.Label14.Text = "Process"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label13.Location = New System.Drawing.Point(224, 4)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(58, 13)
        Me.Label13.TabIndex = 49
        Me.Label13.Text = "LOTCODE"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label12.Location = New System.Drawing.Point(118, 4)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(29, 13)
        Me.Label12.TabIndex = 48
        Me.Label12.Text = "SEQ"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label11.Location = New System.Drawing.Point(29, 4)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(41, 13)
        Me.Label11.TabIndex = 47
        Me.Label11.Text = "Source"
        '
        'lblVersion
        '
        Me.lblVersion.AutoSize = True
        Me.lblVersion.Location = New System.Drawing.Point(582, 74)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(103, 13)
        Me.lblVersion.TabIndex = 47
        Me.lblVersion.Text = "Version 19/08/2018"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblStatus})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 579)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(723, 22)
        Me.StatusStrip1.TabIndex = 49
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'lblStatus
        '
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(0, 17)
        '
        'btnProcessConfig
        '
        Me.btnProcessConfig.BackColor = System.Drawing.Color.DimGray
        Me.btnProcessConfig.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnProcessConfig.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnProcessConfig.ForeColor = System.Drawing.Color.White
        Me.btnProcessConfig.Location = New System.Drawing.Point(622, 376)
        Me.btnProcessConfig.Name = "btnProcessConfig"
        Me.btnProcessConfig.Size = New System.Drawing.Size(75, 41)
        Me.btnProcessConfig.TabIndex = 50
        Me.btnProcessConfig.Text = "Process Config"
        Me.btnProcessConfig.UseVisualStyleBackColor = False
        '
        'lbllen
        '
        Me.lbllen.AutoSize = True
        Me.lbllen.Location = New System.Drawing.Point(379, 8)
        Me.lbllen.Name = "lbllen"
        Me.lbllen.Size = New System.Drawing.Size(0, 13)
        Me.lbllen.TabIndex = 51
        '
        'btnSaveCCR
        '
        Me.btnSaveCCR.BackColor = System.Drawing.Color.DimGray
        Me.btnSaveCCR.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveCCR.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnSaveCCR.ForeColor = System.Drawing.Color.White
        Me.btnSaveCCR.Location = New System.Drawing.Point(163, 116)
        Me.btnSaveCCR.Name = "btnSaveCCR"
        Me.btnSaveCCR.Size = New System.Drawing.Size(54, 23)
        Me.btnSaveCCR.TabIndex = 52
        Me.btnSaveCCR.Text = "Save"
        Me.btnSaveCCR.UseVisualStyleBackColor = False
        '
        'btnserverConfig
        '
        Me.btnserverConfig.BackColor = System.Drawing.Color.DimGray
        Me.btnserverConfig.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnserverConfig.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnserverConfig.ForeColor = System.Drawing.Color.White
        Me.btnserverConfig.Location = New System.Drawing.Point(622, 422)
        Me.btnserverConfig.Name = "btnserverConfig"
        Me.btnserverConfig.Size = New System.Drawing.Size(75, 41)
        Me.btnserverConfig.TabIndex = 182
        Me.btnserverConfig.Text = "Server Config"
        Me.btnserverConfig.UseVisualStyleBackColor = False
        '
        'btnCancelCCR
        '
        Me.btnCancelCCR.BackColor = System.Drawing.Color.DimGray
        Me.btnCancelCCR.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancelCCR.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnCancelCCR.ForeColor = System.Drawing.Color.White
        Me.btnCancelCCR.Location = New System.Drawing.Point(223, 116)
        Me.btnCancelCCR.Name = "btnCancelCCR"
        Me.btnCancelCCR.Size = New System.Drawing.Size(54, 23)
        Me.btnCancelCCR.TabIndex = 188
        Me.btnCancelCCR.Text = "Cancel"
        Me.btnCancelCCR.UseVisualStyleBackColor = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(148, 93)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(32, 13)
        Me.Label7.TabIndex = 189
        Me.Label7.Text = "Port :"
        '
        'CBSendCCR
        '
        Me.CBSendCCR.AutoSize = True
        Me.CBSendCCR.Checked = True
        Me.CBSendCCR.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CBSendCCR.Location = New System.Drawing.Point(7, 6)
        Me.CBSendCCR.Name = "CBSendCCR"
        Me.CBSendCCR.Size = New System.Drawing.Size(73, 17)
        Me.CBSendCCR.TabIndex = 190
        Me.CBSendCCR.Text = "SendCCR"
        Me.CBSendCCR.UseVisualStyleBackColor = True
        '
        'CBDeleteExport
        '
        Me.CBDeleteExport.AutoSize = True
        Me.CBDeleteExport.Checked = True
        Me.CBDeleteExport.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CBDeleteExport.Location = New System.Drawing.Point(7, 52)
        Me.CBDeleteExport.Name = "CBDeleteExport"
        Me.CBDeleteExport.Size = New System.Drawing.Size(90, 17)
        Me.CBDeleteExport.TabIndex = 191
        Me.CBDeleteExport.Text = "Delete Export"
        Me.CBDeleteExport.UseVisualStyleBackColor = True
        '
        'btnStart
        '
        Me.btnStart.BackColor = System.Drawing.Color.Red
        Me.btnStart.FlatAppearance.BorderSize = 0
        Me.btnStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnStart.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnStart.ForeColor = System.Drawing.Color.White
        Me.btnStart.Location = New System.Drawing.Point(585, 14)
        Me.btnStart.Name = "btnStart"
        Me.btnStart.Size = New System.Drawing.Size(109, 57)
        Me.btnStart.TabIndex = 192
        Me.btnStart.Text = "START"
        Me.btnStart.UseVisualStyleBackColor = False
        '
        'CBCheckComplete
        '
        Me.CBCheckComplete.AutoSize = True
        Me.CBCheckComplete.Checked = True
        Me.CBCheckComplete.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CBCheckComplete.Location = New System.Drawing.Point(7, 121)
        Me.CBCheckComplete.Name = "CBCheckComplete"
        Me.CBCheckComplete.Size = New System.Drawing.Size(101, 17)
        Me.CBCheckComplete.TabIndex = 193
        Me.CBCheckComplete.Text = "CheckComplete"
        Me.CBCheckComplete.UseVisualStyleBackColor = True
        '
        'CBgetExport
        '
        Me.CBgetExport.AutoSize = True
        Me.CBgetExport.Checked = True
        Me.CBgetExport.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CBgetExport.Location = New System.Drawing.Point(7, 29)
        Me.CBgetExport.Name = "CBgetExport"
        Me.CBgetExport.Size = New System.Drawing.Size(71, 17)
        Me.CBgetExport.TabIndex = 194
        Me.CBgetExport.Text = "getExport"
        Me.CBgetExport.UseVisualStyleBackColor = True
        '
        'DGVbuffer
        '
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.LightSkyBlue
        Me.DGVbuffer.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle3
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.CornflowerBlue
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGVbuffer.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.DGVbuffer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVbuffer.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.No, Me.LOT, Me.SEQ, Me.address})
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DGVbuffer.DefaultCellStyle = DataGridViewCellStyle5
        Me.DGVbuffer.GridColor = System.Drawing.Color.Black
        Me.DGVbuffer.Location = New System.Drawing.Point(340, 182)
        Me.DGVbuffer.Name = "DGVbuffer"
        Me.DGVbuffer.RowHeadersVisible = False
        Me.DGVbuffer.Size = New System.Drawing.Size(276, 380)
        Me.DGVbuffer.TabIndex = 195
        '
        'No
        '
        Me.No.DataPropertyName = "No"
        Me.No.HeaderText = "No."
        Me.No.Name = "No"
        Me.No.Width = 40
        '
        'LOT
        '
        Me.LOT.DataPropertyName = "LOT"
        Me.LOT.HeaderText = "LOT"
        Me.LOT.Name = "LOT"
        Me.LOT.Width = 80
        '
        'SEQ
        '
        Me.SEQ.DataPropertyName = "SEQ"
        Me.SEQ.HeaderText = "SEQ"
        Me.SEQ.Name = "SEQ"
        Me.SEQ.Width = 50
        '
        'address
        '
        Me.address.DataPropertyName = "address"
        Me.address.HeaderText = "address"
        Me.address.Name = "address"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.Location = New System.Drawing.Point(337, 159)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(93, 20)
        Me.Label2.TabIndex = 196
        Me.Label2.Text = "PIS Buffer"
        '
        'txtbuffer_LOT
        '
        Me.txtbuffer_LOT.Location = New System.Drawing.Point(431, 161)
        Me.txtbuffer_LOT.Name = "txtbuffer_LOT"
        Me.txtbuffer_LOT.Size = New System.Drawing.Size(57, 20)
        Me.txtbuffer_LOT.TabIndex = 197
        Me.txtbuffer_LOT.Text = "P1-R140"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(450, 147)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(28, 13)
        Me.Label16.TabIndex = 198
        Me.Label16.Text = "LOT"
        '
        'txtbuffer_SEQ
        '
        Me.txtbuffer_SEQ.Location = New System.Drawing.Point(527, 161)
        Me.txtbuffer_SEQ.Name = "txtbuffer_SEQ"
        Me.txtbuffer_SEQ.Size = New System.Drawing.Size(62, 20)
        Me.txtbuffer_SEQ.TabIndex = 199
        Me.txtbuffer_SEQ.Text = "P1-D1001"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(548, 145)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(29, 13)
        Me.Label19.TabIndex = 200
        Me.Label19.Text = "SEQ"
        '
        'btnGetBuffer
        '
        Me.btnGetBuffer.AutoSize = True
        Me.btnGetBuffer.Checked = True
        Me.btnGetBuffer.CheckState = System.Windows.Forms.CheckState.Checked
        Me.btnGetBuffer.Location = New System.Drawing.Point(7, 144)
        Me.btnGetBuffer.Name = "btnGetBuffer"
        Me.btnGetBuffer.Size = New System.Drawing.Size(69, 17)
        Me.btnGetBuffer.TabIndex = 202
        Me.btnGetBuffer.Text = "getBuffer"
        Me.btnGetBuffer.UseVisualStyleBackColor = True
        '
        'txtbuffer_LOTLEN
        '
        Me.txtbuffer_LOTLEN.Location = New System.Drawing.Point(494, 161)
        Me.txtbuffer_LOTLEN.Name = "txtbuffer_LOTLEN"
        Me.txtbuffer_LOTLEN.Size = New System.Drawing.Size(27, 20)
        Me.txtbuffer_LOTLEN.TabIndex = 203
        Me.txtbuffer_LOTLEN.Text = "80"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(496, 147)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(25, 13)
        Me.Label21.TabIndex = 204
        Me.Label21.Text = "Len"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(597, 145)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(25, 13)
        Me.Label22.TabIndex = 206
        Me.Label22.Text = "Len"
        '
        'txtbuffer_SEQLEN
        '
        Me.txtbuffer_SEQLEN.Location = New System.Drawing.Point(595, 161)
        Me.txtbuffer_SEQLEN.Name = "txtbuffer_SEQLEN"
        Me.txtbuffer_SEQLEN.Size = New System.Drawing.Size(27, 20)
        Me.txtbuffer_SEQLEN.TabIndex = 205
        Me.txtbuffer_SEQLEN.Text = "160"
        '
        'PNExport
        '
        Me.PNExport.Location = New System.Drawing.Point(3, 364)
        Me.PNExport.Name = "PNExport"
        Me.PNExport.Size = New System.Drawing.Size(331, 198)
        Me.PNExport.TabIndex = 207
        '
        'btnResend
        '
        Me.btnResend.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnResend.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnResend.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnResend.ForeColor = System.Drawing.Color.White
        Me.btnResend.Location = New System.Drawing.Point(302, 88)
        Me.btnResend.Name = "btnResend"
        Me.btnResend.Size = New System.Drawing.Size(62, 23)
        Me.btnResend.TabIndex = 208
        Me.btnResend.Text = "ReSend"
        Me.btnResend.UseVisualStyleBackColor = False
        '
        'ServerPN
        '
        Me.ServerPN.Controls.Add(Me.Label75)
        Me.ServerPN.Controls.Add(Me.ServerPingPN)
        Me.ServerPN.Controls.Add(Me.lblServer1)
        Me.ServerPN.Controls.Add(Me.ServerPingPN2)
        Me.ServerPN.Controls.Add(Me.lblServer2)
        Me.ServerPN.Controls.Add(Me.Label74)
        Me.ServerPN.Controls.Add(Me.ServerLinkPN)
        Me.ServerPN.Controls.Add(Me.ServerLinkPN2)
        Me.ServerPN.Location = New System.Drawing.Point(412, 88)
        Me.ServerPN.Name = "ServerPN"
        Me.ServerPN.Size = New System.Drawing.Size(147, 45)
        Me.ServerPN.TabIndex = 224
        '
        'Label75
        '
        Me.Label75.AutoSize = True
        Me.Label75.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label75.ForeColor = System.Drawing.Color.DodgerBlue
        Me.Label75.Location = New System.Drawing.Point(3, 1)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(20, 13)
        Me.Label75.TabIndex = 205
        Me.Label75.Text = "S1"
        '
        'ServerPingPN
        '
        Me.ServerPingPN.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.ServerPingPN.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.ServerPingPN.Location = New System.Drawing.Point(25, 0)
        Me.ServerPingPN.Name = "ServerPingPN"
        Me.ServerPingPN.Size = New System.Drawing.Size(20, 18)
        Me.ServerPingPN.TabIndex = 202
        '
        'lblServer1
        '
        Me.lblServer1.AutoSize = True
        Me.lblServer1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblServer1.ForeColor = System.Drawing.Color.DodgerBlue
        Me.lblServer1.Location = New System.Drawing.Point(71, 2)
        Me.lblServer1.Name = "lblServer1"
        Me.lblServer1.Size = New System.Drawing.Size(68, 13)
        Me.lblServer1.TabIndex = 204
        Me.lblServer1.Text = "STMTGW01"
        '
        'ServerPingPN2
        '
        Me.ServerPingPN2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.ServerPingPN2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.ServerPingPN2.Location = New System.Drawing.Point(25, 22)
        Me.ServerPingPN2.Name = "ServerPingPN2"
        Me.ServerPingPN2.Size = New System.Drawing.Size(20, 18)
        Me.ServerPingPN2.TabIndex = 206
        '
        'lblServer2
        '
        Me.lblServer2.AutoSize = True
        Me.lblServer2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblServer2.ForeColor = System.Drawing.Color.DodgerBlue
        Me.lblServer2.Location = New System.Drawing.Point(72, 24)
        Me.lblServer2.Name = "lblServer2"
        Me.lblServer2.Size = New System.Drawing.Size(68, 13)
        Me.lblServer2.TabIndex = 207
        Me.lblServer2.Text = "STMTGW01"
        '
        'Label74
        '
        Me.Label74.AutoSize = True
        Me.Label74.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label74.ForeColor = System.Drawing.Color.DodgerBlue
        Me.Label74.Location = New System.Drawing.Point(2, 24)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(20, 13)
        Me.Label74.TabIndex = 208
        Me.Label74.Text = "S2"
        '
        'ServerLinkPN
        '
        Me.ServerLinkPN.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.ServerLinkPN.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.ServerLinkPN.Location = New System.Drawing.Point(46, 0)
        Me.ServerLinkPN.Name = "ServerLinkPN"
        Me.ServerLinkPN.Size = New System.Drawing.Size(20, 18)
        Me.ServerLinkPN.TabIndex = 203
        '
        'ServerLinkPN2
        '
        Me.ServerLinkPN2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.ServerLinkPN2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.ServerLinkPN2.Location = New System.Drawing.Point(46, 22)
        Me.ServerLinkPN2.Name = "ServerLinkPN2"
        Me.ServerLinkPN2.Size = New System.Drawing.Size(20, 18)
        Me.ServerLinkPN2.TabIndex = 209
        '
        'TimerServerLink
        '
        Me.TimerServerLink.Enabled = True
        Me.TimerServerLink.Interval = 1000
        '
        'CBSendQ
        '
        Me.CBSendQ.AutoSize = True
        Me.CBSendQ.Checked = True
        Me.CBSendQ.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CBSendQ.Location = New System.Drawing.Point(7, 75)
        Me.CBSendQ.Name = "CBSendQ"
        Me.CBSendQ.Size = New System.Drawing.Size(59, 17)
        Me.CBSendQ.TabIndex = 225
        Me.CBSendQ.Text = "SendQ"
        Me.CBSendQ.UseVisualStyleBackColor = True
        '
        'CBSetLamp
        '
        Me.CBSetLamp.AutoSize = True
        Me.CBSetLamp.Checked = True
        Me.CBSetLamp.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CBSetLamp.Location = New System.Drawing.Point(7, 98)
        Me.CBSetLamp.Name = "CBSetLamp"
        Me.CBSetLamp.Size = New System.Drawing.Size(68, 17)
        Me.CBSetLamp.TabIndex = 226
        Me.CBSetLamp.Text = "SetLamp"
        Me.CBSetLamp.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(3, 122)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(99, 13)
        Me.Label9.TabIndex = 227
        Me.Label9.Text = "Replace SEQ 000 :"
        '
        'txtReplace000
        '
        Me.txtReplace000.Location = New System.Drawing.Point(108, 119)
        Me.txtReplace000.Name = "txtReplace000"
        Me.txtReplace000.Size = New System.Drawing.Size(44, 20)
        Me.txtReplace000.TabIndex = 4
        Me.txtReplace000.Text = "111"
        '
        'PNmanualCCR
        '
        Me.PNmanualCCR.Controls.Add(Me.txtBCdata)
        Me.PNmanualCCR.Controls.Add(Me.Label3)
        Me.PNmanualCCR.Controls.Add(Me.btnFsend)
        Me.PNmanualCCR.Controls.Add(Me.lbllen)
        Me.PNmanualCCR.Location = New System.Drawing.Point(79, 563)
        Me.PNmanualCCR.Name = "PNmanualCCR"
        Me.PNmanualCCR.Size = New System.Drawing.Size(488, 30)
        Me.PNmanualCCR.TabIndex = 229
        Me.PNmanualCCR.Visible = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(621, 549)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(99, 13)
        Me.Label10.TabIndex = 230
        Me.Label10.Text = "Ctrl+Q Manual CCR"
        '
        'PNDebug
        '
        Me.PNDebug.Controls.Add(Me.CBSendCCR)
        Me.PNDebug.Controls.Add(Me.CBDeleteExport)
        Me.PNDebug.Controls.Add(Me.CBCheckComplete)
        Me.PNDebug.Controls.Add(Me.CBgetExport)
        Me.PNDebug.Controls.Add(Me.btnGetBuffer)
        Me.PNDebug.Controls.Add(Me.CBSetLamp)
        Me.PNDebug.Controls.Add(Me.CBSendQ)
        Me.PNDebug.Location = New System.Drawing.Point(617, 182)
        Me.PNDebug.Name = "PNDebug"
        Me.PNDebug.Size = New System.Drawing.Size(106, 171)
        Me.PNDebug.TabIndex = 231
        Me.PNDebug.Visible = False
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(621, 563)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(71, 13)
        Me.Label15.TabIndex = 232
        Me.Label15.Text = "Ctrl+D Debug"
        '
        'btnCancelBuffer
        '
        Me.btnCancelBuffer.BackColor = System.Drawing.Color.DimGray
        Me.btnCancelBuffer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancelBuffer.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnCancelBuffer.ForeColor = System.Drawing.Color.White
        Me.btnCancelBuffer.Location = New System.Drawing.Point(669, 159)
        Me.btnCancelBuffer.Name = "btnCancelBuffer"
        Me.btnCancelBuffer.Size = New System.Drawing.Size(51, 23)
        Me.btnCancelBuffer.TabIndex = 234
        Me.btnCancelBuffer.Text = "Cancel"
        Me.btnCancelBuffer.UseVisualStyleBackColor = False
        '
        'btnSaveBuffer
        '
        Me.btnSaveBuffer.BackColor = System.Drawing.Color.DimGray
        Me.btnSaveBuffer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveBuffer.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnSaveBuffer.ForeColor = System.Drawing.Color.White
        Me.btnSaveBuffer.Location = New System.Drawing.Point(624, 159)
        Me.btnSaveBuffer.Name = "btnSaveBuffer"
        Me.btnSaveBuffer.Size = New System.Drawing.Size(42, 23)
        Me.btnSaveBuffer.TabIndex = 233
        Me.btnSaveBuffer.Text = "Save"
        Me.btnSaveBuffer.UseVisualStyleBackColor = False
        '
        'btnDeleteFirst
        '
        Me.btnDeleteFirst.BackColor = System.Drawing.Color.DimGray
        Me.btnDeleteFirst.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeleteFirst.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnDeleteFirst.ForeColor = System.Drawing.Color.White
        Me.btnDeleteFirst.Location = New System.Drawing.Point(624, 523)
        Me.btnDeleteFirst.Name = "btnDeleteFirst"
        Me.btnDeleteFirst.Size = New System.Drawing.Size(77, 23)
        Me.btnDeleteFirst.TabIndex = 235
        Me.btnDeleteFirst.Text = "Delete F"
        Me.btnDeleteFirst.UseVisualStyleBackColor = False
        Me.btnDeleteFirst.Visible = False
        '
        'CBDeleteF
        '
        Me.CBDeleteF.AutoSize = True
        Me.CBDeleteF.Location = New System.Drawing.Point(632, 500)
        Me.CBDeleteF.Name = "CBDeleteF"
        Me.CBDeleteF.Size = New System.Drawing.Size(76, 17)
        Me.CBDeleteF.TabIndex = 236
        Me.CBDeleteF.Text = "DeleteFirst"
        Me.CBDeleteF.UseVisualStyleBackColor = True
        Me.CBDeleteF.Visible = False
        '
        'txtDeleteFaddr
        '
        Me.txtDeleteFaddr.Location = New System.Drawing.Point(624, 474)
        Me.txtDeleteFaddr.Name = "txtDeleteFaddr"
        Me.txtDeleteFaddr.Size = New System.Drawing.Size(42, 20)
        Me.txtDeleteFaddr.TabIndex = 237
        Me.txtDeleteFaddr.Text = "D1001"
        Me.txtDeleteFaddr.Visible = False
        '
        'txtDeleteFlen
        '
        Me.txtDeleteFlen.Location = New System.Drawing.Point(672, 474)
        Me.txtDeleteFlen.Name = "txtDeleteFlen"
        Me.txtDeleteFlen.Size = New System.Drawing.Size(26, 20)
        Me.txtDeleteFlen.TabIndex = 238
        Me.txtDeleteFlen.Text = "4"
        Me.txtDeleteFlen.Visible = False
        '
        'btnDisconnect
        '
        Me.btnDisconnect.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnDisconnect.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDisconnect.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnDisconnect.ForeColor = System.Drawing.Color.White
        Me.btnDisconnect.Location = New System.Drawing.Point(304, 116)
        Me.btnDisconnect.Name = "btnDisconnect"
        Me.btnDisconnect.Size = New System.Drawing.Size(80, 23)
        Me.btnDisconnect.TabIndex = 239
        Me.btnDisconnect.Text = "Disconnect"
        Me.btnDisconnect.UseVisualStyleBackColor = False
        '
        'lblRetryresend
        '
        Me.lblRetryresend.AutoSize = True
        Me.lblRetryresend.Location = New System.Drawing.Point(565, 89)
        Me.lblRetryresend.Name = "lblRetryresend"
        Me.lblRetryresend.Size = New System.Drawing.Size(78, 13)
        Me.lblRetryresend.TabIndex = 240
        Me.lblRetryresend.Text = "Retry Resend :"
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DimGray
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(639, 105)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 41)
        Me.Button2.TabIndex = 241
        Me.Button2.Text = "Process Config"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'SendSeqUC
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.lblRetryresend)
        Me.Controls.Add(Me.btnDisconnect)
        Me.Controls.Add(Me.txtDeleteFlen)
        Me.Controls.Add(Me.txtDeleteFaddr)
        Me.Controls.Add(Me.CBDeleteF)
        Me.Controls.Add(Me.btnDeleteFirst)
        Me.Controls.Add(Me.btnCancelBuffer)
        Me.Controls.Add(Me.btnSaveBuffer)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.PNDebug)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.PNmanualCCR)
        Me.Controls.Add(Me.txtReplace000)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.ServerPN)
        Me.Controls.Add(Me.btnResend)
        Me.Controls.Add(Me.PNExport)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.txtbuffer_SEQLEN)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.txtbuffer_LOTLEN)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.txtbuffer_SEQ)
        Me.Controls.Add(Me.txtbuffer_LOT)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.DGVbuffer)
        Me.Controls.Add(Me.btnStart)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.btnCancelCCR)
        Me.Controls.Add(Me.btnserverConfig)
        Me.Controls.Add(Me.btnSaveCCR)
        Me.Controls.Add(Me.btnProcessConfig)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.lblVersion)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.btnConnect)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtCCRPort)
        Me.Controls.Add(Me.txtCCRip)
        Me.Name = "SendSeqUC"
        Me.Size = New System.Drawing.Size(723, 601)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DGVComplete, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.DGVbuffer, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ServerPN.ResumeLayout(False)
        Me.ServerPN.PerformLayout()
        Me.PNmanualCCR.ResumeLayout(False)
        Me.PNmanualCCR.PerformLayout()
        Me.PNDebug.ResumeLayout(False)
        Me.PNDebug.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents txtPISip As TextBox
    Friend WithEvents txtPisPort As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents txtCCRPort As TextBox
    Friend WithEvents txtCCRip As TextBox
    Friend WithEvents btnFsend As Button
    Friend WithEvents Label3 As Label
    Friend WithEvents txtBCdata As TextBox
    Friend WithEvents Timer1 As Timer
    Friend WithEvents btnConnect As Button
    Friend WithEvents lblPlant As Label
    Friend WithEvents lblSEQ As Label
    Friend WithEvents lblLOtcode As Label
    Friend WithEvents lblProcess As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label14 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents lblVersion As Label
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents lblStatus As ToolStripStatusLabel
    Friend WithEvents btnProcessConfig As Button
    Friend WithEvents lbllen As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents lblJobState As Label
    Friend WithEvents btnSavePis As Button
    Friend WithEvents btnSaveCCR As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents btnserverConfig As Button
    Friend WithEvents btnCancelCCR As Button
    Friend WithEvents Label7 As Label
    Friend WithEvents CBSendCCR As CheckBox
    Friend WithEvents CBDeleteExport As CheckBox
    Friend WithEvents btnStart As Button
    Friend WithEvents CBCheckComplete As CheckBox
    Friend WithEvents CBgetExport As CheckBox
    Friend WithEvents DGVbuffer As DataGridView
    Friend WithEvents Label2 As Label
    Friend WithEvents txtbuffer_LOT As TextBox
    Friend WithEvents Label16 As Label
    Friend WithEvents txtbuffer_SEQ As TextBox
    Friend WithEvents Label19 As Label
    Friend WithEvents No As DataGridViewTextBoxColumn
    Friend WithEvents LOT As DataGridViewTextBoxColumn
    Friend WithEvents SEQ As DataGridViewTextBoxColumn
    Friend WithEvents address As DataGridViewTextBoxColumn
    Friend WithEvents DGVComplete As DataGridView
    Friend WithEvents Label6 As Label
    Friend WithEvents txtCURPIS As TextBox
    Friend WithEvents txtOLDPIS As TextBox
    Friend WithEvents Label20 As Label
    Friend WithEvents Complete_ADDR As DataGridViewTextBoxColumn
    Friend WithEvents complete_type As DataGridViewTextBoxColumn
    Friend WithEvents Complete_Lenght As DataGridViewTextBoxColumn
    Friend WithEvents Complete_value As DataGridViewTextBoxColumn
    Friend WithEvents btnGetBuffer As CheckBox
    Friend WithEvents txtbuffer_LOTLEN As TextBox
    Friend WithEvents Label21 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents txtbuffer_SEQLEN As TextBox
    Friend WithEvents PNExport As Panel
    Friend WithEvents btnSetBuffer As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents CBgetComplete_Buffer As CheckBox
    Friend WithEvents CBgetComplete_Change As CheckBox
    Friend WithEvents btnResend As Button
    Friend WithEvents ServerPN As Panel
    Friend WithEvents Label75 As Label
    Friend WithEvents ServerPingPN As Panel
    Public WithEvents lblServer1 As Label
    Friend WithEvents ServerPingPN2 As Panel
    Public WithEvents lblServer2 As Label
    Friend WithEvents Label74 As Label
    Friend WithEvents ServerLinkPN As Panel
    Friend WithEvents ServerLinkPN2 As Panel
    Friend WithEvents TimerServerLink As Timer
    Friend WithEvents CBSendQ As CheckBox
    Friend WithEvents CBSetLamp As CheckBox
    Friend WithEvents Label9 As Label
    Friend WithEvents txtReplace000 As TextBox
    Friend WithEvents PNmanualCCR As Panel
    Friend WithEvents Label10 As Label
    Friend WithEvents PNDebug As Panel
    Friend WithEvents Label15 As Label
    Friend WithEvents txtBuffer As Label
    Friend WithEvents btnCancelBuffer As Button
    Friend WithEvents btnSaveBuffer As Button
    Friend WithEvents btnDeleteFirst As Button
    Friend WithEvents CBDeleteF As CheckBox
    Friend WithEvents txtDeleteFaddr As TextBox
    Friend WithEvents txtDeleteFlen As TextBox
    Friend WithEvents btnDisconnect As Button
    Friend WithEvents lblRetryresend As Label
    Friend WithEvents Button2 As Button
End Class
