﻿Imports System.Threading
Imports System.Runtime.InteropServices
Public Class SendSeqUC
    WithEvents Winsock1 As New Winsock

    Dim DAL As New DB.SqlConnect


    Dim pClsPC10G As New clsPc10G

    Dim timer_Process As Threading.Timer


    Dim Process_Start As Boolean = True
    Dim DTbuffer, dtGetComplete As New DataTable
    Dim selectgetComplete As String = "select * from tb_getcomplete"

    Dim Export As New getExportUC
    Private Sub SendSeqUC_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ValidSQLite()

        LoadProcess()
        LoadCurState()


        LoadSetting()

        initDTbuffer()
        pClsPC10G.IP = txtPISip.Text
        pClsPC10G.PORT = txtPisPort.Text

        SetDoubleBuffered(DGVbuffer)
        Export.sendseq = Me
        Export.pClsPC10G = pClsPC10G
        PNExport.Controls.Add(Export)


        RegisterHotKey(Me.Handle, 100, MOD_CTRL, Keys.Q)
        RegisterHotKey(Me.Handle, 200, MOD_CTRL, Keys.D)

        timer_serverstate = New Threading.Timer(TDS_Serverlink, Nothing, 0, 3)
        Dim TDS2 As TimerCallback = AddressOf Process
        timer_Process = New Threading.Timer(TDS2, Nothing, 1000, 1000)
        Try


            Dim sql, str As String
            sql = "select ConfirmSendCCR From tb_pitch  where Linename like '" & sLinename & "' and Pitch =" & sPitch & " and [No] =" & sStation

            str = DAL.getScalar(sql, Connstr)
            If str = "True" Then
                ConfirmSendCCR = True
            Else
                ConfirmSendCCR = False
            End If
        Catch ex As Exception
            MsgBox("LoadSendCCR:" & ex.Message)
        End Try
    End Sub
    Sub ValidSQLite()
        Dim sql As String
        sql = "create table IF NOT EXISTS tb_getComplete (Address TEXT PRIMARY KEY,type TEXT,lenght text,value text)"
        DALlite.GetExecute(sql, Connstr_lite)

    End Sub


    Sub LoadSetting()
            Dim sql As String
            sql = "select * from setting"
            Dim dt As New DataTable
            dt = DALlite.getDataTable(sql, Connstr_lite)
        For Each dr As DataRow In dt.Rows


            If dr("name").ToString.ToUpper = "CCR_Replace000".ToUpper Then
                txtReplace000.Text = dr("value").ToString
            End If
            If dr("name").ToString.ToUpper = "CCR_IP".ToUpper Then
                txtCCRip.Text = dr("value").ToString
            End If
            If dr("name").ToString.ToUpper = "CCR_PORT".ToUpper Then
                txtCCRPort.Text = dr("value").ToString
            End If
            If dr("name").ToString.ToUpper = "Complete_IP".ToUpper Then
                txtPISip.Text = dr("value").ToString
            End If
            If dr("name").ToString.ToUpper = "Complete_PORT".ToUpper Then
                txtPisPort.Text = dr("value").ToString
            End If
            If dr("name").ToString.ToUpper = "CBgetComplete_Change".ToUpper And dr("value").ToString = "True" Then

                CBgetComplete_Change.Checked = True
            End If
            If dr("name").ToString.ToUpper = "CBgetComplete_Buffer".ToUpper And dr("value").ToString = "True" Then
                CBgetComplete_Buffer.Checked = True
            End If
            If dr("name").ToString.ToUpper = "Buffer".ToUpper Then
                txtBuffer.Text = dr("value").ToString
            End If
            If dr("name").ToString.ToUpper = "buffer_lot".ToUpper Then
                txtbuffer_LOT.Text = dr("value").ToString
            End If
            If dr("name").ToString.ToUpper = "buffer_lotlen".ToUpper Then
                txtbuffer_LOTLEN.Text = dr("value").ToString
            End If
            If dr("name").ToString.ToUpper = "buffer_seq".ToUpper Then
                txtbuffer_SEQ.Text = dr("value").ToString
            End If
            If dr("name").ToString.ToUpper = "buffer_seqlen".ToUpper Then
                txtbuffer_SEQLEN.Text = dr("value").ToString
            End If



        Next

        dtGetComplete = DALlite.getDataTable(selectgetComplete, Connstr_lite)
        DGVComplete.DataSource = dtGetComplete
        CCRip = txtCCRip.Text
        CCRport = txtCCRPort.Text
        Replace000 = txtReplace000.Text
    End Sub
    Dim lastDataArrival As DateTime
    Private Sub Winsock1_DataArrival(ByVal sender As Winsock, ByVal BytesTotal As Integer) Handles Winsock1.DataArrival

        Dim ReceiveData As String
        Winsock1.GetData(ReceiveData)

        Try
            '   Debug.WriteLine(Now.ToString("ss"))
            Try
                lastDataArrival = Now
            Catch ex As Exception

            End Try

            ReceiveData = ReceiveData.Replace(vbNullChar, "")
            If ReceiveData = "" Then Exit Sub
            If ReceiveData.Substring(24, 2) = "00" Then
                ConfirmSendCCR = True
                RetryResend = 0
                UpdateConfirmCCR()
                SaveMSG("ConfirmSendCCR:TRUE", "ACT")
            Else
                SaveMSG("ConfirmSendCCR:False", "ERR")
            End If

            If ReceiveData.Substring(24, 2) = "76" Then

                '  Winsock1.Close() 'ใส่แล้ว ไม่ Reconnect ต้อง close ที่ตัวรับด้วย
                SaveMSG("ConfirmSendCCR:76", "ERR")
                ConfirmSendCCR = False
                UpdateConfirmCCR()
            End If
        Catch ex As Exception
            SaveMSG("ConfirmSendCCR:" & ex.Message, "ACT")
            ShowStatus("ConfirmSendCCR:False", 3)
        End Try
    End Sub

    Sub LoadProcess()
        Dim sql As String
        sql = "select * from ProcessConfig"
        Dim dt As New DataTable
        dt = DALlite.getDataTable(sql, Connstr_lite)
        For Each dr As DataRow In dt.Rows
            sLinename = dr("LineName").ToString()
            sPitch = dr("Pitch").ToString()
            sStation = dr("Station").ToString()
        Next
        lblProcess.Text = sLinename & ", P" & sPitch & ", " & "S" & sStation
        ProcessName = lblProcess.Text
    End Sub
    Dim CCRip, CCRport, Replace000 As String
    Dim ConfirmSendCCR As Boolean = True
    Function SendCCR(strSend As String)
        'If connected = False Then
        '    MsgBox("Please Connect CCR")
        '    Exit Sub
        'End If
        Try
            'If Winsock1 Is Nothing Then
            '    Winsock1 = New Winsock
            'End If

            If Winsock1.GetState <> Winsock.WinsockStates.Connected Then
                SaveMSG("Connect CCR", "ACT")

                Winsock1.Close()


                Threading.Thread.Sleep(1000)
                Winsock1.Connect(CCRip, CCRport)
                Threading.Thread.Sleep(1000)
            End If
        Catch ex As Exception
            ShowStatus("Connect CCR: " & ex.Message, 3, Color.Red)
            SaveMSG("Connect CCR: " & ex.Message, "ERR")
        End Try
        Try
            ConfirmSendCCR = False
            UpdateConfirmCCR()
            ' strSend &= "s"
            Winsock1.Send(strSend)
            txtBCdata.Text = strSend
            SaveMSG("Send CCR:" & CurSEQ & "-" & CurLOT, "ACT")

            '///////////////
#Region "ConfirmWithBuffer"


            'Dim lastseq, lastlot As String
            'Do
            '    Thread.Sleep(2000)
            '    For Each dr As DataRow In DTbuffer.Rows
            '        If dr("LOT").ToString = "" Then
            '            Exit For
            '        Else
            '            lastlot = dr("LOT").ToString
            '            lastseq = dr("seq").ToString
            '        End If
            '    Next

            '    If lastlot & lastseq = lblLOtcode.Text & lblSEQ.Text Then
            '        ConfirmSendCCR = True
            '        Exit Do
            '    Else
            '        SaveMSG("Send CCR Retry:" & lblSEQ.Text & "-" & lblLOtcode.Text, "ACT")
            '    End If
            'Loop
#End Region
            '//////////

            Return True
        Catch ex As Exception
            ShowStatus("Send CCR: " & ex.Message, 3, Color.Red)
            SaveMSG("Send CCR: " & ex.Message, "ERR")
            Return False
        End Try

    End Function

    Private Sub btnFsend_Click(sender As Object, e As EventArgs) Handles btnFsend.Click

        SendCCR(txtBCdata.Text)
        SaveMSG("Manual CCR: " & txtBCdata.Text, "ACT")
    End Sub
    Public connected As Boolean
    Private Sub Winsock1_Connected(ByVal sender As Winsock) Handles Winsock1.Connected
        connected = True
    End Sub
    Private Sub Winsock1_Disconnected(ByVal sender As Winsock) Handles Winsock1.Disconnected
        connected = False
    End Sub
    Sub DisplayData()
        Try


            If Not connected Then
                txtCCRip.BackColor = Color.Red
                txtCCRPort.BackColor = Color.Red
            Else
                txtCCRip.BackColor = Color.Lime
                txtCCRPort.BackColor = Color.Lime
            End If
        Catch ex As Exception

        End Try
        If Process_Start Then
            btnStart.BackColor = Color.Lime
        Else
            btnStart.BackColor = Color.Red

        End If
        Try
            lblPlant.Text = CurPlant
            lblSEQ.Text = CurSEQ
            lblLOtcode.Text = CurLOT
            If CurState = 1 Then
                lblJobState.Text = "WORKING"
            ElseIf CurState = 2 Then
                lblJobState.Text = "RESTART"
            ElseIf CurState = 3 Then
                lblJobState.Text = "COMPLETE"
            ElseIf CurState = 0 Then
                lblJobState.Text = "IDLE"
            Else
                lblJobState.Text = ""
            End If


            '  If RetryResend > 0 Then
            lblRetryresend.Text = "Retry Resend:" & RetryResend
                lblRetryresend.Visible = True

           ' End If
        Catch ex As Exception

        End Try

        txtCURPIS.Text = CurPISseq
        txtOLDPIS.Text = OldPISseq
    End Sub

    Dim ProcessDisable As Boolean
    Dim CurID, CurLOT, CurSEQ, CurState, CurPlant, Oldid, OldLOT, OldSeq, OldPlant, OldState As String

    Private Sub LoadCurLOT()
        Dim sql As String
        Try
            sql = "SELECT [Plant],[Linename],[STATE],[Pitch],[Disable],[IP],[No],[COM],[ERR],[Run],[LOT],[SEQ],[JOBID],lotPlant,[pitchName],lotonly,isolate,Disable"
            sql += " FROM [TB_Pitch]"
            sql += " where Linename like '" & sLinename & "' and Pitch =" & sPitch & " and [No]=" & sStation & " " 'and Disable=0 " 'and (STATE=0 or STATE=1 or STATE=2 or STATE=3)"

            Dim dt As DataTable
            ' Dim dv As DataView


            dt = DAL.getDataTable(sql, Connstr)
            'dv = dt.DefaultView
            If dt Is Nothing Then
                Exit Sub
            End If
            If dt.Rows.Count > 0 Then
                If dt.Rows(0).Item("Disable").ToString() = "1" Then
                    Me.BeginInvoke(New MethodInvoker(Sub()
                                                         Me.ParentForm.Text = "Process Disable "
                                                         Me.BackColor = Color.Gray
                                                     End Sub))


                    '  showErrTime = 10
                    ProcessDisable = True
                    Exit Sub
                Else
                    Me.BeginInvoke(New MethodInvoker(Sub()


                                                         Me.ParentForm.Text = "Front Process"
                                                         Me.BackColor = Color.White
                           End Sub))
                End If
                ProcessDisable = False
                CurState = dt.Rows(0).Item("STATE").ToString()
                '  If CurState = "3" Then

                '   lblStationName.Text = dt.Rows(0).Item("pitchName").ToString()
                CurSEQ = dt.Rows(0).Item("SEQ").ToString()
                CurLOT = dt.Rows(0).Item("LOT").ToString()
                CurPlant = dt.Rows(0).Item("lotPlant").ToString()
                'If dt.Rows(0).Item("Plant").ToString().ToUpper <> "COMMON" Then
                '    CurPlant = dt.Rows(0).Item("Plant").ToString()
                'End If


                CurID = dt.Rows(0).Item("JOBID").ToString()
                'End If
                If dt.Rows(0).Item("IP").ToString = "" Then

                    UpdateIP(Winsock1.LocalIP.ToString, sLinename)
                    sql = "update tb_pitch set ip ='" & Winsock1.LocalIP.ToString & "' where "
                End If


                'Try


                '    Project.Lotonly = dt.Rows(0).Item("LOTONLY").ToString()
                '    setLotonlyButton(Project.Lotonly)
                'Catch ex As Exception

                'End Try
                'If lblError.Text = "NO LOT CODE" Then

                '    lblError.Text = ""
                '    showErrTime = 0
                'End If
            Else

                'lblStatus.Text = "NO Pitch Data "
                'lblStatus.BackColor = Color.Red
                ShowStatus("NO Pitch Data ", 10, Color.Red)
                '  showErrTime = 10

            End If

        Catch ex As Exception
            ShowStatus("LoadCurLOT:" & ex.Message, 10, Color.Red)
            'MsgBox(ex.Message)
            Exit Sub
        End Try


    End Sub

    Private Sub txtBCdata_TextChanged(sender As Object, e As EventArgs) Handles txtBCdata.TextChanged
        lbllen.Text = txtBCdata.Text.Length
    End Sub

    Private Sub UpdateIP(ByVal sIP As String, ByVal LineName As String)
        Dim strSql_update As String
        Try
            strSql_update = "update tb_pitch set ip='" & sIP & "',version='" & lblVersion.Text & "' where Linename like '" & LineName & "' and Pitch =" & sPitch & " and [No] =" & sStation

            DAL.GetExecute(strSql_update, Connstr)

        Catch ex As Exception
            'MsgBox(ex.Message)
        End Try

    End Sub
    Private Sub UpdateConfirmCCR()
        Dim strSql_update As String
        Try
            strSql_update = "update tb_pitch set ConfirmSendCCR='" & ConfirmSendCCR & "' where Linename like '" & sLinename & "' and Pitch =" & sPitch & " and [No] =" & sStation

            DAL.GetExecute(strSql_update, Connstr)

        Catch ex As Exception
            ShowStatus("UpdateConfirmCCR:" & ex.Message, 5)
            ' MsgBox(ex.Message)
        End Try

    End Sub
    Private Sub UpdateState(ByVal sState As String)
        Dim strSql_update As String
        Try
            strSql_update = "update tb_pitch set STATE='" & sState & "' where Linename like '" & sLinename & "' and Pitch =" & sPitch & " and [No] =" & sStation

            DAL.GetExecute(strSql_update, Connstr)

        Catch ex As Exception
            ' MsgBox(ex.Message)
        End Try

    End Sub
    Sub UpdateCurState()

        Oldid = CurID
        OldLOT = CurLOT
        OldSeq = CurSEQ
        OldState = CurState
        OldPlant = CurPlant

        Dim sql As String
        sql = "update tb_CurState set CurID='" & CurID & "'"
        sql += " , CurLOT='" & CurLOT & "'"
        sql += ", CurSEQ='" & CurSEQ & "'"
        sql += ", CurState='" & CurState & "'"
        sql += ", CurPlant='" & CurPlant & "'"
        sql += ", Oldid='" & Oldid & "'"
        sql += ", OldLOT='" & OldLOT & "'"
        sql += ", OldSeq='" & OldSeq & "'"
        sql += ", OldPlant='" & OldPlant & "'"
        sql += ", OldState='" & OldState & "'"

        DALlite.GetExecute(sql, Connstr_lite)
    End Sub
    Sub LoadCurState()
        Dim dt As New DataTable
        Dim sql As String
        sql = "select * from tb_curstate"
        dt = DALlite.getDataTable(sql, Connstr_lite)
        For Each dr As DataRow In dt.Rows
            CurLOT = dr("CurLOT").ToString
            CurSEQ = dr("CurSEQ").ToString
            CurState = dr("CurState").ToString
            CurPlant = dr("CurPlant").ToString
            CurID = dr("CurID").ToString
            Oldid = dr("Oldid").ToString
            OldLOT = dr("OldLOT").ToString
            OldSeq = dr("OldSeq").ToString
            OldPlant = dr("OldPlant").ToString
            OldState = dr("OldState").ToString
            CurPISseq = (dr("CurPISseq").ToString)
            OldPISseq = (dr("OldPISseq").ToString)

        Next
    End Sub
    Function stringNulltoInt(str As String)
        If IsNumeric(str) Then
            Return str
        Else
            Return -1
        End If
    End Function
    Sub UpdatePISread()
        OldPISseq = CurPISseq
        Dim sql As String
        sql = "update tb_CurState set "
        sql += " CurPISseq='" & CurPISseq & "'"
        sql += ", OldPISseq='" & OldPISseq & "'"
        DALlite.GetExecute(sql, Connstr_lite)
    End Sub
    Dim OldPISseq, CurPISseq As String

    Private Sub btnSavePis_Click(sender As Object, e As EventArgs) Handles btnSavePis.Click
        SaveSetting("Complete_IP", txtPISip.Text)
        SaveSetting("Complete_Port", txtPisPort.Text)
        '     SaveSetting("Complete_Address", txtPISAddress.Text)

        DALlite.Update(dtGetComplete, selectgetComplete, Connstr_lite)

        pClsPC10G.IP = txtPISip.Text
        pClsPC10G.PORT = txtPisPort.Text
    End Sub

    Private Sub btnSaveCCR_Click(sender As Object, e As EventArgs) Handles btnSaveCCR.Click
        SaveSetting("CCR_Replace000", txtReplace000.Text)
        SaveSetting("CCR_IP", txtCCRip.Text)
        SaveSetting("CCR_PORT", txtCCRPort.Text)
        CCRip = txtCCRip.Text
        CCRport = txtCCRPort.Text
        Replace000 = txtReplace000.Text
    End Sub
    Private Sub btnCancelCCR_Click(sender As Object, e As EventArgs) Handles btnCancelCCR.Click
        txtCCRip.Text = CCRip
        txtCCRPort.Text = CCRport
    End Sub


    Private Sub btnserverConfig_Click(sender As Object, e As EventArgs) Handles btnserverConfig.Click
        Dim frm As New ServerConfig
        frm.Show()
    End Sub

    Private Sub btnStart_Click(sender As Object, e As EventArgs) Handles btnStart.Click
        Process_Start = Not Process_Start
    End Sub

    Private Sub btnProcessConfig_Click(sender As Object, e As EventArgs) Handles btnProcessConfig.Click
        Dim frm As New ProcessConfig
        frm.Show()
    End Sub
#Region "PIS"




    Function CheckComplete() As Boolean
        Try
            Dim Result As Boolean = False
            If CBgetComplete_Change.Checked Then



                CurPISseq = ""
                For Each dr As DataRow In dtGetComplete.Rows
                    If dr("type").ToString.ToUpper = "BYTE" Then
                        Dim Value() As Integer
                        Value = pClsPC10G.fReadByteAdr(dr("address"))
                        dr("value") = Value(0) '& "," & Value(1)
                        CurPISseq += Convert.ToInt32(Value(0)).ToString
                        dr("value") = Convert.ToInt32(Value(0)).ToString
                    ElseIf dr("type").ToString.ToUpper = "STRING" Then
                        Dim str As String
                        str = pClsPC10G.Getstring(dr("address"), dr("lenght"))
                        If str = "" Then Return False
                        CurPISseq += str
                        dr("value") = str

                    End If
                Next

                txtPISip.BeginInvoke(New MethodInvoker(Sub()
                                                           txtPISip.BackColor = Color.Lime
                                                           txtPisPort.BackColor = Color.Lime
                                                       End Sub))
                '   pClsPC10G.Getstring(txtPISip.Text, txtPisPort.Text, txtPISAddress.Text, 4)
                '     CurPISseq = Convert.ToInt32(byteBuffer(5))
                'txtPISvalue.BeginInvoke(New MethodInvoker(Sub()
                '                                              txtPISvalue.Text = CurPISseq
                '                                          End Sub))
                If CurPISseq.Trim = "0" Or CurPISseq.Trim = "" Then
                    Return False
                End If
                If OldPISseq <> CurPISseq Then
                    UpdatePISread()

                    Result = True
                Else
                    Return False
                End If
            End If


            If CBgetComplete_Buffer.Checked Then
                If DTbuffer.Select("LOT <> ''").Count < txtBuffer.Text Then
                    Result = True
                Else
                    Return False
                End If
            End If


            Return Result
        Catch ex As Exception
            txtPISip.BeginInvoke(New MethodInvoker(Sub()
                                                       txtPISip.BackColor = Color.Red
                                                       txtPisPort.BackColor = Color.Red
                                                   End Sub))
            ShowStatus("getComplete:" & ex.Message, 5, Color.Red)
            Return False

        Finally

        End Try
    End Function
#End Region




#Region "Buffer"
    Private Sub btnCancelBuffer_Click(sender As Object, e As EventArgs) Handles btnCancelBuffer.Click

        Dim sql As String
        sql = "select * from setting"
        For Each dr As DataRow In DALlite.getDataTable(sql, Connstr_lite).Rows


            If dr("name").ToString.ToUpper = "buffer_lot".ToUpper Then
                txtbuffer_LOT.Text = dr("value").ToString
            End If
            If dr("name").ToString.ToUpper = "buffer_lotlen".ToUpper Then
                txtbuffer_LOTLEN.Text = dr("value").ToString
            End If
            If dr("name").ToString.ToUpper = "buffer_seq".ToUpper Then
                txtbuffer_SEQ.Text = dr("value").ToString
            End If
            If dr("name").ToString.ToUpper = "buffer_seqlen".ToUpper Then
                txtbuffer_SEQLEN.Text = dr("value").ToString
            End If
        Next
    End Sub
    Private Sub btnSaveBuffer_Click(sender As Object, e As EventArgs) Handles btnSaveBuffer.Click
        SaveSetting("buffer_lot", txtbuffer_LOT.Text)
        SaveSetting("buffer_lotlen", txtbuffer_LOTLEN.Text)
        SaveSetting("buffer_seq", txtbuffer_SEQ.Text)
        SaveSetting("buffer_seqlen", txtbuffer_SEQLEN.Text)
    End Sub
    Sub initDTbuffer()
        DTbuffer.Columns.Add("no")
        DTbuffer.Columns.Add("LOT")
        DTbuffer.Columns.Add("SEQ")
        DTbuffer.Columns.Add("ADDRESS")
        For i = 1 To 40
            DTbuffer.Rows.Add(i)
        Next

        DGVbuffer.DataSource = DTbuffer
    End Sub
    Sub getBuffer()
        Try
            Dim LOT As String
            Dim seq(500) As Byte
            LOT = pClsPC10G.Getstring(txtbuffer_LOT.Text, txtbuffer_LOTLEN.Text)

            pClsPC10G.Getstring(txtbuffer_SEQ.Text, txtbuffer_SEQLEN.Text)
            'ReDim seq(UBound(pClsPC10G.ReturnByte))
            seq = pClsPC10G.ReturnByte

            ' LOT = "D001D002D003D001D002D004D006D005"
            'For i = 0 To 100
            '    LOT += "D001"
            '    seq(i) = 2
            'Next
            'For i = 0 To 500

            '    seq(i) = 2
            'Next
            '   seq = {128, 0, 61, 0, 194, 35, 2, 1, 0, 0, 0, 2, 0, 36, 2, 1, 0, 0, 0, 6, 0, 39, 2, 1, 0, 0, 0, 2, 0, 40, 2}
            Dim Addrseq As Integer = txtbuffer_SEQ.Text.Substring(5, 3)

            For i = 0 To DTbuffer.Rows.Count - 1
                Dim dr As DataRow = DTbuffer.Rows(i)
                Try
                    If LOT.Length >= (i * 4) + 4 Then
                        dr("LOT") = LOT.Substring(i * 4, 4)
                    Else
                        dr("LOT") = ""
                    End If

                Catch ex As Exception
                    dr("LOT") = ""
                End Try
                Try
                    dr("SEQ") = (seq(5 + (8 * i)) + (seq(6 + (8 * i)) * 256))
                Catch ex As Exception
                    dr("SEQ") = ""
                End Try
                Try
                    dr("Address") = txtbuffer_SEQ.Text.Substring(0, 5) & (Addrseq + 4 * i).ToString("X3")
                Catch ex As Exception
                    '  dr("LOT") = ""
                End Try
            Next
            '    CurPISseq = Convert.ToInt32(pClsPC10G.ReturnByte(5))


            SaveBuffertoSQL()

        Catch ex As Exception
            ShowStatus("getBuffer:" & ex.Message, 5, Color.Red)
        Finally

        End Try
    End Sub
    Sub SaveBuffertoSQL()
        Try


            Dim sql As String
            sql = "delete from tb_PIS_Buffer"
            DAL.GetExecute(sql, Connstr)
            sql = ""
            For Each dr As DataRow In DTbuffer.Select("LOT <> ''")
                sql += "insert into tb_PIS_Buffer([no],LOT,SEQ)"
                sql += " values('" & dr("no").ToString & "','" & dr("LOT").ToString & "','" & dr("SEQ").ToString & "');"
            Next
            If sql <> "" Then
                DAL.GetExecute(sql, Connstr)
            End If

            sql = "update tb_pis set [buffer]=" & DTbuffer.Select("LOT <> ''").Count & " "
            DAL.GetExecute(sql, Connstr)
        Catch ex As Exception
            ShowStatus("SaveBuffertoSQL:" & ex.Message, 5)
            SaveMSG("SaveBuffertoSQL:" & ex.Message, "ERR")
        End Try
    End Sub

    Private Sub btnSetBuffer_Click(sender As Object, e As EventArgs) Handles btnSetBuffer.Click
        Dim str As String = InputBox("Set Buffer")
        If IsNumeric(str) Then
            txtBuffer.Text = str
            SaveSetting("Buffer", str)
        End If
    End Sub

#End Region

    Private Sub btnResend_Click(sender As Object, e As EventArgs) Handles btnResend.Click
        SendCCR(GenBCdata)
    End Sub
#Region "serverLink"


    Dim ServerPing, ServerPing2, ServerLink, ServerLink2 As Boolean
    Dim timer_serverstate As Threading.Timer

    Dim TDS_Serverlink As TimerCallback = AddressOf checkstateServer
    Private Sub TimerServerLink_Tick(sender As Object, e As EventArgs) Handles TimerServerLink.Tick


        lblServer1.Text = Server.Server
        lblServer2.Text = Server.Server2

        If ServerPing Then

            ServerPingPN.BackColor = Color.Lime
        Else
            ServerPingPN.BackColor = Color.Red
        End If

        If Server.Server2 <> "" Then
            If ServerPing2 Then
                ServerPingPN2.BackColor = Color.Lime
            Else
                ServerPingPN2.BackColor = Color.Red
            End If
        Else
            ServerPingPN2.BackColor = Color.Gray

        End If



        If Server.Server2 <> "" Then
            If ServerLink Then
                ServerLinkPN.BackColor = Color.Lime
            Else
                ServerLinkPN.BackColor = Color.Gray
            End If

            If ServerLink2 Then
                ServerLinkPN2.BackColor = Color.Lime
            Else
                ServerLinkPN2.BackColor = Color.Gray
            End If
        Else
            ServerLinkPN.BackColor = Color.Lime
            ServerLinkPN2.BackColor = Color.Gray
        End If

        'If StartALC = False Then
        '    ServerLinkPN.BackColor = Color.Gray
        '    ServerLinkPN2.BackColor = Color.Gray

        '    Exit Sub
        'End If

    End Sub
    Sub checkstateServer()
        timer_serverstate.Change(Timeout.Infinite, Timeout.Infinite)
        Try
            ServerPing = IPReady(Server.Server)


            If Server.Server2 <> "" Then
                ServerPing2 = IPReady(Server.Server2)
            End If

            If ServerPing Then


                If DAL.getScalar(" Select  mirroring_role From sys.database_mirroring Where DB_NAME(database_id) = 'PARTSHOP_INTERLOCK'", "Data Source=" & Server.Server & ";User ID=sa;Password=" & Server.password & ";Initial Catalog=master") = "1" Then

                    ServerLink = True
                Else
                    ServerLink = False
                End If
            End If

            If ServerPing2 Then


                If DAL.getScalar(" Select  mirroring_role From sys.database_mirroring Where DB_NAME(database_id) = 'PARTSHOP_INTERLOCK'", "Data Source=" & Server.Server2 & ";User ID=sa;Password=" & Server.password & ";Initial Catalog=master") = "1" Then

                    ServerLink2 = True
                Else
                    ServerLink2 = False
                End If
            End If
        Catch ex As Exception
        Finally
            timer_serverstate.Change(1000, 1000)
        End Try

    End Sub
#End Region
    Dim Status_Color As Color
    Dim Status_setlbl As String
    Dim Status_time As Integer
    Sub ShowStatus(str As String, time As Integer, Optional sColor As Color = Nothing)
        Status_setlbl = str
        Status_time = time

        Status_Color = sColor
    End Sub

    Private Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        DisplayData()

        If Status_time > 0 Then
            lblStatus.Text = Status_setlbl
            Status_time -= 1
            If Status_Color <> Nothing Then
                lblStatus.BackColor = Status_Color
            End If

        Else
            lblStatus.Text = ""
            lblStatus.BackColor = Color.Transparent
        End If
        '--------------------
        'If CBDeleteF.Checked And Now.ToString("ss") = "00" Then
        '    DeleteF()

        'End If
    End Sub
    Private Sub btnDeleteFirst_Click(sender As Object, e As EventArgs) Handles btnDeleteFirst.Click
        DeleteF()
    End Sub
    Sub DeleteF()
        Dim clrstr As String = ""
        For i = 1 To txtDeleteFlen.Text * 2
            clrstr += "0"
        Next

        Call pClsPC10G.fWriteWordAdr(txtDeleteFaddr.Text, clrstr, False)
        SaveMSG("delete First", "Act")
    End Sub
    '---------------------------
    Function GenBCdata()
        Try

            Dim sql As String
            Dim dt As New DataTable
            Dim BCdata As String
            If CurPlant <> "E" Then
                sql = " Select  [TB_BC_DATA].[id],[BCdata] ,[TB_BC_DATA].[LotCode],family_code"
                sql += "  From [TB_BC_DATA] left Join TB_Lotcode On [TB_BC_DATA].LotCode=TB_Lotcode.Lotcode"
                sql += "  Where [TB_BC_DATA].id = " & CurID

                dt = DAL.getDataTable(sql, Connstr)
                For Each dr As DataRow In dt.Rows
                    BCdata = dr("BCdata").ToString.Insert(44, "681W")
                    BCdata = BCdata.Insert(40, dr("family_code").ToString.Substring(2, 2))
                    If BCdata.Substring(37, 3) = "000" Then
                        BCdata = BCdata.Remove(37, 3).Insert(37, Replace000)
                    End If
                Next

            Else

                'If CurLOT.Substring(0, 1).ToUpper = "L" Then
                '    BCdata = String.Format("LSA110OUTP_P003810001800  1L028032151" & CurSEQ & "2{0}{1}681W", CurLOT.Substring(3, 1), CurLOT)
                'ElseIf CurLOT.Substring(0, 1).ToUpper = "R" Then
                '    BCdata = String.Format("LSA110OUTP_P003810001800  1L028032151" & CurSEQ & "3{0}{1}681W", CurLOT.Substring(3, 1), CurLOT)
                'Else
                BCdata = String.Format("LSA110OUTP_P003810001800  1L028032151" & CurSEQ & "1{0}{1}681W", CurLOT.Substring(3, 1), CurLOT)


                '  End If
            End If

                Return BCdata
        Catch ex As Exception
            SaveMSG("GenBCdata:" & ex.Message, "ERR")
            ShowStatus("GenBCdata:" & ex.Message, 5)
            Return ""
        End Try
    End Function
    Dim Slock As Boolean
    Dim RetryResend As Integer = 0
    Sub Process()
            timer_Process.Change(Timeout.Infinite, Timeout.Infinite)

            Try
            If Process_Start = False Then Exit Sub
            If btnGetBuffer.Checked Then
                getBuffer()
            End If

            Slock = True
            If CBgetExport.Checked Then
                Export.GetExport() 'มารันที่ Mainform เพื่อป้องกันการดึงชนกัน
            End If



            LoadCurLOT()
            If CurID <> Oldid Or CurState = "2" Then
                If CBSendCCR.Checked Then
                    If SendCCR(GenBCdata) Then
                        UpdateState("1")
                        CurState = "1"
                        UpdateCurState()
                        Threading.Thread.Sleep(3000)
                        Exit Sub
                    End If
                Else
                    UpdateState("1")
                    CurState = "1"
                    UpdateCurState()

                End If



            ElseIf CurState = "1" Then
                If ConfirmSendCCR = False Then
                    RetryResend += 1

                    Try
                        If RetryResend Mod 5 = 0 Then
                            Dim p As Boolean = IPReady(txtCCRip.Text)

                            If p = False Then
                                txtCCRip.BackColor = Color.Red
                                ShowStatus("Connect CCR Lost", 2)
                                SaveMSG("ping CCR:" & p & " lastdata:" & lastDataArrival, "ERR")
                            Else
                                SaveMSG("ping CCR:" & p & " lastdata:" & lastDataArrival, "ACT")
                                If RetryResend > 5 Then
                                    ' SaveMSG("RetryResend:" & RetryResend & ",Close Connect", "ERR")
                                    'ShowStatus("RetryResend:" & RetryResend & ",Close Connect", 3)
                                    '   Winsock1.Close()
                                End If

                            End If
                        End If
                    Catch ex As Exception
                        Dim err As String
                        err = "RetryResend:" & ex.Message
                        SaveMSG(err, "ERR")
                        ShowStatus(err, 2)
                    End Try

                    SendCCR(GenBCdata)
                    SaveMSG("ReSend CCR:" & lblSEQ.Text & "-" & lblLOtcode.Text, "ERR")
                    Threading.Thread.Sleep(3000)
                    Exit Sub
                End If

                If CBCheckComplete.Checked AndAlso CheckComplete() Then
                    UpdateState("3")
                    CurState = "3"
                    savecomplete()
                End If
            End If


            If Slock = False Then
                Debug.WriteLine("LOCK ERROR")
            End If
            '  TextBox4.Text = ReceiveData
        Catch ex As Exception
                ' lblCCrStatus.Text = "Process:" & ex.Message
                ShowStatus("Process:" & ex.Message, 20, Color.Red)
            Finally
            timer_Process.Change(3000, 3000)
            Slock = False
        End Try
        End Sub

    Private Sub btnConnect_Click(sender As Object, e As EventArgs) Handles btnConnect.Click
        Try


            If Winsock1.GetState <> Winsock.WinsockStates.Connected Then
                Winsock1.Close()
                Threading.Thread.Sleep(1000)
                Winsock1.Connect(CCRip, CCRport)
                Threading.Thread.Sleep(1000)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub



    Sub savecomplete()
        Try
            Dim strSQL As String
            strSQL = "insert into TB_LOG" &
                        "([Process]" &
                        ",[Source]" &
                      ",[LOG_TIME]" &
                      ",[LOT]" &
                      ",[SEQ]" &
                       ",[Jobseq]" &
                        ",[Description]" &
                          ",[model]" &
                             ",[LOTPLANT]" &
                             ",[judgement]" &
                        ",[JOBNO])" &
                      " Values(" &
                      "'" & lblProcess.Text & "'," &
                      "'Complete'," &
                      "getdate()," &
                      "'" & lblLOtcode.Text & "'," &
                      "'" & lblSEQ.Text & "'," &
                      "'0'," &
                       "'Complete'," &
                           "''," &
                             "'" & lblPlant.Text & "'," &
                             "'Complete'," &
                        "'')"
            DAL.GetExecute(strSQL, Connstr)

        Catch ex As Exception
            'MsgBox(ex.Message)
        End Try
    End Sub



    Private Sub DGVbuffer_DataBindingComplete(sender As Object, e As DataGridViewBindingCompleteEventArgs) Handles DGVbuffer.DataBindingComplete
        Try

        Catch ex As Exception
            ShowStatus("DGVbuffer_DataBinding:" & ex.Message, 20, Color.Red)
            SaveMSG("DGVbuffer_DataBinding:" & ex.Message, "ERR")
        End Try
    End Sub

    Private Sub btnDisconnect_Click(sender As Object, e As EventArgs) Handles btnDisconnect.Click
        Winsock1.Close()
    End Sub

    Private Sub DGVbuffer_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles DGVbuffer.DataError
        Try

        Catch ex As Exception
            ShowStatus("DGVbuffer_DataError_r=" & e.RowIndex & "c=" & e.ColumnIndex & ":" & ex.Message, 20, Color.Red)
            SaveMSG("DGVbuffer_DataError_r=" & e.RowIndex & "c=" & e.ColumnIndex & ":" & ex.Message, "ERR")
        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Application.Restart()
    End Sub

    Private Sub CBgetComplete_Change_Click(sender As Object, e As EventArgs) Handles CBgetComplete_Change.Click

        SaveSetting("CBgetComplete_Change", CBgetComplete_Change.Checked.ToString)
    End Sub

    Private Sub CBgetComplete_Buffer_Click(sender As Object, e As EventArgs) Handles CBgetComplete_Buffer.Click
        SaveSetting("CBgetComplete_Buffer", CBgetComplete_Buffer.Checked.ToString)
    End Sub
#Region "Hotkeys"
    Public Const MOD_ALT As Integer = &H1 'Alt key
    Public Const MOD_CTRL As Integer = &H2
    Public Const MOD_Shift As Integer = &H4
    Public Const MOD_Winkey As Integer = &H8
    Public Const WM_HOTKEY As Integer = &H312


    <DllImport("User32.dll")>
    Public Shared Function RegisterHotKey(ByVal hwnd As IntPtr,
                        ByVal id As Integer, ByVal fsModifiers As Integer,
                        ByVal vk As Integer) As Integer
    End Function

    <DllImport("User32.dll")>
    Public Shared Function UnregisterHotKey(ByVal hwnd As IntPtr,
                        ByVal id As Integer) As Integer
    End Function

    Private Sub Form1_Load(ByVal sender As System.Object,
                        ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
    Private Sub Label15_Click(sender As Object, e As EventArgs) Handles Label15.Click
        PNDebug.Visible = Not PNDebug.Visible
    End Sub
    Private Sub Label10_Click(sender As Object, e As EventArgs) Handles Label10.Click
        PNmanualCCR.Visible = Not PNmanualCCR.Visible
        If PNmanualCCR.Visible Then
            Me.Height += PNmanualCCR.Height
            Me.ParentForm.Height += PNmanualCCR.Height
        Else
            Me.Height -= PNmanualCCR.Height
            Me.ParentForm.Height -= PNmanualCCR.Height
        End If
    End Sub
    Protected Overrides Sub WndProc(ByRef m As System.Windows.Forms.Message)
        If m.Msg = WM_HOTKEY Then
            Dim id As IntPtr = m.WParam
            Select Case (id.ToString)
                Case "100" 'กำหนดที่ Form Load
                    PNmanualCCR.Visible = Not PNmanualCCR.Visible
                    If PNmanualCCR.Visible Then
                        Me.Height += PNmanualCCR.Height
                        Me.ParentForm.Height += PNmanualCCR.Height
                    Else
                        Me.Height -= PNmanualCCR.Height
                        Me.ParentForm.Height -= PNmanualCCR.Height
                    End If
                Case "200"
                    PNDebug.Visible = Not PNDebug.Visible
            End Select
        End If
        MyBase.WndProc(m)
    End Sub

    Protected Overloads Overrides Sub OnCreateControl()

        MyBase.OnCreateControl()

        AddHandler Me.ParentForm.FormClosing, AddressOf ParentForm_FormClosing

    End Sub

    Private Sub ParentForm_FormClosing(ByVal sender As Object, ByVal e As FormClosingEventArgs)
        UnregisterHotKey(Me.Handle, 100)


    End Sub

    Private Sub SendSeqUC_GiveFeedback(sender As Object, e As GiveFeedbackEventArgs) Handles Me.GiveFeedback

    End Sub



#End Region
End Class
