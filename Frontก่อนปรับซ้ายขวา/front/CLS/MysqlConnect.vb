Imports MySql.Data.MySqlClient


Namespace DB
    Public Class MySqlConnect

        Private _DB_Provider As String
        Private _DB_UserName As String
        Private _DB_Password As String
        Private _DB_DataSource As String
        Private _DB_Name As String
        Private _ConnectStr As String
        Private _UseStoredProc As Boolean = True
        Private _Conn As MySqlConnect

        Public Sub New()
            ReadDALConfigurations()
        End Sub

        Protected Overrides Sub Finalize()
            MyBase.Finalize()
        End Sub

        Public Property Connection()
            Get
                Return _Conn
            End Get
            Set(ByVal value)
                _Conn = value
            End Set
        End Property
        Public Sub ReadDALConfigurations()
            Try


                _ConnectStr = "Data Source=" & _DB_DataSource & ";User ID=" & _DB_UserName & ";Password=" & _DB_Password
                If _DB_Name <> "" Then _ConnectStr += ";Initial Catalog=" & _DB_Name

                '_UseStoredProc = ((System.Configuration.ConfigurationSettings.AppSettings("UserStoredProc") & "").ToLower = "true")

            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End Sub


        Public Function getDataTable(ByVal SQL As String) As DataTable

            Dim DA As MySqlDataAdapter = Nothing
            Dim DT As New DataTable
            Try
                DA = New MySqlDataAdapter(SQL, _ConnectStr)
                DA.Fill(DT)
                DA.Dispose()
                DA = Nothing
                Return DT
            Catch ex As Exception
                If Not IsNothing(DA) Then
                    DA.Dispose()
                    DA = Nothing
                End If
                Throw (ex)
            End Try
            Return Nothing
        End Function

        Public Function getDataTable(ByVal SQL As String, ByVal sCon As MySqlConnection) As DataTable
            Dim DA As MySqlDataAdapter = Nothing
            Dim DT As New DataTable
            Try
                DA = New MySqlDataAdapter(SQL, sCon)
                DA.Fill(DT)
                DA.Dispose()
                DA = Nothing
                Return DT
            Catch ex As Exception
                If Not IsNothing(DA) Then
                    DA.Dispose()
                    DA = Nothing
                End If
                Throw (ex)
            End Try
            Return Nothing
        End Function

        Public Function getDataTable(ByVal SQL As String, ByVal connstr As String) As DataTable
            Dim DT As New DataTable
            Try
                Using conn As New MySqlConnection(connstr)

                    conn.Open()

                    Dim DA As MySqlDataAdapter


                    DA = New MySqlDataAdapter(SQL, conn)
                    DA.Fill(DT)
                    DA.Dispose()
                    DA = Nothing
                End Using
                Return DT
            Catch ex As Exception

                Throw (ex)
            End Try
            Return Nothing
        End Function

        Public Function getScalar(ByVal SQL As String, Connstr As String) As String
            Dim sResult As String = ""
            Try

                Using cn As New MySqlConnection(Connstr)
                    Using cmd As New MySqlCommand
                        cn.Open()
                        cmd.Connection = cn
                        cmd.CommandText = SQL
                        sResult = cmd.ExecuteScalar()
                    End Using
                End Using

            Catch ex As Exception
                sResult = ""
                ' MsgBox(ex.Message)
                ' Throw (ex)
            Finally

            End Try
            Return sResult
        End Function

        Public Function getScalar(ByVal SQL As String) As String
            Dim cn As MySqlConnection
            cn = getConn()
            Dim sResult As String = ""
            Try
                Using cmd As New MySqlCommand
                    cmd.Connection = cn
                    cmd.CommandText = SQL
                    sResult = cmd.ExecuteScalar()
                End Using
            Catch ex As Exception
                sResult = "Error"
                ' MsgBox(ex.Message)
                Throw (ex)
            Finally
                CloseConn(cn)
            End Try
            Return sResult
        End Function

        Public Function getDataView(ByVal SQL As String) As DataView
            Dim DA As MySqlDataAdapter = Nothing
            Dim DT As New DataTable
            Try
                DA = New MySqlDataAdapter(SQL, _ConnectStr)
                DA.Fill(DT)
                DA.Dispose()
                DA = Nothing
                Return DT.DefaultView
            Catch ex As Exception
                If Not IsNothing(DA) Then
                    DA.Dispose()
                    DA = Nothing
                End If
                Throw (ex)
            End Try
            Return Nothing
        End Function

        Public Function getDataView(ByVal cn As MySqlConnection, ByVal SQL As String) As DataView
            Dim DA As MySqlDataAdapter = Nothing
            Dim DT As New DataTable
            Try
                DA = New MySqlDataAdapter(SQL, cn)
                DA.Fill(DT)
                DA.Dispose()
                DA = Nothing
                Return DT.DefaultView
            Catch ex As Exception
                If Not IsNothing(DA) Then
                    DA.Dispose()
                    DA = Nothing
                End If
                Throw (ex)
            End Try
            Return Nothing
        End Function


        Public Function getDataTable(ByVal cn As MySqlConnection, ByVal SQL As String) As DataTable
            Dim DA As MySqlDataAdapter = Nothing
            Dim DT As New DataTable
            Try
                DA = New MySqlDataAdapter(SQL, cn)
                DA.Fill(DT)
                DA.Dispose()
                DA = Nothing
                Return DT
            Catch ex As Exception
                If Not IsNothing(DA) Then
                    DA.Dispose()
                    DA = Nothing
                End If
                Throw (ex)
            End Try
            Return Nothing
        End Function

        Public Function getServerDate() As Date
            Dim DA As MySqlDataAdapter = Nothing
            Dim DT As New DataTable
            Try

                Using cmd As New MySqlCommand
                    cmd.Connection = getConn()
                    cmd.CommandText = "Select getDate()"
                    Return cmd.ExecuteScalar()
                End Using


                'DA = New SqlDataAdapter("Select getDate()", _ConnectStr)
                'DA.Fill(DT)
                'DA.Dispose()
                'DA = Nothing
                'Return CDate(DT.Rows(0).Item(0))
            Catch ex As Exception
                If Not IsNothing(DA) Then
                    DA.Dispose()
                    DA = Nothing
                End If
                Throw (ex)
            End Try
            Return Nothing
        End Function

        Public Function GetExecute(ByVal strSql As String) As Integer
            Dim nRow As Integer
            Try
                Using cmd As New MySqlCommand
                    cmd.Connection = getConn()
                    cmd.CommandText = strSql
                    nRow = cmd.ExecuteNonQuery()
                End Using
            Catch ex As Exception
                Return -1
                Throw (ex)

            End Try
            Return nRow
        End Function

        Public Function GetExecute(ByVal strSql As String, ByVal cnn As MySqlConnection) As Integer
            Dim nRow As Integer
            Try
                Using cmd As New MySqlCommand
                    cnn.Close()

                    If cnn.State = ConnectionState.Closed Then
                        cnn.Open()
                    End If
                    cmd.Connection = cnn
                    cmd.CommandText = strSql
                    nRow = cmd.ExecuteNonQuery()
                End Using
            Catch ex As Exception
                ' Return -1
                Throw (ex)

            End Try
            Return nRow
        End Function
        Public Function GetExecute(ByVal strSql As String, ByVal Connstr As String) As Integer
            Dim nRow As Integer
            Try
                Using conn As New MySqlConnection(Connstr)


                    Using cmd As New MySqlCommand
                        If conn.State = ConnectionState.Closed Then
                            conn.Open()
                        End If
                        cmd.Connection = conn
                        cmd.CommandText = strSql
                        nRow = cmd.ExecuteNonQuery()
                    End Using
                End Using
            Catch ex As Exception
                'Return -1
                Throw (ex)

            End Try
            Return nRow
        End Function

        Public Function getConn() As MySqlConnection
            Dim Conn As New MySqlConnection
            Conn.ConnectionString = _ConnectStr
            Try
                Conn.Open()
            Catch ex As Exception
                '  MsgBox("Cannot connect To DB Server: " & Project.DB_Provider)
                Return Nothing
            End Try
            Return Conn
        End Function

        Public Function CloseConn(ByRef conn As MySqlConnection) As Boolean
            conn.Close()
        End Function

#Region "Container Detail"


#End Region


    End Class
End Namespace









