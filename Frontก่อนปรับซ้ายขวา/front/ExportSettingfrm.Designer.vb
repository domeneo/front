﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ExportSettingfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtTable = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtDBname = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtserver2 = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtPass = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtServername = New System.Windows.Forms.TextBox()
        Me.btnExportCancel = New System.Windows.Forms.Button()
        Me.btnSendQSave = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label9.Location = New System.Drawing.Point(14, 120)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(54, 13)
        Me.Label9.TabIndex = 41
        Me.Label9.Text = "TABLE :"
        '
        'txtTable
        '
        Me.txtTable.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtTable.Location = New System.Drawing.Point(158, 117)
        Me.txtTable.Name = "txtTable"
        Me.txtTable.Size = New System.Drawing.Size(125, 20)
        Me.txtTable.TabIndex = 40
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label10.Location = New System.Drawing.Point(12, 94)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(71, 13)
        Me.Label10.TabIndex = 39
        Me.Label10.Text = "DB NAME :"
        '
        'txtDBname
        '
        Me.txtDBname.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtDBname.Location = New System.Drawing.Point(158, 91)
        Me.txtDBname.Name = "txtDBname"
        Me.txtDBname.Size = New System.Drawing.Size(125, 20)
        Me.txtDBname.TabIndex = 38
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label15.Location = New System.Drawing.Point(10, 66)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(114, 13)
        Me.Label15.TabIndex = 37
        Me.Label15.Text = "SERVER BACKUP:"
        '
        'txtserver2
        '
        Me.txtserver2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtserver2.Location = New System.Drawing.Point(158, 64)
        Me.txtserver2.Name = "txtserver2"
        Me.txtserver2.Size = New System.Drawing.Size(125, 20)
        Me.txtserver2.TabIndex = 36
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label17.Location = New System.Drawing.Point(10, 41)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(82, 13)
        Me.Label17.TabIndex = 35
        Me.Label17.Text = "PASSWORD:"
        '
        'txtPass
        '
        Me.txtPass.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtPass.Location = New System.Drawing.Point(158, 38)
        Me.txtPass.Name = "txtPass"
        Me.txtPass.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPass.Size = New System.Drawing.Size(125, 20)
        Me.txtPass.TabIndex = 34
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label18.Location = New System.Drawing.Point(10, 15)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(100, 13)
        Me.Label18.TabIndex = 33
        Me.Label18.Text = "SERVER NAME:"
        '
        'txtServername
        '
        Me.txtServername.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtServername.Location = New System.Drawing.Point(158, 12)
        Me.txtServername.Name = "txtServername"
        Me.txtServername.Size = New System.Drawing.Size(125, 20)
        Me.txtServername.TabIndex = 32
        '
        'btnExportCancel
        '
        Me.btnExportCancel.BackColor = System.Drawing.Color.DimGray
        Me.btnExportCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExportCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnExportCancel.ForeColor = System.Drawing.Color.White
        Me.btnExportCancel.Location = New System.Drawing.Point(218, 143)
        Me.btnExportCancel.Name = "btnExportCancel"
        Me.btnExportCancel.Size = New System.Drawing.Size(54, 23)
        Me.btnExportCancel.TabIndex = 188
        Me.btnExportCancel.Text = "Cancel"
        Me.btnExportCancel.UseVisualStyleBackColor = False
        '
        'btnSendQSave
        '
        Me.btnSendQSave.BackColor = System.Drawing.Color.DimGray
        Me.btnSendQSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSendQSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnSendQSave.ForeColor = System.Drawing.Color.White
        Me.btnSendQSave.Location = New System.Drawing.Point(145, 143)
        Me.btnSendQSave.Name = "btnSendQSave"
        Me.btnSendQSave.Size = New System.Drawing.Size(54, 23)
        Me.btnSendQSave.TabIndex = 187
        Me.btnSendQSave.Text = "Save"
        Me.btnSendQSave.UseVisualStyleBackColor = False
        '
        'ExportSettingfrmvb
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(284, 170)
        Me.Controls.Add(Me.btnExportCancel)
        Me.Controls.Add(Me.btnSendQSave)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtTable)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.txtDBname)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.txtserver2)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.txtPass)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.txtServername)
        Me.Name = "ExportSettingfrmvb"
        Me.Text = "ExportSettingfrmvb"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label9 As Label
    Friend WithEvents txtTable As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents txtDBname As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents txtserver2 As TextBox
    Friend WithEvents Label17 As Label
    Friend WithEvents txtPass As TextBox
    Friend WithEvents Label18 As Label
    Friend WithEvents txtServername As TextBox
    Friend WithEvents btnExportCancel As Button
    Friend WithEvents btnSendQSave As Button
End Class
