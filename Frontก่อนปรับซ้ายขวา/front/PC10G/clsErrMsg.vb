﻿Public Class clsErrMsg

    Const cstrFileHead = "Error"
    Const cdteFormat = "yyyy_MM"
    Const cstrKakutyousi = ".txt"
    Const cstrFileMidasi = "Date,FunctionName,ErrorMessage,Data"
    Public pstrMsg As String

    '普通のエラー処理に対応
    Public Sub psubErrMsgShow(ByVal Msgex As Exception, _
                              ByVal strFunctionName As String)

        Dim strErrCrlfNon As String
        Dim strErrFileName As String

        Try
            'エラー内容を表示
            mdlCommon.pstrErrorMsg = Now & "," & strFunctionName & "," & _
                                     Msgex.GetBaseException.ToString
            pInsfrmMsg.Show()
            pInsfrmMsg.lblMsg.Text = mdlCommon.pstrErrorMsg

            'ファイル名作成
            strErrFileName = Application.StartupPath & "\" & cstrFileHead & _
                             (Date.Today.AddDays(-1)).ToString(cdteFormat) & _
                             cstrKakutyousi

            'ファイルがなかった場合は見出しを書き込む
            If System.IO.File.Exists(strErrFileName) = False Then
                My.Computer.FileSystem.WriteAllText(strErrFileName, _
                                    cstrFileMidasi & ControlChars.CrLf, True)
            End If

            strErrCrlfNon = mdlCommon.pstrErrorMsg.Replace(ControlChars.CrLf, _
                                                           "")
            'エラー内容をファイルに書き込む
            My.Computer.FileSystem.WriteAllText(strErrFileName, strErrCrlfNon _
                                                & ControlChars.CrLf, True)

        Catch ex As Exception
            mdlCommon.pstrErrorMsg = Now & " : " & ex.GetBaseException.ToString
        End Try
        mdlCommon.pblnEXFlg = False
        mdlCommon.pstrErrorMsg = ""

    End Sub

    'エラーデータだけを書き込み
    '主にTOYOPUCの通信エラーに対応
    Public Sub psubErrMsgShow(ByVal strErrMsgData As String)

        Dim strErrCrlfNon As String
        Dim strErrFileName As String

        Try
            'エラー内容を表示
            mdlCommon.pstrErrorMsg = Now & ",," & strErrMsgData
            pInsfrmMsg.Show()
            pInsfrmMsg.lblMsg.Text = mdlCommon.pstrErrorMsg

            'ファイル名作成
            strErrFileName = Application.StartupPath & "\" & cstrFileHead _
                           & (Date.Today.AddDays(-1)).ToString(cdteFormat) _
                           & cstrKakutyousi

            'ファイルがなかった場合は見出しを書き込む
            If System.IO.File.Exists(strErrFileName) = False Then
                My.Computer.FileSystem.WriteAllText(strErrFileName, _
                                    cstrFileMidasi & ControlChars.CrLf, True)
            End If

            strErrCrlfNon = mdlCommon.pstrErrorMsg.Replace(ControlChars.CrLf, _
                                                           "")
            'エラー内容をファイルに書き込む
            My.Computer.FileSystem.WriteAllText(strErrFileName, strErrCrlfNon _
                                                & ControlChars.CrLf, True)

        Catch ex As Exception
            mdlCommon.pstrErrorMsg = Now & " : " & ex.GetBaseException.ToString
        End Try
        mdlCommon.pstrErrorMsg = ""

    End Sub

    'ファイル関係のエラーに対応
    Public Sub psubErrMsgShow(ByVal Msgex As Exception, _
               ByVal strFunctionName As String, ByVal strFileData As String)

        Dim strErrCrlfNon As String
        Dim strErrFileName As String

        Try
            'エラー内容を表示
            mdlCommon.pstrErrorMsg = Now & "," & strFunctionName & "," & _
                 Msgex.GetBaseException.ToString & "," & strFileData
            pInsfrmMsg.Show()
            pInsfrmMsg.lblMsg.Text = mdlCommon.pstrErrorMsg

            'ファイル名作成
            strErrFileName = Application.StartupPath & "\" & cstrFileHead & _
                             (Date.Today.AddDays(-1)).ToString(cdteFormat) & _
                             cstrKakutyousi

            'ファイルがなかった場合は見出しを書き込む
            If System.IO.File.Exists(strErrFileName) = False Then
                My.Computer.FileSystem.WriteAllText(strErrFileName, _
                                    cstrFileMidasi & ControlChars.CrLf, True)
            End If

            strErrCrlfNon = mdlCommon.pstrErrorMsg.Replace(ControlChars.CrLf, _
                                                           "")
            'エラー内容をファイルに書き込む
            My.Computer.FileSystem.WriteAllText(strErrFileName, _
                                    strErrCrlfNon & ControlChars.CrLf, True)

        Catch ex As Exception
            mdlCommon.pstrErrorMsg = Now & " : " & ex.GetBaseException.ToString
            pInsfrmMsg.lblMsg.Text = mdlCommon.pstrErrorMsg
        End Try
        mdlCommon.pblnEXFlg = False
        mdlCommon.pstrErrorMsg = ""

    End Sub

    Public Sub psubErrWrite(ByVal Msgex As Exception, _
                            ByVal strFunctionName As String)

        Dim strErr As String
        Dim strErrCrlfNon As String
        Dim strErrFileName As String

        Try
            'エラー内容を表示
            strErr = Now & "," & strFunctionName & "," & _
                     Msgex.GetBaseException.ToString

            'ファイル名作成
            strErrFileName = Application.StartupPath & "\" & cstrFileHead & _
                             (Date.Today.AddDays(-1)).ToString(cdteFormat) & _
                             cstrKakutyousi

            'ファイルがなかった場合は見出しを書き込む
            If System.IO.File.Exists(strErrFileName) = False Then
                My.Computer.FileSystem.WriteAllText(strErrFileName, _
                            cstrFileMidasi & ControlChars.CrLf, True)
            End If

            strErrCrlfNon = strErr.Replace(ControlChars.CrLf, "")
            'エラー内容をファイルに書き込む
            My.Computer.FileSystem.WriteAllText(strErrFileName, _
                                    strErrCrlfNon & ControlChars.CrLf, True)

        Catch ex As Exception
        End Try

    End Sub

    Public Sub psubErrWrite(ByVal strErrMsgData As String)

        Dim strErr As String
        Dim strErrCrlfNon As String
        Dim strErrFileName As String

        Try
            'エラー内容を表示
            strErr = Now & ",," & strErrMsgData

            'ファイル名作成
            strErrFileName = Application.StartupPath & "\" & cstrFileHead & _
                (Date.Today.AddDays(-1)).ToString(cdteFormat) & cstrKakutyousi

            'ファイルがなかった場合は見出しを書き込む
            If System.IO.File.Exists(strErrFileName) = False Then
                My.Computer.FileSystem.WriteAllText(strErrFileName, _
                                    cstrFileMidasi & ControlChars.CrLf, True)
            End If

            strErrCrlfNon = strErr.Replace(ControlChars.CrLf, "")
            'エラー内容をファイルに書き込む
            My.Computer.FileSystem.WriteAllText(strErrFileName, strErrCrlfNon _
                                                & ControlChars.CrLf, True)

        Catch ex As Exception
        End Try

    End Sub

End Class
