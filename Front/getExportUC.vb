﻿Public Class getExportUC
    Public sendQ As New SendQcls
    Dim dtExport As New DataTable
    Dim selectgetExport As String = "select * from tb_export_addr"
    Public sendseq As SendSeqUC

    Public pClsPC10G As clsPc10G
    Private Sub getExportUC_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        '   sendseq = Me.Parent
        ValidSQLite()
        Dim frm As New ExportSettingfrm
        frm.sendQ = sendQ
        frm.Show()
        frm.Close()
        loadDRexport()
    End Sub
    Sub loadDRexport()
        dtExport = DALlite.getDataTable(selectgetExport, Connstr_lite)
        DGVComplete.DataSource = dtExport
    End Sub
    Sub ValidSQLite()
        Dim sql As String
        sql = "create table IF NOT EXISTS tb_export_addr (Name TEXT PRIMARY KEY,Address TEXT ,type TEXT,lenght text,value text)"
        DALlite.GetExecute(sql, Connstr_lite)

    End Sub



    Public Sub GetExport()
        Try


            '   pClsPC10G.fConnectON(txtPISip.Text, txtPisPort.Text)
            Dim str As String


            '  If b = 1 Then
            '// LAMP EXPORT



            Try 'ทำให้เป็นสีชมพู
                Dim DRlampCheck As DataRow = dtExport.Select("name='lampcheck'")(0)
                Dim DRlamp As DataRow = dtExport.Select("name='lamp'")(0)
                Dim lampcheck() As Integer = pClsPC10G.fReadByteAdr(DRlampCheck("address"), 3)
                Dim lampstr As String

                For Each i As Integer In lampcheck
                    lampstr += i & " "
                Next
                DRlampCheck("value") = lampstr
                If lampcheck(0) > "&H10" And lampcheck(2) < 8 Then 'ทำให้เป็นสีชมพู
                    If sendseq.CBSetLamp.Checked Then
                        pClsPC10G.fWriteWordAdr(DRlamp("address"), "8", False)
                    End If

                End If
            Catch ex As Exception
                ShowStatus("lampcheck:" & ex.Message, 20, Color.Red)
            End Try


            Try 'ทำให้ติดข้างเดียว
                Dim Dr1st As DataRow = dtExport.Select("name='1st'")(0)
                Dim DrBuffer As DataRow = dtExport.Select("name='buffer'")(0)
                Dim Processcheck() As Integer = pClsPC10G.fReadByteAdr(Dr1st("address"), 2)
                Dim buffercheck() As Integer = pClsPC10G.fReadByteAdr(DrBuffer("address"), 2)
                Dim Processcheckstr As String = ""
                Dim buffercheckstr As String = ""
                For Each i As Integer In Processcheck
                    Processcheckstr += i & " "
                Next
                Dr1st("value") = Processcheckstr

                For Each i As Integer In buffercheck
                    buffercheckstr += i & " "
                Next
                DrBuffer("value") = buffercheckstr
                Dim prCHK As String = Hex(Processcheck(0))
                If prCHK.Length > 1 Then
                    prCHK = prCHK.Substring(prCHK.Length - 2)
                End If
                If CInt(prCHK) > 20 Then
                    Dim changeModel As String = prCHK
                    changeModel = changeModel.Substring(changeModel.Length - 1)
                    '  Dim changemodelInt As Integer = CInt(changeModel)
                    '   Dim writeModel As Integer
                    Dim changemodelInt(1) As Integer
                    changemodelInt(0) = "&H1" & changeModel

                    If CInt(prCHK) > 20 And CInt(prCHK) < 30 Then

                        changemodelInt(1) = 1 ' ซ้ายใส่1
                        pClsPC10G.fWriteWordAdr(Dr1st("address"), changemodelInt)
                        pClsPC10G.fWriteWordAdr(DrBuffer("address"), "000000000000000", False)
                    ElseIf CInt(prCHK) > 30 And CInt(prCHK) < 40 Then
                        changemodelInt(1) = 0
                        pClsPC10G.fWriteWordAdr(Dr1st("address"), changemodelInt)
                        pClsPC10G.fWriteWordAdr(DrBuffer("address"), "000000000000000", False)
                    End If

                    ' pClsPC10G.fWriteWordAdr(DrBuffer("address"), "000000000000000", False)
                End If
            Catch ex As Exception
                ShowStatus("lampcheck:" & ex.Message, 20, Color.Red)
            End Try


            Dim DRexportname As DataRow = dtExport.Select("name='exportname'")(0)
            Dim DRExport As DataRow = dtExport.Select("name='export'")(0)
            Dim DRQTY As DataRow = dtExport.Select("name='qty'")(0)
            Dim QTY As Integer
            QTY = pClsPC10G.fReadByteAdr(DRQTY("address"))(0)
            DRQTY("value") = QTY
            str = pClsPC10G.Getstring(DRexportname("address").ToString, DRexportname("lenght").ToString)
            Dim b() As Byte
            pClsPC10G.Getstring(DRExport("address").ToString, DRExport("lenght").ToString, b)
            DRExport("value") = FormatBytes(b)
            DRexportname("value") = str
            'txtExportvalue.BeginInvoke(New MethodInvoker(Sub()
            '                                                 txtExportvalue.Text = str
            '                                                 txtexport_Bitvalue.Text = b
            '                                             End Sub))
            '  End If
            '   Exit Function
            If str.Trim = "" Then Exit Sub


            Dim clrstr As String = ""
            Dim len As Int16
            If IsNumeric(DRExport("lenght")) Then
                len = DRExport("lenght")
            Else
                len = 4
            End If

            For i = 1 To len * 2
                clrstr += "0"
            Next
            If sendseq.CBDeleteExport.Checked Then
                Call pClsPC10G.fWriteWordAdr(DRExport("address").ToString, clrstr, False)
            End If

            If sendseq.CBSendQ.Checked Then
                ''ใช้จริงเปิดตรงนี้ด้วย
                SaveMSG("GetExport :" & str & " QTY:" & QTY, "USER")
                sendQ.sendQ(str, QTY, "E")
                '  //////////////

                'If str.Length >= 4 Then
                '    str = str.Substring(str.Length - 4)
                'End If


                'For i = 1 To QTY
                '    Try

                '    ' Debug.WriteLine(str.Substring(i, 4))
                'Catch ex As Exception
                '    ShowStatus("GetExportString:" & str & ":" & ex.Message, 20, Color.Red)
                '    SaveMSG("GetExportString:" & str & ":" & ex.Message, "ACT")
                'End Try

                ' Next



                Me.BeginInvoke(New MethodInvoker(Sub()
                                                     PNsend.BackColor = Color.Lime
                                                     txtLastSend.Text = Now.ToString("HH:mm:ss dd/MM/yyyy")
                                                 End Sub))
            End If
        Catch ex As Exception
            Me.BeginInvoke(New MethodInvoker(Sub()
                                                 PNsend.BackColor = Color.Red
                                             End Sub))

            ShowStatus("GetExport:" & ex.Message, 20, Color.Red)
            SaveMSG("GetExport:" & ex.GetBaseException.ToString, "ERR")
        End Try

    End Sub
    Dim Status_setlbl As String
    Dim Status_time As Integer
    Dim Status_Color As Color
    Sub ShowStatus(str As String, time As Integer, Optional sColor As Color = Nothing)
        Status_setlbl = str
        Status_time = time

        Status_Color = sColor
    End Sub
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        If Status_time > 0 Then
            lblStatus.Text = Status_setlbl
            Status_time -= 1
            If Status_Color <> Nothing Then
                lblStatus.BackColor = Status_Color
            End If

        Else
            lblStatus.Text = ""
            lblStatus.BackColor = Color.Transparent
        End If
    End Sub

    Private Sub btnServer_Click(sender As Object, e As EventArgs) Handles btnServer.Click
        Dim frm As New ExportSettingfrm
        frm.sendQ = sendQ
        frm.Show()
    End Sub

    Private Sub btnSendQSave_Click(sender As Object, e As EventArgs) Handles btnSendQSave.Click
        DALlite.Update(dtExport, selectgetExport, Connstr_lite)
    End Sub

    Private Sub btnExportCancel_Click(sender As Object, e As EventArgs) Handles btnExportCancel.Click
        loadDRexport()
    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        Dim Dr1st As DataRow = dtExport.Select("name='1st'")(0)
        '  pClsPC10G.fWriteWordAdr(Dr1st("address"), "000000000000000", False)
        pClsPC10G.fWriteWordAdr(Dr1st("address"), "0000", False)
    End Sub
    'Function GetExport() As String
    '    Try


    '        '   pClsPC10G.fConnectON(txtPISip.Text, txtPisPort.Text)
    '        Dim str As String
    '        Dim b As Integer = pClsPC10G.fReadBitAdr(pClsPC10G.addresstoST_REG(txtexport_bit.Text))

    '        '  If b = 1 Then


    '        str = pClsPC10G.Getstring(txtExportAddress.Text, 2)
    '        txtExportvalue.BeginInvoke(New MethodInvoker(Sub()
    '                                                         txtExportvalue.Text = str
    '                                                         txtexport_Bitvalue.Text = b
    '                                                     End Sub))
    '        '  End If
    '        '   Exit Function
    '        If b = 0 Then Exit Function


    '        Dim clrstr As String = ""
    '        For i = 1 To 10
    '            clrstr += "0"
    '        Next
    '        If CBDeleteExport.Checked Then
    '            Call pClsPC10G.fWriteWordAdr(txtExportAddress.Text, clrstr, False)
    '        End If

    '        'If str.Length >= 4 Then
    '        '    str = str.Substring(str.Length - 4)
    '        'End If

    '        SaveMSG("GetExport :" & str, "ACT")
    '        For i = 0 To str.Length - 1 Step 4
    '            Try
    '                sendQ.sendQ(str.Substring(i, 4), 10, "E")
    '                ' Debug.WriteLine(str.Substring(i, 4))
    '            Catch ex As Exception
    '                ShowStatus("GetExportString:" & str & ":" & ex.Message, 20, Color.Red)
    '                SaveMSG("GetExportString:" & str & ":" & ex.Message, "ACT")
    '            End Try

    '        Next


    '    Catch ex As Exception
    '        ShowStatus("GetExport:" & ex.Message, 20, Color.Red)
    '    End Try

    'End Function
End Class
