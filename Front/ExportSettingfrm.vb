﻿Public Class ExportSettingfrm
    Public sendQ As SendQcls
    Private Sub ExportSettingfrmvb_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        LoadSetting_Export()
    End Sub
    Sub LoadSetting_Export()
        Dim sql As String
        sql = "select * from setting"
        Dim dt As New DataTable
        dt = DALlite.getDataTable(sql, Connstr_lite)
        For Each dr As DataRow In dt.Rows


            If dr("name").ToString.ToUpper = "Export_server".ToUpper Then
                txtServername.Text = dr("value").ToString
            End If
            If dr("name").ToString.ToUpper = "Export_server2".ToUpper Then
                txtserver2.Text = dr("value").ToString
            End If

            If dr("name").ToString.ToUpper = "Export_password".ToUpper Then
                txtPass.Text = dr("value").ToString
            End If

            If dr("name").ToString.ToUpper = "Export_dbname".ToUpper Then
                txtDBname.Text = dr("value").ToString
            End If

            If dr("name").ToString.ToUpper = "Export_table".ToUpper Then
                txtTable.Text = dr("value").ToString
            End If

        Next

        sendQ.server = txtServername.Text
        sendQ.server2 = txtserver2.Text
        sendQ.table = txtTable.Text
        sendQ.dbname = txtDBname.Text
        sendQ.password = txtPass.Text
    End Sub
    Private Sub btnSendQSave_Click(sender As Object, e As EventArgs) Handles btnSendQSave.Click

        sendQ.server = txtServername.Text
        sendQ.server2 = txtserver2.Text
        sendQ.password = txtPass.Text
        sendQ.table = txtTable.Text
        sendQ.dbname = txtDBname.Text

        SaveSetting("Export_server", txtServername.Text)
        SaveSetting("Export_server2", txtserver2.Text)
        SaveSetting("Export_password", txtPass.Text)
        SaveSetting("Export_table", txtTable.Text)
        SaveSetting("Export_dbname", txtDBname.Text)


    End Sub
    Private Sub btnExportCancel_Click(sender As Object, e As EventArgs) Handles btnExportCancel.Click
        LoadSetting_Export()
    End Sub
End Class