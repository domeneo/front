﻿Imports System.Threading
Public Class TraceUC
    Dim DALmysql As New DB.MySqlConnect
    Dim DAL As New DB.SqlConnect

    Dim IPserver, username, password, DBname, fromtable, totable, IDCol As String
    Dim MysqlConnstr As String

    Dim timer_Process As Threading.Timer
    Dim LastID As Int64
    Dim Process_Start As Boolean = True
    Sub server_setting()
        IPserver = "192.168.0.100"
        username = "remote"
        password = "1234"
        DBname = "380A"
        fromtable = "tb_buffer"
        totable = "tb_qi"
        MysqlConnstr = "Data Source=" & IPserver & ";Database=" & DBname & ";User ID=" & username & ";Password=" & password & ";Allow Zero Datetime=true;"
    End Sub



    Private Sub TraceUC_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LoadSetting()
        Dim TDS2 As TimerCallback = AddressOf Process1
        timer_Process = New Threading.Timer(TDS2, Nothing, 1000, 1000)
    End Sub

    Private Sub btnStart_Click(sender As Object, e As EventArgs) Handles btnStart.Click
        Process_Start = Not Process_Start
    End Sub
    Sub DisplayData()
        Try


            '    If Not connected Then
            '        txtCCRip.BackColor = Color.Red
            '        txtCCRPort.BackColor = Color.Red
            '    Else
            '        txtCCRip.BackColor = Color.Lime
            '        txtCCRPort.BackColor = Color.Lime
            '    End If
            'Catch ex As Exception
            '    lblCCrStatus.Text = "ShowConnect:" & ex.Message
            'End Try
            If Process_Start Then
                btnStart.BackColor = Color.Lime
            Else
                btnStart.BackColor = Color.Red

            End If
            'Try
            '    lblPlant.Text = CurPlant
            '    lblSEQ.Text = CurSEQ
            '    lblLOtcode.Text = CurLOT
            '    If CurState = 1 Then
            '        lblJobState.Text = "WORKING"
            '    ElseIf CurState = 2 Then
            '        lblJobState.Text = "RESTART"
            '    ElseIf CurState = 3 Then
            '        lblJobState.Text = "COMPLETE"
            '    ElseIf CurState = 0 Then
            '        lblJobState.Text = "IDLE"
            '    Else
            '        lblJobState.Text = ""
            '    End If

        Catch ex As Exception

        End Try
    End Sub
    Dim Status_setlbl As String
    Dim Status_time As Integer
    Dim Status_Color As Color
    Sub ShowStatus(str As String, time As Integer, Optional sColor As Color = Nothing)
        Status_setlbl = str
        Status_time = time

        Status_Color = sColor
    End Sub
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        DisplayData()

        If Status_time > 0 Then
            lblstatus.Text = Status_setlbl
            Status_time -= 1
            If Status_Color <> Nothing Then
                lblstatus.BackColor = Status_Color
            End If

        Else
            lblstatus.Text = ""
            lblstatus.BackColor = Color.Transparent
        End If

    End Sub

    Private Sub btnReport_Click(sender As Object, e As EventArgs) Handles btnReport.Click
        Dim frm As New ReportFrm
        frm.Show()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        LoadSetting()
    End Sub

    Sub Process1()


        timer_Process.Change(Timeout.Infinite, Timeout.Infinite)
        Dim sql As String
        Try
            If Process_Start = False Then
                Exit Sub
            End If

            If IDCol = "" Then
                ShowStatus("IDCOL=''", 5)
                Exit Sub
            End If

            Dim str As String
            sql = "select max(" & IDCol & ") from tb_qi"
            str = DAL.getScalar(sql, Connstr)
            If IsNumeric(str) Then
                LastID = str
            End If
            Dim dt As New DataTable
            sql = " SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ;"

            sql += "select *  from " & fromtable & " where " & IDCol & ">" & LastID & " order by " & IDCol & "; "
            sql += " COMMIT ;"

            dt = DALmysql.getDataTable(sql, MysqlConnstr)

            If dt.Rows.Count = 0 Then Exit Sub

            InsertTrace(dt)


            LastID = dt.Rows(dt.Rows.Count - 1)(IDCol)
            SaveSetting("Trace_lastID", LastID)
            txtLastID.BeginInvoke(New MethodInvoker(Sub()
                                                        txtLastID.Text = LastID
                                                        txtLastID.BackColor = Color.Lime
                                                    End Sub))

        Catch ex As Exception
            ShowStatus("Process1: " & ex.Message, 3, Color.Red)
            SaveMSG("Process1: " & Sql & ":" & ex.Message, "ERR")

        Finally


            timer_Process.Change(10000, 10000)
        End Try
    End Sub
    Sub InsertTrace(dt As DataTable)
        Dim sql, column, values As String

        For Each dc As DataColumn In dt.Columns
            column += "," & dc.ColumnName
        Next
        If column = "" Then Exit Sub
        column = column.Substring(1)

        For Each dr As DataRow In dt.Rows
            values = ""
            For Each dc As DataColumn In dt.Columns
                If dc.DataType Is GetType(DateTime) And IsDate(dr(dc.ColumnName)) Then
                    values += ",'" & CDate(dr(dc.ColumnName)).ToString("yyyy-MM-dd HH:mm:ss") & "'"

                ElseIf dr(dc.ColumnName).ToString = "" Then
                    values += ",NULL"
                Else

                    values += ",'" & dr(dc.ColumnName) & "'"
                End If

            Next
            values = values.Substring(1)
            sql = "insert into " & totable & "(" & column & ") values(" & values & ");"
            Try


                DAL.GetExecute(sql, Connstr)
            Catch ex As Exception
                SaveMSG("insert Trace:" & ex.Message, "ERR")
                ShowStatus("InsertTrace: " & ex.Message, 3, Color.Red)
            End Try
        Next


    End Sub
    Private Sub btSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        IPserver = txtServername.Text
        username = txtUsername.Text
        password = txtPass.Text
        DBname = txtDBname.Text
        totable = txtTotable.Text
        fromtable = txtfromTable.Text
        LastID = txtLastID.Text
        IDCol = txtIDCol.Text
        SaveSetting("Trace_IP", IPserver)
        SaveSetting("Trace_user", username)
        SaveSetting("Trace_pass", password)
        SaveSetting("Trace_DBname", DBname)
        SaveSetting("Trace_Fromtable", fromtable)
        SaveSetting("Trace_totable", totable)
        SaveSetting("Trace_lastID", LastID)
        SaveSetting("Trace_IDCol", IDCol)
    End Sub
    Sub LoadSetting()
        Dim sql As String
        sql = "select * from setting"
        Dim dt As New DataTable
        dt = DALlite.getDataTable(sql, Connstr_lite)
        For Each dr As DataRow In dt.Rows
            If dr("name").ToString.ToUpper = "Trace_IP".ToUpper Then
                txtServername.Text = dr("value").ToString
            End If
            If dr("name").ToString.ToUpper = "Trace_user".ToUpper Then
                txtUsername.Text = dr("value").ToString
            End If
            If dr("name").ToString.ToUpper = "Trace_pass".ToUpper Then
                txtPass.Text = dr("value").ToString
            End If
            If dr("name").ToString.ToUpper = "Trace_dbname".ToUpper Then
                txtDBname.Text = dr("value").ToString
            End If
            If dr("name").ToString.ToUpper = "Trace_fromtable".ToUpper Then
                txtfromTable.Text = dr("value").ToString
            End If
            If dr("name").ToString.ToUpper = "Trace_totable".ToUpper Then
                txtTotable.Text = dr("value").ToString
            End If
            If dr("name").ToString.ToUpper = "Trace_LastID".ToUpper Then
                LastID = dr("value").ToString
            End If
            If dr("name").ToString.ToUpper = "Trace_IDCol".ToUpper Then
                IDCol = dr("value").ToString
            End If
        Next

        txtIDCol.Text = IDCol
        txtLastID.Text = LastID
        IPserver = txtServername.Text
        username = txtUsername.Text
        password = txtPass.Text
        DBname = txtDBname.Text
        fromtable = txtfromTable.Text
        totable = txtTotable.Text
        '  MysqlConnstr = "Data Source=" & IPserver & ";Database=" & DBname & ";User ID=" & username & ";Password=" & password & ";Allow Zero Datetime=true;"
        MysqlConnstr = "Data Source=" & IPserver & ";Database=" & DBname & ";User ID=" & username & ";Password=" & password & ";SslMode=none"

    End Sub

End Class
