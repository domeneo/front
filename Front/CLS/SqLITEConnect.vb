Imports System.Data.SQLite
Imports System.Web
Imports System.Threading
Namespace DB
    Public Class SqliteConnect

        Private _DB_Provider As String
        Private _DB_UserName As String
        Private _DB_Password As String
        Private _DB_DataSource As String
        Private _DB_DataSource_second As String
        Private _DB_Name As String
        Private _ConnectStr As String
        Private _ConnectStr_main As String
        Private _ConnectStr_second As String
        Private _UseStoredProc As Boolean = True
        Private _Conn As SQLiteConnection
        Public ConState As Boolean
        Dim Constr As String = "Data Source=c:\mydb.db;Version=3;"
        Public Sub New()
            '  ReadDALConfigurations()
        End Sub

        Protected Overrides Sub Finalize()
            MyBase.Finalize()
        End Sub



        Sub CreateDB(path As String, InsertTable As String)
            If System.IO.File.Exists(path) = False Then
                SQLiteConnection.CreateFile(path)
                If InsertTable <> "" Then
                    Dim connstr As String = "Data Source=" & path & ";Version=3;"
                    GetExecute(InsertTable, connstr)
                End If
            End If

        End Sub
        Public Function getDataTable(ByVal SQL As String) As DataTable
            Dim DA As SQLiteDataAdapter = Nothing
            Dim DT As New DataTable
            Try
                DA = New SQLiteDataAdapter(SQL, _ConnectStr)
                DA.Fill(DT)
                DA.Dispose()
                DA = Nothing
                Return DT
                ConState = True
            Catch ex As Exception
                If Not IsNothing(DA) Then
                    DA.Dispose()
                    DA = Nothing
                End If
                ConState = False
                'MsgBox(ex.Message)
            End Try
            Return Nothing
        End Function


        Public Function getScalar(ByVal SQL As String, ByVal _ConnectStr As String) As String
            ' Dim cn As SqlConnection
            ' cn = getConn()
            Dim sResult As String = ""
            Try
                Using conn As New SQLiteConnection

                    Using cmd As New SQLiteCommand
                        conn.ConnectionString = _ConnectStr
                        conn.Open()
                        cmd.Connection = conn
                        cmd.CommandText = SQL
                        sResult = cmd.ExecuteScalar()
                    End Using

                End Using
            Catch ex As Exception
                sResult = ""
                ' MsgBox(ex.Message)
            Finally
                ' CloseConn(cn)
            End Try
            Return sResult
        End Function

        Public Function getDataView(ByVal SQL As String) As DataView
            Dim DA As SQLiteDataAdapter = Nothing
            Dim DT As New DataTable
            Try
                DA = New SQLiteDataAdapter(SQL, _ConnectStr)
                DA.Fill(DT)
                DA.Dispose()
                DA = Nothing
                Return DT.DefaultView
            Catch ex As Exception
                If Not IsNothing(DA) Then
                    DA.Dispose()
                    DA = Nothing
                End If
                MsgBox(ex.Message)
            End Try
            Return Nothing
        End Function

        Public Function getDataView(ByVal cn As SQLiteConnection, ByVal SQL As String) As DataView
            Dim DA As SQLiteDataAdapter = Nothing
            Dim DT As New DataTable
            Try
                If cn.State = ConnectionState.Closed Then
                    cn.Open()
                End If
                DA = New SQLiteDataAdapter(SQL, cn)
                DA.Fill(DT)
                DA.Dispose()
                DA = Nothing
                Return DT.DefaultView
            Catch ex As Exception
                If Not IsNothing(DA) Then
                    DA.Dispose()
                    DA = Nothing
                End If
                MsgBox(ex.Message)
            End Try
            Return Nothing
        End Function

        Public Function getDataTable(ByVal SQL As String, ByVal _ConnectStr As String) As DataTable
            Dim DA As SQLiteDataAdapter = Nothing
            Dim DT As New DataTable
            Try
                '  If cn.State = ConnectionState.Closed Then
                ' cn.Open()
                'End If
                DA = New SQLiteDataAdapter(SQL, _ConnectStr)
                DA.Fill(DT)
                DA.Dispose()
                DA = Nothing
                Return DT
                ConState = True
            Catch ex As Exception
                If Not IsNothing(DA) Then
                    DA.Dispose()
                    DA = Nothing
                End If
                ConState = False
                Throw (ex)
                ' MsgBox(ex.Message) 
            End Try
            Return Nothing
        End Function


        Public Function getServerDate() As Date
            Dim DA As SQLiteDataAdapter = Nothing
            Dim DT As New DataTable
            Try

                Using cmd As New SQLiteCommand
                    cmd.Connection = getConn()
                    cmd.CommandText = "Select getDate()"
                    Return cmd.ExecuteScalar()
                End Using


                'DA = New SqlDataAdapter("Select getDate()", _ConnectStr)
                'DA.Fill(DT)
                'DA.Dispose()
                'DA = Nothing
                'Return CDate(DT.Rows(0).Item(0))
            Catch ex As Exception
                If Not IsNothing(DA) Then
                    DA.Dispose()
                    DA = Nothing
                End If
                MsgBox(ex.Message)
            End Try
            Return Nothing
        End Function


        Public Function GetExecuteTHD(ByVal strSql As String, _ConnectStr As String) As Integer
            Dim thd As New Thread(Sub() GetExecute(strSql, _ConnectStr))
            thd.IsBackground = True
            thd.Start()

        End Function

        Public Function GetExecute(ByVal strSql As String, _ConnectStr As String) As Integer
            Dim nRow As Integer
            Try
                Using conn As New SQLiteConnection


                    Using cmd As New SQLiteCommand
                        cmd.Connection = conn
                        cmd.Connection.ConnectionString = _ConnectStr
                        cmd.Connection.Open()
                        cmd.CommandText = strSql
                        nRow = cmd.ExecuteNonQuery()
                    End Using

                End Using
            Catch ex As Exception
                Return -1
                Throw (ex)
            End Try
            Return nRow
        End Function

        Public Function getConn() As SQLiteConnection
            Dim Conn As New SQLiteConnection
            Conn.ConnectionString = _ConnectStr
            Try
                ' If Conn.State = ConnectionState.Closed Then
                Conn.Open()
                ' End If

                ConState = True
            Catch ex As Exception
                ' MsgBox("Cannot connect To DB Server: " & Project.DB_Provider)
                ConState = False
                Return Nothing
            End Try
            Return Conn
        End Function

        Public Function CloseConn(ByRef conn As SQLiteConnection) As Boolean
            conn.Close()
        End Function

        Public Function checkstate(ByVal strSql As String, ByVal constr As String) As String

            Dim str As Object
            Try
                Using conn As New SQLiteConnection


                    Using cmd As New SQLiteCommand
                        cmd.Connection = conn
                        cmd.Connection.ConnectionString = constr
                        cmd.Connection.Open()
                        cmd.CommandText = strSql
                        str = cmd.ExecuteScalar()
                        If str Is Nothing Then
                            Return ""
                        Else
                            Return str.ToString()

                        End If

                    End Using
                End Using
            Catch ex As Exception
                Return ""

            End Try

        End Function
        Public Sub Update(dt As DataTable, ByVal SQL As String, ByVal connSTR As String)
            Using connection As New SQLiteConnection(connSTR)
                Dim adapter As New SQLiteDataAdapter()
                adapter.SelectCommand = New SQLiteCommand(SQL, connection)
                Dim builder As SQLiteCommandBuilder = New SQLiteCommandBuilder(adapter)
                builder.ConflictOption = ConflictOption.OverwriteChanges
                adapter.InsertCommand = builder.GetInsertCommand
                adapter.UpdateCommand = builder.GetUpdateCommand
                adapter.DeleteCommand = builder.GetDeleteCommand
                connection.Open()


                ' Code to modify data in DataTable here 

                adapter.Update(dt)

            End Using
        End Sub
#Region "Container Detail"


#End Region


    End Class
End Namespace









