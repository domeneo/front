﻿Module Module1
    Public Connstr As String
    Public Connstr_lite As String
    Public sLinename, sPitch, sStation As String

    Public Server As New ServerStruct
    Public sqlitePath As String = Application.StartupPath & "\DB.db"
    Public IP As String

    Public ProcessName As String
    Public DALlite As New DB.SqliteConnect
    Public DAL As New DB.SqlConnect
    Structure ServerStruct
        Public Server As String
        Public Server2 As String
        Public password As String
        Public DBname As String
    End Structure
    Sub loadserver()
        Try


            Dim sql As String
            sql = "select * from Server"
            Dim dt As New DataTable
            dt = DALlite.getDataTable(sql, Connstr_lite)
            For Each dr As DataRow In dt.Rows
                Server.Server = dr("server").ToString
                Server.Server2 = dr("server2").ToString
                Server.password = dr("password").ToString
                Server.DBname = dr("dbname").ToString
                Connstr = "Server=" & Server.Server & "; Failover Partner=" & Server.Server2 & ";User ID=sa;Password=" & Server.password
                Connstr += ";Initial Catalog=" & Server.DBname
            Next
        Catch ex As Exception
            MsgBox("loadserver:" & Connstr_lite & ":" & ex.Message)
        End Try
    End Sub
    Sub SaveSetting(Keys As String, Val As String)
        Try

            Dim sqlstr As String
            sqlstr = "update setting set value='" & Val & "' where name like '" & Keys & "'"
            '   If plant <> "" Then sqlstr += " and plant ='" & plant & "'"

            If DALlite.GetExecute(sqlstr, Connstr_lite) = 0 Then
                sqlstr = "insert into setting(name,value)"
                sqlstr += " values('" & Keys & "','" & Val & "')"
                DALlite.GetExecute(sqlstr, Connstr_lite)
            End If
        Catch ex As Exception
            '  SaveMSG("SaveSetting:" & ex.Message, "ERR")
        End Try
    End Sub
    Sub SaveMSG(MSG As String, ACT As String)
        Try

            MSG = MSG.Replace("'", "''")
            Dim sqlstr As String
            sqlstr = "insert into tb_msg(m_time,m_msg,m_act,m_ip,m_station,m_user)"
            sqlstr += " values(getdate(),'" & MSG & "','" & ACT & "','" & IP & "','" & ProcessName & "','FR')"
            DAL.GetExecute(sqlstr, Connstr)
        Catch ex As Exception

        End Try
    End Sub
    Sub SetDoubleBuffered(ByVal control As Control)
        ' set instance non-public property with name "DoubleBuffered" to true 
        GetType(Control).InvokeMember("DoubleBuffered", System.Reflection.BindingFlags.SetProperty Or System.Reflection.BindingFlags.Instance Or System.Reflection.BindingFlags.NonPublic, Nothing, control, New Object() {True})
    End Sub
    Public Function FormatBytes(bytes() As Byte) As String
        Try


            Dim value As String = ""
            For Each byt In bytes
                Try


                    value += String.Format("{0:d} ", byt)
                Catch ex As Exception

                End Try
            Next



            '        value = String.Format("{0}--> 0x{1:X4} ({1:N0})", value, BitConverter.ToInt32(bytes, 0))
            Return value
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function
    Public Function IPReady(ByVal serverName As String) As Boolean
        Try


            Dim bReturnStatus As Boolean = False


            Dim timeout As Integer = 1

            Dim pingSender As New System.Net.NetworkInformation.Ping()
            Dim options As New System.Net.NetworkInformation.PingOptions()

            options.DontFragment = True

            Dim ipAddressOrHostName As String = serverName
            If ipAddressOrHostName = "" Then
                Exit Function
            End If
            Dim data As String = "zz"
            Dim buffer As Byte() = System.Text.Encoding.ASCII.GetBytes(data)
            Dim reply As System.Net.NetworkInformation.PingReply =
                        pingSender.Send(ipAddressOrHostName, timeout, buffer, options)

            If reply.Status = Net.NetworkInformation.IPStatus.Success Then
                bReturnStatus = True
                'MsgBox(reply.RoundtripTime) 'show ping time by milliseccond
            End If

            Return bReturnStatus

        Catch ex As Exception
            Return False
            Exit Function
        End Try

    End Function
End Module
