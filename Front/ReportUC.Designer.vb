﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ReportUC
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.CBrptSource = New System.Windows.Forms.TextBox()
        Me.DTPfromDate = New System.Windows.Forms.DateTimePicker()
        Me.CBrptJudge = New System.Windows.Forms.TextBox()
        Me.DTPFromTime = New System.Windows.Forms.DateTimePicker()
        Me.lblrptCount = New System.Windows.Forms.Label()
        Me.DTPtoDate = New System.Windows.Forms.DateTimePicker()
        Me.DTPtotime = New System.Windows.Forms.DateTimePicker()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.btnRptShow = New System.Windows.Forms.Button()
        Me.btnRPTExcel = New System.Windows.Forms.Button()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.DGV1 = New System.Windows.Forms.DataGridView()
        Me.PGB1 = New System.Windows.Forms.ProgressBar()
        Me.BWreport = New System.ComponentModel.BackgroundWorker()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.DGV1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.Controls.Add(Me.TextBox1)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Label50)
        Me.Panel1.Controls.Add(Me.CBrptSource)
        Me.Panel1.Controls.Add(Me.DTPfromDate)
        Me.Panel1.Controls.Add(Me.CBrptJudge)
        Me.Panel1.Controls.Add(Me.DTPFromTime)
        Me.Panel1.Controls.Add(Me.lblrptCount)
        Me.Panel1.Controls.Add(Me.DTPtoDate)
        Me.Panel1.Controls.Add(Me.DTPtotime)
        Me.Panel1.Controls.Add(Me.Label51)
        Me.Panel1.Controls.Add(Me.btnRptShow)
        Me.Panel1.Controls.Add(Me.btnRPTExcel)
        Me.Panel1.Controls.Add(Me.Label55)
        Me.Panel1.Controls.Add(Me.Label57)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1081, 120)
        Me.Panel1.TabIndex = 0
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label50.Location = New System.Drawing.Point(10, 25)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(47, 16)
        Me.Label50.TabIndex = 62
        Me.Label50.Text = "From:"
        '
        'CBrptSource
        '
        Me.CBrptSource.Location = New System.Drawing.Point(337, 53)
        Me.CBrptSource.Name = "CBrptSource"
        Me.CBrptSource.Size = New System.Drawing.Size(175, 20)
        Me.CBrptSource.TabIndex = 86
        '
        'DTPfromDate
        '
        Me.DTPfromDate.CustomFormat = "dd/MM/yyyy"
        Me.DTPfromDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.DTPfromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DTPfromDate.Location = New System.Drawing.Point(63, 20)
        Me.DTPfromDate.Name = "DTPfromDate"
        Me.DTPfromDate.Size = New System.Drawing.Size(102, 22)
        Me.DTPfromDate.TabIndex = 60
        '
        'CBrptJudge
        '
        Me.CBrptJudge.Location = New System.Drawing.Point(59, 53)
        Me.CBrptJudge.Name = "CBrptJudge"
        Me.CBrptJudge.Size = New System.Drawing.Size(175, 20)
        Me.CBrptJudge.TabIndex = 85
        '
        'DTPFromTime
        '
        Me.DTPFromTime.CustomFormat = "HH:mm:ss"
        Me.DTPFromTime.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.DTPFromTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DTPFromTime.Location = New System.Drawing.Point(171, 20)
        Me.DTPFromTime.Name = "DTPFromTime"
        Me.DTPFromTime.ShowUpDown = True
        Me.DTPFromTime.Size = New System.Drawing.Size(98, 22)
        Me.DTPFromTime.TabIndex = 61
        '
        'lblrptCount
        '
        Me.lblrptCount.AutoSize = True
        Me.lblrptCount.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblrptCount.Location = New System.Drawing.Point(168, 84)
        Me.lblrptCount.Name = "lblrptCount"
        Me.lblrptCount.Size = New System.Drawing.Size(51, 16)
        Me.lblrptCount.TabIndex = 84
        Me.lblrptCount.Text = "Count:"
        '
        'DTPtoDate
        '
        Me.DTPtoDate.CustomFormat = "dd/MM/yyyy"
        Me.DTPtoDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.DTPtoDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DTPtoDate.Location = New System.Drawing.Point(326, 20)
        Me.DTPtoDate.Name = "DTPtoDate"
        Me.DTPtoDate.Size = New System.Drawing.Size(105, 22)
        Me.DTPtoDate.TabIndex = 63
        '
        'DTPtotime
        '
        Me.DTPtotime.CustomFormat = "HH:mm:ss"
        Me.DTPtotime.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.DTPtotime.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DTPtotime.Location = New System.Drawing.Point(437, 20)
        Me.DTPtotime.Name = "DTPtotime"
        Me.DTPtotime.ShowUpDown = True
        Me.DTPtotime.Size = New System.Drawing.Size(100, 22)
        Me.DTPtotime.TabIndex = 64
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label51.Location = New System.Drawing.Point(289, 23)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(31, 16)
        Me.Label51.TabIndex = 65
        Me.Label51.Text = "To:"
        '
        'btnRptShow
        '
        Me.btnRptShow.BackColor = System.Drawing.Color.DimGray
        Me.btnRptShow.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRptShow.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnRptShow.ForeColor = System.Drawing.Color.White
        Me.btnRptShow.Location = New System.Drawing.Point(13, 79)
        Me.btnRptShow.Name = "btnRptShow"
        Me.btnRptShow.Size = New System.Drawing.Size(69, 25)
        Me.btnRptShow.TabIndex = 72
        Me.btnRptShow.Text = "SHOW"
        Me.btnRptShow.UseVisualStyleBackColor = False
        '
        'btnRPTExcel
        '
        Me.btnRPTExcel.BackColor = System.Drawing.Color.DimGray
        Me.btnRPTExcel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRPTExcel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnRPTExcel.ForeColor = System.Drawing.Color.White
        Me.btnRPTExcel.Location = New System.Drawing.Point(94, 79)
        Me.btnRPTExcel.Name = "btnRPTExcel"
        Me.btnRPTExcel.Size = New System.Drawing.Size(64, 25)
        Me.btnRPTExcel.TabIndex = 75
        Me.btnRPTExcel.Text = "Excel"
        Me.btnRPTExcel.UseVisualStyleBackColor = False
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label55.Location = New System.Drawing.Point(10, 54)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(43, 16)
        Me.Label55.TabIndex = 73
        Me.Label55.Text = "SEQ:"
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label57.Location = New System.Drawing.Point(275, 54)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(56, 16)
        Me.Label57.TabIndex = 74
        Me.Label57.Text = "Souce:"
        '
        'DGV1
        '
        Me.DGV1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DGV1.Location = New System.Drawing.Point(0, 120)
        Me.DGV1.Name = "DGV1"
        Me.DGV1.Size = New System.Drawing.Size(1081, 880)
        Me.DGV1.TabIndex = 1
        '
        'PGB1
        '
        Me.PGB1.Location = New System.Drawing.Point(479, 355)
        Me.PGB1.Name = "PGB1"
        Me.PGB1.Size = New System.Drawing.Size(231, 37)
        Me.PGB1.TabIndex = 87
        Me.PGB1.Visible = False
        '
        'BWreport
        '
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(595, 53)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(175, 20)
        Me.TextBox1.TabIndex = 88
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.Location = New System.Drawing.Point(518, 54)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(71, 16)
        Me.Label1.TabIndex = 87
        Me.Label1.Text = "Barcode:"
        '
        'ReportUC
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.PGB1)
        Me.Controls.Add(Me.DGV1)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "ReportUC"
        Me.Size = New System.Drawing.Size(1081, 1000)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DGV1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents DGV1 As DataGridView
    Friend WithEvents Label50 As Label
    Friend WithEvents CBrptSource As TextBox
    Friend WithEvents DTPfromDate As DateTimePicker
    Friend WithEvents CBrptJudge As TextBox
    Friend WithEvents DTPFromTime As DateTimePicker
    Friend WithEvents lblrptCount As Label
    Friend WithEvents DTPtoDate As DateTimePicker
    Friend WithEvents DTPtotime As DateTimePicker
    Friend WithEvents Label51 As Label
    Friend WithEvents btnRptShow As Button
    Friend WithEvents btnRPTExcel As Button
    Friend WithEvents Label55 As Label
    Friend WithEvents Label57 As Label
    Friend WithEvents PGB1 As ProgressBar
    Friend WithEvents BWreport As System.ComponentModel.BackgroundWorker
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Label1 As Label
End Class
