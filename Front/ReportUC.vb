﻿Public Class ReportUC
    Class ParamDT
        Public strSQL As String
        '  Public SQL_Conn As SqlConnection
    End Class
    Private Sub BWreport_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BWreport.DoWork
        Try


            Dim worker As System.ComponentModel.BackgroundWorker = CType(sender, System.ComponentModel.BackgroundWorker)
            Dim pDT As ParamDT = e.Argument
            worker.ReportProgress(50)


            e.Result = DAL.getDataTable(pDT.strSQL, connstr)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub BWreport_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles BWreport.ProgressChanged
        lblrptCount.Text = "Loading report"
        PGB1.Value += 1
    End Sub

    Private Sub BWreport_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BWreport.RunWorkerCompleted
        PGB1.Visible = False
        Dim dt As DataTable = DirectCast(e.Result, DataTable)
        If dt IsNot Nothing Then
            DGV1.DataSource = dt
            lblrptCount.Text = dt.Rows.Count & " Record"
        End If


        '  lblState2.Text = ""
    End Sub
    Private Sub btnRptShow_Click(sender As Object, e As EventArgs) Handles btnRptShow.Click
        Try
            Dim strSQL As String

            Dim DT As New DataTable
            Dim StationID, Judgement, Source As String
            Judgement = IIf(CBrptJudge.Text = "All", "%", CBrptJudge.Text)
            Source = IIf(CBrptSource.Text = "All", "%", CBrptSource.Text)
            'StationID = Project.getStationID(cbRptLine.Text, CBrptPitch.Text, CbRptNo.Text)


            strSQL = " SET LANGUAGE british "
            strSQL += " select " &
            " *  from tb_qi" &
           " where " +
            "  [dtRecvDateTime]   BETWEEN '" & DTPfromDate.Value.ToString("dd/MM/yyyy") & " " & DTPFromTime.Value.ToString("HH:mm:ss") & "' AND '" & DTPtoDate.Value.ToString("dd/MM/yyyy") & " " & DTPtotime.Value.ToString("HH:mm:ss") & "' "


            strSQL &= " order by id"


            Dim pDT As New ParamDT
            pDT.strSQL = strSQL
            ' pDT.SQL_Conn = Report_Conn
            BWreport.WorkerReportsProgress = True

            PGB1.Maximum = 3
            PGB1.Value = 0
            PGB1.Visible = True

            '  exceltitle = "NEW INTERLOCK REPORT"
            If BWreport.IsBusy = False Then
                BWreport.RunWorkerAsync(pDT)

            End If
            'DT = DAL.getDataTable(strSQL, SQL_Conn)
            'DGrpt.DataSource = DT
            'lblRptCount.Text = DT.Rows.Count & " Record"
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnRPTExcel_Click(sender As Object, e As EventArgs) Handles btnRPTExcel.Click
        Dim ExToExcel As New ExporttoExcel
        ExToExcel.SetDt = DGV1.DataSource
        ExToExcel.Title = "Tracebility"
        ExToExcel.BeginCloumn = "A"
        ExToExcel.QuickShow2()
    End Sub
End Class
