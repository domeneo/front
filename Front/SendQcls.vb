﻿Public Class SendQcls
    Public server, server2, password, dbname, table As String
    Dim DAL As New DB.SqlConnect
    Sub sendQ(LOT As String, QTY As Integer, source As String)
        Dim connstr_send As String



        connstr_send = "Server=" & server & "; Failover Partner=" & server2 & ";User ID=sa;Password=" & password
        connstr_send += ";Initial Catalog=" & dbname


        Dim whereTime As String
        If Now.ToString("HH:mm") >= "19:00" Then 'กลางคืน
            whereTime = " EventDate between '" & Now.ToString("yyyy-MM-dd ") & "18:30:00' and '" & Now.AddDays(1).ToString("yyyy-MM-dd ") & "06:30:00'"


        ElseIf Now.ToString("HH:mm") <= "06:30" Then 'เช้ามืด

            whereTime = " EventDate between '" & Now.AddDays(-1).ToString("yyyy-MM-dd ") & "18:30:00' and '" & Now.ToString("yyyy-MM-dd ") & "06:30:00'"

        Else 'กลางวัน
            whereTime = " EventDate between '" & Now.ToString("yyyy-MM-dd ") & "06:31:00' and '" & Now.ToString("yyyy-MM-dd ") & "18:29:00'"

        End If


        Dim maxseq, LastKanban As String


        Dim dtmaxSeq As New DataTable
        Dim sqlstr As String
        sqlstr = "select  SEQUENCE,Export_seq from  " & table & " where id = (select max(id) from " & table & "  where"
        '   sqlstr += " [EventDate]>DATEADD(HOUR, -4, GETDATE()) )"
        sqlstr += whereTime & ")"

        dtmaxSeq = DAL.getDataTable(sqlstr, connstr_send)
        If dtmaxSeq.Rows.Count > 0 Then
            maxseq = dtmaxSeq.Rows(0)("SEQUENCE").ToString
            LastKanban = dtmaxSeq.Rows(0)("Export_seq").ToString
            If IsNumeric(maxseq) = False Then maxseq = "0"
            maxseq = CInt(CInt(maxseq) + 1).ToString("000")
            If CInt(maxseq) >= 1000 Then maxseq = "001"

            If IsNumeric(LastKanban) = False Then LastKanban = "0"
            LastKanban = CInt(LastKanban) + 1
        Else
            maxseq = "001"
            LastKanban = "1"
        End If

        Dim BC_DATA As String = Now.ToString("dd/MM/yyyy_") & LastKanban & "M"


        ' maxseq = "001"
        Dim sql As String
        Try


            sql = " If  EXISTS(SELECT 1 FROM TB_LOT_EX WHERE [E_ID] like '" & BC_DATA & "') "

            sql += " UPdate TB_LOT_EX set  E_DATE='" & Now.ToString("dd/MM/yyyy") & "'"
            sql += ", E_SEQ='" & LastKanban & "'"
            sql += ", E_LOT='" & LOT & "'"
            sql += ", E_SPEC='" & LOT & "'"
            sql += ", E_QTY='" & QTY & "'"
            sql += ", E_TIME=getdate()"
            sql += ", E_KANBAN='" & LastKanban & "'"
            sql += ", E_ACTQTY=0"
            sql += " where E_ID like '" & BC_DATA & "'"
            sql += " else "
            sql += " insert into TB_LOT_EX ([E_ID], [E_DATE], [E_SEQ], [E_LOT], E_SPEC, [E_QTY], [E_TIME], [E_KANBAN], [E_ACTQTY])"
            sql += " values('" & BC_DATA & "'"

            sql += ",'" & Now.ToString("dd/MM/yyyy") & "'"
            sql += ",'" & LastKanban & "'"
            sql += ",'" & LOT & "'"
            sql += ",'" & LOT & "'"
            sql += ",'" & QTY & "'"
            sql += ",getdate()"
            sql += ",'" & LastKanban & "'"
            sql += ",0);"

            DAL.GetExecute(sql, connstr_send)
        Catch ex As Exception
            SaveMSG("insert LOT SendQcls:" & ex.Message, "ERR")
        End Try

        For i = 1 To QTY


            sql = "insert into " & table & "(id,EventDate,bcdata,Lotcode,Sequence,[source],Export_seq)"
            sql += " values((select isnull(max(id),-1)+1 from " & table & "),getdate(),'" & BC_DATA & "','" & LOT & "','" & maxseq & "'"
            sql += ",'" & source & "','" & LastKanban & "')"

            DAL.GetExecute(sql, connstr_send)

            maxseq = CInt(CInt(maxseq) + 1).ToString("000")
            If CInt(maxseq) >= 1000 Then maxseq = "001"
        Next
    End Sub
End Class
