﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class getExportUC
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DGVComplete = New System.Windows.Forms.DataGridView()
        Me.EName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Complete_ADDR = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.complete_type = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Complete_Lenght = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Complete_value = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnExportCancel = New System.Windows.Forms.Button()
        Me.btnSendQSave = New System.Windows.Forms.Button()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PNsend = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.lblStatus = New System.Windows.Forms.ToolStripStatusLabel()
        Me.btnServer = New System.Windows.Forms.Button()
        Me.txtLastSend = New System.Windows.Forms.TextBox()
        Me.btnClear = New System.Windows.Forms.Button()
        CType(Me.DGVComplete, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'DGVComplete
        '
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.LavenderBlush
        Me.DGVComplete.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.Violet
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGVComplete.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.DGVComplete.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVComplete.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.EName, Me.Complete_ADDR, Me.complete_type, Me.Complete_Lenght, Me.Complete_value})
        Me.DGVComplete.EnableHeadersVisualStyles = False
        Me.DGVComplete.Location = New System.Drawing.Point(5, 23)
        Me.DGVComplete.Name = "DGVComplete"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGVComplete.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.DGVComplete.RowHeadersWidth = 15
        Me.DGVComplete.Size = New System.Drawing.Size(336, 110)
        Me.DGVComplete.TabIndex = 197
        '
        'EName
        '
        Me.EName.DataPropertyName = "Name"
        Me.EName.HeaderText = "Name"
        Me.EName.Name = "EName"
        Me.EName.Width = 70
        '
        'Complete_ADDR
        '
        Me.Complete_ADDR.DataPropertyName = "Address"
        Me.Complete_ADDR.HeaderText = "Address"
        Me.Complete_ADDR.Name = "Complete_ADDR"
        Me.Complete_ADDR.Width = 75
        '
        'complete_type
        '
        Me.complete_type.DataPropertyName = "type"
        Me.complete_type.HeaderText = "Type"
        Me.complete_type.Name = "complete_type"
        Me.complete_type.Width = 45
        '
        'Complete_Lenght
        '
        Me.Complete_Lenght.DataPropertyName = "lenght"
        Me.Complete_Lenght.HeaderText = "Len"
        Me.Complete_Lenght.Name = "Complete_Lenght"
        Me.Complete_Lenght.Width = 30
        '
        'Complete_value
        '
        Me.Complete_value.DataPropertyName = "Value"
        Me.Complete_value.HeaderText = "Value"
        Me.Complete_value.Name = "Complete_value"
        Me.Complete_value.Width = 70
        '
        'btnExportCancel
        '
        Me.btnExportCancel.BackColor = System.Drawing.Color.DimGray
        Me.btnExportCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExportCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnExportCancel.ForeColor = System.Drawing.Color.White
        Me.btnExportCancel.Location = New System.Drawing.Point(268, 139)
        Me.btnExportCancel.Name = "btnExportCancel"
        Me.btnExportCancel.Size = New System.Drawing.Size(54, 23)
        Me.btnExportCancel.TabIndex = 186
        Me.btnExportCancel.Text = "Cancel"
        Me.btnExportCancel.UseVisualStyleBackColor = False
        '
        'btnSendQSave
        '
        Me.btnSendQSave.BackColor = System.Drawing.Color.DimGray
        Me.btnSendQSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSendQSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnSendQSave.ForeColor = System.Drawing.Color.White
        Me.btnSendQSave.Location = New System.Drawing.Point(208, 139)
        Me.btnSendQSave.Name = "btnSendQSave"
        Me.btnSendQSave.Size = New System.Drawing.Size(54, 23)
        Me.btnSendQSave.TabIndex = 184
        Me.btnSendQSave.Text = "Save"
        Me.btnSendQSave.UseVisualStyleBackColor = False
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 1000
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Violet
        Me.Label1.Location = New System.Drawing.Point(3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 20)
        Me.Label1.TabIndex = 198
        Me.Label1.Text = "Export"
        '
        'PNsend
        '
        Me.PNsend.BackColor = System.Drawing.Color.DimGray
        Me.PNsend.Location = New System.Drawing.Point(68, 4)
        Me.PNsend.Name = "PNsend"
        Me.PNsend.Size = New System.Drawing.Size(16, 16)
        Me.PNsend.TabIndex = 199
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Violet
        Me.Label2.Location = New System.Drawing.Point(90, 5)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(71, 15)
        Me.Label2.TabIndex = 200
        Me.Label2.Text = "Last Send"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblStatus})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 178)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(344, 22)
        Me.StatusStrip1.TabIndex = 201
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'lblStatus
        '
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(0, 17)
        '
        'btnServer
        '
        Me.btnServer.BackColor = System.Drawing.Color.DimGray
        Me.btnServer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnServer.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnServer.ForeColor = System.Drawing.Color.White
        Me.btnServer.Location = New System.Drawing.Point(7, 139)
        Me.btnServer.Name = "btnServer"
        Me.btnServer.Size = New System.Drawing.Size(60, 36)
        Me.btnServer.TabIndex = 202
        Me.btnServer.Text = "Server Setting"
        Me.btnServer.UseVisualStyleBackColor = False
        '
        'txtLastSend
        '
        Me.txtLastSend.Location = New System.Drawing.Point(167, 2)
        Me.txtLastSend.Name = "txtLastSend"
        Me.txtLastSend.Size = New System.Drawing.Size(109, 20)
        Me.txtLastSend.TabIndex = 203
        '
        'btnClear
        '
        Me.btnClear.BackColor = System.Drawing.Color.DimGray
        Me.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClear.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnClear.ForeColor = System.Drawing.Color.White
        Me.btnClear.Location = New System.Drawing.Point(148, 139)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(54, 23)
        Me.btnClear.TabIndex = 204
        Me.btnClear.Text = "Clear"
        Me.btnClear.UseVisualStyleBackColor = False
        '
        'getExportUC
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.txtLastSend)
        Me.Controls.Add(Me.btnServer)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.PNsend)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnExportCancel)
        Me.Controls.Add(Me.DGVComplete)
        Me.Controls.Add(Me.btnSendQSave)
        Me.ForeColor = System.Drawing.Color.Black
        Me.Name = "getExportUC"
        Me.Size = New System.Drawing.Size(344, 200)
        CType(Me.DGVComplete, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnExportCancel As Button
    Friend WithEvents btnSendQSave As Button
    Friend WithEvents DGVComplete As DataGridView
    Friend WithEvents Timer1 As Timer
    Friend WithEvents Label1 As Label
    Friend WithEvents PNsend As Panel
    Friend WithEvents Label2 As Label
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents lblStatus As ToolStripStatusLabel
    Friend WithEvents btnServer As Button
    Friend WithEvents txtLastSend As TextBox
    Friend WithEvents EName As DataGridViewTextBoxColumn
    Friend WithEvents Complete_ADDR As DataGridViewTextBoxColumn
    Friend WithEvents complete_type As DataGridViewTextBoxColumn
    Friend WithEvents Complete_Lenght As DataGridViewTextBoxColumn
    Friend WithEvents Complete_value As DataGridViewTextBoxColumn
    Friend WithEvents btnClear As Button
End Class
