USE [PARTSHOP_INTERLOCK_FR]
GO

/****** Object:  UserDefinedFunction [dbo].[ConvertDate]    Script Date: 30/08/2018 3:16:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- Batch submitted through debugger: SQLQuery2.sql|0|0|C:\Users\Dome\AppData\Local\Temp\~vsECAA.sql
CREATE FUNCTION [dbo].[ConvertDate] (@input VARCHAR(250))
RETURNS Datetime
AS BEGIN

    DECLARE @dt Datetime
	   DECLARE @hr VARCHAR(2)
	     DECLARE @hr1 VARCHAR(2)
	       DECLARE @date VARCHAR(2)
		    DECLARE @txt VARCHAR(30)
	
	set	@hr  = substring(@Input,9,2)
		set @hr1=@hr % 24
		set @hr1=RIGHT('00'+CONVERT(varchar,@hr1),2)
				set @txt=  substring(@Input,1,4) + '-' + substring(@Input,5,2) +'-'  + substring(@Input,7,2) + ' ' + @hr1 + ':' + substring(@Input,11,2)+ ':' + substring(@Input,13,2)
						set @dt=CONVERT(Datetime,@txt, 120)
				if (@hr>=24)
				set @dt=DATEADD(d,1,@dt)


	return @dt
END
GO


